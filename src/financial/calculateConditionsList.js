import calculateProductConditions from './calculateProductConditions';
import {getPeriodRate} from './financial';

export const getMaximumCapitalForPayment = (rate, term, periodicPayment) =>
    (periodicPayment / rate / Math.pow(1 + rate, term)) * (Math.pow(1 + rate, term) - 1);

const calculateIncomeMaximum = ({rate, maximumTerm, income, commitments, incomePaymentRatio}) => {
    const maximumPayment = ((income - commitments) * incomePaymentRatio) / 100;
    return getMaximumCapitalForPayment(getPeriodRate(rate / 100, 12), maximumTerm, maximumPayment) * 0.92;
};

export default props => {
    const {
        variableFees,
        fixedFees,
        assetMaximumAmount,
        maximumTerm,
        requestedAmount,
        productPaymentDate,
        rate,
        productUVAValue,
        income,
        commitments,
        incomePaymentRatio
    } = props;

    const maximumPayment = ((income - commitments) * incomePaymentRatio) / 100;
    const incomeMaximum = calculateIncomeMaximum({
        rate,
        maximumTerm,
        income,
        commitments,
        incomePaymentRatio
    });
    const grossRequestAmount = requestedAmount * (1 + variableFees / 100) + Number(fixedFees);
    const rawPrincipal = Math.min(grossRequestAmount, assetMaximumAmount, incomeMaximum);
    const netAmount = (rawPrincipal - Number(fixedFees)) / (1 + variableFees / 100);
    const principalAmount = netAmount * (1 + variableFees / 100) + Number(fixedFees);

    const terms = [60, 48, 36, 24];
    let availableConditions = [];

    terms.forEach(term => {
        if (term <= maximumTerm) {
            let cond = calculateProductConditions({
                term: term,
                principal: principalAmount,
                net: netAmount,
                productPaymentDate,
                originationFees: principalAmount - netAmount,
                tna: rate,
                uvaValue: productUVAValue
            });

            const secondPayment = cond.productAmortizationSchedule[1];

            if (
                secondPayment.amortization > 0 &&
                (secondPayment.interest + secondPayment.amortization) / maximumPayment < 1
            ) {
                availableConditions.push(cond);
            }
        }
    });

    return {availableConditions};
};
