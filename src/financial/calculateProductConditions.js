import {getPaymentDates} from './amortizationSchedule';
import {calculateIRR} from './irr';
import {getPeriodRate, getPeriodicPayment} from './financial';

const round = number => Math.round(number * 100) / 100;

const tna2tea = tna => (Math.pow(1 + tna / 100 / 12, 12) - 1) * 100;

const format = payments =>
    payments.map(payment => {
        const {n, date} = payment;
        return {
            n,
            date,
            remainingPrincipal: round(payment.remainingPrincipal),
            amortization: round(payment.amortization),
            interest: round(payment.interest),
            iva: round(payment.iva),
            periodPayment: round(payment.periodPayment)
        };
    });

export default ({term, net, principal, productPaymentDate, originationFees, tna, uvaValue}) => {
    const periodRate = getPeriodRate(tna / 100, 12);
    const payments = format(getPaymentDates({principal, rate: periodRate, term, cutDate: productPaymentDate}, true));
    const productTotalFinancialCostWithVAT = round(calculateIRR(principal - originationFees, payments));
    const productPeriodicPaymentPesos = round(getPeriodicPayment(periodRate, term, principal));
    const productPeriodicPaymentUva = round(productPeriodicPaymentPesos / uvaValue);
    const productTotalFinancialCost = round(
        calculateIRR(
            principal - originationFees,
            getPaymentDates({principal: principal, rate: periodRate, term, cutDate: productPaymentDate}, false)
        )
    );
    const productUVAEquivalent = round(principal / uvaValue);

    return {
        productTerm: term,
        productNetAmount: net,
        productPrincipalAmount: principal,
        productPaymentDate,
        productFirstPaymentAmount: Math.round(payments[0].periodPayment * 100) / 100,
        productFirstPaymentDate: payments[0].date,
        productAmortizationSchedule: payments,
        productNominalAnualRate: tna,
        productUVAValue: uvaValue,
        productEffectiveAnualRate: tna2tea(tna),
        productTotalFinancialCostWithVAT,
        productTotalFinancialCost,
        productPeriodicPaymentPesos,
        productPeriodicPaymentUva,
        productUVAEquivalent
    };
};
