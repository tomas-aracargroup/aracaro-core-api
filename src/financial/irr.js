import moment from 'moment';

const NPV = (rate, values) => {
    let xnpv = 0.0;
    for (let key in values) {
        let tmp = values[key];
        let value = tmp.Flow;
        let date = tmp.Date;
        xnpv += value / Math.pow(1 + rate, date / 365);
    }
    return xnpv;
};

const IRR = (values, guess) => {
    let rtb, dx;
    if (!guess) guess = 0.1;

    let x1 = 0.0;
    let x2 = guess;
    let f1 = NPV(x1, values);
    let f2 = NPV(x2, values);

    for (let i = 0; i < 100; i++) {
        if (f1 * f2 < 0.0) break;
        if (Math.abs(f1) < Math.abs(f2)) {
            f1 = NPV((x1 += 1.6 * (x1 - x2)), values);
        } else {
            f2 = NPV((x2 += 1.6 * (x2 - x1)), values);
        }
    }

    if (f1 * f2 > 0.0) return null;

    let f = NPV(x1, values);
    if (f < 0.0) {
        rtb = x1;
        dx = x2 - x1;
    } else {
        rtb = x2;
        dx = x1 - x2;
    }

    for (let z = 0; z < 100; z++) {
        dx *= 0.5;
        let x_mid = rtb + dx;
        let f_mid = NPV(x_mid, values);
        if (f_mid <= 0.0) rtb = x_mid;
        if (Math.abs(f_mid) < 1.0e-6 || Math.abs(dx) < 1.0e-6) return x_mid;
    }

    return null;
};

const generateCashFlow = (principal, payments) => {
    let flow = [];
    flow.push({Date: 0, Flow: -principal});
    payments.forEach(payment => {
        flow.push({Date: moment(payment.date).diff(moment(), 'days') + 1, Flow: payment.periodPayment});
    });
    return flow;
};

export const calculateIRR = (principal, payments) => IRR(generateCashFlow(principal, payments)) * 100;
