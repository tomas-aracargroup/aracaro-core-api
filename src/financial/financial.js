import moment from 'moment';

export const getPeriodicPayment = (rate, term, principal) =>
    (principal * rate * Math.pow(1 + rate, term)) / (Math.pow(1 + rate, term) - 1);
export const getPeriodRate = (tna, periodsInYear) => tna / periodsInYear;

export const getPaymentDayOfMonth = () => {
    let number = moment().date();
    return number > 15 || number <= 5 ? 5 : number <= 10 ? 10 : 15;
};
