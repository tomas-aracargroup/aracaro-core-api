import {getPeriodicPayment} from './financial';
import moment from 'moment';

const getDaysUntilCutDate = date =>
    moment()
        .startOf('day')
        .date(date)
        .add(moment().date() > date ? 2 : 1, 'months')
        .diff(moment().startOf('day'), 'days');

const addVAT = payments =>
    payments.map(x => {
        const {date, remainingPrincipal, amortization, interest, periodPayment} = x;
        return {
            n: x.n,
            date,
            remainingPrincipal,
            amortization,
            interest,
            iva: interest * 0.21,
            periodPayment: periodPayment + interest * 0.21
        };
    });

const getAmortizationSchedule = ({principal, term, rate}) => {
    let payments = [],
        remainingPrincipal = principal;
    let periodAmortization = 0;
    let periodInterest = 0;
    const periodicPayment = getPeriodicPayment(rate, term, principal);

    for (let n = 1; n <= term; n++) {
        let amortization = periodicPayment - remainingPrincipal * rate;
        let interest = periodicPayment - amortization;
        remainingPrincipal = remainingPrincipal - amortization;

        periodAmortization = periodAmortization + amortization;
        periodInterest = periodInterest + interest;
        payments.push({
            n: n,
            remainingPrincipal: remainingPrincipal,
            amortization: periodAmortization,
            interest: periodInterest,
            periodPayment: periodInterest + periodAmortization
        });
        periodAmortization = periodInterest = 0;
    }

    return payments;
};

export const getPaymentDates = ({principal, rate, term, cutDate}, iva = true) => {
    if (!term) {
        return [];
    }
    const amortizationSchedule = getAmortizationSchedule({principal, rate, term});
    const daysUntilCutDate = getDaysUntilCutDate(cutDate);
    const schedule = amortizationSchedule.map(payment => {
        const date = moment()
            .date(cutDate)
            .add(payment.n + (moment().date() > cutDate ? 1 : 0), 'months')
            .format('YYYY-MM-DD');
        return {date, ...payment};
    });

    let interest = (principal * rate * daysUntilCutDate) / 30;

    schedule[0] = {
        ...schedule[0],
        interest,
        periodPayment: Number(interest) + Number(schedule[0].amortization)
    };

    if (iva) {
        return addVAT(schedule);
    }

    return schedule;
};
