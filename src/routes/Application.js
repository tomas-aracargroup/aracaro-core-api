import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as appActions from '../actions/appActions';
import {getByID, put} from '../api/dealers';
import Application from '../views/Application';
import {notification} from 'antd';

class DealersLoanApplicationRoute extends React.Component {
    state = {fetching: true, entity: {}};

    constructor(props) {
        super(props);
        getByID(props.match.params.entityName, props.match.params.id).then(response => {
            this.setState({entity: response});
        });
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    handleChangeStatus = async status => {
        const {entity} = this.state;
        const {app} = this.props;
        this.onChange('status', status);
        const user = app.auth.user.firstname + ' ' + app.auth.user.lastname;

        try {
            notification.success({message: 'Actualizando...', duration: 1.5});
            await put(`application/change-status`, {status, user}, entity._id);
            notification.success({
                message: (
                    <span>
                        Nuevo Status = <b>{status}</b>
                    </span>
                ),
                duration: 2.5
            });
        } catch (err) {
            notification.error({message: 'ERROR AL INTENTAR GUARDAR LOS DATOS', duration: 1.5});
        }
    };

    handleSubmit = async () => {
        const {entity} = this.state;
        const {match} = this.props;
        this.setState({submmited: true});
        let data;
        try {
            data = await put(`byId/${match.params.entityName}`, entity, entity._id);
            notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5});
        } catch (err) {
            notification.error({message: 'ERROR AL INTENTAR GUARDAR LOS DATOS', duration: 1.5});
        } finally {
            this.setState({submmited: false});
        }
        return data;
    };

    addNote = note => {
        const {user} = this.props.app.auth;
        let {entity} = this.state;
        let {actions, match} = this.props;

        entity.notes.push({
            text: note,
            user: user.firstname + ' ' + user.lastname,
            timeStamp: new Date()
        });
        entity.notesLastMessage = note;
        put(`byId/${match.params.entityName}`, entity, entity._id).then(result => {
            actions.updateDocument(
                match.params.entityName === 'entity/dealers-application' ? 'applicationsDealers' : 'onlineApplications',
                result.data
            );
            notification.success({message: 'Nota agregada.', duration: 1.5});
        });
        this.setState({entity});
    };

    render() {
        const {entity, submmited} = this.state;
        const {app, match} = this.props;

        if (!entity) return <p>Loading</p>;

        return (
            <div>
                <Application
                    onChange={this.onChange}
                    handleChangeStatus={this.handleChangeStatus}
                    app={app}
                    entityName={match.params.entityName}
                    entity={entity}
                    handleSubmit={this.handleSubmit}
                    handlePrevious={this.handlePrevious}
                    setSubmmited={this.setSubmmited}
                    submmited={submmited}
                    onAddNote={this.addNote}
                />
            </div>
        );
    }
}

const mapState = state => ({app: state.app});
const mapDispatch = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    mapState,
    mapDispatch
)(DealersLoanApplicationRoute);
