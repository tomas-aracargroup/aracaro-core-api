import React from 'react';
import {Tabs} from 'antd';
import {eAPP_STATUS} from '../constants/enumerators';
import {eAPP_STATUS as RTO_STATUS} from '../constants/rtoEnumerators';
import getStatusCellClass from '../helpers/getStatusCellClass';

const TabPane = Tabs.TabPane;

class Help extends React.Component {
    render() {
        return (
            <div className="help">
                <div className="loan-header">
                    <div className="loan-header__client">
                        <h2 className="loan-header__title">Ayuda / Estados</h2>
                    </div>
                </div>
                <div className="ml-3 pl-3">
                    <Tabs>
                        <TabPane
                            tab={
                                <div>
                                    <b>R</b>ussell<b>C</b>ar{' '}
                                    <i>
                                        <b>R</b>ent <b>T</b>o <b>O</b>wn
                                    </i>
                                    ( {RTO_STATUS.length} )
                                </div>
                            }
                            key="1">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Descripción</th>
                                        <th>Accion Requerida</th>
                                        <th>Responsable</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {RTO_STATUS.map(x => (
                                        <tr key={x.id}>
                                            <td className={getStatusCellClass(x.id)}>{x.id}</td>
                                            <td>{x.description}</td>
                                            <td>{x.requiredAction}</td>
                                            <td>{x.responsible}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </TabPane>
                        <TabPane tab={<div>ARACAR (Prenda Rapida, Dealers, etc) ( {eAPP_STATUS.length} )</div>} key="2">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Estado</th>
                                        <th>Descripción</th>
                                        <th>Accion Requerida</th>
                                        <th>Responsable</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {eAPP_STATUS.map(x => (
                                        <tr key={x.id}>
                                            <td className={getStatusCellClass(x.id)}>{x.id}</td>
                                            <td>{x.description}</td>
                                            <td>{x.requiredAction}</td>
                                            <td>{x.responsible}</td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        );
    }
}

export default Help;
