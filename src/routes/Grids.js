import React from 'react';
import Leads from '../views/leads/LeadsGrid';
import Sessions from '../views/sessions/Sessions';
import Users from '../views/users/UsersGrid';
import ConfigsGrid from '../views/config/ConfigsGrid';
import RTOGrid from '../views/rto/RTOGrid';
import ApplicationsGrid from '../views/application/ApplicationsGrid';
import {Route, Switch} from 'react-router-dom';
import DealersApplicationsGrid from '../views/dealers/applications/DealersApplicationsGrid';
import OnlineApplicationsGrid from '../views/grids/OnlineApplicationsGrid';
import PlanesGrid from '../views/grids/PlanesGrid';
import OnlineCustomersGrid from '../views/grids/OnlineCustomersGrid';
import DealersCustomers from '../views/dealers/customers/DealersCustomersGrid';
import UsersDealers from '../views/dealers/users/Users';
import CompanyGrid from '../views/dealers/companies/CompanyGrid';
import BeSmartGrid from '../views/besmart/BeSmartGrid';

export default () => (
    <Switch>
        <Route exact path="/grids/lead/" component={Leads} />
        <Route exact path="/grids/company" component={CompanyGrid} />
        <Route exact path="/grids/dealers-user" component={UsersDealers} />
        <Route exact path="/grids/dealers-customer" component={DealersCustomers} />
        <Route exact path="/grids/dealers-application" component={DealersApplicationsGrid} />
        <Route exact path="/grids/online-customer" component={OnlineCustomersGrid} />
        <Route exact path="/grids/online-application" component={OnlineApplicationsGrid} />
        <Route exact path="/grids/session/" component={Sessions} />
        <Route exact path="/grids/user" component={Users} />
        <Route exact path="/grids/application/" component={ApplicationsGrid} />
        <Route exact path="/grids/config/" component={ConfigsGrid} />
        <Route exact path="/grids/rto/" component={RTOGrid} />
        <Route exact path="/grids/besmart/" component={BeSmartGrid} />
        <Route exact path="/grids/plan-de-ahorro/" component={PlanesGrid} />
    </Switch>
);
