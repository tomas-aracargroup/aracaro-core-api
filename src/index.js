import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import App from './App';
import './style/style.min.css';
import './style/index.css';
import './style/utilities.css';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store/configureStore';
import getToken from './helpers/getToken';
import {setCurrentUser} from './actions/appActions';
import jwtDecode from 'jwt-decode';
import setAuthorizationToken from './helpers/setAuthorizationToken';

const store = configureStore();

if (getToken()) {
    const token = getToken();
    setAuthorizationToken(token);
    store.dispatch(setCurrentUser(jwtDecode(token)));
}

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
