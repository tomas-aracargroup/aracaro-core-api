/* eslint-disable */
import {sortBy as ord} from 'lodash'

//Default to empty string in order to avoid calling a function from undefined
export const ucwords = (str = '') => str.toLowerCase().replace(/\b[a-z]/g, l => l.toUpperCase());

export const formatNumber = number => number && number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

export const toggleArrayItem = (array, item) => array.includes(item) ? [...array].filter(x => x !== item) : [...array, item];


export const getLabelByKey = (options, value) => {
    let byKey = getByKey(options, value);
    return byKey ? byKey['nombre'] : null
};

export const splitString = (needle = '', haystack = '') => {
    haystack = haystack || ''
    const re = new RegExp(needle, "i" );
    const array = haystack.split(re);
    let match = haystack.match(re);
    return needle === '' || !match ? {0: haystack} : {0: array[0], 1: match[0], 2: array[1]}
};


export const getById = (collection, id) => {
    const item = collection.filter(item => item.id == id);
    if (item) return item[0];
    return null;
};

export const getByKey = (collection, value, key = 'id') => {
    const item = collection.filter(item => item[key] === value);
    if (item) return item[0];
    return null;
};

export const getMes = x => {
    const meses = [
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    ];
    return meses[x - 1];
};

export const sumCollection = col => col.reduce((acc, val) => acc + Number(val.costo), 0);

export const contains = (needle, haystack) => (new RegExp(needle, 'i')).test(haystack);

export const textFilter = (text, collection, cols) =>
    collection.filter(s => {
        for (let i = 0; i < cols.length; i++) {
            if (contains(text, s[cols[i]] || '')) {
                return true;
            }
        }
        return false;
    });

export const selectFormat = data => data.map(item => ({value: item.id, label: item.nombre}));
export const filterCollection = (collection, id, key) => collection.filter(item => item[key].toString() === id.toString());

export const getLastYears = (amount = 30) => {
    let years = [];
    const year = (new Date()).getFullYear();

    for (let x = 0; x <= amount; x++) {
        years.push(year - x) ;
    }
    return years;
};

export const sortBy = (collection, column, asc) => asc ?  ord(collection, column) : ord(collection, column).reverse();