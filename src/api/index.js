import dataServer from '../constants/server';
import axios from 'axios';

export const get = name => axios.get(`${dataServer}/${name}`).then(response => response.data);
export const post = (endPoint, payload) =>
    axios.post(`${dataServer}/${endPoint}`, payload).then(response => response.data);
export const put = (endPoint, payload, id) =>
    axios.put(`${dataServer}/${endPoint}/${id}`, payload).then(response => response.data);

export const getByID = (endPoint, id) =>
    axios.get(`${dataServer}/byId/${endPoint}/${id}`).then(response => response.data);
