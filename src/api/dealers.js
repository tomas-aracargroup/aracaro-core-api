import {dealersServer as dataServer} from '../constants/server';
import axios from 'axios';
import {formatCarBrands} from '../views/dealers/helpers/carBrands';

export const get = name => axios.get(`${dataServer}/${name}`).then(response => response.data);
export const post = (endPoint, payload) =>
    axios.post(`${dataServer}/${endPoint}`, payload).then(response => response.data);
export const put = (endPoint, payload, id) =>
    axios.put(`${dataServer}/${endPoint}/${id}`, payload).then(response => response.data);

export const getByID = (endPoint, id) =>
    axios.get(`${dataServer}/byId/${endPoint}/${id}`).then(response => response.data);

export const deleteByID = (endPoint, id) =>
    axios.delete(`${dataServer}/byId/${endPoint}/${id}`).then(response => response.data);

export const getProvinces = () => {
    return post(`queries/provinces`, {
        limit: 10000,
        sortBy: {name: 1},
        project: {
            _id: 1,
            name: 1
        }
    }).then(response =>
        response.collection.map(x => ({
            id: x.name,
            name: x.name
        }))
    );
};
export const getCities = province => {
    return post(`queries/cities`, {
        limit: 10000,
        sortBy: {name: 1},
        filters: {province: {value: province, type: 'string'}},
        project: {
            _id: 1,
            name: 1,
            zipCode: 1
        }
    }).then(response =>
        response.collection.map(x => ({
            _id: `${x._id}`,
            id: `${x.name}`,
            name: `${x.name}`,
            zipCode: `${x.zipCode}`
        }))
    );
};

export const getCarBrands = post(`queries/car-brands`, {
    filters: {status: {type: 'boolean', value: true}},
    limit: 1000000,
    sortBy: {year: -1},
    project: {
        _id: 0,
        brand: 1,
        codia: 1,
        model: 1,
        year: 1,
        value: 1,
        status: 1
    }
}).then(response => formatCarBrands(response.collection));
