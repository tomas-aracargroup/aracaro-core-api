import {post} from './dealers';
import {formatMarcas} from '../helpers/marcas';

export const getMarcas = post(`queries/car-brands`, {
    filters: {status: {value: true, type: 'boolean'}},
    limit: 1000000,
    sortBy: {year: -1},
    project: {
        _id: 0,
        year: 1,
        model: 1,
        brand: 1,
        value: 1,
        codia: 1,
        status: 1
    }
})
    .then(response => {
        var cars = response.collection.map(z => ({
            anio: z.year,
            nombre: z.model,
            marca: z.brand,
            valor: z.value,
            codia: z.codia
        }));
        return formatMarcas(cars);
    })
    .catch(err => console.log(err));
