import {post} from './index';

export const getLocalidades = province => {
    return post(`queries/localidades`, {
        limit: 10000,
        sortBy: {value: 1},
        filters: {provincia: {value: province, type: 'string'}},
        project: {
            _id: 1,
            code: 1,
            value: 1
        }
    }).then(response => response.collection.map(x => ({id: `${x.value}`, name: x.value, zipCode: x.code})));
};
