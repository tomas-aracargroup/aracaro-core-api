import * as types from './actionTypes';
import {deleteByID} from '../api/dealers';

export const updateVisibleColumn = (collectionName, column) => ({
    type: types.GRID_TOGGLE_VISIBLE_COLUMN,
    payload: {collectionName, column}
});
export const updateFilter = (filterName, column, value) => ({
    type: types.UPDATE_FILTER,
    payload: {filterName, column, value}
});
export const deleteFilter = (filterName, column) => ({
    type: types.DELETE_FILTER,
    payload: {filterName, column}
});

export const setActiveModal = payload => ({type: types.SET_ACTIVE_MODAL, payload});
export const setCurrentUser = user => ({type: types.SET_CURRENT_USER, user});

export const fetchSuccess = (collectionName, collection, count) => ({
    type: types.COLLECTION_FETCH_SUCCESS,
    payload: {collectionName, collection, count}
});

export const deleteDocument = (collectionName, documentId) => {
    deleteByID(collectionName, documentId).then(console.log('Deleted'));

    return {
        type: types.COLLECTION_INIT_DELETE,
        payload: {collectionName, documentId}
    };
};

export const updateDocument = (collectionName, document) => {
    return {
        type: types.COLLECTION_UPDATE_SUCCESS,
        payload: {collectionName, document}
    };
};
