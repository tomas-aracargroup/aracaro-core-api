import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Layout} from 'antd';
import Header from './components/Header';
import Sidebar from './views/Sidebar';
import Lead from './views/leads/LeadRoute';
import Modal from './components/Modal';
import Calculadora from './views/calculator/CalculadoraComercial';
import Login from './components/Login';
import LeadForm from './views/leads/LeadNew';
import UserForm from './views/users/UserForm';
import LoanApplication from './views/application/LoanApplicationRoute';
import {BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';
import Application from './routes/Application';
import DealersCustomer from './views/dealers/customers/Customer';
import OnlineCustomer from './views/dealers/customers/OnlineCustomer';
import MailSubscription from './views/config/MailSubscription';
import UploadCars from './views/config/UploadCars';
import User from './views/dealers/users/User';
import ManualApplication from './views/dealers/applications/ManualApplication';
import Company from './views/dealers/companies/Company';
import Rto from './views/rto/Rto';
import Summary from './components/Summary';
import NewCompany from './views/dealers/companies/NewCompany';
import Grids from './routes/Grids';
import Help from './routes/Help';
import Bulk from './views/bulk/index';

const {Content} = Layout;

class App extends Component {
    render() {
        const {app} = this.props;
        let home = '/grids/online-application';

        if (!app.auth.user) {
            return <Login onHandleLogin={this.onHandleLogin} />;
        } else {
            home = app.auth.user.home || home;
        }

        return (
            <Router>
                <Layout>
                    <Header user={app.auth.user} />
                    <Layout>
                        <Sidebar app={app} />
                        <Content className="panel-content">
                            <Switch>
                                <Redirect from="/" exact to={home} />
                                <Route path="/grids" component={Grids} />
                                <Route path="/carga-masiva" component={Bulk} />
                                <Route exact path="/create/company" component={NewCompany} />
                                <Route exact path="/leads/builder/" component={LeadForm} />
                                <Route exact path="/users/create" component={UserForm} />
                                <Route path="/leads/:id" component={Lead} />
                                <Route exact path="/users/:id" component={UserForm} />
                                <Route exact path="/company/:id" component={Company} />
                                <Route exact path="/entity/rto/:id" component={Rto} />
                                <Route path="/summaries/:model" component={Summary} />
                                <Route exact path="/entity/online-customer/:id" component={OnlineCustomer} />
                                <Route exact path="/entity/:entityName/:id" component={Application} />
                                <Route path="/applications/:id" component={LoanApplication} />
                                <Route exact path="/dealers-users/:id" component={User} />
                                <Route exact path="/manual-application/:id" component={ManualApplication} />
                                <Route exact path="/dealers-customers/:id" component={DealersCustomer} />
                                <Route path="/calc/" component={Calculadora} />
                                <Route path="/mails/subscriptions" component={MailSubscription} />
                                <Route exact path="/upload/cars" component={UploadCars} />
                                <Route exact path="/ayuda" component={Help} />
                                <Redirect to="/grids/online-application" />
                            </Switch>
                        </Content>
                        <Modal app={app} />
                    </Layout>
                </Layout>
            </Router>
        );
    }
}

export default connect(x => ({app: x.app}))(App);
