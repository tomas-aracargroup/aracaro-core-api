import React, {Component} from 'react';
import {Layout, Menu, Icon} from 'antd';
import {withRouter} from 'react-router';

const SubMenu = Menu.SubMenu;

const {Sider} = Layout;
const navigation = {
    leads: 'Leads',
    applications: 'Aplicaciones',
    calc: 'Calculadora',
    channel: 'Canales',
    config: 'Configuración',
    sessions: 'Sesiones',
    users: 'Usuarios'
};

const {leads, calc, channel, config, sessions, users, applications} = navigation;

const includes = (haystack, needle) => haystack.length > 0 && haystack.includes(needle);

class Sidebar extends Component {
    state = {collapsed: false};

    onCollapse = collapsed => this.setState({collapsed});

    onClick = key => this.props.history.push(`${key.key}`);

    render() {
        const {app} = this.props;
        const tasks = app.auth.user.tasks;
        const prendaRapidaItems = [
            {key: 'applications', icon: 'exception', label: applications},
            {key: 'leads', icon: 'folder-open', label: leads},
            {key: 'sessions', icon: 'fork', label: sessions}
        ].map(
            item =>
                includes(tasks, item.key) ? (
                    <Menu.Item key={`/${item.key}`}>
                        <Icon type={item.icon} />
                        <span>{item.label}</span>
                    </Menu.Item>
                ) : null
        );

        const menuItemsConfig = [
            {key: 'channel', icon: 'shop', label: channel},
            {key: 'users', icon: 'user-add', label: users},
            {key: 'config', icon: 'setting', label: config}
        ].map(
            item =>
                includes(tasks, item.key) ? (
                    <Menu.Item key={`/${item.key}`}>
                        <Icon type={item.icon} />
                        <span>{item.label}</span>
                    </Menu.Item>
                ) : null
        );

        const menuItems = [{key: 'calc', icon: 'calculator', label: calc}].map(
            item =>
                includes(tasks, item.key) ? (
                    <Menu.Item key={`/${item.key}`}>
                        <Icon type={item.icon} />
                        <span>{item.label}</span>
                    </Menu.Item>
                ) : null
        );

        const menuItemsDealers = [
            {key: 'dealers-applications', icon: 'exception', label: applications},
            {key: 'dealers-users', icon: 'user-add', label: 'Usuarios'},
            {key: 'dealers-customers', icon: 'shop', label: 'Clientes'}
        ].map(
            item =>
                includes(tasks, item.key) ? (
                    <Menu.Item key={`/${item.key}`}>
                        <Icon type={item.icon} />
                        <span>{item.label}</span>
                    </Menu.Item>
                ) : null
        );

        return (
            <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse} className="panel-sidebar">
                <Menu
                    theme="dark"
                    onClick={this.onClick}
                    defaultSelectedKeys={[this.props.location.pathname]}
                    mode="inline">
                    <SubMenu
                        key="prenda-rapida"
                        title={
                            <span>
                                <Icon type="user" />
                                <span>Prenda Rapida</span>
                            </span>
                        }>
                        {prendaRapidaItems}
                    </SubMenu>
                    <SubMenu
                        key="dealers"
                        title={
                            <span>
                                <Icon type="car" />
                                <span>Dealers</span>
                            </span>
                        }>
                        {menuItemsDealers}
                    </SubMenu>
                    <SubMenu
                        key="config"
                        title={
                            <span>
                                <Icon type="setting" />
                                <span>Config</span>
                            </span>
                        }>
                        {menuItemsConfig}
                    </SubMenu>
                    {menuItems}
                </Menu>
            </Sider>
        );
    }
}

export default withRouter(Sidebar);
