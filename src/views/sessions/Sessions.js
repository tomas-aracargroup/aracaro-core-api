import React from 'react';
import Table from '../../components/Grid';

const columns = [
    {name: 'leadId', label: 'Lead ID'},
    {name: 'result', label: 'Result', cell: 'link', searchable: true, collectionName: 'leads', keyName: 'leadId'},
    {name: 'actions', label: 'Detalle Acciones', cell: 'text', hideInColumnPicker: true},
    {name: 'actionsCount', label: '# Acciones', cell: 'action', modalName: 'session', searchable: true},
    {name: 'startTime', label: 'Start Time', searchable: true},
    {name: 'referrer', label: 'Referido', searchable: true},
    {name: 'product', label: 'Producto', searchable: true},
    {name: 'userAgent', label: 'Tipo disp.', cell: 'device', searchable: true}
];

export default () => (
    <Table
        className="sessions"
        columns={columns}
        title={'Prenda Rapida / Sesiones iniciadas '}
        collectionName="sessions"
        columnPicker
        endPoint={'sessions'}
        sortBy={{startTime: -1}}
    />
);
