import React from 'react';
import {Col, Card, Modal} from 'antd';
import moment from 'moment';

const callbackfn = x => (
    <tr>
        <td>{x.type}</td>
        <td>{JSON.stringify(x.payload)}</td>
        <td>{moment(x.timeStamp).format('LTS')}</td>
    </tr>
);

class Session extends React.Component {
    state = {};

    render() {
        const {entity, handleOk} = this.props;

        const title = (
            <div className="name">
                <label>{entity.userAgent}</label>
                <div>{entity['canal']}</div>
            </div>
        );

        return (
            <Modal iconType={'cross-circle'} visible onOk={handleOk} onCancel={handleOk} width={'960px'} closable>
                <Col>
                    <Card title={title}>
                        <div className="table-responsive">
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Payload</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>{entity.actions.map(callbackfn)}</tbody>
                            </table>
                        </div>
                    </Card>
                </Col>
            </Modal>
        );
    }
}

export default Session;
