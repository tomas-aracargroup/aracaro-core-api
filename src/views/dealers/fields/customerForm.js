class Fields {
    fields = [
        {
            name: 'firstName',
            label: 'Nombre',
            order: 1,
            required: true,
            letterOnly: true,
            min: 2,
            max: 50
        },
        {
            name: 'lastName',
            label: 'Apellido',
            order: 2,
            required: true,
            letterOnly: true,
            min: 2,
            max: 50
        },
        {name: 'documentNumber', label: 'DNI', order: 3, required: true},
        {name: 'dob', label: 'Fecha de Nacimiento', type: 'date', order: 4, required: true},
        {name: 'email', order: 5, type: 'email', required: true},
        {name: 'cellPhone', label: 'celular', order: 6, required: true},
        {name: 'beSmartResult', label: 'Resultado BeSmart', order: 7, required: true},
        {name: 'estimatedIncome', label: 'Ingreso Estimado', order: 8, required: true},
        {name: 'maximumGrossLoanAmount', label: 'Máximo a Prestar', order: 9, required: true},
        {name: 'maximumLoanToValue', label: 'Maximum Loan To Value', order: 10, required: true},
        {name: 'maximumMonthlyPayment', label: 'MaximumMonthlyPayment', order: 11, required: true},
        {name: 'maximumTerm', label: 'MaximumTerm', order: 12, required: true},
        {name: 'nosisScore', label: 'Score Nosis', order: 13, required: true},
        {name: 'siisaScore', label: 'Score Siisa', order: 14, required: true}
    ];

    fieldsAsync = () => {
        return new Promise(async resolve => {
            resolve(this.fields);
        });
    };
}
export default new Fields();
