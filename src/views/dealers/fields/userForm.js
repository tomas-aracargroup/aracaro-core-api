import {post} from '../../../api/dealers';

let enumLoading = [{id: 'loading', name: 'loading...'}];

class Fields {
    fields = [
        {
            name: 'firstName',
            label: 'Nombre',
            order: 1,
            required: true,
            letterOnly: true,
            min: 2,
            max: 50
        },
        {
            name: 'lastName',
            label: 'Apellido',
            order: 2,
            required: true,
            letterOnly: true,
            min: 2,
            max: 50
        },
        {name: 'email', order: 3, type: 'email', required: true},
        {
            name: 'companyId',
            order: 4,
            type: 'select',
            required: true,
            label: 'Canal',
            enum: enumLoading
        },
        {name: 'verified', label: 'Verificado', type: 'checkbox', order: 5},
        {name: 'active', label: 'Habilitado', type: 'checkbox', order: 6}
    ];

    fieldsAsync = () => {
        return new Promise(async (resolve, reject) => {
            let channels = await post('queries/companies');
            channels = channels.collection.map(x => ({id: x._id, name: x.name}));
            this.fields.filter(x => x.name === 'companyId')[0].enum = channels;

            resolve(this.fields);
        });
    };
}
export default new Fields();
