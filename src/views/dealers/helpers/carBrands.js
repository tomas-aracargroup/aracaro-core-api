import Enumerable from 'linq/linq';

export const formatCarBrands = marcas => {
    return Enumerable.from(marcas)
        .orderByDescending(x => x.anio)
        .groupBy('$.year', null, (key, group) => ({
            value: key.toString(),
            label: key.toString(),
            children: Enumerable.from(group.getSource())
                .orderBy(x => x.brand)
                .groupBy('$.brand', null, (key, g) => ({
                    value: key,
                    label: key,
                    children: Enumerable.from(g.getSource())
                        .orderBy(x => x.model)
                        .select(item => ({
                            value: item.model,
                            label: item.model,
                            valor: item.value,
                            codia: item.codia
                        }))
                        .toArray()
                }))
                .toArray()
        }))
        .toArray();
};
