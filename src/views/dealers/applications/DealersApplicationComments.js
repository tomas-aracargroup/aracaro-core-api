import React from 'react';
import CommentsModal from '../../../components/CommentsModal';
import {put} from '../../../api/dealers';

export default props => (
    <CommentsModal {...props} endPoint="application/addNote" collectionName="applicationsDealers" putFunction={put} />
);
