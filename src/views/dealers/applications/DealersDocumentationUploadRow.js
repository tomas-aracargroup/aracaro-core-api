import React from 'react';
import FilestackUploadButton from '../../../components/FilestackUploadButton';
import imgPDF from '../../../assets/img/pdf.jpg';

class DealersDocumentationUploadRow extends React.Component {
    render() {
        const {onSuccess, onError, src, alt} = this.props;
        let imageSource;
        if (src) {
            imageSource = src.mimetype !== 'application/pdf' ? src.url : imgPDF;
        }

        return (
            <div>
                <FilestackUploadButton onSuccess={onSuccess} onError={onError} text={'Subir'} link={true} />
                {imageSource && (
                    <div className="documentation-upload__image">
                        <a href={src.url} target="_blank">
                            {<img src={imageSource} style={{maxHeight: '64px'}} alt={alt} />}
                        </a>
                    </div>
                )}
            </div>
        );
    }
}

export default DealersDocumentationUploadRow;
