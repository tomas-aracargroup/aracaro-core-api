import {
    eID_TAXES_GANANCIAS,
    eID_TAXES_INGRESOSBRUTOS,
    eID_TAXES_IVA,
    eCUIL_TYPE,
    eVEHICULO_TIPO,
    eVEHICULO_USO_DECLARADO_APPLICATION,
    eYES_NO,
    eGENERO,
    eMARITAL_STATUS,
    eID_TYPE,
    eCOUNTRIES,
    eMOTIVO_PRESTAMO,
    eCONTRACT_CONCEPT,
    eACTIVITY
} from '../../../constants/enumerators';

const nameToValue = enumObject => enumObject.map(x => ({id: x.name, name: x.name}));

export default [
    {
        title: 'Cliente',
        fields: [
            {name: 'firstName', label: 'Nombre', required: true},
            {name: 'lastName', label: 'Apellido', required: true},
            {name: 'gender', label: 'Género', type: 'select', enum: eGENERO, required: true},
            {name: 'dob', label: 'Fecha Nacimiento', type: 'date'},
            {name: 'documentNumber', label: 'Documento', readOnly: true},
            {name: 'cuit', label: 'CUIT/CUIL'},
            {name: 'cuitType', label: 'CUIT/CUIL Tipo', type: 'select', enum: eCUIL_TYPE},
            {name: 'countryOfBirth', label: 'País Nacimiento', type: 'select', enum: nameToValue(eCOUNTRIES)},
            {name: 'nationality', label: 'Nacionalidad', type: 'select', enum: nameToValue(eCOUNTRIES)},
            {name: 'maritalStatus', label: 'Estado Civil', type: 'select', enum: eMARITAL_STATUS},
            {name: 'email', label: 'Mail'},
            {name: 'cellPhone', label: 'Teléfono'},
            {name: 'addressStreet', label: 'Calle'},
            {name: 'addressNumber', label: 'Número'},
            {name: 'addressFloor', label: 'Piso'},
            {name: 'addressApartment', label: 'Departamento'},
            {name: 'addressProvince', label: 'Provincia', type: 'selectdb', enum: []},
            {name: 'addressCity', label: 'Ciudad', type: 'selectdb', enum: []},
            {name: 'addressZipCode', label: 'Código Postal'},
            {name: 'addressBetweenStreets', label: 'Entre Calles'}
        ]
    },
    {
        title: 'Préstamo',
        fields: [
            {name: 'productTerm', label: 'Plazo', type: 'number', readOnly: true},
            {name: 'productNetAmount', label: 'Neto a liquidar', type: 'money', readOnly: true},
            {name: 'productPrincipalAmount', label: 'Monto', type: 'money', readOnly: true},
            {name: 'productOriginationFees', label: 'Gastos Otorgamiento', type: 'money', readOnly: true},
            {name: 'productUVAEquivalent', label: 'Capital en UVA', type: 'decimal', readOnly: true},
            {name: 'productPeriodicPaymentUva', label: 'Cuotas pura en UVA', type: 'decimal', readOnly: true},
            {name: 'productPeriodicPaymentPesos', label: 'Cuotas pura en Pesos', type: 'decimal', readOnly: true},
            {name: 'productFirstPaymentAmount', label: 'Monto primer pago', type: 'money', readOnly: true},
            {name: 'productFirstPaymentDate', label: 'Fecha primer pago', type: 'date', readOnly: true},

            {name: 'productUVAValue', label: 'Cotizacion UVA', type: 'decimal', readOnly: true},
            {name: 'createdAt', label: 'Fecha cotizacion', type: 'date', readOnly: true},
            {name: 'productNominalAnualRate', label: 'TNA', type: 'percentage', readOnly: true},
            {name: 'productTotalFinancialCostWithVAT', label: 'CFTNA', type: 'percentage', readOnly: true},
            {name: 'salesPerson', label: 'Vendedor'},
            {name: 'companyCode', label: 'Codigo Empresa'},
            {name: 'companyName', label: 'Nombre Empresa'},
            {name: 'productDestination', label: 'Destino Préstamo', type: 'select', enum: eMOTIVO_PRESTAMO},
            {name: 'productName', label: 'Destino Préstamo'}
        ]
    },
    {
        title: 'Besmart',
        fields: [
            {label: 'Fecha', name: 'bs_solicitudFecha', type: 'date'},
            {label: 'Producto', name: 'bs_producto'},
            {label: 'Vehiculo Antigüedad ', name: 'bs_vehiculoAntiguedad'},
            {label: 'Vehiculo Modelo', name: 'bs_vehiculoMarcaModeloVersion'},
            {label: 'Vehiculo Valor', name: 'bs_vehiculoValor', type: 'money'},
            {label: 'Dictamen', name: 'bs_dictamen'},
            {label: 'Motivo', name: 'bs_motivo'},
            {label: 'Explicación', name: 'bs_explicacion'},
            {label: 'Sobre Tasa', name: 'bs_coefTasa', type: 'percentage'},
            {label: 'Cuota Máxima', name: 'bs_cuotaMax', type: 'money'},
            {label: 'Monto Máximo', name: 'bs_capitalMaximo', type: 'money'},
            {label: 'Plazo Maximo', name: 'bs_plazoMaxAntig'},
            {label: 'RCI', name: 'bs_rci', type: 'percentage'},
            {label: 'LTV', name: 'bs_ltvFinal', type: 'percentage'},
            {label: 'Ingreso Computado', name: 'bs_ingresoComputado', type: 'money'},
            {label: 'Compromisos Mensuales', name: 'bs_ciVigCompMensual', type: 'money'},
            {label: 'Nivel de Riesgo', name: 'bs_riesgoNivel'},
            {label: 'Bancarizado', name: 'bs_ciBancarizado'},
            {name: 'bscdPeorSituUlt', label: 'BCRA'},
            {name: 'bscdPeorSituUlt2_3', label: 'BCRA 2_3'},
            {name: 'bscdPeorSituUlt4_6', label: 'BCRA 4_6'},
            {name: 'bscdPeorSituUlt7_12', label: 'BCRA 7_12'},
            {label: 'Nosis', name: 'bs_scoVig'},
            {label: 'Siisa', name: 'bs_siScore'},
            {label: 'Situación Laboral', name: 'bs_situacionLaboralFinal'},
            {label: 'Subproducto', name: 'bs_subproducto'},
            {label: 'Ingreso Estimado', name: 'bs_ingresoEstimado', type: 'money'},
            {label: 'Ingreso Verificado', name: 'bs_ingresoVerificado', type: 'money'}
        ]
    },
    {
        title: 'Cliente - Dirección Legal',
        fields: [
            {name: 'legalAddressStreet', label: 'Calle'},
            {name: 'legalAddressNumber', label: 'Número'},
            {name: 'legalAddressFloor', label: 'Piso'},
            {name: 'legalAddressApartment', label: 'Departamento'},
            {name: 'legalAddressProvince', label: 'Provincia', type: 'string', enum: []},
            {name: 'legalAddressCity', label: 'Ciudad', type: 'string', enum: []},
            {name: 'legalAddressZipCode', label: 'Código Postal'},
            {name: 'legalAddressBetweenStreets', label: 'Entre Calles'}
        ]
    },
    {
        title: 'Cliente - Datos Laborales',
        fields: [
            {name: 'mainProfessionalActivityType', label: 'Actividad Principal', type: 'select', enum: eACTIVITY},
            {name: 'mainProfessionalActivity', label: 'Profesión', type: 'string'},
            {name: 'workingCompany', label: 'Empresa'},
            {name: 'workingCUIT', label: 'CUIT'},
            {name: 'workingBranch', label: 'Ramo'},
            {name: 'workingJob', label: 'Cargo'},
            {name: 'declaredIncome', label: 'Ingreso Declarado', type: 'currency'},
            {name: 'workingAddressStreet', label: 'Calle'},
            {name: 'workingAddressNumber', label: 'Número'},
            {name: 'workingAddressFloor', label: 'Piso'},
            {name: 'workingAddressApartment', label: 'Depto.'},
            {name: 'workingAddressBetweenStreets', label: 'Entre Calles'},
            {name: 'workingAddressProvince', label: 'Provincia'},
            {name: 'workingAddressCity', label: 'Ciudad'},
            {name: 'workingAddressZipCode', label: 'Código Postal'}
        ]
    },
    {
        title: 'Cónjuge - Conviviente',
        fields: [
            {name: 'spouseFirstName', label: 'Nombre'},
            {name: 'spouseLastName', label: 'Apellido'},
            {name: 'spouseDocumentType', label: 'Documento Tipo', type: 'select', enum: eID_TYPE},
            {name: 'spouseDocumentNumber', label: 'Documento'},
            {name: 'spouseCuitType', label: 'Cuit Tipo', type: 'select', enum: eCUIL_TYPE},
            {name: 'spouseCuit', label: 'CUIT/CUIL', type: 'string'},
            {name: 'spouseDob', label: 'Fecha Nacimiento', type: 'date'},
            {name: 'spouseNationality', label: 'Nacionalidad', type: 'select', enum: nameToValue(eCOUNTRIES)}
        ]
    },
    {
        title: 'Cónyuge - Conviviente - Datos Laborales',
        fields: [
            {name: 'spouseMainProfessionalActivityType', label: 'Actividad Principal', type: 'select', enum: eACTIVITY},
            {name: 'spouseMainProfessionalActivity', label: 'Profesión', type: 'string'},
            {name: 'spouseWorkingCompany', label: 'Empresa'},
            {name: 'spouseWorkingCUIT', label: 'CUIT Empresa'},
            {name: 'spouseWorkingBranch', label: 'Ramo'},
            {name: 'spouseWorkingJob', label: 'Cargo'},
            {name: 'spouseNetMonthlyIncome', label: 'Ingreso Declarado', type: 'currency'},
            {name: 'spouseWorkingDateFrom', label: 'Fecha Inicio', type: 'date'},
            {name: 'spouseWorkingAddressStreet', label: 'Calle'},
            {name: 'spouseWorkingAddressNumber', label: 'Número'},
            {name: 'spouseWorkingAddressFloor', label: 'Piso'},
            {name: 'spouseWorkingAddressApartment', label: 'Depto.'},
            {name: 'spouseWorkingAddressBetweenStreets', label: 'Entre Calles'},
            {name: 'spouseWorkingAddressProvince', label: 'Provincia'},
            {name: 'spouseWorkingAddressCity', label: 'Ciudad'},
            {name: 'spouseWorkingAddressZipCode', label: 'Código Postal'},
            {name: 'spouseWorkingCompanyPhone', label: 'Teléfono'}
        ]
    },
    {
        title: 'Vehículo',
        fields: [
            {name: 'vehicleBrand', label: 'Marca', type: 'string', readOnly: true},
            {name: 'vehicleModel', label: 'Modelo', type: 'string', readOnly: true},
            {name: 'vehicleModelForm03', label: 'Modelo (impresiones)', type: 'string'},
            {name: 'vehicleYear', label: 'Año', type: 'string', readOnly: true},
            {name: 'vehicleValue', label: 'Precio Info Auto', type: 'currency'},
            {name: 'vehicleType', label: 'Tipo', type: 'string'},
            {name: 'vehicleDomain', label: 'Dominio', type: 'string'},
            {name: 'vehicleChassisMake', label: 'Marca Chasis', type: 'string'},
            {name: 'vehicleChassisNumber', label: 'Número de Chasis', type: 'string'},
            {name: 'vehicleEngineMake', label: 'Marca Motor', type: 'string'},
            {name: 'vehicleEngineNumber', label: 'Número de Motor', type: 'string'},
            {name: 'vehicleCategory', label: 'Categoría', type: 'select', enum: eVEHICULO_TIPO},
            {name: 'vehicleUsageType', label: 'Uso', type: 'select', enum: eVEHICULO_USO_DECLARADO_APPLICATION}
        ]
    },
    {
        title: 'Seguro Vehículo',
        fields: [
            {name: 'vehicleInsuranceCompany', label: 'Compañía', type: 'string'},
            {name: 'vehicleInsuranceCoverageType', label: 'Cobertura', type: 'string'},
            {name: 'vehicleInsuranceCost', label: 'Cuota', type: 'currency'},
            {name: 'vehicleInsuranceInstallments', label: 'Cantidad de cuotas'},
            {name: 'vehicleInsuranceTerm', label: 'Duracion'},
            {name: 'vehicleInsuranceBudgetId', label: 'BudgetId', type: 'string'},
            {name: 'vehicleInsuranceId', label: 'Id', type: 'string'}
        ]
    },
    {
        title: 'Datos Impositivos',
        fields: [
            {
                name: 'conditionIncomeTax',
                label: 'Condición Ganancias',
                type: 'select',
                enum: eID_TAXES_GANANCIAS
            },
            {
                name: 'conditionVAT',
                label: 'Condición IVA',
                type: 'select',
                enum: eID_TAXES_IVA
            },
            {
                name: 'conditionIibb',
                label: 'Condición IIBB',
                type: 'select',
                enum: eID_TAXES_INGRESOSBRUTOS
            },
            {name: 'conditionIibbNumber', label: 'Nro. de Insc. IIBB', type: 'string'},
            {
                name: 'conditionInflationAdjustment',
                label: 'Ajuste por inflación',
                type: 'select',
                enum: eYES_NO
            }
        ]
    },
    {
        title: 'Formulario Prenda',
        fields: [
            {
                name: 'documentBy',
                label: 'Documento Otorgado por',
                type: 'string'
            },
            {
                name: 'contractPledgeDegree',
                label: 'Grado de Prenda',
                type: 'string'
            },
            {
                name: 'contractConcept',
                label: 'Concepto Contrato',
                type: 'select',
                enum: eCONTRACT_CONCEPT
            },
            {
                name: 'contractClause',
                label: 'Cláusula Contrato',
                type: 'select',
                enum: eYES_NO
            }
        ]
    },
    {
        title: 'Referencias',
        fields: [
            {name: 'referenceLastName1', label: 'Apellido'},
            {name: 'referenceName1', label: 'Nombre'},
            {name: 'referenceTelephone1', label: 'Teléfono'},
            {name: 'referenceLastName2', label: 'Apellido'},
            {name: 'referenceName2', label: 'Nombre'},
            {name: 'referenceTelephone2', label: 'Teléfono'},
            {name: 'referenceLastName3', label: 'Apellido'},
            {name: 'referenceName3', label: 'Nombre'},
            {name: 'referenceTelephone3', label: 'Teléfono'}
        ]
    },
    {
        title: 'Riesgo',
        fields: [{name: 'riskObservations', label: 'Observaciones', type: 'textArea'}]
    }
];
