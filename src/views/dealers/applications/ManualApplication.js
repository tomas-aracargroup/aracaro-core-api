import React from 'react';
import calculateConditionsList from '../../../financial/calculateConditionsList';
import {Col, Row, Card, Button} from 'antd';
import {get, getByID, getCarBrands, post} from '../../../api/dealers';
import {getByKey} from '../../../helpers';
import {getPaymentDayOfMonth} from '../../../financial/financial';
import EditableCard from '../../../components/EditableCard';
import format from '../../../helpers/format';
import {startCase} from 'lodash';

const vehiclefields = [
    {name: 'vehicleBrand', label: 'Vehículo', type: 'cascader'},
    {name: 'vehicleValue', label: 'Valor vehículo', type: 'currency'},
    {name: 'ltv', label: 'Loan To Value', type: 'percentage'}
];

const incomeFields = [
    {name: 'income', label: 'Ingresos Netos Mensuales', type: 'currency'},
    {name: 'monthlyCompromises', label: 'Compromisos Mensuales', type: 'currency'},
    {name: 'incomePaymentRatio', label: 'Relación Cuota/Ingreso', type: 'percentage', min: 1, max: 35}
];

const fields = [
    {name: 'maximumTerm', label: 'Plazo máximo', type: 'number'},
    {name: 'rate', label: 'Tasa', type: 'percentage'},
    {name: 'fixedFees', label: 'Gastos Fijos de otorgamiento', type: 'currency'},
    {name: 'variableFees', label: 'Gastos Variables de otorgamiento', type: 'percentage'}
];

const offeredLoanFields = [
    {name: 'productUVAValue', label: 'Valor UVA'},
    {name: 'productFirstPaymentAmount', label: 'Monto primer pago', type: 'currency'},
    {name: 'productFirstPaymentDate', label: 'Fecha primer pago'},
    {name: 'productNominalAnualRate', label: 'TNA', type: 'percentage'},
    {name: 'productTotalFinancialCost', label: 'CFT', type: 'percentage'},
    {name: 'productTotalFinancialCostWithVAT', label: 'CFT (Con IVA):', type: 'percentage'}
];

const getSelectedProduct = entity =>
    getByKey(calculateConditionsList(getConditions(entity)).availableConditions, Number(entity.term), 'productTerm');

const getConditions = entity => {
    const {
        monthlyCompromises,
        income,
        variableFees,
        fixedFees,
        rate,
        maximumTerm,
        productPaymentDate,
        incomePaymentRatio,
        vehicleValue,
        ltv,
        requestedAmount,
        uvaValue
    } = entity;

    return {
        variableFees,
        fixedFees,
        maximumTerm,
        rate,
        productPaymentDate,
        assetMaximumAmount: (vehicleValue || 0) * (ltv || 0) * 0.01,
        incomePaymentRatio,
        requestedAmount: requestedAmount,
        productUVAValue: uvaValue,
        income,
        commitments: monthlyCompromises
    };
};

class ManualApplication extends React.Component {
    state = {fetching: true, entity: {}, uvaValue: 1};

    createApplication = () => {
        const {entity} = this.state;

        post(`customer/create-application/${this.state.customer._id}`, {
            fixedFees: entity.fixedFees,
            variableFees: entity.variableFees,
            term: entity.term,
            productPaymentDate: entity.productPaymentDate,
            rate: entity.rate,
            netAmount: getSelectedProduct(entity).productNetAmount,
            vehicleBrand: entity.vehicleBrand,
            vehicleModel: entity.vehicleModel,
            vehicleYear: entity.vehicleYear,
            vehicleCodia: entity.vehicleCodia,
            vehicleValue: entity.vehicleValue
        }).then(response => this.props.history.push(`/dealers-applications/${response.data._id}`));
    };

    constructor(props) {
        super();
        this.state.stateVehicleFields = vehiclefields;

        get('uvas/current').then(data => this.setState({uvaValue: data.currentValue}));
        getByID('customers', props.match.params.id).then(response => {
            this.setState({
                _id: response._id,
                customer: response,
                entity: {
                    income: Math.round(response.estimatedIncome * 0.7) || 10000,
                    monthlyCompromises: response.monthlyCompromises || 0,
                    ltv: response.maximumLoanToValue || 90,
                    maximumTerm: response.maximumTerm || 60,
                    fixedFees: 0,
                    variableFees: 0,
                    productPaymentDate: getPaymentDayOfMonth(),
                    incomePaymentRatio: response.incomePaymentRatio || 25,
                    rate: 13 + (response.riskSpread || 0),
                    vehicleValue: 200000,
                    requestedAmount: 100000,
                    uvaValue: this.state.uvaValue,
                    term: 60
                },
                fetching: false
            });
        });

        getCarBrands.then(marcaOptions => {
            vehiclefields.filter(x => x.name === 'vehicleBrand')[0].enum = marcaOptions;
            this.setState({stateVehicleFields: vehiclefields});
        });
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    onChangeNumber = e => {
        const value = e.target.value.replace(/\D/g, '').replace(/\./gi, '');

        if (value >= 1) {
            this.onChange('requestedAmount', value);
        } else {
            this.onChange('requestedAmount', '0');
        }
    };

    onChangeCascader = (e, selectedOptions) => {
        let carValue = 0,
            carCodia = 0;

        if (selectedOptions.length > 0) {
            carValue = selectedOptions[selectedOptions.length - 1].valor * 1000;
            carCodia = selectedOptions[selectedOptions.length - 1].codia;
        }

        this.onChange('vehicleYear', e[0]);
        this.onChange('vehicleBrand', e[1]);
        this.onChange('vehicleModel', e[2]);
        this.onChange('vehicleValue', carValue);
        this.onChange('vehicleCodia', carCodia);
    };

    render() {
        const {entity, stateVehicleFields, customer} = this.state;

        if (!entity.requestedAmount) {
            return null;
        }
        const conditions = calculateConditionsList(getConditions(entity));
        const selectedProduct = getByKey(conditions.availableConditions, Number(entity.term), 'productTerm');

        return (
            <div className="manual-application">
                <div className="loan-header">
                    <div className="loan-header__client">
                        <h2 className="loan-header__title">
                            <a href="">
                                {customer['firstName']} {customer['lastName']}
                            </a>{' '}
                            / Generar Aplicación
                        </h2>
                    </div>
                    <div className="loan-header__buttons">
                        <Button type={'primary'} className="ml-2" onClick={this.createApplication}>
                            CONFIRMAR
                        </Button>
                    </div>
                </div>
                <Row gutter={16}>
                    <Col lg={12}>
                        <div className="ant-form-item monto" style={{marginBottom: '50px'}}>
                            <label htmlFor={'requestedAmount'}>Monto Solicitado:</label>
                            <input
                                autoFocus
                                type="text"
                                id={'requestedAmount'}
                                onChange={this.onChangeNumber}
                                value={format(entity.requestedAmount, 'currency')}
                                className="ant-input"
                            />
                        </div>
                        <EditableCard
                            fields={stateVehicleFields}
                            className={'secondary'}
                            onChangeCascader={this.onChangeCascader}
                            entity={entity}
                            onChange={this.onChange}
                            title={'Vehículo'}
                        />
                        <EditableCard
                            fields={incomeFields}
                            className={'secondary'}
                            entity={entity}
                            onChange={this.onChange}
                            title={'Ingresos'}
                        />
                        <EditableCard
                            fields={fields}
                            className={'secondary'}
                            entity={entity}
                            onChange={this.onChange}
                            title={'Condiciones producto'}
                        />
                    </Col>
                    <Col lg={12}>
                        {selectedProduct ? (
                            <Card title={''}>
                                <h2>Cantidad de cuotas:</h2>
                                <div style={{display: 'flex'}}>
                                    {conditions.availableConditions.map(x => {
                                        return (
                                            <div key={x.productTerm} className="radio-button">
                                                <input
                                                    type="radio"
                                                    value={x.productTerm}
                                                    id={x.productTerm}
                                                    name="contact"
                                                    onChange={e => this.onChange('term', e.target.value)}
                                                    // eslint-disable-next-line
                                                    checked={entity.term == x.productTerm}
                                                />
                                                <label htmlFor={x.productTerm}>{x.productTerm}</label>
                                            </div>
                                        );
                                    })}
                                </div>
                                <hr />
                                <h2>Cuota Pura</h2>

                                <table style={{fontSize: '18px', fontWeight: 'bold'}}>
                                    <tbody>
                                        <tr>
                                            <td style={{textAlign: 'right'}}>UVA:</td>
                                            <td>{selectedProduct.productPeriodicPaymentUva}</td>
                                        </tr>
                                        <tr>
                                            <td style={{textAlign: 'right'}}>$:</td>
                                            <td>{format(selectedProduct.productPeriodicPaymentPesos, 'number')}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <hr />
                                <h2>Monto Prestamo</h2>

                                <table style={{fontSize: '18px', fontWeight: 'bold'}}>
                                    <tbody>
                                        <tr>
                                            <td style={{textAlign: 'right'}}>Neto:</td>
                                            <td>{format(selectedProduct.productNetAmount, 'currency')}</td>
                                        </tr>
                                        <tr>
                                            <td style={{textAlign: 'right'}}>Bruto:</td>
                                            <td>{format(selectedProduct.productPrincipalAmount, 'currency')}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <hr />
                                <h2>Detalles:</h2>

                                <table>
                                    <tbody>
                                        {offeredLoanFields.map(field => (
                                            <tr key={field.name}>
                                                <td>{field.label || startCase(field.name)}:</td>
                                                <td>{format(selectedProduct[field.name], field.type)}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </Card>
                        ) : (
                            <Card title={''} style={{width: '600px'}}>
                                No hay opciones disponibles
                            </Card>
                        )}
                    </Col>
                </Row>
            </div>
        );
    }
}

export default ManualApplication;
