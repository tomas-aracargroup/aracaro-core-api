export const approbalLetterFields = [
    {name: 'email', required: true},
    {name: 'vehicleBrand', required: true},
    {name: 'vehicleModel', required: true},
    {name: 'addressStreet', required: true},
    {name: 'addressNumber', required: true},
    {name: 'addressProvince', required: true},
    {name: 'addressCity', required: true},
    {name: 'vehicleYear', required: true},
    {name: 'vehicleInsuranceCompany', required: true},
    {name: 'vehicleInsuranceCost', required: true},
    {name: 'vehicleInsuranceCoverageType', required: true}
];

export const applicationLetterFields = [
    {name: 'email', required: true},
    {name: 'vehicleBrand', required: true},
    {name: 'vehicleModel', required: true},
    {name: 'addressStreet', required: true},
    {name: 'addressNumber', required: true},
    {name: 'addressProvince', required: true},
    {name: 'addressCity', required: true},
    {name: 'vehicleInsuranceCompany', required: true},
    {name: 'vehicleInsuranceCost', required: true},
    {name: 'vehicleInsuranceCoverageType', required: true},
    {name: 'productCurrency', required: true},
    {name: 'productAmortizationMethod', required: true}
];

export const form03Fields = [
    {name: 'email', required: true},
    {name: 'documentBy', required: true},
    {name: 'vehicleBrand', required: true},
    {name: 'vehicleModel', required: true},
    {name: 'vehicleModelForm03', required: true},
    {name: 'vehicleType', required: true},
    {name: 'vehicleChassisMake', required: true},
    {name: 'vehicleChassisNumber', required: true},
    {name: 'vehicleEngineMake', required: true},
    {name: 'vehicleEngineNumber', required: true},
    {name: 'contractPledgeDegree', required: true},
    {name: 'legalAddressStreet', required: true},
    {name: 'legalAddressNumber', required: true},
    {name: 'legalAddressProvince', required: true},
    {name: 'legalAddressCity', required: true}
];

export const contractFields = [
    {name: 'vehicleBrand', required: true},
    {name: 'vehicleModel', required: true},
    {name: 'vehicleModelForm03', required: true},
    {name: 'vehicleType', required: true},
    {name: 'vehicleChassisMake', required: true},
    {name: 'vehicleChassisNumber', required: true},
    {name: 'vehicleEngineMake', required: true},
    {name: 'vehicleEngineNumber', required: true},
    {name: 'vehicleUsageType', required: true},
    {name: 'mainProfessionalActivity', required: true},
    {name: 'legalAddressStreet', required: true},
    {name: 'legalAddressNumber', required: true},
    {name: 'legalAddressProvince', required: true},
    {name: 'legalAddressCity', required: true}
];
