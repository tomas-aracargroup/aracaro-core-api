import React from 'react';
import {Row, Col} from 'antd';
import {getByID, put} from '../../../api/dealers';
import EditableCard from '../../../components/EditableCard';
import cards from './cards';
import {notification} from 'antd/lib/index';
import {Link} from 'react-router-dom';

const modelName = 'users';
const modelNameBis = 'user';
const label = 'Usuarios Dealers';
const dataGridLink = '/grids/dealers-user';

class Customer extends React.Component {
    state = {fetching: true, entity: {}, cardsState: cards};

    constructor(props) {
        super();
        //Load entity
        getByID(modelName, props.match.params.id).then(response =>
            this.setState({_id: response._id, entity: response, fetching: false})
        );
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    onSubmit = () =>
        put(modelNameBis, this.state.entity, this.state._id)
            .then(() => notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5}))
            .catch(e => notification.error({message: e.message, duration: 1.5}));

    render() {
        const {entity, cardsState} = this.state;
        return (
            <div>
                <div className="grid-header">
                    <h2>
                        <Link to={dataGridLink}>{label}</Link> / {entity.firstName} {entity.lastName}
                    </h2>
                    <div />
                </div>
                <div className="manual-application">
                    <Row gutter={16}>
                        <Col lg={24}>
                            <div className="card-flex-wrapper">
                                {cardsState.map(card => (
                                    <EditableCard
                                        key={card.title}
                                        fields={card.fields}
                                        readOnly={card.readOnly}
                                        onExitEditMode={this.onSubmit}
                                        entity={entity}
                                        onChange={this.onChange}
                                        className="card-custom-width"
                                        title={card.title}
                                    />
                                ))}
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Customer;
