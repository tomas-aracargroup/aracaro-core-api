export default [
    {
        title: 'General',
        readOnly: false,
        fields: [
            {name: 'firstName', label: 'Nombre'},
            {name: 'lastName', label: 'Apellido'},
            {name: 'email', type: 'email'},
            {name: 'phoneNumber', label: 'Teléfono'},
            {
                name: 'companyCode',
                type: 'asyncSelect',
                endPoint: 'company',
                optionLabel: 'code',
                optionKey: 'code',
                label: 'Código Empresa'
            },
            {name: 'lastTimeActive', type: 'datetime', label: 'Última actividad'},
            {name: 'company', label: 'Empresa Declarada'}
        ]
    }
];
