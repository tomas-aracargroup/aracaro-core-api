import React from 'react';
import Table from '../../../components/Grid';
import {post} from '../../../api/dealers';

const filterGenerator = item => item.firstName + ' ' + item.lastName;

const columns = [
    {name: 'email', label: 'Email', cell: 'link', searchable: true, collectionName: 'dealers-users'},
    {name: 'firstName', label: 'Nombre', searchable: true},
    {name: 'lastName', label: 'Apellido', searchable: true},
    {name: 'companyCode', label: 'Codigo Compañia', searchable: true},
    {name: 'verified', label: 'Verificado', cell: 'checkbox'},
    {name: 'active', label: 'Habilitado', cell: 'checkbox'},
    {name: 'lastTimeActive', cell: 'timeSince', label: 'Última actividad', searchable: true},
    {name: 'delete', label: '.', cell: 'delete', collectionName: 'users'},
    {
        name: 'applicationCount',
        label: 'Aplicaciones',
        cell: 'query',
        collectionName: 'dealers-application',
        filterColumn: 'salesPerson',
        filterName: 'applicationsDealers',
        filterGenerator
    },
    {
        name: 'customerCount',
        label: 'Clientes',
        cell: 'query',
        collectionName: 'dealers-customer',
        filterColumn: 'userFullName',
        filterName: 'customers',
        filterGenerator
    }
];

export default () => (
    <Table
        post={post}
        columns={columns}
        title={'Dealers / Usuarios'}
        columnPicker
        collectionName="usersDealers"
        downloadDealers
        endPoint="users"
        sortBy={{lastName: 1}}
    />
);
