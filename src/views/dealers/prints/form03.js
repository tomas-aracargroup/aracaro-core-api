import {post} from '../../../api/dealers';

export const form03 = application => {
    const body = {
        filters: {_id: application._id}
    };
    return post('print/form03', body);
};
