import {post} from '../../../api/dealers';

export const contract = application => {
    const body = {
        filters: {_id: application._id}
    };
    return post('print/contract', body);
};
