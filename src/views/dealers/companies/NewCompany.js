import React from 'react';
import {Row, Col, Button} from 'antd';
import {kebabCase} from 'lodash';
import {post} from '../../../api/dealers';
import EditableCard from '../../../components/EditableCard';
import cards from './cards';
import {notification} from 'antd';
import {Link} from 'react-router-dom';

const modelName = 'company';
const label = 'Empresas';

class NewEntity extends React.Component {
    state = {
        entity: {
            fixedOriginationFees: 0,
            baseRate: 15,
            variableOriginationFees: 0,
            code: '',
            maxTerm: 60,
            type: 'CONCESIONARIO'
        }
    };

    onChange = (name, value) => {
        let entity = this.state.entity;

        if (name === 'name') {
            entity.code = kebabCase(value).toUpperCase();
        }

        entity[name] = value;
        this.setState({entity});
    };

    onSubmit = () =>
        post(modelName, this.state.entity)
            .then(() => {
                this.props.history.push(`/grids/${modelName}`);
                notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5});
            })
            .catch(e => notification.error({message: e.response.data.message || e.message, duration: 1.5}));

    render() {
        const {entity} = this.state;
        return (
            <div>
                <div className="grid-header">
                    <h2>
                        <Link to={'/grids/company'}>{label}</Link> / Crear
                    </h2>
                    <div />
                </div>
                <div className="manual-application">
                    <Row gutter={16}>
                        <Col lg={24}>
                            <div className="card-flex-wrapper">
                                {cards.map(card => (
                                    <EditableCard
                                        key={card.title}
                                        fields={card.fields}
                                        readOnly={card.readOnly}
                                        entity={entity}
                                        onChange={this.onChange}
                                        className="card-custom-width"
                                        title={card.title}
                                    />
                                ))}
                            </div>
                        </Col>
                        <Col lg={24}>
                            <div className="card-flex-wrapper">
                                <Button type={'primary'} onClick={this.onSubmit}>
                                    CONFIRMAR
                                </Button>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default NewEntity;
