import React from 'react';
import {Row, Col} from 'antd';
import {getByID, put} from '../../../api/dealers';
import EditableCard from '../../../components/EditableCard';
import cards from './cards';
import {notification} from 'antd';
import {Link} from 'react-router-dom';
import validator from '../../../helpers/validator';

const modelName = 'company';
const label = 'Empresas';
const dataGridLink = '/grids/company';

const flattenFields = [];
cards.forEach(x => {
    x.fields.forEach(y => {
        flattenFields.push(y);
    });
});

class Customer extends React.Component {
    state = {fetching: true, entity: {}, cardsState: cards};

    constructor(props) {
        super();
        getByID(modelName, props.match.params.id).then(response =>
            this.setState({_id: response._id, entity: response, fetching: false})
        );
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    onSubmit = () => {
        validator(this.state.entity, flattenFields);

        this.setState({submitted: true});
        put(modelName, this.state.entity, this.state._id)
            .then(() => {
                notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5});
                this.setState({submitted: false});
            })
            .catch(e => notification.error({message: e.message, duration: 1.5}));
    };

    render() {
        const {entity, cardsState, submitted} = this.state;

        const errors = validator(entity, flattenFields);

        return (
            <div>
                <div className="grid-header">
                    <h2>
                        <Link to={dataGridLink}>{label}</Link> / {entity.name}
                    </h2>
                    <div />
                </div>

                <div className="manual-application">
                    <Row gutter={16}>
                        <Col lg={24}>
                            <div className="card-flex-wrapper">
                                {cardsState.map(card => (
                                    <EditableCard
                                        key={card.title}
                                        fields={card.fields}
                                        readOnly={card.readOnly}
                                        onExitEditMode={this.onSubmit}
                                        entity={entity}
                                        errors={errors}
                                        submitted={submitted}
                                        onChange={this.onChange}
                                        className="card-custom-width"
                                        title={card.title}
                                    />
                                ))}
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Customer;
