import {eTIPO_EMPRESA} from '../../../constants/enumerators';

export default [
    {
        title: 'General',
        readOnly: false,
        fields: [
            {name: 'name', label: 'Nombre', required: true},
            {name: 'code', label: 'Codigo'},
            {name: 'group', label: 'Grupo'},
            {label: 'Tasa Base', name: 'baseRate', type: 'percentage'},
            {label: 'Gastos Fijos', name: 'fixedOriginationFees', type: 'currency'},
            {label: 'Gastos Variables', name: 'variableOriginationFees', type: 'percentage'},
            {name: 'maxTerm', label: 'Plazo Maximo'},
            {
                name: 'type',
                label: 'Tipo Empresa',
                type: 'select',
                enum: eTIPO_EMPRESA
            }
        ]
    },
    {
        title: 'Contacto',
        readOnly: false,
        fields: [
            {name: 'addressStreet', label: 'Calle'},
            {name: 'addressNumber', label: 'Número'},
            {name: 'addressProvince', label: 'Provincia'},
            {name: 'addressCity', label: 'Ciudad'},
            {name: 'addressZipCode', label: 'Código Postal'},
            {name: 'email', type: 'email'},
            {name: 'phone', label: 'Teléfono'},
            {name: 'salesPerson', label: 'Vendedor'}
        ]
    },

    {
        title: 'Facturación',
        readOnly: false,
        fields: [
            {name: 'invoicingLegalName', label: 'Razón Social'},
            {name: 'invoicingCuit', label: 'CUIT', min: 11, max: 11},
            {name: 'invoicingCbu', label: 'CBU', min: 22, max: 22},
            {name: 'invoicingCbuAlias', label: 'CBU Alias'}
        ]
    }
];
