import React from 'react';
import Table from '../../../components/Grid';
import {post} from '../../../api/dealers';

const filterGenerator = item => item.code;

const columns = [
    {name: 'name', label: 'Empresa', cell: 'link', searchable: true, collectionName: 'company'},
    {name: 'type', label: 'Tipo', searchable: true},
    {name: 'code', label: 'Codigo', searchable: true},
    {name: 'group', label: 'Grupo', searchable: true},
    {name: 'addressProvince', label: 'Provincia', searchable: true},
    {name: 'createdAt', label: 'Creado', cell: 'timeSince', searchable: true},
    {name: 'addressCity', label: 'Ciudad', searchable: true},
    {name: 'baseRate', cell: 'percentage', label: 'Tasa Base', searchable: true},
    {name: 'fixedOriginationFees', cell: 'money', label: 'Gastos Fijos', searchable: true},
    {
        name: 'variableOriginationFees',
        cell: 'percentage',
        label: 'Gastos Variables',
        searchable: true
    },
    {
        name: 'applicationCount',
        label: 'Aplicaciones',
        cell: 'query',
        collectionName: 'dealers-application',
        filterColumn: 'companyCode',
        filterName: 'applicationsDealers',
        filterGenerator
    },
    {
        name: 'customerCount',
        label: 'Clientes',
        cell: 'query',
        collectionName: 'dealers-customer',
        filterColumn: 'companyCode',
        filterName: 'customers',
        filterGenerator
    }
];

export default () => {
    return (
        <Table
            post={post}
            columns={columns}
            columnPicker
            stripped
            title={'Dealers / Empresas'}
            createRoute="/create/company"
            collectionName="company"
            downloadDealers
            endPoint="company"
            filters={{}}
            sortBy={{_id: -1}}
        />
    );
};
