import React from 'react';
import moment from 'moment';
import {connect} from 'react-redux';
import {Row, Col} from 'antd';
import {getByID, put, post} from '../../../api/dealers';
import EditableCard from '../../../components/EditableCard';
import {Link} from 'react-router-dom';
import {notification} from 'antd';
import GridCard from '../../../components/GridCard';

const generalCard = {
    title: 'General',
    fields: [
        {
            name: 'documentNumber',
            label: 'Documento',
            placeholder: '10.200.300',
            required: true,
            type: 'number',
            min: 100000,
            max: 99999999
        },
        {name: 'firstName', label: 'Nombre', required: true, letterOnly: true, min: 2, max: 50},
        {name: 'lastName', label: 'Apellido', letterOnly: true, min: 2, max: 50},
        {name: 'dob', label: 'Edad', type: 'time', mask: '11/11/1111'},
        {name: 'phoneNumber', label: 'Telefono'},
        {name: 'email', label: 'Email', type: 'email', required: true, letterOnly: true, min: 2, max: 50}
    ]
};

const beSmartCard = {
    title: 'Ultima Consulta',
    fields: [
        {label: 'Bancarizado', name: 'bs_ciBancarizado'},
        {label: 'Canal', name: 'bs_canal'},
        {label: 'Compromisos Mensuales', name: 'bs_ciVigCompMensual', type: 'money'},
        {label: 'Cuota Máxima', name: 'bs_cuotaMax', type: 'money'},
        {label: 'Dictamen', name: 'bs_dictamen'},
        {label: 'Explicación', name: 'bs_explicacion'},
        {label: 'Fecha', name: 'bs_solicitudFecha', type: 'date'},
        {label: 'Ingreso Computado', name: 'bs_ingresoComputado', type: 'money'},
        {label: 'Ingreso Declarado', name: 'bs_ingresoDeclarado', type: 'money'},
        {label: 'Ingreso Estimado', name: 'bs_ingresoEstimado', type: 'money'},
        {label: 'Ingreso Verificado', name: 'bs_ingresoVerificado', type: 'money'},
        {label: 'LTV', name: 'bs_ltvFinal', type: 'percentage'},
        {label: 'Monto ', name: 'bs_monto', type: 'money'},
        {label: 'Monto Máximo', name: 'bs_capitalMaximo', type: 'money'},
        {label: 'Motivo', name: 'bs_motivo'},
        {label: 'Nivel de Riesgo', name: 'bs_riesgoNivel'},
        {label: 'Nosis', name: 'bs_scoVig'},
        {label: 'Plazo Maximo', name: 'bs_plazoMaxAntig'},
        {label: 'Producto', name: 'bs_producto'},
        {label: 'RCI', name: 'bs_rci', type: 'percentage'},
        {label: 'Siisa', name: 'bs_siScore'},
        {label: 'Situación Laboral', name: 'bs_situacionLaboralFinal'},
        {label: 'Sobre Tasa', name: 'bs_coefTasa', type: 'percentage'},
        {label: 'Subproducto', name: 'bs_subproducto'},
        {label: 'Vehiculo Antigüedad ', name: 'bs_vehiculoAntiguedad'},
        {label: 'Vehiculo Modelo', name: 'bs_vehiculoMarcaModeloVersion'},
        {label: 'Vehiculo Valor', name: 'bs_vehiculoValor', type: 'money'},
        {name: 'bscdPeorSituUlt', label: 'BCRA'},
        {name: 'bscdPeorSituUlt2_3', label: 'BCRA 2_3'},
        {name: 'bscdPeorSituUlt4_6', label: 'BCRA 4_6'},
        {name: 'bscdPeorSituUlt7_12', label: 'BCRA 7_12'}
    ]
};

const gridCardCols = [
    {
        header: 'Modelo',
        field: 'vehicleModel'
    },
    {
        header: 'Año',
        field: 'vehicleYear'
    },
    {
        header: 'Monto Préstamo',
        field: 'productNetAmount',
        type: 'money'
    }
];

class Customer extends React.Component {
    state = {fetching: true, entity: {}, applications: []};

    constructor(props) {
        super();
        getByID('online-customer', props.match.params.id).then(response => {
            this.setState({
                _id: response._id,
                entity: response,
                fetching: false
            });
        });

        post('queries/online-application', {
            filters: {onlineCustomerId: props.match.params.id}
        }).then(x => {
            this.setState({applications: x.collection});
        });
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    onSubmit = () =>
        put(`byId/online-customer`, this.state.entity, this.state._id)
            .then(() => notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5}))
            .catch(e => notification.error({message: e.message, duration: 1.5}));

    addNote = note => {
        const {user} = this.props.app.auth;
        let {entity} = this.state;

        entity.notes.push({
            text: note,
            user: user.firstname + ' ' + user.lastname,
            timeStamp: new Date()
        });
        put(`byId/online-customer`, entity, entity._id);
        this.setState({entity});
    };

    onRowClick = row => {
        this.props.history.push(`/entity/online-application/${row._id}`);
    };

    render() {
        const {entity, applications} = this.state;

        return (
            <div>
                <div className="grid-header">
                    <h2>
                        <Link to={'/grids/online-customer'}>{'Clientes Online'}</Link> / {entity.firstName}{' '}
                        {entity.lastName} {entity.dob ? '(' + moment().diff(entity.dob, 'years') + ')' : ''}
                    </h2>
                </div>
                <div className="manual-application">
                    <Row gutter={16}>
                        <Col lg={24}>
                            <div className="card-flex-wrapper">
                                {applications.length > 0 && (
                                    <GridCard
                                        data={applications}
                                        headers={gridCardCols}
                                        title="Aplicaciones"
                                        onRowClick={this.onRowClick}
                                        className="card-custom-width"
                                    />
                                )}

                                <EditableCard
                                    key={generalCard.title}
                                    fields={generalCard.fields}
                                    readOnly={generalCard.readOnly}
                                    onExitEditMode={this.onSubmit}
                                    entity={entity}
                                    onChange={this.onChange}
                                    className="card-custom-width"
                                    title={generalCard.title}
                                />
                                {entity.bs_dictamen && (
                                    <EditableCard
                                        key={beSmartCard.title}
                                        fields={beSmartCard.fields}
                                        readOnly={beSmartCard.readOnly}
                                        onExitEditMode={this.onSubmit}
                                        entity={entity}
                                        onChange={this.onChange}
                                        className="card-custom-width"
                                        title={beSmartCard.title}
                                    />
                                )}
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default connect(x => ({app: x.app}))(Customer);
