import React from 'react';
import Table from '../../../components/Grid';
import {post} from '../../../api/dealers';

const columns = [
    {name: 'documentNumber', label: 'Documento', cell: 'link', searchable: true, collectionName: 'dealers-customers'},
    {name: 'firstName', label: 'Nombre', searchable: true},
    {name: 'lastName', label: 'Apellido', searchable: true},
    {name: 'userFullName', label: 'Creado por', searchable: true},
    {name: 'beSmartResult', label: 'Besmart', cell: 'dictamen', searchable: true},
    {name: 'riskLevel', label: 'Nivel de Riesgo', searchable: true},
    {name: 'laboralSituation', label: 'Situacion laboral', searchable: true},
    {name: 'companyCode', label: 'Codigo Compañia', searchable: true},
    {name: 'employer', label: 'Empleador', searchable: true},
    {name: 'bancarized', cell: 'string', label: 'Bancarizado'},
    {name: 'createdAt', label: 'Creado', cell: 'timeSince', searchable: true},
    {name: 'maximumGrossLoanAmount', label: 'Monto Maximo', cell: 'money'},
    {name: 'maximumLoanToValue', label: 'LTV Max', cell: 'percentage'},
    {name: 'maximumMonthlyPayment', label: 'Cuota Max', cell: 'money'},
    {name: 'maximumTerm', label: 'Plazo Max', cell: 'number'},
    {name: 'monthlyCompromises', label: 'Compromisos', cell: 'money'},
    {name: 'nosisScore', label: 'NOSIS', cell: 'number', searchable: true},
    {name: 'siisaScore', label: 'SIISA', cell: 'number', searchable: true},
    {name: 'riskSpread', label: 'Sobre Tasa', cell: 'percentage'},
    {name: 'delete', label: '.', cell: 'delete', collectionName: 'customers'}
];

export default props => {
    return (
        <Table
            className="sessions"
            post={post}
            columnPicker
            title={'Dealers / Clientes'}
            columns={columns}
            forceRefresh={props.history.location.state && props.history.location.state.forceRefresh}
            collectionName="customers"
            downloadDealers
            endPoint="customers"
            filters={{}}
            sortBy={{_id: -1}}
        />
    );
};
