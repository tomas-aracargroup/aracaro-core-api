import {eGENERO} from '../../../constants/enumerators';

export default [
    {
        title: 'Besmart',
        readOnly: true,
        fields: [
            {name: 'beSmartResult', label: 'Dictamen', readOnly: true},
            {name: 'beSmartMotivo', label: 'Motivo', readOnly: true},
            {name: 'beSmartExplicacion', label: 'Explicación', readOnly: true},
            {name: 'employer', label: 'Empleador', readOnly: true},
            {name: 'siisaName', label: 'Nombre SIISA', readOnly: true},
            {name: 'siisaScore', label: 'SIISA', type: 'number', readOnly: true},
            {name: 'nosisScore', label: 'NOSIS', type: 'number', readOnly: true},
            {name: 'riskLevel', label: 'Nivel Riesgo', readOnly: true},
            {name: 'riskSpread', label: 'Sobre Tasa', type: 'percentage', readOnly: true},
            {name: 'estimatedIncome', type: 'money', label: 'Ing. estimado'},
            {name: 'laboralSituation', type: 'string', label: 'Sit. laboral', readOnly: true},
            {name: 'bancarized', type: 'string', label: 'Bancarizado', readOnly: true},
            {name: 'monthlyCompromises', label: 'Compromisos', type: 'money'},
            {name: 'maximumTerm', label: 'Plazo Máximo', type: 'number'},
            {name: 'incomePaymentRatio', label: 'RCI', type: 'percentage'},
            {name: 'beSmartBCRA', label: 'BCRA'},
            {name: 'beSmartBCRA_2_3', label: 'BCRA 2_3'},
            {name: 'beSmartBCRA_4_6', label: 'BCRA 4_6'},
            {name: 'beSmartBCRA_7_12', label: 'BCRA 7_12'},
            {name: 'beSmartSubproducto', label: 'Subproducto'},
            {name: 'beSmartRiesgoCategoria', label: 'Riesgo Categoria'}
        ]
    },
    {
        title: 'General',
        fields: [
            {
                name: 'documentNumber',
                label: 'Documento',
                placeholder: '10.200.300',
                required: true,
                type: 'number',
                min: 100000,
                max: 99999999
            },
            {name: 'gender', enum: eGENERO, required: true, type: 'radioButton', label: 'Género'},
            {name: 'firstName', label: 'Nombre', required: true, letterOnly: true, min: 2, max: 50},
            {name: 'lastName', label: 'Apellido', letterOnly: true, min: 2, max: 50},
            {name: 'dob', label: 'Fecha Nac.', type: 'date', mask: '11-11-1111'},
            {name: 'email', label: 'Email', type: 'email', required: true, letterOnly: true, min: 2, max: 50},
            {name: 'cuit', label: 'CUIT', type: 'string'},
            {name: 'cellPhone', label: 'Teléfono', letterOnly: true, min: 2, max: 50},
            {name: 'addressStreet', label: 'Calle', min: 2, max: 50},
            {name: 'addressNumber', label: 'Número', min: 2, max: 50},
            {name: 'addressFloor', label: 'Piso', required: true, min: 2, max: 50},
            {name: 'addressProvince', label: 'Provincia', type: 'select', enum: []},
            {name: 'addressCity', label: 'Ciudad', type: 'select', enum: []},
            {name: 'addressZipCode', label: 'Código Postal'},
            {name: 'userFullName', label: 'Creado por'},
            {name: 'companyCode', label: 'Codigo Empresa'}
        ]
    }
];
