import React from 'react';
import {connect} from 'react-redux';
import {Button, Row, Col} from 'antd';
import {getByID, getCities, getProvinces, put} from '../../../api/dealers';
import EditableCard from '../../../components/EditableCard';
import {getByKey} from '../../../helpers/index';
import cards from './customerFields';
import NotesCard from '../../../components/NotesCard';
import {Link} from 'react-router-dom';
import {notification} from 'antd/lib/index';

const label = 'Clientes Dealers';
const modelName = 'customer';
const dataGridLink = '/grids/company';

class Customer extends React.Component {
    state = {fetching: true, entity: {}, cardsState: cards};

    constructor(props) {
        super();
        getByID('customers', props.match.params.id).then(response => {
            this.setState({
                _id: response._id,
                entity: response,
                fetching: false
            });
            if (this.state.entity['addressProvince']) {
                getCities(this.state.entity['addressProvince']).then(x => {
                    cards
                        .filter(x => x.title === 'General')[0]
                        .fields.filter(x => x.name === 'addressCity')[0].enum = x;
                    this.setState({cardsState: cards, fetchingCities: false, cities: x});
                });
            }
        });
        getProvinces().then(provinces => {
            cards
                .filter(x => x.title === 'General')[0]
                .fields.filter(x => x.name === 'addressProvince')[0].enum = provinces;
            this.setState({cardsState: cards});
        });
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;

        if (name === 'addressProvince') {
            entity['addressCity'] = '';

            cards.filter(x => x.title === 'General')[0].fields.filter(x => x.name === 'addressCity')[0].enum = [
                {id: 'fetching', name: 'Buscando Ciudades...'}
            ];
            this.setState({entity, cardsState: cards});

            getCities(entity['addressProvince']).then(x => {
                cards.filter(x => x.title === 'General')[0].fields.filter(x => x.name === 'addressCity')[0].enum = x;
                this.setState({cities: x, cardsState: cards});
            });
        } else if (name === 'addressCity') {
            const cityItem = getByKey(this.state.cities, value, 'id');
            if (cityItem) {
                this.onChange('addressZipCode', cityItem.zipCode);
            } else {
                this.onChange('addressZipCode', '');
            }
        } else {
            this.setState({entity});
        }
    };

    createApplication = () => {
        this.props.history.push(`/manual-application/${this.state.entity._id}`);
    };

    onSubmit = () =>
        put(modelName, this.state.entity, this.state._id)
            .then(() => notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5}))
            .catch(e => notification.error({message: e.message, duration: 1.5}));

    addNote = note => {
        const {user} = this.props.app.auth;
        let {entity} = this.state;

        entity.notes.push({
            text: note,
            user: user.firstname + ' ' + user.lastname,
            timeStamp: new Date()
        });
        put(`customer`, entity, entity._id);
        this.setState({entity});
    };

    render() {
        const {entity, cardsState} = this.state;

        return (
            <div>
                <div className="grid-header">
                    <h2>
                        <Link to={dataGridLink}>{label}</Link> / {entity.firstName} {entity.lastName}
                    </h2>
                    <div className="loan-header__buttons">
                        <Button className="ml-2" onClick={this.createApplication}>
                            CREAR APLICACIÓN
                        </Button>
                    </div>
                </div>
                <div className="manual-application">
                    <Row gutter={16}>
                        <Col lg={24}>
                            <div className="card-flex-wrapper">
                                {cardsState.map(card => (
                                    <EditableCard
                                        key={card.title}
                                        fields={card.fields}
                                        readOnly={card.readOnly}
                                        onExitEditMode={this.onSubmit}
                                        entity={entity}
                                        onChange={this.onChange}
                                        className="card-custom-width"
                                        title={card.title}
                                    />
                                ))}
                                <NotesCard
                                    title="Notas"
                                    notes={entity.notes}
                                    onAddNote={this.addNote}
                                    className="card-custom-width"
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default connect(x => ({app: x.app}))(Customer);
