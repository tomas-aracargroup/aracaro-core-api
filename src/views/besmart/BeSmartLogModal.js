import React from 'react';
import {Col, Card, Modal, Row, Tabs} from 'antd';

const TabPane = Tabs.TabPane;
const tdStyle = {textAlign: 'right', fontWeight: 'bold', paddingRight: '10px'};

const RawTable = ({obj}) => (
    <table>
        <tbody>
            {Object.keys(obj)
                .sort()
                .map(key => (
                    <tr key={key}>
                        <td style={tdStyle}>{key}:</td>
                        <td>{obj[key]}</td>
                    </tr>
                ))}
        </tbody>
    </table>
);

class BeSmartLogModal extends React.Component {
    render() {
        const {entity, handleOk} = this.props;
        const title = (
            <div className="name">
                <label style={{fontWeight: 'bold', fontSize: '1.2rem'}}>
                    {entity.nombre} {entity.apellido}
                </label>
            </div>
        );

        return (
            <Modal iconType={'cross-circle'} visible onCancel={handleOk} width={'960px'} closable>
                <Col>
                    <Card title={title}>
                        <Row>
                            <Col span={24}>
                                <Tabs defaultActiveKey="in">
                                    <TabPane tab="Entrada" key="in">
                                        {<RawTable obj={entity.parametersIn || {}} />}
                                    </TabPane>
                                    <TabPane tab="Salida" key="out">
                                        {<RawTable obj={entity.parametersOut || {}} />}
                                    </TabPane>
                                </Tabs>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Modal>
        );
    }
}

export default BeSmartLogModal;
