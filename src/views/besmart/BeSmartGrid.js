import React from 'react';
import Table from '../../components/Grid';
import {post} from '../../api/dealers';

const columns = [
    {
        name: 'documento',
        label: 'Documento',
        cell: 'action',
        modalName: 'besmartlog',
        searchable: true
    },
    {name: '_id', label: 'Es Conductor', searchable: true},
    {name: 'nombre', label: 'Nombre', searchable: true},
    {name: 'apellido', label: 'Apellido', searchable: true},
    {name: 'producto', label: 'Codigo Producto', searchable: true},
    {name: 'canalOriginacion', label: 'Codigo Canal', searchable: true},
    {name: 'createdAt', label: 'Timestamp', searchable: true},
    {name: 'tiempoRespuesta', label: 'Tiempo respuesta', cell: 'number'},
    {name: 'parametersIn', label: 'Es Conductor', searchable: true, hideInColumnPicker: true},
    {name: 'parametersOut', label: 'Es Conductor', searchable: true, hideInColumnPicker: true}
];

export default () => {
    return (
        <Table
            title={'Besmart Log'}
            columnPicker
            post={post}
            columns={columns}
            downloadDealers
            collectionName="besmartlog"
            endPoint="besmartlog"
            sortBy={{_id: -1}}
        />
    );
};
