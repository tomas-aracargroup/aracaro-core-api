import enums from '../../helpers/enumerators';

export const externalUserFields = [
    //personal
    {name: 'documento', placeholder: '10.200.300', required: true, type: 'number', min: 100000, max: 99999999},
    {name: 'genero', enum: enums.eGENERO, required: true, type: 'radioButton', label: 'Género'},
    {name: 'nombre', required: true, letterOnly: true, min: 2, max: 50},
    {name: 'apellido', required: true, letterOnly: true, min: 2, max: 50},
    {name: 'marca', type: 'cascader', label: 'Año/Marca/Modelo', required: true},
    {name: 'es0KM', type: 'checkbox', label: 'es 0KM?'},
    {name: 'monto', type: 'currency', required: true},
    {name: 'kilometros', type: 'number', label: 'Kilómetros', placeholder: '0', min: 0},
    {
        name: 'vehiculoUsoDeclarado',
        type: 'select',
        placeholder: 'Particular, taxi, etc',
        enum: enums.eVEHICULO_USO_DECLARADO,
        label: 'Vehículo Uso'
    },
    {name: 'email', type: 'email', label: 'Email'},
    {name: 'vehiculoTipo', type: 'select', enum: enums.eVEHICULO_TIPO, label: 'Vehículo Tipo'},
    {name: 'destinoCredito', enum: enums.eDESTINO_CREDITO, type: 'select', label: 'Destino'},
    {name: 'producto', type: 'select', enum: enums.ePRODUCTO}
];

export const officeUserFields = [
    //personal
    {name: 'documento', placeholder: '10.200.300', required: true, type: 'number', min: 100000, max: 99999999},
    {name: 'genero', enum: enums.eGENERO, required: true, type: 'radioButton', label: 'Género'},
    {name: 'nombre', required: true, letterOnly: true, min: 2, max: 50},
    {name: 'apellido', required: true, letterOnly: true, min: 2, max: 50},
    {name: 'marca', type: 'cascader', label: 'Año/Marca/Modelo', required: true},
    {name: 'es0KM', type: 'checkbox', label: 'Es 0KM'},
    {name: 'monto', type: 'currency', required: true},
    {name: 'kilometros', type: 'number', label: 'Kilómetros', placeholder: '0', min: 0},
    {
        name: 'vehiculoUsoDeclarado',
        type: 'select',
        placeholder: 'Particular, taxi, etc',
        enum: enums.eVEHICULO_USO_DECLARADO,
        label: 'Vehículo Uso'
    },
    {name: 'ingresoVerificado', type: 'currency', placeholder: '$40.000'},
    {name: 'ingresoDeclarado', type: 'currency', placeholder: '$40.000'},
    {name: 'email', type: 'email', label: 'Email'},
    {name: 'vehiculoTipo', type: 'select', enum: enums.eVEHICULO_TIPO, label: 'Vehículo Tipo'},
    {name: 'destinoCredito', enum: enums.eDESTINO_CREDITO, type: 'select', label: 'Destino'},
    {name: 'producto', type: 'select', enum: enums.ePRODUCTO},
    {name: 'canalOriginacion', type: 'select', enum: enums.eCANAL_ORIGINACION, label: 'Canal'}
];
