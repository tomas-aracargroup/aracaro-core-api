import enums from '../../helpers/enumerators';
import FieldsMain from './fieldsMain';

class Fields extends FieldsMain {
    constructor() {
        super();
        this.fields = [
            {
                group: 'solicitud',
                label: 'Solicitud',
                order: 1,
                fields: [
                    {name: 'FECHA_SOLICITUD', label: 'Fecha', type: 'date', order: 1, required: true},
                    {name: 'CANAL', label: 'Canal', order: 2, disabled: true},
                    {name: 'SUCURSAL', label: 'Sucursal', order: 3},
                    {name: 'VENDEDOR', label: 'Venderdor', order: 4}
                ]
            },
            {
                group: 'deudor',
                label: 'Solicitante',
                order: 1,
                fields: [
                    {name: 'SOLICITANTE_NOMBRE_APELLIDO', label: 'Solicitante', order: 5, disabled: true},
                    {name: 'SOLICITANTE_NUMERO_DOCUMENTO', label: 'Documento', order: 6, disabled: true},
                    {name: 'SOLICITANTE_FECHA_NACIMIENTO', label: 'Fecha Nac.', order: 7, disabled: true},
                    {name: 'SOLICITANTE_EDAD', label: 'Edad', order: 8, disabled: true},
                    {name: 'SOLICITANTE_TELEFONO_FIJO', label: 'Tel. Part.', order: 9},
                    {name: 'SOLICITANTE_TELEFONO_CELULAR', label: 'Cel', order: 10},
                    {name: 'SOLICITANTE_CALLE_NUMERO', label: 'Dirección', order: 11, required: true},
                    {name: 'SOLICITANTE_LOCALIDAD', label: 'Localidad', order: 12, required: true},
                    {
                        name: 'SOLICITANTE_PROVINCIA',
                        label: 'Provincia',
                        order: 13,
                        type: 'select',
                        enum: enums.ePROVINCIA,
                        required: true
                    },
                    {
                        name: 'SOLICITANTE_ESTADO_CIVIL',
                        label: 'Estado Civil',
                        order: 14,
                        type: 'select',
                        enum: enums.eMARITAL_STATUS,
                        required: true
                    }
                ]
            },
            {
                group: 'conyugue',
                label: 'Cónyugue',
                order: 1,
                fields: [
                    {name: 'CONYUGE_NOMBRE_APELLIDO', label: 'Cónyuge/Cotitular', order: 15, required: true},
                    {name: 'CONYUGE_NUMERO_DOCUMENTO', label: 'Documento', order: 16, required: true},
                    {name: 'CONYUGE_FECHA_NACIMIENTO', label: 'Fecha Nac.', type: 'date', order: 17, required: true},
                    {name: 'CONYUGE_EDAD', label: 'Edad', order: 18, disabled: true}
                ]
            },
            {
                group: 'prestamo',
                label: 'Préstamo',
                order: 1,
                fields: [
                    {name: 'CAPITAL_A_FINANCIAR', label: 'Capital a Financiar', order: 19, disabled: true},
                    {name: 'SISTEMA_DE_AMORTIZACION', label: 'Amortización', order: 20, disabled: true},
                    {name: 'PLAZO', label: 'Plazo', order: 21, disabled: true},
                    {name: 'PRODUCTO', label: 'Producto', order: 22, disabled: true},
                    {name: 'CUOTA_PURA', label: 'Cuota Pura', order: 23, disabled: true},
                    {name: 'TNA', label: 'TNA', order: 24, disabled: true},
                    {name: 'TEA', label: 'TEA', order: 25, disabled: true},
                    {name: 'SEGURO_VIDA', label: 'Seguro de Vida (+IVA s/int)', order: 26, disabled: true},
                    {name: 'CFT', label: 'CFT', order: 27, disabled: true},
                    {name: 'CFT_MAS_IVA', label: 'CFT(+IVA)', order: 28, disabled: true},
                    {name: 'CUOTA_INICIAL_APROXIMADA', label: 'Cuota Inicial Apróx.', order: 28, disabled: true},
                    {name: 'TIPO_TASA', label: 'Tipo de Tasa', order: 29, disabled: true},
                    {name: 'CUOTA_UVA', label: 'Cuota UVA', order: 30, disabled: true},
                    {name: 'MONEDA', label: 'Moneda', order: 31, disabled: true}
                ]
            },
            {
                group: 'vehiculo',
                label: 'Datos Vehículo',
                order: 2,
                fields: [
                    {name: 'MARCA', label: 'Marca', order: 32, disabled: true},
                    {name: 'MODELO', label: 'Modelo', order: 33, disabled: true},
                    {name: 'ANIO', label: 'Modelo', order: 34, disabled: true},
                    {name: 'NUEVO_USADO', label: 'Nuevo', order: 35, disabled: true},
                    {name: 'USO', label: 'Nuevo', order: 36, disabled: true},
                    {name: 'VALOR', label: 'Valor del Bien', order: 37, disabled: true},
                    {name: 'COMPANIA_ASEGURADORA', label: 'Cobertura', order: 38, disabled: true},
                    {name: 'COSTO_SEGURO_AUTOMOTOR_APROXIMADO', label: 'Costo Seguro', order: 39, disabled: true}
                ]
            }
        ];
    }
}
export default new Fields();
