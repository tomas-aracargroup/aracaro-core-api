class FieldsMain {
    setGroupVisibility(groupName, show) {
        const group = this.fields.filter(x => x.group === groupName)[0];
        if (group) {
            group.show = show;
        }
    }
    setRequired(groupName, fieldName, isRequired) {
        const field = this.fields.filter(x => x.group === groupName)[0].fields.filter(x => x.name === fieldName)[0];
        if (field) {
            field.required = isRequired;
        }
    }

    flatter() {
        const result = [];
        this.fields.forEach(group => group.fields.forEach(field => result.push(field)));
        return result;
    }
}

export default FieldsMain;
