import enums from '../../helpers/enumerators';

export default [
    {name: 'addressStreet', required: true, min: 2, max: 50},
    {name: 'addressNumber', type: 'number', required: true}
];
