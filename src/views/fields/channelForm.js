import enums from '../../helpers/enumerators';

export default [
    {name: 'name', label: 'Nombre', order: 1, required: true, min: 2, max: 25},
    {name: 'description', label: 'Descripción', order: 2},
    {name: 'address', label: 'Dirección', order: 3, required: true},
    {name: 'type', label: 'Tipo', order: 4, required: true, type: 'select', enum: enums.eCANAL_ORIGINACION},
    {name: 'company', label: 'Empresa', order: 5, type: 'select'},
    {name: 'email', label: 'Email', order: 6, type: 'email'},
    {name: 'status', label: 'Habilitado', type: 'checkbox', order: 7}
];
