import enums from '../../helpers/enumerators';
import FieldsMain from './fieldsMain';

class Fields extends FieldsMain {
    constructor() {
        super();
        this.fields = [
            {
                group: 'solicitud',
                label: 'Solicitud',
                order: 1,
                fields: [
                    {name: 'SOLICITUD_NUMERO', label: 'Solicitud #', disabled: true, order: 1},
                    {name: 'SOLICITUD_FECHA_DIA_MES_ANO', label: 'Fecha', type: 'date', order: 2},
                    {name: 'SOLICITUD_SUCURSAL', label: 'Sucursal', disabled: true, order: 3},
                    {name: 'SOLICITUD_CANAL', label: 'Canal', disabled: true, order: 4}
                ]
            },
            {
                group: 'cliente',
                label: 'Datos del Cliente (1° Titular)',
                order: 2,
                fields: [
                    {name: 'DEUDOR_NOMBRE', label: 'Nombre', disabled: true, order: 5},
                    {name: 'DEUDOR_APELLIDO', label: 'Apellido', disabled: true, order: 6},
                    {
                        name: 'DEUDOR_TIPO_DOCUMENTO',
                        label: 'Tipo Doc.',
                        type: 'select',
                        disabled: true,
                        enum: enums.eID_TYPE,
                        own: 1,
                        order: 7
                    },
                    // {name: 'DEUDOR_TIPO_DOCUMENTO_DNI', label: 'DNI', order: 7},
                    // {name: 'DEUDOR_TIPO_DOCUMENTO_LC', label: 'LC', order: 8},
                    // {name: 'DEUDOR_TIPO_DOCUMENTO_LE', label: 'LE', order: 9},
                    // {name: 'DEUDOR_TIPO_DOCUMENTO_PASAPORTE', label: 'Pasaporte', order: 10},
                    {name: 'DEUDOR_NUMERO_DOCUMENTO', label: 'Documento', disabled: true, order: 8},
                    {
                        name: 'DEUDOR_TIPO',
                        label: 'CUIT-CUIL-CDI',
                        type: 'select',
                        disabled: true,
                        enum: enums.eCUIL_TYPE,
                        own: 1,
                        order: 9
                    },
                    // {name: 'DEUDOR_TIPO_CUIL', label: '', order: 50},
                    // {name: 'DEUDOR_TIPO_CUIT', label: '', order: 50},
                    // {name: 'DEUDOR_TIPO_CDI', label: '', order: 50},
                    {name: 'DEUDOR_CUIL_CUIT_CDI_N', label: 'Número', disabled: true, order: 10},
                    {
                        name: 'DEUDOR_FECHA_NACIMIENTO',
                        label: 'Fecha Nac.',
                        type: 'date',
                        required: true,
                        order: 12,
                        min: new Date().setFullYear(new Date().getFullYear() - 75),
                        max: new Date().setFullYear(new Date().getFullYear() - 18)
                    },
                    {
                        name: 'DEUDOR_GENERO',
                        label: 'Género',
                        type: 'radioButton',
                        disabled: true,
                        enum: enums.eGENERO,
                        own: 1,
                        order: 14
                    },
                    // {name: 'DEUDOR_GENERO_F', label: '', order: 50},
                    {
                        name: 'DEUDOR_ESTADO_CIVIL',
                        label: 'Estado Civil',
                        type: 'select',
                        enum: enums.eMARITAL_STATUS,
                        own: 1,
                        order: 15
                    },
                    // {name: 'DEUDOR_ESTADO_CIVIL_SOLTERO', label: '', order: 50},
                    // {name: 'DEUDOR_ESTADO_CIVIL_CASADO', label: '', order: 50},
                    // {name: 'DEUDOR_ESTADO_CIVIL_DIVORCIADO', label: '', order: 50},
                    // {name: 'DEUDOR_ESTADO_CIVIL_VIUDO', label: '', order: 50},
                    // {name: 'DEUDOR_ESTADO_CIVIL_UNION_CONVENCIONAL', label: '', order: 50},
                    {name: 'DEUDOR_TELEFONO_FIJO', label: 'Tel.Fijo', order: 16},
                    {name: 'DEUDOR_CELULAR', label: 'Celular', order: 17},
                    {name: 'DEUDOR_MAIL', label: 'e-Mail', order: 18},
                    {
                        name: 'DEUDOR_PAIS_NACIMIENTO',
                        label: 'País Nac.',
                        type: 'select',
                        enum: enums.eCOUNTRIES,
                        order: 19
                    },
                    {
                        name: 'DEUDOR_NACIONALIDAD',
                        label: 'Nacionalidad',
                        type: 'select',
                        enum: enums.eCOUNTRIES,
                        order: 20
                    },
                    {
                        name: 'DEUDOR_RESIDENCIA',
                        label: 'Residencia',
                        type: 'radio',
                        enum: enums.eRESIDENCIA,
                        own: 1,
                        order: 21
                    }
                    // {name: 'DEUDOR_RESIDENCIA_TEMPORARIA', label: 'Residencia Temporal', order: 21},
                    // {name: 'DEUDOR_RESIDENCIA_PERMANENTE', label: 'Residencia Permanente', order: 22}
                ]
            },
            {
                group: 'complementarios',
                label: 'Datos Complementarios: Domicilio Particular',
                order: 3,
                fields: [
                    {name: 'DEUDOR_CALLE', label: 'Calle', required: true, order: 23},
                    {name: 'DEUDOR_NUMERO_PUERTA', label: 'Nro.', required: true, order: 24},
                    {
                        name: 'DEUDOR_PISO_DEPARTAMENTO_CODIGO_POSTAL',
                        label: 'Piso-Depto-Cod.Postal',
                        order: 25
                    },
                    {name: 'DEUDOR_ENTRE_CALLES', label: 'Entre Calles', order: 26},
                    {
                        name: 'DEUDOR_PROVINCIA',
                        label: 'Provincia',
                        type: 'select',
                        enum: enums.ePROVINCIA,
                        order: 27
                    },
                    {name: 'DEUDOR_LOCALIDAD', label: 'Localidad', order: 28}
                    // {name: 'DEUDOR_DOMICILIO_PARTICULAR_ENTRE_CALLES', label: 'Entre Calles', order: 29}
                ]
            },
            {
                group: 'conyugue',
                label: 'Cónyuge/Conviviente',
                order: 4,
                fields: [
                    {name: 'DEUDOR_CONYUGE_NOMBRE', label: 'Nombre', order: 30},
                    {name: 'DEUDOR_CONYUGE_APELLIDO', label: 'Apellido', order: 31},
                    {
                        name: 'DEUDOR_CONYUGE_TIPO_DOCUMENTO',
                        label: 'Tipo Doc.',
                        type: 'select',
                        enum: enums.eID_TYPE,
                        own: 1,
                        order: 32
                    },
                    // {name: 'DEUDOR_CONYUGE_TIPO_DOCUMENTO_DNI', label: '', order: 50},
                    // {name: 'DEUDOR_CONYUGE_TIPO_DOCUMENTO_LC', label: '', order: 50},
                    // {name: 'DEUDOR_CONYUGE_TIPO_DOCUMENTO_LE', label: '', order: 50},
                    // {name: 'DEUDOR_CONYUGE_TIPO_DOCUMENTO_CI', label: '', order: 50},
                    // {name: 'DEUDOR_CONYUGE_TIPO_DOCUMENTO_PASAPORTE', label: '', order: 50},
                    {name: 'DEUDOR_CONYUGE_NRO_DOCUMENTO', label: 'Documento', order: 33},
                    {name: 'DEUDOR_CONYUGE_NACIMIENTO_MES', label: 'Mes Nacimiento', order: 34},
                    {
                        name: 'DEUDOR_CONYUGE_FECHA_NACIMIENTO',
                        label: 'Fecha Nac.',
                        type: 'date',
                        order: 35,
                        min: new Date().setFullYear(new Date().getFullYear() - 75),
                        max: new Date().setFullYear(new Date().getFullYear() - 18)
                    },
                    {
                        name: 'DEUDOR_CONYUGE_NACIONALIDAD',
                        label: 'Nacionalidad',
                        type: 'select',
                        enum: enums.eCOUNTRIES,
                        order: 36
                    },
                    {
                        name: 'DEUDOR_CONYUGE_TIPO',
                        label: 'CUIT-CUIL-CDI',
                        type: 'select',
                        enum: enums.eCUIL_TYPE,
                        own: 1,
                        order: 37
                    },
                    // {name: 'DEUDOR_CONYUGE_TIPO_CUIL', label: '', order: 50},
                    // {name: 'DEUDOR_CONYUGE_TIPO_CUIT', label: '', order: 50},
                    // {name: 'DEUDOR_CONYUGE_TIPO_CDI', label: '', order: 50},
                    {name: 'DEUDOR_CONYUGE_CUIL_CUIT_CDI_N', label: 'Número', order: 50}
                ]
            },
            {
                group: 'laborales',
                label: 'Datos Laborales Solicitante',
                order: 5,
                fields: [
                    {name: 'DEUDOR_ACT_PRINCIPAL', label: 'Rel.Dependencia', type: 'checkbox', order: 1},
                    {name: 'DEUDOR_ACT_PRINCIPAL1', label: 'Independiente', type: 'checkbox', order: 2},
                    {name: 'DEUDOR_ACT_PRINCIPAL2', label: 'Jubilado', type: 'checkbox', order: 3},
                    {name: 'DEUDOR_ACT_PRINCIPAL3', label: 'Estudiante', type: 'checkbox', order: 4},
                    {name: 'DEUDOR_ACT_PRINCIPAL4', label: 'Empleo Informal', type: 'checkbox', order: 5},
                    {name: 'DEUDOR_ACT_PRINCIPAL5', label: 'Ama de Casa', type: 'checkbox', order: 6},
                    {name: 'DEUDOR_ACT_PRINCIPAL6', label: 'Sin Trabajo', type: 'checkbox', order: 7},
                    {
                        name: 'DEUDOR_INGRESOS_NETOS_MENSUALES',
                        label: 'Ingresos Netos Mensuales',
                        type: 'text',
                        order: 8
                    }
                ]
            },
            {
                group: 'laboralesConyuge',
                label: 'Datos Laborales Conyuge/Conviviente',
                order: 6,
                fields: [
                    {
                        name: 'DEUDOR_CONYUGE_ACT_PRINCIPAL',
                        label: 'Rel.Dependencia',
                        type: 'checkbox',
                        order: 1
                    },
                    {
                        name: 'DEUDOR_CONYUGE_ACT_PRINCIPAL1',
                        label: 'Independiente',
                        type: 'checkbox',
                        order: 2
                    },
                    {name: 'DEUDOR_CONYUGE_ACT_PRINCIPAL2', label: 'Jubilado', type: 'checkbox', order: 3},
                    {name: 'DEUDOR_CONYUGE_ACT_PRINCIPAL3', label: 'Estudiante', type: 'checkbox', order: 4},
                    {
                        name: 'DEUDOR_CONYUGE_ACT_PRINCIPAL4',
                        label: 'Empleo Informal',
                        type: 'checkbox',
                        order: 5
                    },
                    {name: 'DEUDOR_CONYUGE_ACT_PRINCIPAL5', label: 'Ama de Casa', type: 'checkbox', order: 6},
                    {name: 'DEUDOR_CONYUGE_ACT_PRINCIPAL6', label: 'Sin Trabajo', type: 'checkbox', order: 7},
                    {
                        name: 'DEUDOR_CONYUGE_INGRESOS_NETOS_MENSUALES',
                        label: 'Ingresos Netos Mensuales',
                        order: 8
                    }
                ]
            },
            {
                group: 'referencias',
                label: 'Referencias',
                order: 7,
                fields: [
                    {name: 'DEUDOR_REFERENCIA_1_NOMBRE', label: 'Nombre 1', order: 1},
                    {name: 'DEUDOR_REFERENCIA_1_APELLIDO', label: 'Apellido 1', order: 2},
                    {name: 'DEUDOR_REFERENCIA_1_TELEFONO', label: 'Teléfono 1', order: 3},
                    {name: 'DEUDOR_REFERENCIA_2_NOMBRE', label: 'Nombre 2', order: 4},
                    {name: 'DEUDOR_REFERENCIA_2_APELLIDO', label: 'Apellido 2', order: 5},
                    {name: 'DEUDOR_REFERENCIA_2_TELEFONO', label: 'Teléfono 2', order: 6},
                    {name: 'DEUDOR_REFERENCIA_3_NOMBRE', label: 'Nombre 3', order: 7},
                    {name: 'DEUDOR_REFERENCIA_3_APELLIDO', label: 'Apellido 3', order: 50},
                    {name: 'DEUDOR_REFERENCIA_3_TELEFONO', label: 'Teléfono 3', order: 50}
                ]
            },
            {
                group: 'prestamo',
                label: 'Prestamo Prendario',
                order: 8,
                fields: [
                    {
                        name: 'PRODUCTO_CODIGO',
                        label: 'Código Producto',
                        disabled: true,
                        type: 'select',
                        enum: enums.ePRODUCTO,
                        order: 1
                    },
                    {
                        name: 'PRODUCTO_MONEDA',
                        label: 'Moneda',
                        disabled: true,
                        type: 'select',
                        enum: enums.eMONEDA,
                        order: 2
                    },
                    {name: 'PRODUCTO_MONTO', label: 'Monto', disabled: true, order: 3},
                    {name: 'PRODUCTO_CFT', label: 'CFT', disabled: true, order: 4},
                    {name: 'PRODUCTO_TNA', label: 'TNA', disabled: true, order: 5},
                    {name: 'PRODUCTO_TEA', label: 'TEA', disabled: true, order: 6},
                    {name: 'PRODUCTO_PLAZO', label: 'Plazo', disabled: true, order: 7},
                    {
                        name: 'PRODUCTO_FECHA_PAGO',
                        label: 'Fecha pago 1ra Cuota',
                        disabled: true,
                        type: 'text',
                        order: 8
                    },
                    {
                        name: 'PRODUCTO_MOTIVO_PRESTAMO',
                        label: 'Motivo Préstamo',
                        type: 'radio',
                        enum: enums.eMOTIVO_PRESTAMO,
                        order: 9,
                        own: 1
                    }
                    // {name: 'PRODUCTO_MOTIVO_PRESTAMO_ADQUISICION', label: '', order: 50},
                    // {name: 'PRODUCTO_MOTIVO_PRESTAMO_GARANTIA', label: '', order: 50},
                ]
            },
            {
                group: 'seguro',
                label: 'Seguro del Automotor',
                order: 9,
                fields: [
                    {name: 'PRODUCTO_SEGURO_COMPANIA', label: 'Compañia', order: 1},
                    {name: 'PRODUCTO_SEGURO_COBERTURA', label: 'Cobertura', order: 2},
                    {name: 'PRODUCTO_SEGURO_COSTO', label: 'Costo', order: 3}
                ]
            },
            {
                group: 'automotor',
                label: 'Datos del Vehículo',
                order: 10,
                fields: [
                    {name: 'AUTOMOTOR_0KM', label: '0KM', type: 'checkbox', order: 1},
                    {name: 'AUTOMOTOR_ANO', label: 'Año', order: 2},
                    {name: 'AUTOMOTOR_POSEE_GNC', label: 'GNC', type: 'checkbox', order: 3},
                    {name: 'AUTOMOTOR_MARCA', label: 'Marca', order: 4},
                    {name: 'AUTOMOTOR_MODELO', label: 'Modelo', order: 5},
                    {name: 'AUTOMOTOR_VALOR', label: 'Valor', order: 6},
                    {
                        name: 'AUTOMOTOR_USO',
                        label: 'Uso',
                        type: 'select',
                        enum: enums.eVEHICULO_USO_DECLARADO,
                        order: 7
                    },
                    {name: 'AUTOMOTOR_PORCENTAJE_FINANCIACION', label: 'Financiación %', order: 8}
                ]
            }
        ];
    }
}
export default new Fields();
