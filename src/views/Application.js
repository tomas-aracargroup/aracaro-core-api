import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Select} from 'antd';
import EditableCard from '../components/EditableCard';
import cards from './dealers/applications/applicationFields';
import NotesCard from '../components/NotesCard';
import {getCities, getProvinces} from '../api/dealers';
import {getByKey} from '../helpers/index';
import getStatus from '../helpers/getStatusCellClass';
import validator from '../helpers/validator';
import PrinterButton from './PrinterButton';
import RawDataCard from '../components/RawDataCard';
import {isEmpty} from 'lodash';
import {eAPP_STATUS} from '../constants/enumerators';
import {getAgeWithParenthesis} from '../helpers/index';
import {post} from '../api/index';
import DocumentationCard from '../components/DocumentationCard';

const filesList = [
    {key: 'fileDocumentFront', label: 'DNI Frente'},
    {key: 'fileDocumentBack', label: 'DNI Reverso'},
    {key: 'fileMarriageCertificate', label: 'Libreta de Matrimonio:'},
    {key: 'fileCarTitle', label: 'Título del Auto'},
    {key: 'fileApprovalLetter', label: 'Carta de Aprobación'},
    {key: 'fileSignedApplication', label: 'Solicitud Firmada'},
    {key: 'fileOther1', label: 'Otro Archivo (1)'},
    {key: 'fileOther2', label: 'Otro Archivo (2)'}
];

const Option = Select.Option;

class DealersApplication extends Component {
    state = {
        localSubmmited: false,
        cardsState: cards,
        users: [],
        fieldsToValidate: cards[0].fields
    };

    constructor(props) {
        super(props);

        post('queries/users', {sortBy: {lastname: 1}}).then(data => {
            this.setState({
                users: data.collection.map(x => ({
                    _id: x._id,
                    firstName: x.firstname,
                    lastName: x.lastname
                }))
            });
        });

        getProvinces().then(provinces => {
            cards
                .filter(x => x.title === 'Cliente')[0]
                .fields.filter(x => x.name === 'addressProvince')[0].enum = provinces;
            this.setState({cardsState: cards});
        });
        if (props.entity['addressProvince']) {
            getCities(props.entity['addressProvince']).then(x => {
                cards.filter(x => x.title === 'Cliente')[0].fields.filter(x => x.name === 'addressCity')[0].enum = x;
                this.setState({cardsState: cards, fetchingCities: false, cities: x});
            });
        }
    }

    onChange = (name, value) => {
        const {onChange, entity} = this.props;
        onChange(name, value);
        const filteredCards = cards.filter(x => x.title === 'Cliente');

        if (name === 'addressProvince') {
            onChange('addressCity', '');
            filteredCards[0].fields.filter(x => x.name === 'addressCity')[0].enum = [
                {id: 'fetching', name: 'Buscando Ciudades...'}
            ];
            this.setState({cardsState: cards});

            getCities(entity['addressProvince']).then(x => {
                filteredCards[0].fields.filter(x => x.name === 'addressCity')[0].enum = x;
                this.setState({cities: x, cardsState: cards});
            });
        } else if (name === 'addressCity') {
            const cityItem = getByKey(this.state.cities, value, 'id');
            this.onChange('addressZipCode', cityItem ? cityItem.zipCode : '');
        }
    };

    onSubmit = () => {
        const {handleSubmit, entity} = this.props;
        this.setState({localSubmmited: true});
        if (isEmpty(validator(entity, cards[0].fields))) {
            this.setState({localSubmmited: false});
            handleSubmit();
        }
    };

    render() {
        const {cardsState, fieldsToValidate, localSubmmited} = this.state;
        const {entity, submmited, onAddNote, entityName, handleChangeStatus} = this.props;

        let errors = {};

        if (submmited || localSubmmited) errors = validator(entity, fieldsToValidate);

        return (
            <div>
                <div className="loan-header">
                    <div className="loan-header__client">
                        <h2 className="loan-header__title">
                            <Link to={`/grids/${entityName}`}>
                                Aplicaciones {entityName === 'online-application' ? 'Online' : 'Dealers'}
                            </Link>{' '}
                            {' / '}
                            {entity['firstName']} {entity['lastName']} {getAgeWithParenthesis(entity['dob'])}{' '}
                        </h2>
                        <Select
                            value={entity.status}
                            style={{minWidth: 400}}
                            onChange={handleChangeStatus}
                            className={`status ${getStatus(entity.status)}`}>
                            {eAPP_STATUS.map(i => (
                                <Option key={i.id} value={i.id}>
                                    {i.id}
                                </Option>
                            ))}
                        </Select>
                    </div>
                    <div className="loan-header__buttons">
                        <PrinterButton entity={entity} />
                    </div>
                </div>
                <div className="card-flex-wrapper">
                    {cardsState.map(card => (
                        <EditableCard
                            key={card.title}
                            fields={card.fields}
                            readOnly={card.readOnly}
                            entity={entity}
                            onExitEditMode={this.onSubmit}
                            onChange={this.onChange}
                            className={`${entityName === 'online-application' ? 'online' : 'dealers'} m-3 p-1`}
                            errors={errors}
                            submmited={submmited || localSubmmited}
                            title={card.title}
                        />
                    ))}
                    <DocumentationCard
                        modelName={'application'}
                        entityId={entity._id}
                        title="Documentación"
                        filesList={filesList}
                    />

                    <NotesCard title="Notas" notes={entity.notes} onAddNote={onAddNote} className="m-3 p-1" />
                    <RawDataCard entity={entity} />
                </div>
            </div>
        );
    }
}

export default DealersApplication;
