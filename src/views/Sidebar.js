import React, {Component} from 'react';
import {Layout, Menu, Icon} from 'antd';
import {withRouter} from 'react-router';
import {includes} from '../helpers';

const SubMenu = Menu.SubMenu;

const {Sider} = Layout;

const prendaRapidaTasks = [
    {key: 'grids/application', icon: 'exception', label: 'Aplicaciones'},
    {key: 'grids/lead', icon: 'folder-open', label: 'Leads'},
    {key: 'grids/session', icon: 'fork', label: 'Sesiones'}
];

const dealersTasks = [
    {key: 'grids/company', icon: 'shop', label: 'Empresas'},
    {key: 'grids/dealers-user', icon: 'user-add', label: 'Usuarios'},
    {key: 'grids/dealers-customer', icon: 'smile', label: 'Clientes'},
    {key: 'grids/dealers-application', icon: 'exception', label: 'Aplicaciones'},
    {key: 'summaries/dealers-application', icon: 'bar-chart', label: 'Reportes'}
];

const onlineTasks = [
    {key: 'grids/online-customer', icon: 'smile', label: 'Clientes'},
    {key: 'grids/online-application', icon: 'exception', label: 'Aplicaciones'},
    {key: 'grids/plan-de-ahorro', icon: 'wallet', label: 'Plan de Ahorro'},
    {key: 'summaries/online-application', icon: 'bar-chart', label: 'Reportes'}
];

const rtoTasks = [
    {key: 'grids/rto', icon: 'dingding', label: 'Rent To Own'},
    {key: 'summaries/rtos', icon: 'bar-chart', label: 'Reportes'}
];

const configTasks = [
    {key: 'grids/channel', icon: 'shop', label: 'Canales'},
    {key: 'grids/user', icon: 'user-add', label: 'Usuarios'},
    {key: 'grids/config', icon: 'setting', label: 'Configuración'},
    {key: 'mails/subscriptions', icon: 'mail', label: 'Subscripción Mail'},
    {key: 'grids/besmart', icon: 'eye', label: 'Be Smart Log'},
    {key: 'carga-masiva', icon: 'database', label: 'Carga Masiva'}
];

class Sidebar extends Component {
    state = {
        collapsed: false,
        selectedKeys: []
    };

    constructor(props) {
        super();
        props.history.listen(location => {
            const pathname = location.pathname;
            this.setState({selectedKeys: [pathname]});
        });
    }

    onCollapse = collapsed => this.setState({collapsed});

    onClick = key => this.props.history.push(`${key.key}`);

    render() {
        const {app} = this.props;
        const {selectedKeys} = this.state;
        const userTasks = app.auth.user.tasks;
        const resolveTask = tasksList => {
            const response = [];
            tasksList.forEach(item => {
                if (includes(userTasks, item.key)) {
                    response.push(
                        <Menu.Item key={`/${item.key}`}>
                            <Icon type={item.icon} />
                            <span>{item.label}</span>
                        </Menu.Item>
                    );
                }
            });
            return response;
        };

        const prendaRapidaUserTasks = resolveTask(prendaRapidaTasks);
        const dealersUserTasks = resolveTask(dealersTasks);
        const rtoUserTasks = resolveTask(rtoTasks);
        const onlineUserTasks = resolveTask(onlineTasks);
        const configUserTasks = resolveTask(configTasks);

        return (
            <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse} className="panel-sidebar">
                <Menu
                    theme="dark"
                    onClick={this.onClick}
                    selectedKeys={selectedKeys}
                    defaultSelectedKeys={[this.props.location.pathname]}
                    mode="inline">
                    {onlineTasks.length > 0 ? (
                        <SubMenu
                            key="online"
                            title={
                                <span>
                                    <Icon type="dribbble" style={{color: '#ca2486'}} />
                                    <span>Online</span>
                                </span>
                            }>
                            {onlineUserTasks}
                        </SubMenu>
                    ) : null}
                    {dealersUserTasks.length > 0 ? (
                        <SubMenu
                            key="dealers"
                            title={
                                <span>
                                    <Icon type="car" style={{color: '#52c41a'}} />
                                    <span>Dealers</span>
                                </span>
                            }>
                            {dealersUserTasks}
                        </SubMenu>
                    ) : null}

                    {rtoTasks.length > 0 ? (
                        <SubMenu
                            key="rto"
                            title={
                                <span>
                                    <Icon type="dingding" style={{color: '#c49a1a'}} />
                                    <span>Rent To Own</span>
                                </span>
                            }>
                            {rtoUserTasks}
                        </SubMenu>
                    ) : null}

                    {[{key: 'calc', icon: 'calculator', label: 'Calculadora'}].map(item => (
                        <Menu.Item key={`/${item.key}`}>
                            <Icon type={item.icon} style={{color: '#009688'}} />
                            <span>{item.label}</span>
                        </Menu.Item>
                    ))}

                    {configUserTasks.length > 0 ? (
                        <SubMenu
                            key="config"
                            title={
                                <span>
                                    <Icon type="setting" />
                                    <span>Config</span>
                                </span>
                            }>
                            {configUserTasks}
                        </SubMenu>
                    ) : null}
                    {prendaRapidaUserTasks.length > 0 ? (
                        <SubMenu
                            key="prenda-rapida"
                            title={
                                <span>
                                    <Icon type="user" style={{color: '#ca2486'}} />
                                    <span>Prenda Rapida Legacy</span>
                                </span>
                            }>
                            {prendaRapidaUserTasks}
                        </SubMenu>
                    ) : null}
                    <Menu.Item key={`/ayuda`}>
                        <Icon type={'question-circle'} />
                        <span>Ayuda</span>
                    </Menu.Item>
                </Menu>
            </Sider>
        );
    }
}

export default withRouter(Sidebar);
