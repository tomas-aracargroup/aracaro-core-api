import React, {Component} from 'react';
import {Col, Form, Button, Icon, Modal} from 'antd';
import axios from 'axios';
import {isEmpty} from 'lodash';
import server from '../../constants/server';
import {externalUserFields, officeUserFields} from '../fields/leadForm';
import fieldResolver from '../../components/Fields/fieldResolver';
import {getMarcas} from '../../api/marcas';
import validateForm from '../../helpers/validator';
import {connect} from 'react-redux';

const defaultLayout = {sm: {span: 24}, md: {span: 12}, xl: {span: 12}};

class LeadForm extends Component {
    state = {
        entity: {genero: 'M', es0KM: false},
        submitting: false,
        submitted: false,
        beSmartResult: '',
        showModal: false,
        submitProgress: 0,
        marcaOptions: [{value: 'Loading...', label: 'Loading...'}],
        hideVehicleIsNew: true
    };

    constructor() {
        super();
        getMarcas.then(marcaOptions => {
            const {location} = this.props;
            const clonedApplication =
                !isEmpty(location.state) && !isEmpty(location.state.clonedApplication)
                    ? location.state.clonedApplication
                    : undefined;
            this.setState({marcaOptions});
            if (
                !isEmpty(clonedApplication) &&
                clonedApplication.anio &&
                clonedApplication.marca &&
                clonedApplication.modelo
            ) {
                this.onCloneCascader(
                    [clonedApplication.anio, clonedApplication.marca, clonedApplication.modelo],
                    marcaOptions
                );
                if (clonedApplication.anio === new Date().getFullYear().toString()) {
                    this.setState({hideVehicleIsNew: false});
                }
            }
        });
    }

    componentWillMount() {
        const {location} = this.props;
        const clonedApplication =
            location.state && location.state.clonedApplication ? location.state.clonedApplication : undefined;

        if (clonedApplication) {
            this.setState({entity: clonedApplication});
        }
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    onChangeCascader = (e, selectedOptions) => {
        let carValue = 0,
            carCodia = 0,
            carAge = 0;

        if (selectedOptions.length > 0) {
            carValue = selectedOptions[selectedOptions.length - 1].valor * 1000;
            carCodia = selectedOptions[selectedOptions.length - 1].codia;
            carAge = new Date().getFullYear() - parseInt(e[0], 10);
        }

        this.onChange('anio', e[0]);
        this.onChange('marca', e[1]);
        this.onChange('modelo', e[2]);
        this.onChange('vehiculoValor', carValue);
        this.onChange('vehiculoAntiguedad', carAge);
        this.onChange('kilometros', carAge * 10000);
        this.onChange('codia', carCodia);

        if (e[0] === new Date().getFullYear().toString()) {
            this.setState({hideVehicleIsNew: false});
            this.onChange('es0KM', true);
        } else {
            this.setState({hideVehicleIsNew: true});
            this.onChange('es0KM', false);
        }
    };

    onCloneCascader = clonedAnioMarcaModeloArray => {
        this.onChange('anio', clonedAnioMarcaModeloArray[0]);
        this.onChange('marca', clonedAnioMarcaModeloArray[1]);
        this.onChange('modelo', clonedAnioMarcaModeloArray[2]);
    };

    handleOk = () => this.setState({showModal: false});

    onSubmit = async () => {
        let interval;
        const {entity} = this.state;
        const {app, history} = this.props;
        const user = app.auth.user;
        const salesPerson = user.firstname + ' ' + user.lastname;

        const fields = user.role === 'EXTERNO' ? externalUserFields : officeUserFields;

        const {email} = user;
        if (!entity['user'] || entity['user'] === '') {
            entity['salesPerson'] = salesPerson;
            entity['user'] = email;
        }

        const onfulfilled = response => {
            const beSmartResult = response.data;
            this.setState({submitting: false, beSmartResult: beSmartResult});
            clearInterval(interval);
            history.push(`/leads/${beSmartResult._id}`);
        };

        //Mark form as submitted.
        this.setState({submitted: true});

        let errors = validateForm(entity, fields);

        const canalOriginacion = user.role === 'EXTERNO' ? user.channel.type : entity.canalOriginacion;

        if (isEmpty(errors)) {
            this.setState({submitting: true, showModal: true});

            try {
                interval = setInterval(() => {
                    this.setState({submitProgress: this.state.submitProgress + 1});
                }, 100);
                onfulfilled(await axios.post(`${server}/besmart`, {...entity, canalOriginacion}));
            } catch (e) {
                this.setState({apiError: 'API ERROR'});
            }
        }
    };

    render() {
        const {
            entity,
            submitting,
            submitted,
            showModal,
            beSmartResult,
            submitProgress,
            marcaOptions,
            hideVehicleIsNew
        } = this.state;
        const {app} = this.props;

        let puntos = '';
        for (let i = 0; i < submitProgress; i++) {
            if (i % 50 === 0) {
                puntos = '';
            }
            puntos = puntos + '.';
        }

        const s = submitting ? 'Conectando' : beSmartResult.DICTAMEN === 'Error' ? 'Error' : 'Resultado';
        const iconConectando = <Icon type="loading" />;
        const iconResultado = <Icon type="check-circle" />;
        const iconError = <Icon type="cross-circle" />;
        const icon = submitting ? iconConectando : beSmartResult.DICTAMEN === 'Error' ? iconError : iconResultado;
        const fields = app.auth.user.role === 'EXTERNO' ? externalUserFields : officeUserFields;
        const validation = validateForm(entity, fields);

        let cascaderDefaultValue =
            entity && entity.anio && entity.marca && entity.modelo ? [entity.anio, entity.marca, entity.modelo] : [];

        return (
            <Form className="querybuilder">
                <Modal
                    className="mquerybuilder"
                    title={
                        <div>
                            {icon} {s}
                        </div>
                    }
                    visible={showModal}
                    iconType={'cross-circle'}
                    closable
                    footer={null}>
                    {submitting && <strong className="search">Evaluando perfil crediticio {puntos}</strong>}
                </Modal>
                <Form.Item>
                    <Button loading={submitting} type="primary" onClick={this.onSubmit}>
                        Consultar
                    </Button>
                </Form.Item>
                <fieldset>
                    {fields.map(x => {
                        const Input = fieldResolver(x);
                        let options = x.enum;
                        if (x.type === 'cascader') options = marcaOptions;
                        return (
                            <Col
                                {...defaultLayout}
                                style={x.name === 'es0KM' && hideVehicleIsNew ? {display: 'none'} : {display: 'block'}}
                                key={x.name}>
                                <Input
                                    required={x.required}
                                    onChange={this.onChange}
                                    onChangeCascader={this.onChangeCascader}
                                    cascaderDefaultValue={cascaderDefaultValue}
                                    options={options}
                                    entity={entity}
                                    min={x.min}
                                    max={x.max}
                                    name={x.name}
                                    label={x.label}
                                    placeholder={x.placeholder}
                                    type={x.type}
                                    error={validation[x.name]}
                                    submitted={submitted}
                                />
                            </Col>
                        );
                    })}
                </fieldset>
            </Form>
        );
    }
}

export default connect(x => ({app: x.app}))(LeadForm);
