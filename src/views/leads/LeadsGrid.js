import React from 'react';
import Grid from '../../components/Grid';

const resolveColor = (item, name) => {
    switch (item[name]) {
        case 'Rechazar':
        case 'Rechazo Condicional':
            return '#bb0000';

        case 'Preaprobar':
            return '#a0d911';

        case 'Verificar':
            return '#fa8c16';

        case 'Error':
            return '#5c0011';

        default:
            return 'initial';
    }
};

const columns = [
    {
        name: 'documento',
        label: 'Documento',
        cell: 'link',
        searchable: true,
        searchType: 'string',
        collectionName: 'leads'
    },
    {name: 'leadNumber', label: 'Nro.', searchable: true},
    {name: 'dictamen', label: 'Dictamen', cell: 'dictamen', searchable: true, searchType: 'string', resolveColor},
    {name: 'fechaDictamen', label: 'Fecha', searchable: true},
    {name: 'motivo', label: 'Motivo', searchable: true, searchType: 'string'},
    {name: 'explicacion', label: 'Explicación', searchable: true, searchType: 'string', style: {fontStyle: 'italic'}},
    {name: 'canal', label: 'Canal', searchable: true, searchType: 'string'},
    {name: 'subproducto', label: 'Subproducto', searchable: true, searchType: 'string'},
    {name: 'riesgoNivel', label: 'Nivel Riesgo', searchable: true, searchType: 'string'},
    {name: 'nombre', label: 'Nombre', searchable: true},
    {name: 'apellido', label: 'Apellido', searchable: true},
    {name: 'producto', label: 'Producto', searchable: true},
    {name: 'telefono', label: 'Telefono', searchable: true},
    {name: 'email', label: 'Mail', searchable: true},
    {name: 'bscdPeorSituUlt', label: 'BCRA', searchable: true},
    {name: 'bscdPeorSituUlt2_3', label: 'BCRA 2_3', searchable: true},
    {name: 'bscdPeorSituUlt4_6', label: 'BCRA 4_6', searchable: true},
    {name: 'bscdPeorSituUlt7_12', label: 'BCRA 7_12', searchable: true},
    {name: 'ciBancarizado', label: 'Bancarizado', searchable: true},
    {name: 'vehiculoMarcaModeloVersion', label: 'Vehiculo Modelo'},
    {name: 'viDomAfProv', label: 'Provincia', searchable: true},
    {name: 'situacionLaboralFinal', label: 'Situación Laboral'},
    {name: 'vehiculoUsoDeclarado', label: 'Vehiculo Uso'},
    {name: 'coefTasa', label: 'Sobre Tasa', searchable: false, cell: 'percentage', searchType: 'number'},
    {name: 'ltvFinal', label: 'LTV', searchable: false, cell: 'percentage', searchType: 'number'},
    {name: 'rci', label: 'RCI', searchable: false, cell: 'percentage', searchType: 'number'},
    {name: 'siScore', label: 'SIISA', searchable: true, cell: 'number', searchType: 'number'},
    {name: 'plazoMaxAntig', label: 'Plazo Maximo', searchable: false, cell: 'number', searchType: 'number'},
    {name: 'monto', label: 'Monto', searchable: false, cell: 'money'},
    {name: 'vehiculoAntiguedad', label: 'Antiguedad Vehiculo', searchable: false, cell: 'number'},
    {name: 'vehiculoKm', label: 'Vehiculo KMs', searchable: false, cell: 'number'},
    {name: 'ingresoEstimado', label: 'Ingreso Estimado', searchable: false, cell: 'money'},
    {name: 'ingresoComputado', label: 'Ingreso Computado', searchable: false, cell: 'money'},
    {name: 'ingresoVerificado', label: 'Ingreso Verificado', searchable: false, cell: 'money'},
    {name: 'vehiculoValor', label: 'Valor Vehiculo', searchable: false, cell: 'money'},
    {name: 'cuotaMax', label: 'Cuota Maxima', searchable: false, cell: 'money'},
    {name: 'capitalMaximo', label: 'Monto Maximo', searchable: false, cell: 'money'},
    {name: 'ciVigCompMensual', label: 'Comp. Mensuales', searchable: false, cell: 'money'},
    {name: 'scoVig', label: 'Nosis', cell: 'number', searchable: true, searchType: 'number'},
    {name: 'user', label: 'Usuario', searchable: true}
];

export default () => (
    <Grid
        className="leads"
        title={'Prenda Rapida / Leads '}
        withAdd={false}
        columns={columns}
        columnPicker
        downloadDealers
        createRoute="/leads/builder"
        collectionName="leads"
        endPoint="leads"
        sortBy={{fechaDictamen: -1}}
    />
);
