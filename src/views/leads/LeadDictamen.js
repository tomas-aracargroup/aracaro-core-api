import React from 'react';
import groups from './camposDictamen';
import {Card, Button} from 'antd';
import moment from 'moment';
import {withRouter} from 'react-router';
import 'moment/locale/es';
import LoanHeader from '../application/LoanHeader';
import format from '../../helpers/format';
import {cloneApplication} from '../../helpers/cloneApplication';

moment.locale('es');

class LeadDictamen extends React.Component {
    state = {showRaw: false};

    toggleRawMode = () => this.setState({showRaw: !this.state.showRaw});

    cloneApplication = () => {
        const {entity, history} = this.props;
        history.push({pathname: '/leads/builder/', state: {clonedApplication: cloneApplication(entity)}});
    };

    render() {
        const {showRaw} = this.state;
        const {entity, app, onGenerateClick} = this.props;
        const extUser = app.auth.user.role === 'EXTERNO';

        const enabledGroups = extUser
            ? ['solicitud', 'resultado2']
            : ['solicitud', 'resultado', 'riesgo', 'ingresos', 'rt', 'dp'];

        if (!entity) return null;

        const tdStyle = {textAlign: 'right', fontWeight: 'bold', paddingRight: '10px'};
        let getField = field => (
            <tr key={field.id}>
                <td style={tdStyle}>{field.label}:</td>
                <td>{format(entity[field.id], field.type)}</td>
            </tr>
        );

        const pretty = groups.map(
            group =>
                enabledGroups.includes(group.id) ? (
                    <Card key={group.title} title={group.title} className={'lead'}>
                        <table>
                            <tbody>{group.fields.map(getField)}</tbody>
                        </table>
                    </Card>
                ) : null
        );

        const raw = (
            <Card title={<strong>Dictamen Completo</strong>}>
                <table>
                    <tbody>
                        {Object.keys(entity).map(key => (
                            <tr key={key}>
                                <td style={tdStyle}>{key}:</td>
                                <td>{entity[key]}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </Card>
        );

        Object.assign(entity, {firstName: entity.nombre, lastName: entity.apellido, dob: entity.siXtsFnac});

        return (
            <div>
                <LoanHeader
                    title="Dictamen"
                    entity={entity}
                    handlePrevious={() => this.props.history.goBack()}
                    handleNext={entity.applicationId ? onGenerateClick : null}
                    cloneApplication={this.cloneApplication}
                />
                {/* <Card title={title} className={'lead-wrapper'}> */}
                <div className="lead-wrapper">{showRaw ? raw : pretty}</div>
                {/* </Card> */}
                {!extUser ? (
                    <Button
                        type="primary"
                        style={{marginLeft: '2rem'}}
                        className="ant-btn-lg"
                        onClick={this.toggleRawMode}>
                        {showRaw ? 'Ver normal' : 'Ver crudo'}
                    </Button>
                ) : null}
            </div>
        );
    }
}

export default withRouter(LeadDictamen);
