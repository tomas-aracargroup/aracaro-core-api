const solicitud = [
    {label: 'Fecha', id: 'solicitudFecha', type: 'date'},
    {label: 'Monto Solicitado', id: 'monto', type: 'money'},
    {label: 'Producto', id: 'producto'},
    {label: 'Vehiculo Antigüedad ', id: 'vehiculoAntiguedad'},
    {label: 'Vehiculo Modelo', id: 'vehiculoMarcaModeloVersion'},
    {label: 'Vehiculo Valor', id: 'vehiculoValor', type: 'money'}
];

const resultado = [
    {label: 'Dictamen', id: 'dictamen'},
    {label: 'Motivo', id: 'motivo'},
    {label: 'Explicación', id: 'explicacion'},
    {label: 'Sobre Tasa', id: 'coefTasa', type: 'percentage'},
    {label: 'Cuota Máxima', id: 'cuotaMax', type: 'money'},
    {label: 'Monto Máximo', id: 'capitalMaximo', type: 'money'},
    {label: 'Plazo Maximo', id: 'plazoMaxAntig'}
];

const resultado2 = [
    {label: 'Dictamen', id: 'dictamen'},
    {label: 'Motivo', id: 'motivo'},
    {label: 'Explicación', id: 'explicacion'},
    {label: 'Cuota Máxima', id: 'cuotaMax', type: 'money'},
    {label: 'Monto Máximo', id: 'capitalMaximo', type: 'money'},
    {label: 'Plazo Maximo', id: 'plazoMaxAntig'},
    {label: 'RCI', id: 'rci', type: 'percentage'},
    {label: 'LTV', id: 'ltvFinal', type: 'percentage'}
];

const riesgo = [
    {label: 'Ingreso Computado', id: 'ingresoComputado', type: 'money'},
    {label: 'Compromisos Mensuales', id: 'ciVigCompMensual', type: 'money'},
    {label: 'Nivel de Riesgo', id: 'riesgoNivel'},
    {label: 'Bancarizado', id: 'ciBancarizado'},
    {label: 'RCI', id: 'rci'},
    {id: 'bscdPeorSituUlt', label: 'BCRA'},
    {id: 'bscdPeorSituUlt2_3', label: 'BCRA 2_3'},
    {id: 'bscdPeorSituUlt4_6', label: 'BCRA 4_6'},
    {id: 'bscdPeorSituUlt7_12', label: 'BCRA 7_12'},
    {label: 'Nosis', id: 'scoVig'},
    {label: 'Siisa', id: 'siScore'},
    {label: 'Situación Laboral', id: 'situacionLaboralFinal'},
    {label: 'Subproducto', id: 'subproducto'}
];

const ingresos = [
    {label: 'Ingreso Estimado', id: 'ingresoEstimado', type: 'money'},
    {label: 'Ingreso Verificado', id: 'ingresoVerificado', type: 'money'},
    {label: 'Ingreso Declarado', id: 'ingresoDeclarado', type: 'money'},
    {label: 'Ingreso Computado', id: 'ingresoComputado', type: 'money'}
];

const rt = [{label: 'LTV', id: 'ltvFinal', type: 'percentage'}, {label: 'RCI', id: 'rci', type: 'percentage'}];

const dp = [{label: 'Mail', id: 'email'}, {label: 'Telefono', id: 'telefono'}];

export default [
    {fields: solicitud, title: 'Solicitud', id: 'solicitud'},
    {fields: resultado, title: 'Resultado', id: 'resultado'},
    {fields: resultado2, title: 'Resultado', id: 'resultado2'},
    {fields: riesgo, title: 'Analisis de Riesgo', id: 'riesgo'},
    {fields: ingresos, title: 'Ingresos', id: 'ingresos'},
    {fields: rt, title: 'Relaciones Técnicas', id: 'rt'},
    {fields: dp, title: 'Datos Personales', id: 'dp'}
];
