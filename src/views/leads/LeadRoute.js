import React from 'react';
import LeadDictamen from './LeadDictamen';
import {connect} from 'react-redux';
import {getByID, post} from '../../api/index';
import {Switch, Route} from 'react-router-dom';
import {isEmpty} from 'lodash';

class Lead extends React.Component {
    state = {
        config: {}
    };

    constructor(props) {
        super();

        post(`queries/config`, {
            sortBy: {updatedAt: -1},
            limit: 1
        }).then(response => this.setState({config: response.collection[0]}));
        getByID('leads', props.match.params.id).then(response => this.setState({entity: response}));
    }

    handleNext = route => {
        const {history, match} = this.props;
        history.push(`${match.url}/${route}`);
    };

    onGenerateClick = () => {
        const {entity} = this.state;
        const {history} = this.props;

        if (entity.applicationId) {
            history.push(`/applications/${entity.applicationId}/loan-data`);
        } else {
            post('application', entity).then(data => {
                history.push('/applications/' + data._id + '/loan-data');
            });
        }
    };

    handlePrevious = () => {
        const {history} = this.props;
        history.goBack();
    };

    render() {
        const {config, entity} = this.state;
        const {match, app} = this.props;

        if (isEmpty(entity) || isEmpty(config)) {
            return null;
        }

        return (
            <Switch>
                <Route
                    exact
                    path={match.path + '/'}
                    render={() => (
                        <LeadDictamen
                            config={config}
                            entity={entity}
                            app={app}
                            onGenerateClick={this.onGenerateClick}
                        />
                    )}
                />
            </Switch>
        );
    }
}

const mapStateToProps = state => ({entities: state.entities, app: state.app});
export default connect(mapStateToProps)(Lead);
