import React, {Component} from 'react';
import {Button, notification, Menu, Dropdown, Icon} from 'antd';
import approvalLetter from './dealers/prints/approvalLetter';
import applicationLetter from './dealers/prints/applicationLetter';
import {form03} from './dealers/prints/form03';
import {contract} from './dealers/prints/contract';
import validator from '../helpers/validator';
import {isEmpty} from 'lodash';
import {
    applicationLetterFields,
    approbalLetterFields,
    form03Fields,
    contractFields
} from './dealers/applications/applicationFieldsPrinter';

class PrinterButton extends Component {
    handlePrint = key => {
        const {entity} = this.props;
        const keyNotification = `open${Date.now()}`;

        if (key.key === 'approval-letter') {
            const btnClickApproval = () => {
                notification.close(keyNotification);
                approvalLetter(entity);
            };

            let notificationMsg = 'Se descargará el formulario en formato pdf.';
            let notificationType = 'success';

            const errors = validator(entity, approbalLetterFields);
            this.setState({localSubmmited: true, fieldsToValidate: approbalLetterFields});
            if (!isEmpty(errors)) {
                notificationType = 'error';
                notificationMsg =
                    'Datos incompletos para la impresión de la Carta de Aprobación. ¿Imprimir de todas formas?';
            }
            notification[notificationType]({
                message: 'Imprimir Carta de Aprobación',
                description: notificationMsg,
                btn: (
                    <Button type="primary" size="small" onClick={() => btnClickApproval()}>
                        DESCARGAR
                    </Button>
                ),
                key: keyNotification,
                duration: 7
            });
        } else if (key.key === 'application-letter') {
            const btnClickApplication = () => {
                notification.close(keyNotification);
                applicationLetter(entity);
            };

            let notificationMsg = 'Se descargará el formulario en formato pdf.';
            let notificationType = 'success';

            const errors = validator(entity, applicationLetterFields);
            this.setState({localSubmmited: true, fieldsToValidate: applicationLetterFields});
            if (!isEmpty(errors)) {
                notificationType = 'error';
                notificationMsg =
                    'Datos incompletos para la impresión de la Solicitud de Préstamo. ¿Imprimir de todas formas?';
            }
            notification[notificationType]({
                message: 'Imprimir Solicitud de Préstamo',
                description: notificationMsg,
                btn: (
                    <Button type="primary" size="small" onClick={() => btnClickApplication()}>
                        DESCARGAR
                    </Button>
                ),
                key: keyNotification,
                duration: 7
            });
        } else if (key.key === 'form-03') {
            const btnClickForm03 = () => {
                notification.close(keyNotification);
                form03(entity)
                    .then(() => notification.success({message: 'Impresión Finalizada.', duration: 1.5}))
                    .catch(() => notification.error({message: 'Error en la Impresión.', duration: 3}));
            };

            let notificationMsg = 'Por favor revise la Bandeja Manual de su impresora y coloque el Formulario 03.',
                notificationType = 'success';

            const errors = validator(entity, form03Fields);
            this.setState({localSubmmited: true, fieldsToValidate: form03Fields});
            if (!isEmpty(errors)) {
                notificationType = 'error';
                notificationMsg = 'Datos incompletos para la impresión de la Prenda. ¿Imprimir de todas formas?';
            }
            notification[notificationType]({
                message: 'Imprimir Prenda',
                description: notificationMsg,
                btn: (
                    <Button type="primary" size="small" onClick={() => btnClickForm03(false)}>
                        IMPRIMIR
                    </Button>
                ),
                key: keyNotification,
                duration: 7
            });
        } else {
            const btnClickContract = () => {
                notification.close(keyNotification);
                contract(entity)
                    .then(x => notification.success({message: 'Impresión Finalizada.', duration: 1.5}))
                    .catch(err => notification.error({message: 'Error en la Impresión.', duration: 3}));
            };

            let notificationMsg =
                'Por favor revise la Bandeja Legal de su impresora y coloque el Formulario de Contrato.';
            let notificationType = 'success';

            this.setState({localSubmmited: true, fieldsToValidate: contractFields});
            const errors = validator(entity, contractFields);
            if (!isEmpty(errors)) {
                notificationType = 'error';
                notificationMsg = 'Datos incompletos para la impresión del Contrato. ¿Imprimir de todas formas?';
            }

            notification[notificationType]({
                message: 'Imprimir Contrato',
                description: notificationMsg,
                btn: (
                    <Button type="primary" size="small" onClick={() => btnClickContract()}>
                        IMPRIMIR
                    </Button>
                ),
                key: keyNotification,
                duration: 7
            });
        }
    };

    render() {
        return (
            <Dropdown
                overlay={
                    <Menu onClick={this.handlePrint}>
                        <Menu.Item key="approval-letter">CARTA APROBACIÓN</Menu.Item>
                        <Menu.Item key="application-letter">SOLICITUD DE PRÉSTAMO</Menu.Item>
                        <Menu.Item key="form-03">PRENDA</Menu.Item>
                        <Menu.Item key="contract">CONTRATO</Menu.Item>
                    </Menu>
                }>
                <Button className={'ml-2'}>
                    <Icon type="printer" />
                </Button>
            </Dropdown>
        );
    }
}

export default PrinterButton;
