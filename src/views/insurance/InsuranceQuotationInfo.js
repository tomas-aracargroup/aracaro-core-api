import React, {Component} from 'react';
import {Row, Col} from 'antd';
import {formatMoney} from '../../helpers/index';

class InsuranceQuotationInfo extends Component {
    selectInsurance = (onChange, id, company, cost, description) => {
        return () => {
            onChange('vehicleInsuranceId', id);
            onChange('vehicleInsuranceCompany', company);
            onChange('vehicleInsuranceCost', cost);
            onChange('vehicleInsuranceCoverageType', description);
        };
    };

    render() {
        const {quotation} = this.props;

        if (quotation) {
            return (
                <div className="insurance-details">
                    <p className="insurance-details__title">Detalle del Seguro</p>
                    <div className="clearfix">
                        <div className="insurance-details__left">
                            <div className="insurance-details__image">
                                <img src={quotation.ciaAseguradoraLogo} alt="Logo de la compañía de seguros" />
                            </div>
                            <div className="insurance-company__ID">
                                <p>
                                    <strong>Presupuesto ID:</strong>
                                </p>
                                <p>{quotation.id}</p>
                            </div>
                        </div>
                        <div className="insurance-details__middle">
                            <Row gutter={16}>
                                {quotation.detalleCobertura && (
                                    <div>
                                        <Col md={{offset: 0, span: 6}} lg={{offset: 0, span: 6}}>
                                            <p>
                                                <strong>Detalle Cobertura:</strong>
                                            </p>
                                        </Col>
                                        <Col md={{offset: 0, span: 18}} lg={{offset: 0, span: 18}}>
                                            <p>{quotation.detalleCobertura}</p>
                                        </Col>
                                    </div>
                                )}
                            </Row>
                            <Row gutter={16}>
                                {quotation.listCoberturas &&
                                    quotation.listCoberturas.length > 0 && (
                                        <div>
                                            <Col md={{offset: 0, span: 6}} lg={{offset: 0, span: 6}}>
                                                <p>
                                                    <strong>Lista de Coberturas:</strong>
                                                </p>
                                            </Col>
                                            <Col md={{offset: 0, span: 18}} lg={{offset: 0, span: 18}}>
                                                {quotation.listCoberturas.map((itemCobertura, key) => {
                                                    return <p key={key}>{itemCobertura}</p>;
                                                })}
                                            </Col>
                                        </div>
                                    )}
                            </Row>
                            <Row gutter={16}>
                                {quotation.moneda && (
                                    <div>
                                        <Col md={{offset: 0, span: 6}} lg={{offset: 0, span: 6}}>
                                            <p>
                                                <strong>Moneda:</strong>
                                            </p>
                                        </Col>
                                        <Col md={{offset: 0, span: 18}} lg={{offset: 0, span: 18}}>
                                            <p>{quotation.moneda}</p>
                                        </Col>
                                    </div>
                                )}
                            </Row>
                            <Row gutter={16}>
                                {quotation.premioAnual && (
                                    <div>
                                        <Col md={{offset: 0, span: 6}} lg={{offset: 0, span: 6}}>
                                            <p>
                                                <strong>Premio Anual:</strong>
                                            </p>
                                        </Col>
                                        <Col md={{offset: 0, span: 18}} lg={{offset: 0, span: 18}}>
                                            <p>{formatMoney(quotation.premioAnual)}</p>
                                        </Col>
                                    </div>
                                )}
                            </Row>
                            <Row gutter={16}>
                                {quotation.premioMensual && (
                                    <div>
                                        <Col md={{offset: 0, span: 6}} lg={{offset: 0, span: 6}}>
                                            <p>
                                                <strong>Premio Mensual:</strong>
                                            </p>
                                        </Col>
                                        <Col md={{offset: 0, span: 18}} lg={{offset: 0, span: 18}}>
                                            <p>{formatMoney(quotation.premioMensual)}</p>
                                        </Col>
                                    </div>
                                )}
                            </Row>
                            <Row gutter={16}>
                                {quotation.franquicia && (
                                    <div>
                                        <Col md={{offset: 0, span: 6}} lg={{offset: 0, span: 6}}>
                                            <p>
                                                <strong>Franquicia:</strong>
                                            </p>
                                        </Col>
                                        <Col md={{offset: 0, span: 18}} lg={{offset: 0, span: 18}}>
                                            <p>{quotation.franquicia}</p>
                                        </Col>
                                    </div>
                                )}
                            </Row>
                            <Row gutter={16}>
                                {quotation.valorAsegurado && (
                                    <div>
                                        <Col md={{offset: 0, span: 6}} lg={{offset: 0, span: 6}}>
                                            <p>
                                                <strong>Valor Asegurado:</strong>
                                            </p>
                                        </Col>
                                        <Col md={{offset: 0, span: 18}} lg={{offset: 0, span: 18}}>
                                            <p>{formatMoney(quotation.valorAsegurado)}</p>
                                        </Col>
                                    </div>
                                )}
                            </Row>
                        </div>
                        <div className="insurance-details__right">
                            <Row gutter={16}>
                                {quotation.granizo && (
                                    <div>
                                        <Col md={{offset: 0, span: 12}} lg={{offset: 0, span: 12}}>
                                            <p>
                                                <strong>Cobertura Adicional:</strong>
                                            </p>
                                        </Col>
                                        <Col md={{offset: 0, span: 12}} lg={{offset: 0, span: 12}}>
                                            <p>{'Granizo'}</p>
                                        </Col>
                                    </div>
                                )}
                            </Row>

                            <Row gutter={16}>
                                {quotation.granizo &&
                                    quotation.valorGranizo && (
                                        <div>
                                            <Col md={{offset: 0, span: 12}} lg={{offset: 0, span: 12}}>
                                                <p>
                                                    <strong>Valor Granizo:</strong>
                                                </p>
                                            </Col>
                                            <Col md={{offset: 0, span: 12}} lg={{offset: 0, span: 12}}>
                                                <p>{formatMoney(quotation.valorGranizo)}</p>
                                            </Col>
                                        </div>
                                    )}
                            </Row>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <div />;
        }
    }
}

export default InsuranceQuotationInfo;
