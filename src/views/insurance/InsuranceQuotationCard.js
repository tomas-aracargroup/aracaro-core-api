import React, {Component} from 'react';
import {Card, Col} from 'antd';
import {formatNumber} from '../../helpers/index';

class InsuranceQuotationCard extends Component {
    selectInsurance = (onChange, id, company, cost, description) => {
        return () => {
            onChange('vehicleInsuranceId', id);
            onChange('vehicleInsuranceCompany', company);
            onChange('vehicleInsuranceCost', cost);
            onChange('vehicleInsuranceCoverageType', description);
        };
    };

    render() {
        const {quotation, onChange, selected, layout} = this.props;
        const {id, ciaAseguradora, premioMensual, detalleCobertura, valorAsegurado} = quotation;

        return (
            <Col {...layout}>
                <Card
                    title={<strong>{ciaAseguradora}</strong>}
                    className={selected ? 'lead selected' : 'lead'}
                    onClick={this.selectInsurance(onChange, id, ciaAseguradora, premioMensual, detalleCobertura)}
                    style={{margin: '10px'}}>
                    <table style={{width: '100%'}}>
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Premio Mensual:</strong>
                                </td>
                                <td style={{textAlign: 'right'}}>$ {formatNumber(premioMensual)}</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Cobertura:</strong>
                                </td>
                                <td style={{textAlign: 'right'}}>{detalleCobertura}</td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Suma Asegurada:</strong>
                                </td>
                                <td style={{textAlign: 'right'}}>$ {formatNumber(valorAsegurado)}</td>
                            </tr>
                        </tbody>
                    </table>
                </Card>
            </Col>
        );
    }
}

export default InsuranceQuotationCard;
