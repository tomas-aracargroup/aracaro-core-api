import React, {Component} from 'react';
import InsuranceQuotationCard from './InsuranceQuotationCard';

class InsuranceQuotationCards extends Component {
    render() {
        const {entity, quotations, onChange, layout} = this.props;

        if (quotations.length === 0) {
            return (
                <p style={{width: '100%', textAlign: 'center', margin: '20px', fontWeight: 'bold'}}>
                    No hay cotizaciones de seguros disponibles.
                </p>
            );
        } else {
            return (
                <div className={'insurance-results'}>
                    {quotations.map(q => {
                        return (
                            <InsuranceQuotationCard
                                key={q.id}
                                quotation={q}
                                onChange={onChange}
                                selected={entity.vehicleInsuranceId === q.id}
                                layout={layout}
                            />
                        );
                    })}
                </div>
            );
        }
    }
}

export default InsuranceQuotationCards;
