import React, {Component} from 'react';
import {Spin, Button} from 'antd';
import {post} from '../../api/index';
import {withRouter} from 'react-router';
import InsuranceQuotationsTable from './InsuranceQuotationsTable';
import InsuranceQuotationInfo from './InsuranceQuotationInfo';
import moment from 'moment';
import {isEmpty} from 'lodash';

const setBirthDate = value => {
    let year, month, day;
    let parsedDate = value.split('/');
    day = parsedDate[0];
    month = parsedDate[1];
    year = parsedDate[2];
    let date = month + ',' + day + ',' + year;
    let time = new Date(date).getTime() + '-0300';
    return `/Date(${time})/`;
};

const defaultLayout = {sm: {span: 24}, md: {span: 12}, xl: {span: 12}};

class InsuranceAutoQuery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            vehicleInsuranceId: this.props.entity.vehicleInsuranceId,
            selectedQuotationInfo: '',
            errorMsg: '',
            iconLoading: false
        };

        if (!this.state.vehicleInsuranceId) {
            this.loadTableInfo(props.entity, props.onChange, false);
        }
    }

    loadTableInfo = async (entity, onChange, forceReload = false) => {
        if (!forceReload && !isEmpty(this.props.ultuTableObject)) {
            return;
        }
        let {
            vehicleYear,
            province,
            city,
            maritalStatus,
            gender,
            dob,
            vehicleCodia,
            zipCode,
            vehicleIsNew,
            vehicleHasNaturalGas
        } = entity;
        maritalStatus = maritalStatus !== 'CASADO' ? 'Soltero(a)' : 'Casado(a)';
        gender = gender === 'M' ? 'Masculino' : 'Femenino';
        dob = setBirthDate(moment(dob, 'YYYY-MM-DD').format('DD/MM/YYYY'));

        let payload;

        if (province.toLowerCase() === 'capital federal') {
            province = 'CAPITAL FEDERAL';
            city = 'CIUDAD AUTONOMA BUENOS AIRES';
        }
        payload = {
            vehicleYear: vehicleYear || '2014',
            vehicleCode: vehicleCodia || '320649',
            addressZipCode: zipCode || '1640',
            vehicleIsNew: vehicleIsNew || 'false',
            maritalStatus: maritalStatus,
            dob: dob,
            addressProvince: province || 'BUENOS AIRES',
            addressCity: city || 'MARTINEZ',
            gender: gender,
            vehicleHasNaturalGas: vehicleHasNaturalGas || 'false'
        };

        post('ultu/QuotationsTable', payload)
            .then(response => {
                this.props.onUltuTableLoaded(response);
                this.setState({iconLoading: false});
            })
            .catch(error => {
                this.setState({errorMsg: error.message});
                this.setState({iconLoading: false});
            });
    };

    loadQuotationInfo = quotation => {
        this.setState({selectedQuotationInfo: quotation});
    };

    reloadQuotations = (entity, onChange, forceReload = false) => {
        return () => {
            if (forceReload) {
                this.setState({iconLoading: true});
            }
            this.setState({vehicleInsuranceId: ''});
            this.loadTableInfo(entity, onChange, forceReload);
        };
    };

    render() {
        const {entity, onChange, ultuTableObject, submitted} = this.props;
        const {errorMsg} = this.state;
        const ultuQuotationsTable = !isEmpty(ultuTableObject) ? ultuTableObject.quotations : [];
        const ultuErrors = !isEmpty(ultuTableObject) ? ultuTableObject.errors : [];

        if (this.state.vehicleInsuranceId) {
            return (
                <div>
                    <p>
                        <strong>{'Id del Seguro: '}</strong>
                        {entity.vehicleInsuranceId}
                    </p>
                    <p>
                        <strong>{'Compañía: '}</strong>
                        {entity.vehicleInsuranceCompany}
                    </p>
                    <p>
                        <strong>{'Premio Mensual: '}</strong>
                        {entity.vehicleInsuranceCost}
                    </p>
                    {entity.vehicleInsuranceCoverageType && (
                        <p>
                            <strong>{'Cobertura: '}</strong>
                            {entity.vehicleInsuranceCoverageType}
                        </p>
                    )}
                    <div>
                        <Button type="primary" onClick={this.reloadQuotations(entity, onChange, false)}>
                            Buscar Cotizaciones
                        </Button>
                    </div>
                </div>
            );
        }

        if (errorMsg) {
            return <p style={{width: '100%', textAlign: 'center', margin: '20px', fontWeight: 'bold'}}>{errorMsg}</p>;
        }

        if (isEmpty(ultuTableObject)) {
            return (
                <div className="insurance-loading" style={{width: '100%', textAlign: 'center', marginTop: '5vw'}}>
                    <div className="insurance-spinner" style={{marginLeft: 'auto', marginRight: 'auto'}}>
                        <Spin size="large" />
                    </div>
                    <h2 className="insurance-loading-highlight">Consultando cotizaciones disponibles</h2>
                    <p>{entity.vehicleMake}</p>
                    <p>
                        <strong>{entity.vehicleModel}</strong>
                    </p>
                    <p>
                        <small>({entity.vehicleYear})</small>
                    </p>
                </div>
            );
        }

        if (!isEmpty(ultuTableObject) && ultuQuotationsTable.length === 0 && ultuErrors.length > 0) {
            return (
                <div>
                    {ultuErrors.map(errorDescription => {
                        return (
                            <p style={{width: '100%', textAlign: 'center', margin: '20px', fontWeight: 'bold'}}>
                                {errorDescription}
                            </p>
                        );
                    })}
                </div>
            );
        }

        let msgCSS = {textAlign: 'center'};
        if (submitted && !entity.vehicleInsuranceId) {
            msgCSS.color = 'red';
        }
        return (
            <div>
                <div style={msgCSS}>
                    <div className="insurance-known-info">
                        <ul>
                            <li>
                                <strong>Titular: </strong> {entity.firstName} {entity.lastName}
                            </li>
                            <li>
                                <strong>Marca/Modelo/Año:</strong> {entity.vehicleMake} {entity.vehicleModel}{' '}
                                {entity.vehicleYear}
                            </li>
                        </ul>
                    </div>
                </div>
                <p className="insurance-results__title">Seleccione alguna de las siguientes opciones de seguros:</p>
                <div>
                    <InsuranceQuotationsTable
                        entity={entity}
                        tableRows={ultuQuotationsTable}
                        layout={defaultLayout}
                        onChange={onChange}
                        loadQuotationInfo={this.loadQuotationInfo}
                    />
                </div>
                <div style={{marginTop: '1.2rem', marginBottom: '20px'}}>
                    <Button
                        type="secondary"
                        loading={this.state.iconLoading}
                        onClick={this.reloadQuotations(entity, onChange, true)}>
                        Actualizar Cotizaciones
                    </Button>
                </div>
                <div>
                    <InsuranceQuotationInfo quotation={this.state.selectedQuotationInfo} />
                </div>
            </div>
        );
    }
}

export default withRouter(InsuranceAutoQuery);
