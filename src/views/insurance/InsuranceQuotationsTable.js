import React, {Component} from 'react';

class InsuranceQuotationsTable extends Component {
    state = {
        selectedQuotation: ''
    };

    componentWillMount() {
        const {entity, tableRows, loadQuotationInfo} = this.props;

        let isSelectedId,
            isSelectedCompanyAndPrice = false;
        if (tableRows) {
            tableRows.forEach(row => {
                row.forEach(data => {
                    isSelectedId = data.id && entity.vehicleInsuranceId === data.id;
                    isSelectedCompanyAndPrice =
                        data.ciaAseguradora &&
                        entity.vehicleInsuranceCompany === data.ciaAseguradora &&
                        data.premioMensual &&
                        entity.vehicleInsuranceCost === data.premioMensual;
                    if (isSelectedId || isSelectedCompanyAndPrice) {
                        loadQuotationInfo(data);
                        this.setState({selectedQuotation: data});
                    }
                });
            });
        }
    }

    selectQuotation = (onChange, onQuotationLoaded, quotation) => {
        return () => {
            this.setState({selectedQuotation: quotation});
            onQuotationLoaded(quotation);
            onChange('vehicleInsuranceBudgetId', quotation.presupuestoId);
            onChange('vehicleInsuranceId', quotation.id);
            onChange('vehicleInsuranceCompany', quotation.ciaAseguradora);
            onChange('vehicleInsuranceCost', quotation.premioMensual);
            onChange('vehicleInsuranceCoverageType', quotation.detalleCobertura);
            onChange('vehicleInsuranceTerm', quotation.tipoRefa);
            onChange('vehicleInsuranceInstallments', quotation.cantCuotas);
        };
    };

    render() {
        const {tableRows, onChange, loadQuotationInfo} = this.props;

        // Logic to complete the table showing all the available insurance companies:
        let allCompaniesTableRows = [
            tableRows[0],
            ['ALLIANZ ARG.CIA.DE SEG.S.A', '-', '-', '-'],
            ['SEGUROS SURA SA', '-', '-', '-'],
            ['ZURICH ARGENTINA CIA.DE SEG.SA', '-', '-', '-']
        ];
        for (let i = 1; i < allCompaniesTableRows.length; i++) {
            for (let j = 1; j < tableRows.length; j++) {
                if (allCompaniesTableRows[i][0] === tableRows[j][0]) {
                    allCompaniesTableRows[i] = tableRows[j];
                }
            }
        }

        let rowNumber = 0;
        if (tableRows && tableRows.length === 1) {
            return (
                <p style={{width: '100%', textAlign: 'center', margin: '20px', fontWeight: 'bold'}}>
                    No hay cotizaciones de seguros disponibles.
                </p>
            );
        } else {
            return (
                <table cellSpacing="0" cellPadding="0" border="0" className={'insurance-results-grid'}>
                    <tbody>
                        {allCompaniesTableRows &&
                            allCompaniesTableRows.map(row => {
                                let currentRow = rowNumber++;
                                let colNumber = 0;
                                let cellClass = '';
                                return (
                                    <tr key={'Row_' + currentRow}>
                                        {row &&
                                            row.map(data => {
                                                let currentCol = colNumber++;
                                                if (currentRow === 0) {
                                                    cellClass =
                                                        currentCol % 2 === 0 ? 'cell even-header' : 'cell odd-header';
                                                } else if (currentCol === 0) {
                                                    cellClass = 'cell odd-header';
                                                } else {
                                                    cellClass =
                                                        data === this.state.selectedQuotation
                                                            ? 'cell data selected'
                                                            : 'cell data';
                                                }
                                                if (!data.premioMensual) {
                                                    return (
                                                        <td
                                                            key={'Cell_' + currentRow + '-' + currentCol}
                                                            className={cellClass}>
                                                            {data}
                                                        </td>
                                                    );
                                                } else {
                                                    cellClass += ' full';
                                                    return (
                                                        <td
                                                            key={'Cell_' + currentRow + '-' + currentCol}
                                                            className={cellClass}
                                                            onClick={this.selectQuotation(
                                                                onChange,
                                                                loadQuotationInfo,
                                                                data
                                                            )}>
                                                            {data.premioMensual}
                                                        </td>
                                                    );
                                                }
                                            })}
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            );
        }
    }
}

export default InsuranceQuotationsTable;
