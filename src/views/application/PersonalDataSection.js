import React, {Component} from 'react';
import {Row, Col, Form, Radio, Input} from 'antd';
import enums from '../../helpers/enumerators';
import {getLocalidades} from '../../api/localidades';
import Steps from './Steps';
import LoanHeader from './LoanHeader';
import MaskedDateField from '../../components/Fields/MaskedDateField';
import validation from '../../helpers/validator';
import {isEmpty} from 'lodash';

let enumLoading = [{id: 'loading', name: 'loading...'}];

const fields = [
    {name: 'maritalStatus', required: true, error: 'Estado Civil Requerido'},
    {name: 'city', required: true, error: 'Localidad Requerida'},
    {name: 'dob', type: 'date', required: true, error: 'Fecha de Nacimiento Requerida'},
    {name: 'zipCode', required: true, error: 'Código Postal Requerido'},
    {name: 'addressStreet', required: true, error: 'Calle Requerida'},
    {name: 'phoneMobile', required: true, error: 'Teléfono Requerido'}
];

class PersonalDataField extends Component {
    state = {
        provinces: enumLoading,
        submitted: false
    };

    constructor(props) {
        super();
        const {entity} = props;
        getLocalidades(entity.province).then(x => this.setState({citiesBuenosAires: x}));
    }

    handleNext = () => {
        const {entity, handleSubmit, handleNext} = this.props;
        this.setState({submitted: true});
        if (isEmpty(validation(entity, fields))) {
            handleSubmit();
            handleNext('insurance-data');
        }
    };

    render() {
        const {submitted} = this.state;
        const {entity, onChange, handlePrevious, citiesProvince, onChangeCity, onChangeProvince} = this.props;

        const cities =
            entity.province !== 'Capital Federal' ? citiesProvince : [{id: 'Capital Federal', name: 'Capital Federal'}];

        const errors = validation(entity, fields);
        return (
            <div className={'personal-data'}>
                <LoanHeader
                    title="Generar Solicitud"
                    entity={entity}
                    handlePrevious={handlePrevious}
                    handleNext={() => this.handleNext()}
                />
                <Steps current={1} />
                <Row gutter={16}>
                    <Col md={{offset: 6, span: 12}} sm={{offset: 0, span: 24}}>
                        <div>
                            <Form.Item
                                label={'Fecha Nacimiento'}
                                submitted={submitted}
                                validateStatus={errors['dob'] && submitted ? 'error' : 'success'}
                                help={submitted && errors['dob']}>
                                <MaskedDateField
                                    value={entity.dob}
                                    className={'ant-input'}
                                    placeholder={'24-12-1980'}
                                    onBlur={value => {
                                        onChange('dob', value);
                                    }}
                                />
                            </Form.Item>
                        </div>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col md={{offset: 6, span: 12}} sm={{offset: 0, span: 24}}>
                        <div>
                            <Form.Item
                                label="Estado Civil"
                                validateStatus={errors['maritalStatus'] && submitted ? 'error' : 'success'}
                                help={submitted && errors['maritalStatus']}>
                                <Radio.Group
                                    value={entity.maritalStatus}
                                    onChange={e => onChange('maritalStatus', e.target.value)}>
                                    {enums.eMARITAL_STATUS.map(option => (
                                        <Radio key={option.id} value={option.id}>
                                            {option.name}
                                        </Radio>
                                    ))}
                                </Radio.Group>
                            </Form.Item>
                        </div>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col md={{offset: 6, span: 12}} sm={{offset: 0, span: 24}}>
                        <div>
                            <Form.Item
                                label={'Provincia / Localidad / Código Postal'}
                                validateStatus={errors['zipCode'] && submitted ? 'error' : 'success'}
                                help={submitted && errors['zipCode']}>
                                <select
                                    onChange={e => onChangeProvince('province', e.target.value)}
                                    value={entity.province}>
                                    {enums.ePROVINCIA.map(option => (
                                        <option key={option.id} value={option.id}>
                                            {option.name}
                                        </option>
                                    ))}
                                </select>
                                <select
                                    onChange={e => onChangeCity('city', e.target.value, 'zipCode')}
                                    value={entity.city}>
                                    {[{id: '', name: ''}, ...cities].map(option => (
                                        <option key={option.id} value={option.id}>
                                            {option.name}
                                        </option>
                                    ))}
                                </select>
                                <Input
                                    value={entity.zipCode}
                                    placeholder={'Código Postal'}
                                    onChange={e => onChange('zipCode', e.target.value)}
                                />
                            </Form.Item>
                        </div>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col md={{offset: 6, span: 12}} sm={{offset: 0, span: 24}}>
                        <div>
                            <Form.Item
                                label={'Dirección'}
                                // wrapperCol={'vertical'}
                                validateStatus={errors['addressStreet'] && submitted ? 'error' : 'success'}
                                help={submitted && errors['addressStreet']}>
                                <Input
                                    className="input-direccion"
                                    value={entity.addressStreet}
                                    placeholder={'Calle'}
                                    onChange={e => onChange('addressStreet', e.target.value)}
                                />
                                <Input
                                    value={entity.addressNumber}
                                    placeholder={'Número'}
                                    onChange={e => onChange('addressNumber', e.target.value)}
                                    className="not-required"
                                />
                                <Input
                                    value={entity.addressFloor}
                                    placeholder={'Piso'}
                                    onChange={e => onChange('addressFloor', e.target.value)}
                                    className="not-required"
                                />
                                <Input
                                    value={entity.addressApartment}
                                    placeholder={'Depto'}
                                    onChange={e => onChange('addressApartment', e.target.value)}
                                    className="not-required"
                                />
                            </Form.Item>
                        </div>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col md={{offset: 6, span: 12}} sm={{offset: 0, span: 24}}>
                        <div>
                            <Form.Item
                                label={'Telefonos'}
                                validateStatus={errors['phoneMobile'] && submitted ? 'error' : 'success'}
                                help={submitted && errors['phoneMobile']}>
                                <Input
                                    placeholder={'Celular'}
                                    value={entity.phoneMobile}
                                    onChange={e => onChange('phoneMobile', e.target.value)}
                                />
                                <Input
                                    placeholder={'Teléfono Fijo'}
                                    value={entity.phoneLandLine}
                                    onChange={e => onChange('phoneLandLine', e.target.value)}
                                    className="not-required"
                                />
                            </Form.Item>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default PersonalDataField;
