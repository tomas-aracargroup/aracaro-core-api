import React from 'react';
import {Input, Row, Col, Form} from 'antd';

export default ({entity, onChange}) => {
    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Seguro del Automotor</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item label={'Compañía:'}>
                            <Input
                                value={entity['vehicleInsuranceCompany']}
                                placeholder={'Compañía'}
                                onChange={x => onChange('vehicleInsuranceCompany', x.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Cobertura:'}>
                            <Input
                                value={entity['vehicleInsuranceCoverageType']}
                                placeholder={'Cobertura'}
                                onChange={x => onChange('vehicleInsuranceCoverageType', x.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Costo:'}>
                            <Input
                                value={entity['vehicleInsuranceCost']}
                                placeholder={'Costo'}
                                onChange={x => onChange('vehicleInsuranceCost', x.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item label={'Id del Presupuesto:'}>
                            <Input
                                value={entity['vehicleInsuranceBudgetId']}
                                placeholder={'Id del Presupuesto'}
                                onChange={x => onChange('vehicleInsuranceBudgetId', x.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Id del Seguro:'}>
                            <Input
                                value={entity['vehicleInsuranceId']}
                                placeholder={'Id del Seguro'}
                                onChange={x => onChange('vehicleInsuranceId', x.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
