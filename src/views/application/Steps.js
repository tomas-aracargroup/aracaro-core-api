import React from 'react';
import {Steps} from 'antd';
const Step = Steps.Step;

export default ({current}) => (
    <Steps current={current} className="loan-steps">
        <Step title="Plan de pago" />
        <Step title="Datos Personales" />
        <Step title="Seguro Automotor" />
    </Steps>
);
