import React from 'react';
import {Row, Col, Radio, Form} from 'antd';
import enums from '../../helpers/enumerators';
import MaskedDateField from '../../components/Fields/MaskedDateField';

export default ({entity, onChange, textField}) => {
    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Datos Impositivos / Jurídicos</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Condición frente al Impuesto a las Ganancias">
                            <Radio.Group
                                value={entity.conditionIncomeTax}
                                onChange={e => onChange('conditionIncomeTax', e.target.value)}>
                                {enums.eID_TAXES_GANANCIAS.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Condición frente al IVA">
                            <Radio.Group
                                value={entity.conditionVAT}
                                onChange={e => onChange('conditionVAT', e.target.value)}>
                                {enums.eID_TAXES_IVA.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Condición frente a Ingresos Brutos">
                            <Radio.Group
                                value={entity.conditionIibb}
                                onChange={e => onChange('conditionIibb', e.target.value)}>
                                {enums.eID_TAXES_INGRESOSBRUTOS.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label={'Número de Inscripción IIBB:'}>
                            {textField('conditionIibbNumber', 'Número de Inscripción IIBB')}
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Practica ajuste por inflación:">
                            <Radio.Group
                                value={entity.conditionInflationAdjustment}
                                onChange={e => onChange('conditionInflationAdjustment', e.target.value)}>
                                {enums.eYES_NO.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item label={'Personeria Otorgada por:'}>
                            {textField('contractRegistration', 'Personeria Otorgada por')}
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Datos Inscripción Personeria:'}>
                            {textField('contractRegistrationData', 'Datos Inscripción Personeria')}
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Fecha Incripción'}>
                            <MaskedDateField
                                value={entity['contractRegistrationDate']}
                                className={'ant-input'}
                                placeholder={'24-12-1980'}
                                onBlur={value => {
                                    onChange('contractRegistrationDate', value);
                                }}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
