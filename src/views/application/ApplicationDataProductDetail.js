import React from 'react';
import {Row, Col, Form, Input, Radio} from 'antd';
import enums from '../../helpers/enumerators';
import {formatMoney, formatNumber, formatPercentage} from '../../helpers/index';

export default ({entity, onChange, textField}) => {
    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Prestamo a otorgar</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'Producto'}>{textField('productCode', 'Código de Producto', true)}</Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label="Cuotas iguales en UVA">
                            <Input
                                placeholder="Cuotas iguales en UVA"
                                value={formatNumber(entity['productPeriodicPaymentUva'], 2)}
                                disabled
                            />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Monto'}>
                            <Input placeholder="Monto" value={formatMoney(entity['productPrincipalAmount'])} disabled />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Fecha de pago cuota (1 al 30)'}>
                            {textField('productPaymentDate', 'Fecha de pago cuota (1 al 30)', true)}
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'CFTNA'}>
                            <Input
                                placeholder="CFTNA"
                                value={formatPercentage(entity['productTotalFinancialCostWithVAT'])}
                                disabled
                            />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'TNA'}>
                            <Input
                                placeholder="TNA"
                                value={formatPercentage(entity['productNominalAnualRate'])}
                                disabled
                            />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'TEA'}>
                            <Input
                                placeholder="TEA"
                                value={formatPercentage(entity['productEffectiveAnualRate'])}
                                disabled
                            />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Plazo'}>{textField('productTerm', 'Plazo', true)}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'Gastos Otorgamiento'}>
                            <Input value={formatMoney(entity.productOriginationFees)} disabled />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Neto a liquidar'}>
                            <Input value={formatMoney(entity.productNetAmount)} disabled />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'Grado de Prenda'}>
                            <Input
                                value={entity.contractPledgeDegree}
                                onChange={e => onChange('contractPledgeDegree', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Clausula de actualización'}>
                            <Radio.Group
                                value={entity.contractTerm}
                                onChange={e => onChange('contractTerm', e.target.value)}>
                                {enums.eYES_NO.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>{' '}
                    <Col span={12}>
                        <Form.Item label={'Concepto Contrato'}>
                            <Radio.Group
                                value={entity.contractConcept}
                                onChange={e => onChange('contractConcept', e.target.value)}>
                                {[{id: true, name: 'Saldo de Precio'}, {id: false, name: 'Préstamo'}].map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
