import React from 'react';
import {connect} from 'react-redux';
import {Button, Col, Card, Modal, Row} from 'antd';
import {put} from '../../api/index';
import MaskedDateTimeField from '../../components/Fields/MaskedDateTimeField';

class ApplicationFollowUp extends React.Component {
    state = {
        selectedDate: null
    };

    handleDateSelection = value => {
        this.setState({selectedDate: value});
    };

    setFollowUpDate = () => {
        const {entity} = this.props;
        const applicationId = entity._id;
        const followUpDate = this.state.selectedDate;
        const previousFollowUpDate = entity.followUpDate;
        const payload = {followUpDate: followUpDate};

        if (followUpDate && followUpDate !== previousFollowUpDate) {
            put('application', payload, applicationId).then(result => window.location.reload());
        }
    };

    onChange = (key, value) => {
        this.setState({setSelectedState: value});
    };

    render() {
        const {entity, handleOk} = this.props;

        const title = (
            <div className="name">
                <label style={{fontWeight: 'bold', fontSize: '1.2rem'}}>{'Contactar al cliente el:'}</label>
            </div>
        );

        return (
            <Modal
                iconType={'cross-circle'}
                visible
                onOk={this.setFollowUpDate}
                onCancel={handleOk}
                width={'600px'}
                closable>
                <Col>
                    <Card title={title}>
                        <Row
                            style={{
                                display: 'block',
                                width: '100%',
                                overflow: 'hidden',
                                clear: 'both',
                                marginLeft: '0px',
                                marginBottom: '20px'
                            }}>
                            <Col span={20}>
                                <MaskedDateTimeField
                                    value={entity.followUpDate}
                                    className={'ant-input'}
                                    placeholder={'DD/MM/AAAA HH:mm'}
                                    onBlur={value => {
                                        this.handleDateSelection(value);
                                    }}
                                />
                            </Col>
                            <Col span={4}>
                                <Button type="primary" size="large" span={6} onClick={this.setFollowUpDate}>
                                    {' '}
                                    Agendar
                                </Button>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Modal>
        );
    }
}

const mapStateToProps = state => ({app: state.app});
export default connect(mapStateToProps)(ApplicationFollowUp);
