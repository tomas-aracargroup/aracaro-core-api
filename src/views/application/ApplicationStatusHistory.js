import React from 'react';
import StatusModal from '../../components/StatusModal';
import {put} from '../../api/index';
import {eAPP_STATUS} from '../../constants/enumerators';

export default props => (
    <StatusModal
        {...props}
        endPoint="application/changeStatus"
        collectionName="applications"
        options={eAPP_STATUS.map(x => x.id)}
        putFunction={put}
    />
);
