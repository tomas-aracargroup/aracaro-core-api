import React from 'react';
import {Input, Row, Col, Form} from 'antd';
import enums from '../../helpers/enumerators';

export default ({entity, onChangeCity, onChange, onChangeProvince, citiesLegalProvince, errors, submitted}) => {
    const cities =
        entity.legalProvince !== 'Capital Federal'
            ? citiesLegalProvince
            : [{id: 'Capital Federal', name: 'Capital Federal'}];

    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Domicilio Legal</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item
                            label={'Dirección'}
                            required={true}
                            // validateStatus={errors['addressStreet'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressStreet']}
                        >
                            <Input
                                className="input-direccion"
                                value={entity.legalAddressStreet}
                                placeholder={'Calle'}
                                onChange={e => onChange('legalAddressStreet', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item
                            label={'Nro'}
                            required={true}
                            // validateStatus={errors['addressNumber'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressNumber']}
                        >
                            <Input
                                value={entity.legalAddressNumber}
                                placeholder={'Número'}
                                onChange={e => onChange('legalAddressNumber', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Piso'}>
                            <Input
                                value={entity.legalAddressFloor}
                                placeholder={'Piso'}
                                onChange={e => onChange('legalAddressFloor', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Depto'}>
                            <Input
                                value={entity.legalAddressApartment}
                                placeholder={'Depto'}
                                onChange={e => onChange('legalAddressApartment', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item label={'Provincia'}>
                            <select
                                onChange={e => onChangeProvince('legalProvince', e.target.value)}
                                value={entity.legalProvince}>
                                {[{id: '', name: ''}, ...enums.ePROVINCIA].map((option, idx) => (
                                    <option key={idx} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Localidad'}>
                            <select
                                onChange={e => onChangeCity('legalCity', e.target.value, 'legalZipCode')}
                                value={entity.legalCity}>
                                {[{id: '', name: ''}, ...cities].map((option, idx) => (
                                    <option key={idx} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Código Postal'}>
                            <Input
                                value={entity.legalZipCode}
                                placeholder={'Código Postal'}
                                onChange={e => onChange('legalZipCode', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Entre calles">
                            <Input
                                value={entity.legalAddressBetweenStreets}
                                placeholder={'Entre calles'}
                                onChange={e => onChange('legalAddressBetweenStreets', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
