import React from 'react';
import {Row, Col, Radio, Form, Select} from 'antd';
import enums from '../../helpers/enumerators';
import MaskedDateField from '../../components/Fields/MaskedDateField';

export default ({entity, onChange, textField}) => {
    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Cónyuge / Conviviente</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Nombre(s)'}>{textField('spouseFirstName', 'Nombre')}</Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Apellido(s)'}>{textField('spouseLastName', 'Apellido')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="Documento">
                            <Radio.Group
                                value={entity['spouseDocumentType']}
                                onChange={e => onChange('spouseDocumentType', e.target.value)}>
                                {enums.eID_TYPE.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Numero'}>
                            {textField('spouseDocumentNumber', 'Número de documento')}
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="CUIL/CUIT/CDI">
                            <Radio.Group
                                value={entity['spouseCuitType']}
                                onChange={e => onChange('spouseCuitType', e.target.value)}>
                                {enums.eCUIL_TYPE.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Numero'}>{textField('spouseCuit', 'Número de documento')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Fecha Nacimiento'}>
                            <MaskedDateField
                                value={entity['spouseDob']}
                                className={'ant-input'}
                                placeholder={'24-12-1980'}
                                onBlur={value => {
                                    onChange('spouseDob', value);
                                }}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Nacionalidad'}>
                            <Select
                                placeholder="Nacionalidad"
                                onChange={value => onChange('nationality', value)}
                                value={entity['spouseNationality']}>
                                {enums.eCOUNTRIES.map(option => (
                                    <Select.Option key={option.id} value={option.name}>
                                        {option.name}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
