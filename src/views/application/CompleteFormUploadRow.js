import React from 'react';
import {Row, Col} from 'antd';
import FilestackUploadButton from '../../components/FilestackUploadButton';
import imgPDF from '../../assets/img/pdf.jpg';

class CompleteFormUploadRow extends React.Component {
    render() {
        const {titleText, onSuccess, onError, text, src, alt} = this.props;
        let imageSource;
        if (src) {
            imageSource = src.mimetype !== 'application/pdf' ? src.url : imgPDF;
        }

        return (
            <Row gutter={16}>
                <Col sm={{offset: 0, span: 24}} md={{offset: 0, span: 24}}>
                    {titleText && <h2 className="documentation-upload__title">{titleText}</h2>}
                    <div className={`documentation-upload__item ${src && src.url ? 'uploaded' : ''}`}>
                        <div className="documentation-upload__button">
                            <FilestackUploadButton onSuccess={onSuccess} onError={onError} text={text} link={true} />
                        </div>
                        {imageSource && (
                            <div className="documentation-upload__image">
                                <a href={src.url} target="_blank">
                                    {<img src={imageSource} style={{maxHeight: '64px'}} alt={alt} />}
                                </a>
                            </div>
                        )}
                    </div>
                </Col>
            </Row>
        );
    }
}

export default CompleteFormUploadRow;
