import React from 'react';
import {Row, Col} from 'antd';
import {get, put} from '../../api/index';
import CompleteFormUploadRow from './CompleteFormUploadRow';

class CompleteForm extends React.Component {
    state = {
        fileDocumentFront: '',
        fileDocumentBack: '',
        fileMarriageCertificate: '',
        FileCarTitle: ''
    };

    componentDidMount() {
        get('byId/applications/' + this.props.appId).then(data => {
            let alreadyUploadedFiles = {};
            if (data.fileDocumentFront && data.fileDocumentFront.url) {
                alreadyUploadedFiles.fileDocumentFront = data.fileDocumentFront;
            }
            if (data.fileDocumentBack && data.fileDocumentBack.url) {
                alreadyUploadedFiles.fileDocumentBack = data.fileDocumentBack;
            }
            if (data.fileMarriageCertificate && data.fileMarriageCertificate.url) {
                alreadyUploadedFiles.fileMarriageCertificate = data.fileMarriageCertificate;
            }
            if (data.fileCarTitle && data.fileCarTitle.url) {
                alreadyUploadedFiles.fileCarTitle = data.fileCarTitle;
            }
            this.setState(alreadyUploadedFiles);
        });
    }

    onFileUpload = (result, fileDocumentName) => {
        this.setState({[fileDocumentName]: result[0]});
        put('application', {[fileDocumentName]: result[0]}, this.props.appId);
    };

    onError = result => {
        // handle result here
        console.log('onError!');
    };

    render() {
        return (
            <div>
                <Row gutter={16}>
                    <Col md={{offset: 4, span: 16}} lg={{offset: 6, span: 12}}>
                        <div className="documentation-upload">
                            <CompleteFormUploadRow
                                titleText={'Subir documentación:'}
                                onSuccess={result => this.onFileUpload(result, 'fileDocumentFront')}
                                onError={this.onError}
                                text={'Subir Copia DNI - Frente'}
                                src={this.state.fileDocumentFront}
                                alt={'Imagen del frente del DNI'}
                            />
                            <CompleteFormUploadRow
                                onSuccess={result => this.onFileUpload(result, 'fileDocumentBack')}
                                onError={this.onError}
                                text={'Subir Copia DNI - Reverso'}
                                src={this.state.fileDocumentBack}
                                alt={'Imagen del reverso del DNI'}
                            />
                            <CompleteFormUploadRow
                                onSuccess={result => this.onFileUpload(result, 'fileMarriageCertificate')}
                                onError={this.onError}
                                text={'Subir Libreta de Matrimonio'}
                                src={this.state.fileMarriageCertificate}
                                alt={'Imagen de la Libreta de Matrimonio.'}
                            />
                            <CompleteFormUploadRow
                                onSuccess={result => this.onFileUpload(result, 'fileCarTitle')}
                                onError={this.onError}
                                text={'Subir Título del Auto'}
                                src={this.state.fileCarTitle}
                                alt={'Imagen del Título del Auto'}
                            />
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default CompleteForm;
