import React from 'react';
import {Input, Row, Col, Radio, Form, Select} from 'antd';
import enums from '../../helpers/enumerators';
import MaskedDateField from '../../components/Fields/MaskedDateField';

export default ({entity, errors, submitted, onChange, textField}) => {
    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Solicitante</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Nombre(s)'}>{textField('firstName', 'Nombre')}</Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Apellido(s)'}>{textField('lastName', 'Apellido')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="Tipo de Documento">
                            <Radio.Group
                                value={entity.documentType}
                                onChange={e => onChange('documentType', e.target.value)}>
                                {enums.eID_TYPE.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Nº de documento'}>
                            {textField('documentNumber', 'Número de documento')}
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="CUIL/CUIT/CDI">
                            <Radio.Group value={entity.cuitType} onChange={e => onChange('cuitType', e.target.value)}>
                                {enums.eCUIL_TYPE.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Nº de CUIL/CUIT/CDI'}>
                            {textField('cuit', 'Número de CUIL/CUIT/CDI')}
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="Genero">
                            <Radio.Group value={entity.gender} onChange={e => onChange('gender', e.target.value)}>
                                {enums.eGENERO.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Fecha Nacimiento'}>
                            <MaskedDateField
                                value={entity.dob}
                                className={'ant-input'}
                                placeholder={'24-12-1980'}
                                onBlur={value => {
                                    onChange('dob', value);
                                }}
                            />
                        </Form.Item>
                    </Col>
                </Row>

                <Form.Item label="Estado Civil">
                    <Radio.Group value={entity.maritalStatus} onChange={e => onChange('maritalStatus', e.target.value)}>
                        {enums.eMARITAL_STATUS.map(option => (
                            <Radio key={option.id} value={option.id}>
                                {option.name}
                            </Radio>
                        ))}
                    </Radio.Group>
                </Form.Item>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="Telefono Celular">
                            <Input
                                placeholder={'Celular'}
                                value={entity.phoneMobile}
                                onChange={e => onChange('phoneMobile', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Teléfono Fijo'}>
                            <Input
                                placeholder={'Teléfono Fijo'}
                                value={entity.phoneLandLine}
                                onChange={e => onChange('phoneLandLine', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="Email">
                            <Input
                                placeholder={'Email'}
                                value={entity.email}
                                onChange={e => onChange('email', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'País Nacimiento'}>
                            <Select
                                placeholder="País Nacimiento"
                                onChange={value => onChange('countryOfBirth', value)}
                                value={entity['countryOfBirth']}>
                                {enums.eCOUNTRIES.map(option => (
                                    <Select.Option key={option.id} value={option.name}>
                                        {option.name}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Nacionalidad'}>
                            <Select
                                placeholder="Nacionalidad"
                                onChange={value => onChange('nationality', value)}
                                value={entity['nationality']}>
                                {enums.eCOUNTRIES.map(option => (
                                    <Select.Option key={option.id} value={option.name}>
                                        {option.name}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Tipo Residencia">
                            <Radio.Group
                                value={entity.residenceType}
                                onChange={e => onChange('residenceType', e.target.value)}>
                                {enums.eRESIDENCIA.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
