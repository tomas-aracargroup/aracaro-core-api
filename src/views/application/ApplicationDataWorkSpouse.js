import React from 'react';
import {Row, Col, Form, Radio} from 'antd';
import enums from '../../helpers/enumerators';
import MaskedDateField from '../../components/Fields/MaskedDateField';

export default ({textField, entity, citiesSpouseWorkingProvince, onChangeCity, onChangeProvince, onChange}) => {
    const cities =
        entity.citiesSpouseWorkingProvince !== 'Capital Federal'
            ? citiesSpouseWorkingProvince
            : [{id: 'Capital Federal', name: 'Capital Federal'}];

    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Datos Laborales Cónyuge / Conviviente</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Actividad principal">
                            <Radio.Group
                                value={entity['spouseMainProfessionalActivityType']}
                                onChange={e => onChange('spouseMainProfessionalActivityType', e.target.value)}>
                                {enums.eACTIVITY.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Empresa'}>{textField('spouseWorkingCompany', 'Empresa')}</Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Ramo">{textField('spouseWorkingBranch', 'Ramo')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="Profesión">
                            {textField('spouseMainProfessionalActivity', 'Profesión')}
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Cargo">{textField('spouseWorkingJob', 'Cargo')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Fecha de Ingreso'}>
                            <MaskedDateField
                                value={entity['spouseWorkingDateFrom']}
                                className={'ant-input'}
                                placeholder={'24-12-1980'}
                                onBlur={value => {
                                    onChange('spouseWorkingDateFrom', value);
                                }}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Ingresos Netos Mensuales'}>
                            {textField('spouseNetMonthlyIncome', 'Ingresos Netos Mensuales')}
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="CUIT Empresa">{textField('spouseWorkingCUIT', 'CUIT Empresa')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item
                            label={'Dirección'}
                            required={true}
                            // validateStatus={errors['addressStreet'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressStreet']}
                        >
                            {textField('spouseWorkingAddressStreet', 'Dirección')}
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item
                            label={'Nro'}
                            required={true}
                            // validateStatus={errors['addressNumber'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressNumber']}
                        >
                            {textField('spouseWorkingAddressNumber', 'Nro.')}
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Piso'}>{textField('spouseWorkingAddressFloor', 'Piso')}</Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Depto'}>{textField('spouseWorkingAddressApartment', 'Dpto.')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item label={'Provincia'}>
                            <select
                                onChange={e => onChangeProvince('spouseWorkingProvince', e.target.value)}
                                value={entity.spouseWorkingProvince}>
                                {[{id: '', name: ''}, ...enums.ePROVINCIA].map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Localidad'}>
                            <select
                                onChange={e =>
                                    onChangeCity('spouseWorkingCity', e.target.value, 'spouseWorkingZipCode')
                                }
                                value={entity.spouseWorkingCity}>
                                {[{id: '', name: ''}, ...cities].map((option, idx) => (
                                    <option key={idx} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Código Postal'}>
                            {textField('spouseWorkingZipCode', 'Código Postal')}
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Entre calles">
                            {textField('spouseWorkingAddressBetweenStreets', 'Entre Calles')}
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
