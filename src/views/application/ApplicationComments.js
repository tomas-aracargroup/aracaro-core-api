import React from 'react';
import CommentsModal from '../../components/CommentsModal';
import {put} from '../../api/index';

export default props => (
    <CommentsModal {...props} endPoint="application/addNote" collectionName="applications" putFunction={put} />
);
