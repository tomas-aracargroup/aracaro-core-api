import React from 'react';
import {Row, Col, Form, Radio, InputNumber} from 'antd';
import enums from '../../helpers/enumerators';

export default ({entity, onChange, marcas, textField}) => {
    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Destino del Préstamo - Datos del Automotor</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'Marca'}>{textField('vehicleMake', 'Marca', true)}</Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Modelo'}>{textField('vehicleModel', 'Modelo', true)}</Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Modelo (form 03)'}>
                            {textField('vehicleModelForm03', 'Modelo (form 03)')}
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Tipo:'}>{textField('vehicleType', 'Tipo')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'Patente'}>{textField('vehicleDomain', 'Patente')}</Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Es 0Km'}>
                            <Radio.Group
                                value={entity.vehicleIsNew}
                                onChange={e => onChange('vehicleIsNew', e.target.value)}>
                                {enums.eYES_NO.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Modelo año'}>{textField('vehicleYear', 'Modelo año', true)}</Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Kilometraje:'}>{textField('vehicleKm', 'Kilometraje')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Chasis número'}>
                            {textField('vehicleChassisNumber', 'Chasis número')}
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Chasis marca:">
                            <select
                                onChange={e => onChange('vehicleChassisMake', e.target.value)}
                                value={entity.vehicleChassisMake}>
                                {marcas.map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Motor número:'}>
                            {textField('vehicleEngineNumber', 'Motor número')}
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Motor marca:">
                            <select
                                onChange={e => onChange('vehicleEngineMake', e.target.value)}
                                value={entity.vehicleEngineMake}>
                                {marcas.map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'Categoría:'}>
                            <select
                                onChange={e => onChange('vehicleCategory', e.target.value)}
                                value={entity.vehicleCategory}>
                                {[{id: '', name: ''}, ...enums.eVEHICULO_TIPO].map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Uso:'}>
                            <select
                                onChange={e => onChange('vehicleUsageType', e.target.value)}
                                value={entity.vehicleUsageType}>
                                {[{id: '', name: ''}, ...enums.eVEHICULO_USO_DECLARADO_APPLICATION].map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Posee GNC'}>
                            <Radio.Group
                                value={entity.vehicleHasNaturalGas}
                                onChange={e => onChange('vehicleHasNaturalGas', e.target.value)}>
                                {enums.eYES_NO.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={6}>
                        <Form.Item label={'Valor'}>{textField('vehicleValuation', 'Valor')}</Form.Item>
                    </Col>
                    <Col span={6}>
                        <Form.Item label={'Porcentaje de financiación:'}>
                            <InputNumber
                                placeholder={'Porcentaje de financiación:'}
                                min={0}
                                max={100}
                                value={entity['vehicleFinalcialPercentage']}
                                onChange={value =>
                                    onChange('vehicleFinalcialPercentage', value.toString().replace(/[^0-9.,]/g, ''))
                                }
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
