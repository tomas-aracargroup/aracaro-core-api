import React from 'react';
import {Input, Row, Col, Form} from 'antd';

export default ({entity, onChange}) => {
    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Referencias</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label={'Referencia 1'}>
                            <Row gutter={16}>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceName1']}
                                        placeholder={'Nombre(s)'}
                                        onChange={x => onChange('referenceName1', x.target.value)}
                                    />
                                </Col>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceLastName1']}
                                        placeholder={'Apellido(s)'}
                                        onChange={x => onChange('referenceLastName1', x.target.value)}
                                    />
                                </Col>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceTelephone1']}
                                        placeholder={'Teléfono(s)'}
                                        onChange={x => onChange('referenceTelephone1', x.target.value)}
                                    />
                                </Col>
                            </Row>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label={'Referencia 2'}>
                            <Row gutter={16}>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceName2']}
                                        placeholder={'Nombre(s)'}
                                        onChange={x => onChange('referenceName2', x.target.value)}
                                    />
                                </Col>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceLastName2']}
                                        placeholder={'Apellido(s)'}
                                        onChange={x => onChange('referenceLastName2', x.target.value)}
                                    />
                                </Col>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceTelephone2']}
                                        placeholder={'Teléfono(s)'}
                                        onChange={x => onChange('referenceTelephone2', x.target.value)}
                                    />
                                </Col>
                            </Row>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label={'Referencia 3'}>
                            <Row gutter={16}>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceName3']}
                                        placeholder={'Nombre(s)'}
                                        onChange={x => onChange('referenceName3', x.target.value)}
                                    />
                                </Col>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceLastName3']}
                                        placeholder={'Apellido(s)'}
                                        onChange={x => onChange('referenceLastName3', x.target.value)}
                                    />
                                </Col>
                                <Col span={8}>
                                    <Input
                                        value={entity['referenceTelephone3']}
                                        placeholder={'Teléfono(s)'}
                                        onChange={x => onChange('referenceTelephone3', x.target.value)}
                                    />
                                </Col>
                            </Row>
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
