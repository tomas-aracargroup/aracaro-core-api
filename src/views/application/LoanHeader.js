import React from 'react';
import {Button, Icon} from 'antd';
import {getAgeWithParenthesis} from '../../helpers';

export default ({title, entity, number, handlePrevious, handleNext, cloneApplication, current}) => {
    return (
        <div className="loan-header">
            <div className="loan-header__client">
                <h2 className="loan-header__title">{`${entity['firstName']} ${
                    entity['lastName']
                }  ${getAgeWithParenthesis(entity['dob'])}`}</h2>
            </div>
            <div className="loan-header__buttons">
                <Button type="secondary" className="ml-2 loan-imprimir" onClick={cloneApplication}>
                    Reevaluar
                </Button>
                <Button type="secondary" className="ml-2 loan-imprimir" onClick={handlePrevious}>
                    Anterior
                </Button>
                {handleNext ? (
                    <Button type="primary" className="ml-2 loan-imprimir" onClick={handleNext}>
                        {current === 3 ? <Icon type={'printer'} /> : null}
                        {current === 3 ? 'IMPRIMIR CARTA DE APROBACIÓN' : 'Siguiente'}
                    </Button>
                ) : null}
            </div>
        </div>
    );
};
