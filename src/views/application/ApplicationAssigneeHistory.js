import React from 'react';
import {connect} from 'react-redux';
import {Col, Card, Modal, Row, Select} from 'antd';
import {put} from '../../api/index';
import moment from 'moment';

const Option = Select.Option;

const sortByDateDesc = (x, y) => {
    const fechaX = moment(x.timeStamp).format('YYYYMMDDHHmmss');
    const fechaY = moment(y.timeStamp).format('YYYYMMDDHHmmss');

    return fechaX * 1 > fechaY * 1 ? -1 : fechaX * 1 < fechaY * 1 ? 1 : 0;
};

class ApplicationAssigneeHistory extends React.Component {
    state = {
        selectedAssignee: ''
    };

    handleAssigneeSelection = newValue => {
        this.setState({selectedAssignee: newValue});
    };

    handleOk = () => {
        const {app, entity} = this.props;
        const applicationId = entity._id;
        const user = app.auth.user.firstname + ' ' + app.auth.user.lastname;
        const assignee = this.state.selectedAssignee;
        const previousAssignee = entity.assignee;
        const payload = {assignee: assignee, user: user};

        if (assignee && assignee !== previousAssignee) {
            put('application/assign', payload, applicationId).then(result => window.location.reload());
        }
    };

    render() {
        const {entity, handleOk} = this.props;
        let assigneeList = [];
        if (entity.assigneeHistory) {
            assigneeList = [...entity.assigneeHistory];
        }

        const title = (
            <div className="name">
                <label style={{fontWeight: 'bold', fontSize: '1.2rem'}}>{'Asignar a'}</label>
            </div>
        );

        const callbackfn = (x, key) => (
            <tr key={key}>
                <td>{x.assignee}</td>
                <td>{x.user}</td>
                <td>{moment(x.timeStamp).format('DD/MM/YYYY - h:mm:ss a')}</td>
            </tr>
        );

        let assigneePositionInHistory = 0;

        return (
            <Modal iconType={'cross-circle'} visible onOk={this.handleOk} onCancel={handleOk} width={'600px'} closable>
                <Col>
                    <Card title={title}>
                        <Row
                            style={{
                                display: 'block',
                                width: '100%',
                                overflow: 'hidden',
                                clear: 'both',
                                marginLeft: '0px',
                                marginBottom: '20px'
                            }}>
                            <Col span={24}>
                                <Select
                                    defaultValue={entity.assignee}
                                    style={{}}
                                    size="large"
                                    onChange={this.handleAssigneeSelection}>
                                    <Option value="Adolfo Olivera">Adolfo Olivera</Option>
                                    <Option value="Bruno Pisani">Bruno Pisani</Option>
                                    <Option value="Camila La Giglia">Camila La Giglia</Option>
                                    <Option value="Diego Reyes">Diego Reyes</Option>
                                    <Option value="Hernan Paldino">Hernan Paldino</Option>
                                    <Option value="Karla Buitron">Karla Buitron</Option>
                                    <Option value="Laura Mercado">Laura Mercado</Option>
                                    <Option value="Lucas Canedo">Lucas Canedo</Option>
                                    <Option value="Ruben Lombardo">Ruben Lombardo</Option>
                                    <Option value="Russell Abrams">Russell Abrams</Option>
                                    <Option value="Santiago Villafañe">Santiago Villafañe</Option>
                                </Select>
                            </Col>
                        </Row>
                        {assigneeList.length > 1 && (
                            <div>
                                <Row style={{marginBottom: '20px', fontWeight: 'bold', fontSize: '1rem'}}>
                                    <Col>{'Status Anteriores:'}</Col>
                                </Row>
                                <div className="table-responsive">
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Asignado a</th>
                                                <th>Usuario</th>
                                                <th>Fecha y Hora</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {assigneeList.sort(sortByDateDesc).map((status, key) => {
                                                if (++assigneePositionInHistory !== 1) {
                                                    return callbackfn(status, key);
                                                } else {
                                                    return <tr key={key} />;
                                                }
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        )}
                    </Card>
                </Col>
            </Modal>
        );
    }
}

const mapStateToProps = state => ({app: state.app});
export default connect(mapStateToProps)(ApplicationAssigneeHistory);
