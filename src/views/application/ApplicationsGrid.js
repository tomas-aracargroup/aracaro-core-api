import React from 'react';
import Grid from '../../components/Grid';

const resolveColor = (item, name) => {
    switch (item[name]) {
        case 'Rechazar':
        case 'Rechazo Condicional':
            return '#bb0000';

        case 'Preaprobar':
            return '#a0d911';

        case 'Verificar':
            return '#fa8c16';

        case 'Error':
            return '#5c0011';

        default:
            return 'initial';
    }
};

const columns = [
    {
        name: 'documentNumber',
        label: 'Documento',
        cell: 'link',
        searchable: true,
        searchType: 'string',
        collectionName: 'applications'
    },
    // {name: 'applicationDate', label: 'Fecha', searchable: true},
    {name: 'applicationDate', label: 'Fecha', searchable: true, cell: 'timeSince'},
    {name: 'beSmartResult', label: 'Besmart', cell: 'dictamen', searchable: true, resolveColor},
    {name: 'applicationNumber', label: 'Nro.', cell: 'number', searchable: true, searchType: 'number'},
    {name: 'firstName', label: 'Nombre', searchable: true},
    {name: 'lastName', label: 'Apellido', searchable: true},
    {name: 'applicationStatus', label: 'Status', cell: 'action', modalName: 'statusHistory', searchable: true},
    {name: 'applicationStatusHistory', label: 'Historial de Status', cell: 'text', hideInColumnPicker: true},
    {name: 'followUpDate', label: 'Contactar el', cell: 'actionDate', modalName: 'followUp', searchable: true},
    {name: 'assignee', label: 'Asignado a', cell: 'action', modalName: 'assigneeHistory', searchable: true},
    {name: 'declinedReason', label: 'Motivo rechazo', searchable: true},
    {name: 'beSmartFinancialCommitments', label: 'Compromisos Mensuales'},
    {name: 'beSmartIncomePaymentRatio', label: 'RCI'},
    {name: 'declaredIncome', label: 'Ingreso Declarado', cell: 'money'},
    {name: 'salesPerson', label: 'Vendedor', searchable: true},
    {name: 'productEffectiveAnualRate', label: 'TEA', cell: 'percentage'},
    {name: 'productFixedOriginationFees', label: 'Gastos $', cell: 'money'},
    {name: 'productVariableOriginationFees', label: 'Gastos %', cell: 'percentage'},
    {name: 'productNominalAnualRate', label: 'TNA', cell: 'percentage'},
    {name: 'productFirstPaymentAmount', label: 'Primer Pago', cell: 'money'},
    {name: 'requestedAmount', label: 'Solicitado', cell: 'money', searchable: true, searchType: 'number'},
    {name: 'productNetAmount', label: 'Neto', cell: 'money'},
    {name: 'productPrincipalAmount', label: 'Bruto', cell: 'money'},
    {name: 'vehicleModel', label: 'Modelo'},
    {name: 'vehicleValuation', label: '$ Vehiculo', cell: 'money'},
    {name: 'vehicleInsuranceBudgetId', label: 'Id del Presupuesto'},
    {name: 'vehicleInsuranceId', label: 'Id del Seguro'},
    {name: 'insuranceCost', label: 'Seguro Costo', cell: 'money'},
    {name: 'productTerm', label: 'Cuotas', searchType: 'number'},
    {name: 'notes', label: 'Historial de Notas', cell: 'text', hideInColumnPicker: true},
    {name: 'notesLastMessage', label: '# Notas', cell: 'action', modalName: 'notes', searchable: true}
];

export default () => (
    <Grid
        title={'Prenda Rapida / Aplicaciones '}
        columns={columns}
        columnPicker
        downloadDealers
        collectionName="applications"
        endPoint="applications"
        sortBy={{}}
    />
);
