import React, {Component} from 'react';
import {getPeriodicPayment, getPeriodRate, convertToUVAS} from '../../helpers/financial';
import {formatMoney, formatPercentage, formatNumber} from '../../helpers';
import moment from 'moment-timezone';
import {Col, Select, Radio} from 'antd';
import 'moment/locale/es';
import Table from '../calculator/Table';
import Steps from './Steps';
import LoanHeader from './LoanHeader';
import LoanFooter from './LoanFooter';

const headers = [
    {title: 'Fecha', dataIndex: 'date'},
    {title: 'Período #', dataIndex: 'n'},
    {title: 'Capital', dataIndex: 'remainingPrincipal'},
    {title: 'Amortización', dataIndex: 'amortization'},
    {title: 'Interés', dataIndex: 'interest'},
    {title: 'IVA', dataIndex: 'iva'},
    {title: 'Total', dataIndex: 'periodPayment'}
];

moment.locale('es');

const plazos = [60, 48, 36, 24];

const style = {fontWeight: 'bold', textAlign: 'right', paddingRight: 10};

const getPayment = (plazo, entity) =>
    getPeriodicPayment(getPeriodRate(entity.productNominalAnualRate / 100, 12), plazo, entity.productPrincipalAmount);

class LoadSection extends Component {
    state = {currency: 'uvas'};

    onSelectChange = value => {
        const {onTermChange} = this.props;
        onTermChange(value);
    };

    handleNext = () => {
        const {handleNext, handleSubmit} = this.props;
        handleSubmit();
        handleNext('personal-data');
    };

    render() {
        const {entity, handleNext, handlePrevious} = this.props;
        const {currency} = this.state;
        const productAmortizationSchedule =
            currency === 'uvas'
                ? convertToUVAS(entity.productAmortizationSchedule, entity.productUVAValue)
                : entity.productAmortizationSchedule;

        return (
            <div>
                <LoanHeader
                    title="Generar Solicitud"
                    entity={entity}
                    handlePrevious={handlePrevious}
                    handleNext={() => this.handleNext()}
                />
                <Steps current={0} />
                <div className="loan-body clearfix">
                    <Col span={8} className={'p-1'}>
                        <h2>Condiciones</h2>
                        <table className={'table table-striped'}>
                            <tbody>
                                <tr>
                                    <td style={style}>Seleccion plazo:</td>
                                    <td>
                                        <Select
                                            onChange={e => this.onSelectChange(e)}
                                            value={Number(entity.productTerm)}>
                                            {plazos.map(plazo => {
                                                if (
                                                    plazo > entity.productMaximumTerm ||
                                                    getPayment(plazo, entity) >
                                                        Number(entity.productMaximumMonthlyPayment)
                                                ) {
                                                    return null;
                                                }
                                                return (
                                                    <Select.Option key={plazo} value={plazo}>
                                                        {plazo} meses
                                                    </Select.Option>
                                                );
                                            })}
                                        </Select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan={2} style={style} />
                                </tr>
                                <tr>
                                    <td style={style}>Monto Prestamo ($):</td>
                                    <td>{formatMoney(entity.productPrincipalAmount)}</td>
                                </tr>
                                <tr>
                                    <td style={style}>Neto a Desembolsar ($):</td>
                                    <td>{formatMoney(entity.productNetAmount)}</td>
                                </tr>
                                <tr>
                                    <td style={style}>Cuota Pura ($) </td>
                                    <td>{formatMoney(entity.productPeriodicPaymentPesos)}</td>
                                </tr>
                                <tr>
                                    <td colSpan={2} style={style} />
                                </tr>

                                <tr>
                                    <td style={style}>Monto Prestamo (UVAs):</td>
                                    <td>{formatNumber(entity.productUVAEquivalent)}</td>
                                </tr>
                                <tr>
                                    <td style={style}>Valor UVA : </td>
                                    <td>
                                        {formatNumber(entity.productUVAValue, 2)} ({moment
                                            .utc(entity.applicationDate)
                                            .format('DD/MM/YY')})
                                    </td>
                                </tr>
                                <tr>
                                    <td style={style}>Cuota Pura UVA: </td>
                                    <td>{formatNumber(entity.productPeriodicPaymentUva, 2)}</td>
                                </tr>
                                <tr>
                                    <td colSpan={2} />
                                </tr>
                                <tr>
                                    <td style={style}>TNA:</td>
                                    <td>{formatPercentage(entity.productNominalAnualRate)}</td>
                                </tr>
                                <tr>
                                    <td style={style}>TEA:</td>
                                    <td>{formatPercentage(entity.productEffectiveAnualRate)}</td>
                                </tr>
                                <tr>
                                    <td style={style}>CFT:</td>
                                    <td>{formatPercentage(entity.productTotalFinancialCost)}</td>
                                </tr>
                                <tr>
                                    <td style={style}>CFT + IVA:</td>
                                    <td>{formatPercentage(entity.productTotalFinancialCostWithVAT)}</td>
                                </tr>
                            </tbody>
                        </table>
                    </Col>
                    <Col span={16} className={'p-1'}>
                        <div className="cuadro-de-marcha-top">
                            <h2>Cuadro de Marcha</h2>
                            <div className="currency-switcher">
                                <p>Mostrar pagos en:</p>
                                {
                                    <Radio.Group
                                        value={this.state.currency}
                                        onChange={e => this.setState({currency: e.target.value})}>
                                        {[{id: 'uvas', name: 'UVAs'}, {id: 'pesos', name: 'Pesos'}].map(option => (
                                            <Radio key={option.id} value={option.id}>
                                                {option.name}
                                            </Radio>
                                        ))}
                                    </Radio.Group>
                                }
                            </div>
                        </div>
                        <Table collection={productAmortizationSchedule} headers={headers} />
                    </Col>
                </div>
                <LoanFooter
                    entity={entity}
                    handlePrevious={handlePrevious}
                    handleNext={() => handleNext('personal-data')}
                />
            </div>
        );
    }
}
export default LoadSection;
