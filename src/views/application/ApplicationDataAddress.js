import React from 'react';
import {Input, Row, Col, Form} from 'antd';
import enums from '../../helpers/enumerators';

export default ({entity, onChangeCity, onChange, onChangeProvince, citiesProvince, errors, submitted}) => {
    const cities =
        entity.province !== 'Capital Federal' ? citiesProvince : [{id: 'Capital Federal', name: 'Capital Federal'}];

    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Domicilio Particular</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item
                            label={'Dirección'}
                            required={true}
                            // validateStatus={errors['addressStreet'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressStreet']}
                        >
                            <Input
                                className="input-direccion"
                                value={entity.addressStreet}
                                placeholder={'Calle'}
                                onChange={e => onChange('addressStreet', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item
                            label={'Nro'}
                            required={true}
                            // validateStatus={errors['addressNumber'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressNumber']}
                        >
                            <Input
                                value={entity.addressNumber}
                                placeholder={'Número'}
                                onChange={e => onChange('addressNumber', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Piso'}>
                            <Input
                                value={entity.addressFloor}
                                placeholder={'Piso'}
                                onChange={e => onChange('addressFloor', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Depto'}>
                            <Input
                                value={entity.addressApartment}
                                placeholder={'Depto'}
                                onChange={e => onChange('addressApartment', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item label={'Provincia'}>
                            <select
                                onChange={e => onChangeProvince('province', e.target.value)}
                                value={entity.province}>
                                {[{id: '', name: ''}, ...enums.ePROVINCIA].map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Localidad'}>
                            <select onChange={e => onChangeCity('city', e.target.value, 'zipCode')} value={entity.city}>
                                {[{id: '', name: ''}, ...cities].map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Código Postal'}>
                            <Input
                                value={entity.zipCode}
                                placeholder={'Código Postal'}
                                onChange={e => onChange('zipCode', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Entre calles">
                            <Input
                                value={entity.addressBetweenStreets}
                                placeholder={'Entre calles'}
                                onChange={e => onChange('addressBetweenStreets', e.target.value)}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
