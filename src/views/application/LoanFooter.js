import React from 'react';
import {Button} from 'antd';

export default ({handlePrevious, handleNext, nextDisabled}) => {
    return (
        <div className="loan-footer">
            <div className="loan-footer__buttons">
                <Button type="secondary" className="ml-2" onClick={handlePrevious}>
                    Anterior
                </Button>
                {handleNext ? (
                    <Button type="primary" disabled={nextDisabled} className="ml-2" onClick={handleNext}>
                        Siguiente
                    </Button>
                ) : null}
            </div>
        </div>
    );
};
