import React from 'react';
import {Row, Col, Form, Radio} from 'antd';
import enums from '../../helpers/enumerators';
import MaskedDateField from '../../components/Fields/MaskedDateField';

export default ({textField, entity, citiesWorkingProvince, onChangeProvince, onChangeCity, onChange}) => {
    const cities =
        entity.workingProvince !== 'Capital Federal'
            ? citiesWorkingProvince
            : [{id: 'Capital Federal', name: 'Capital Federal'}];

    return (
        <Row gutter={16} className="personal-data-all__container">
            <h3 className="personal-data-all__title">Datos Laborales del Solicitante</h3>
            <Col md={{offset: 4, span: 16}} sm={{offset: 0, span: 24}}>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Actividad principal">
                            <Radio.Group
                                value={entity['mainProfessionalActivityType']}
                                onChange={e => onChange('mainProfessionalActivityType', e.target.value)}>
                                {enums.eACTIVITY.map(option => (
                                    <Radio key={option.id} value={option.id}>
                                        {option.name}
                                    </Radio>
                                ))}
                            </Radio.Group>
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Empresa'}>{textField('workingCompany', 'Empresa')}</Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Ramo">{textField('workingBranch', 'Ramo')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="Profesión">{textField('mainProfessionalActivity', 'Profesión')}</Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label="Cargo">{textField('workingJob', 'Cargo')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label={'Fecha de Ingreso'}>
                            <MaskedDateField
                                value={entity['workingDateFrom']}
                                className={'ant-input'}
                                placeholder={'24-12-1980'}
                                onBlur={value => {
                                    onChange('workingDateFrom', value);
                                }}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={12}>
                        <Form.Item label={'Ingresos Netos Mensuales'}>
                            {textField('netMonthlyIncome', 'Ingresos Netos Mensuales')}
                        </Form.Item>
                    </Col>
                </Row>

                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item label="CUIT Empresa">{textField('workingCUIT', 'CUIT Empresa')}</Form.Item>
                    </Col>
                </Row>

                <Row gutter={16}>
                    <Col span={12}>
                        <Form.Item
                            label={'Dirección'}
                            // validateStatus={errors['addressStreet'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressStreet']}
                        >
                            {textField('workingAddressStreet', 'Dirección')}
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item
                            label={'Nro'}
                            // validateStatus={errors['addressNumber'] && submitted ? 'error' : 'success'}
                            // help={submitted && errors['addressNumber']}
                        >
                            {textField('workingAddressNumber', 'Nro.')}
                        </Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Piso'}>{textField('workingAddressFloor', 'Piso')}</Form.Item>
                    </Col>
                    <Col span={4}>
                        <Form.Item label={'Depto'}>{textField('workingAddressApartment', 'Dpto.')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item label={'Provincia'}>
                            <select
                                onChange={e => onChangeProvince('workingProvince', e.target.value)}
                                value={entity.workingProvince}>
                                {[{id: '', name: ''}, ...enums.ePROVINCIA].map(option => (
                                    <option key={option.id} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Localidad'}>
                            <select
                                onChange={e => onChangeCity('workingCity', e.target.value, 'workingZipCode')}
                                value={entity.workingCity}>
                                {[{id: '', name: ''}, ...cities].map((option, idx) => (
                                    <option key={idx} value={option.id}>
                                        {option.name}
                                    </option>
                                ))}
                            </select>
                        </Form.Item>
                    </Col>
                    <Col span={8}>
                        <Form.Item label={'Código Postal'}>{textField('workingZipCode', 'Código Postal')}</Form.Item>
                    </Col>
                </Row>
                <Row gutter={16}>
                    <Col span={24}>
                        <Form.Item label="Entre calles">
                            {textField('workingAddressBetweenStreets', 'Entre Calles')}
                        </Form.Item>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};
