import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import LoanSection from './LoanSection';
import {Switch, Route, Redirect} from 'react-router-dom';
import {getByID, put} from '../../api';
import {calculateProductConditions} from '../../helpers/financial';
import PersonalDataSection from './PersonalDataSection';
import InsuranceSection from './InsuranceSection';
import Application from './Application';
import Finish from './Finish';
import {getLocalidades} from '../../api/localidades';
import {getByKey} from '../../helpers';

class LoanApplicationSteps extends Component {
    state = {
        ultuTableObject: {},
        iconSaving: false
    };

    constructor(props) {
        super(props);
        getByID('applications', props.match.params.id).then(response => {
            this.setState({entity: response});
            getLocalidades(response.province).then(x => this.setState({citiesProvince: x}));
            getLocalidades(response.legalProvince).then(x => this.setState({citiesLegalProvince: x}));
            getLocalidades(response.workingProvince).then(x => this.setState({citiesWorkingProvince: x}));
            getLocalidades(response.spouseWorkingProvince).then(x => this.setState({citiesSpouseWorkingProvince: x}));
        });
    }

    handlePrevious = () => {
        this.props.history.goBack();
    };

    onChangeCity = (name, value, zipCodeName) => {
        this.onChange(name, value);

        const resolveSource = name => {
            switch (name) {
                case 'zipCode':
                    return this.state.citiesProvince;
                case 'legalZipCode':
                    return this.state.citiesLegalProvince;
                case 'workingZipCode':
                    return this.state.citiesWorkingProvince;
                case 'spouseWorkingZipCode':
                    return this.state.citiesSpouseWorkingProvince;
                default:
                    return [];
            }
        };

        const city = getByKey(resolveSource(zipCodeName), value, 'id');
        if (city) {
            this.onChange(zipCodeName, city.zipCode);
        } else {
            this.onChange(zipCodeName, '');
        }
    };

    handleNext = route => {
        const {history, match} = this.props;
        history.push(`${match.url}/${route}`);
    };

    onTermChange = term => {
        const entity = this.state.entity;
        this.setState({
            entity: {
                ...entity,
                ...calculateProductConditions({
                    term,
                    principal: entity.productPrincipalAmount,
                    cutDate: entity.productPaymentDate,
                    originationFees: entity.productOriginationFees,
                    tna: entity.productNominalAnualRate,
                    uvaValue: entity.productUVAValue
                })
            }
        });
    };

    onUltuTableLoaded = tableObject => {
        this.setState({ultuTableObject: tableObject});
    };

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    onChangeProvince = (name, value) => {
        let entity = this.state.entity;
        const resolveField = (name, items) => {
            switch (name) {
                case 'province':
                    return {citiesProvince: items};
                case 'legalProvince':
                    return {citiesLegalProvince: items};
                case 'workingProvince':
                    return {citiesWorkingProvince: items};
                case 'spouseWorkingProvince':
                    return {citiesSpouseWorkingProvince: items};
                default:
                    return [];
            }
        };
        const correspondingZipCodeField = name => {
            switch (name) {
                case 'province':
                    return 'zipCode';
                case 'legalProvince':
                    return 'legalZipCode';
                case 'workingProvince':
                    return 'workingZipCode';
                case 'spouseWorkingProvince':
                    return 'spouseWorkingZipCode';
                default:
                    return '';
            }
        };

        entity[name] = value;
        this.setState({entity});
        this.onChange(correspondingZipCodeField(name), '');
        getLocalidades(value).then(x => this.setState(resolveField(name, x)));
    };

    handleSubmit = async () => {
        const {entity} = this.state;
        this.setState({iconSaving: true});
        let data;
        try {
            data = await put(`application`, entity, entity._id);
        } finally {
            this.setState({iconSaving: false});
        }
        return data;
    };

    render() {
        const {
            entity,
            citiesProvince,
            citiesLegalProvince,
            citiesWorkingProvince,
            citiesSpouseWorkingProvince,
            iconSaving
        } = this.state;
        const {app, match} = this.props;

        if (!citiesProvince) {
            return <p style={{margin: '1rem'}}>Loading</p>;
        }

        const renderDictamen = () => (
            <LoanSection
                handleNext={this.handleNext}
                handlePrevious={this.handlePrevious}
                handleSubmit={this.handleSubmit}
                onTermChange={this.onTermChange}
                entity={entity}
                config={app.config}
            />
        );
        const renderApplication = () => (
            <Application
                onChange={this.onChange}
                app={app}
                entity={entity}
                handleSubmit={this.handleSubmit}
                handlePrevious={this.handlePrevious}
                onChangeCity={this.onChangeCity}
                onChangeProvince={this.onChangeProvince}
                iconSaving={iconSaving}
                citiesProvince={citiesProvince || []}
                citiesLegalProvince={citiesLegalProvince || []}
                citiesWorkingProvince={citiesWorkingProvince || []}
                citiesSpouseWorkingProvince={citiesSpouseWorkingProvince || []}
                appId={this.props.match.params.id}
            />
        );
        const renderPersonalData = () => (
            <PersonalDataSection
                entity={entity}
                handleNext={this.handleNext}
                handlePrevious={this.handlePrevious}
                citiesProvince={citiesProvince || []}
                errors={{}}
                onChange={this.onChange}
                onChangeProvince={this.onChangeProvince}
                onChangeCity={this.onChangeCity}
                handleSubmit={this.handleSubmit}
                renderDictamen
            />
        );
        const renderInsurance = () => (
            <InsuranceSection
                entity={entity}
                onChange={this.onChange}
                handlePrevious={this.handlePrevious}
                handleNext={this.handleNext}
                handleSubmit={this.handleSubmit}
                ultuTableObject={this.state.ultuTableObject}
                onUltuTableLoaded={this.onUltuTableLoaded}
            />
        );

        const renderFinish = () => (
            <Finish
                handlePrevious={this.handlePrevious}
                handleNext={this.handleNext}
                entity={entity}
                config={app.config}
            />
        );

        if (!entity) return <p style={{margin: '1rem'}}>Loading</p>;

        return (
            <div>
                <Switch>
                    <Redirect from={match.path + '/'} exact to={match.url + '/personal-data-all'} />
                    <Route exact path={match.path + '/loan-data'} render={renderDictamen} />
                    <Route exact path={match.path + '/personal-data-all'} render={renderApplication} />
                    <Route exact path={match.path + '/personal-data'} render={renderPersonalData} />
                    <Route exact path={match.path + '/insurance-data'} render={renderInsurance} />
                    <Route exact path={match.path + '/finish'} render={renderFinish} />
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = state => ({entities: state.entities, app: state.app});
export default connect(mapStateToProps)(withRouter(LoanApplicationSteps));
