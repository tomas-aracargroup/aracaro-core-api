import React from 'react';
import Steps from './Steps';
import LoanHeader from './LoanHeader';
import {Button, Icon} from 'antd';
import {approvalLetter} from '../../prints/approvalLetter';
import {withRouter} from 'react-router';

export default withRouter(props => {
    const {entity, handlePrevious} = props;

    const ruta = props.match.url.substr(0, props.match.url.indexOf('/finish'));

    return (
        <div>
            <LoanHeader
                entity={entity}
                handlePrevious={handlePrevious}
                handleNext={() => approvalLetter(entity)}
                current={3}
            />
            <Steps current={3} />
            <div className="finish-message">
                <h2>Excelente!</h2>
                <p>Ya está generada la solicitud de {entity.firstName}.</p>
                <Button type="primary" className="loan-imprimir" onClick={() => approvalLetter(entity)}>
                    <Icon type={'printer'} />
                    Imprimir Carta de Aprobación
                </Button>
                <Button
                    type="primary"
                    className="loan-imprimir ml-2"
                    onClick={() => props.history.push(`${ruta}/personal-data-all`)}>
                    Ver Solicitud Completa
                </Button>
            </div>
        </div>
    );
});
