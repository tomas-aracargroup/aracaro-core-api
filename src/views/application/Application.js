import React, {Component} from 'react';
import {Input, Button, Icon, notification, Dropdown, Menu, Tabs} from 'antd';
import ApplicationDataClient from './ApplicationDataClient';
import ApplicationDataAddress from './ApplicationDataAddress';
import ApplicationDataLegalAddress from './ApplicationDataLegalAddress';
import ApplicationDataSpouse from './ApplicationDataSpouse';
import ApplicationDataWork from './ApplicationDataWork';
import ApplicationDataWorkSpouse from './ApplicationDataWorkSpouse';
import ApplicationDataTaxes from './ApplicationDataTaxes';
import ApplicationDataReferrals from './ApplicationDataReferrals';
import ApplicationDataProductDetail from './ApplicationDataProductDetail';
import ApplicationDataProductDetailCarInsurance from './ApplicationDataProductDetailCarInsurance';
import ApplicationDataProductDetailCar from './ApplicationDataProductDetailCar';
import CompleteForm from './CompleteForm';
import {approvalLetter} from '../../prints/approvalLetter';
import {applicationLetter} from '../../prints/applicationLetter';
import {form03} from '../../prints/form03';
import {form03Pdf} from '../../prints/form03Pdf';
import {contract} from '../../prints/contract';
import {post} from '../../api/index';
import {getAgeWithParenthesis} from '../../helpers';

let enumLoading = [{id: 'loading', name: 'loading...'}];

class Application extends Component {
    state = {
        provinces: enumLoading,
        marcas: [],
        citiesBuenosAires: [],
        submitted: false
    };

    constructor() {
        super();
        (async () => {
            let marcas = await post('queries/marca/list', {});
            this.setState({marcas: [{id: '', name: ''}, ...marcas]});
        })();
    }

    handlePrint = key => {
        const {entity} = this.props;
        const keyNotification = `open${Date.now()}`;

        const btnClickForm03 = withBackground => {
            notification.close(keyNotification);
            if (withBackground) {
                form03Pdf(entity);
            } else {
                form03(entity)
                    .then(x => {
                        if (x.status === 'OK') {
                            notification.success({message: 'Impresión Finalizada.', duration: 1.5});
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        notification.error({message: 'Error en la Impresión.', duration: 3});
                    });
            }
        };

        const buttonsForm03 = (
            <span>
                <Button type="primary" className="mr-2" size="small" onClick={() => btnClickForm03(true)}>
                    VISTA PREVIA
                </Button>

                <Button type="primary" size="small" onClick={() => btnClickForm03(false)}>
                    FORMULARIO
                </Button>
            </span>
        );

        const btnClickContract = () => {
            notification.close(keyNotification);
            contract(entity)
                .then(x => {
                    if (x.status === 'OK') {
                        notification.success({message: 'Impresión Finalizada.', duration: 1.5});
                    }
                })
                .catch(err => {
                    console.log(err);
                    notification.error({message: 'Error en la Impresión.', duration: 3});
                });
        };

        const buttonsContract = (
            <Button type="primary" size="small" onClick={() => btnClickContract()}>
                IMPRIMIR
            </Button>
        );
        if (key.key === 'approval-letter') {
            approvalLetter(entity);
        } else if (key.key === 'application-letter') {
            applicationLetter(entity);
        } else if (key.key === 'form-03') {
            notification.open({
                message: 'Imprimir Prenda',
                description: `Por favor revise la Bandeja Manual de su impresora y coloque el Formulario 03.
                    ¿Como desea imprimir el Formulario de Prenda?`,
                btn: buttonsForm03,
                key: keyNotification,
                duration: 7
            });
        } else {
            notification.open({
                message: 'Imprimir Contrato',
                description: 'Por favor revise la Bandeja de su impresora y coloque el Formulario de Contrato.',
                btn: buttonsContract,
                key: keyNotification,
                duration: 7
            });
        }
    };

    onSubmit = () =>
        this.props
            .handleSubmit()
            .then(() => {
                notification.success({message: 'Datos salvados', duration: 1.5});
            })
            .catch(err => {
                notification.error({message: 'Ha ocurrido un error', duration: 1.5});
            });

    render() {
        const {
            entity,
            onChange,
            citiesProvince,
            citiesLegalProvince,
            citiesWorkingProvince,
            citiesSpouseWorkingProvince,
            onChangeProvince,
            onChangeCity,
            iconSaving
        } = this.props;

        if (!entity['vehicleModelForm03'] || entity['vehicleModelForm03'] === '') {
            entity['vehicleModelForm03'] = entity['vehicleModel'];
        }

        const {marcas} = this.state;
        const textField = (name, placeholder, disabled = false) => {
            return (
                <Input
                    placeholder={placeholder}
                    value={entity[name]}
                    disabled={disabled}
                    onChange={e => onChange(name, e.target.value)}
                />
            );
        };

        const menu = (
            <Menu onClick={this.handlePrint}>
                <Menu.Item key="approval-letter">CARTA APROBACIÓN</Menu.Item>
                <Menu.Item key="application-letter">SOLICITUD DE PRÉSTAMO</Menu.Item>
                <Menu.Item key="form-03">PRENDA</Menu.Item>
                <Menu.Item key="contract">CONTRATO</Menu.Item>
            </Menu>
        );

        const TabPane = Tabs.TabPane;

        return (
            <div>
                <div className="loan-header">
                    <div className="loan-header__client">
                        <h2 className="loan-header__title">
                            {entity['firstName']} {entity['lastName']} {getAgeWithParenthesis(entity['dob'])}{' '}
                        </h2>
                    </div>
                    <div className="loan-header__buttons">
                        <Dropdown overlay={menu}>
                            <Button className={'ml-2'}>
                                IMPRIMIR <Icon type="down" />
                            </Button>
                        </Dropdown>
                        <Button type={'primary'} className="ml-2" onClick={this.onSubmit} loading={iconSaving}>
                            GUARDAR
                        </Button>
                    </div>
                </div>

                <Tabs defaultActiveKey="1" style={{margin: '1rem 2rem 2rem'}}>
                    <TabPane tab="Datos Completos" key="1">
                        <ApplicationDataProductDetail entity={entity} onChange={onChange} textField={textField} />
                        <ApplicationDataClient entity={entity} onChange={onChange} textField={textField} />
                        <ApplicationDataAddress
                            onChangeCity={onChangeCity}
                            onChangeProvince={onChangeProvince}
                            citiesProvince={citiesProvince}
                            entity={entity}
                            onChange={onChange}
                            textField={textField}
                        />
                        <ApplicationDataLegalAddress
                            onChangeCity={onChangeCity}
                            onChangeProvince={onChangeProvince}
                            citiesLegalProvince={citiesLegalProvince}
                            entity={entity}
                            onChange={onChange}
                            textField={textField}
                        />
                        <ApplicationDataSpouse entity={entity} onChange={onChange} textField={textField} />
                        <ApplicationDataWork
                            entity={entity}
                            onChange={onChange}
                            citiesWorkingProvince={citiesWorkingProvince}
                            onChangeProvince={onChangeProvince}
                            onChangeCity={onChangeCity}
                            textField={textField}
                        />
                        <ApplicationDataWorkSpouse
                            entity={entity}
                            onChange={onChange}
                            citiesSpouseWorkingProvince={citiesSpouseWorkingProvince}
                            onChangeProvince={onChangeProvince}
                            onChangeCity={onChangeCity}
                            textField={textField}
                        />
                        <ApplicationDataTaxes entity={entity} onChange={onChange} textField={textField} />
                        <ApplicationDataReferrals entity={entity} onChange={onChange} textField={textField} />
                        <h2 className="product-detail-title">Detalle de Producto:</h2>
                        <ApplicationDataProductDetailCarInsurance
                            entity={entity}
                            onChange={onChange}
                            textField={textField}
                        />
                        <ApplicationDataProductDetailCar
                            entity={entity}
                            onChange={onChange}
                            marcas={marcas}
                            textField={textField}
                        />
                    </TabPane>
                    <TabPane tab="Subir Documentación" key="2">
                        <CompleteForm appId={this.props.appId} />
                    </TabPane>
                </Tabs>
            </div>
        );
    }
}

export default Application;
