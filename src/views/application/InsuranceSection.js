import React, {Component} from 'react';
import InsuranceAutoQuery from '../insurance/InsuranceAutoQuery';
import Steps from './Steps';
import LoanHeader from './LoanHeader';

class InsuranceSection extends Component {
    state = {
        submitted: false
    };

    handleNext = () => {
        const {entity, handleSubmit, handleNext} = this.props;

        this.setState({submitted: true});
        if (entity.vehicleInsuranceId) {
            handleSubmit();
            handleNext('finish');
        }
    };

    render() {
        const {entity, onChange, handlePrevious, ultuTableObject, onUltuTableLoaded} = this.props;

        return (
            <div>
                <LoanHeader
                    title="Generar Solicitud"
                    entity={entity}
                    handlePrevious={handlePrevious}
                    handleNext={() => this.handleNext()}
                />
                <Steps current={2} />
                <InsuranceAutoQuery
                    entity={entity}
                    onChange={onChange}
                    ultuTableObject={ultuTableObject}
                    onUltuTableLoaded={onUltuTableLoaded}
                    submitted={this.state.submitted}
                />
            </div>
        );
    }
}

export default InsuranceSection;
