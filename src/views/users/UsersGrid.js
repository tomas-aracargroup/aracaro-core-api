import React from 'react';
import Grid from '../../components/Grid';

const columns = [
    {name: 'email', label: 'e-mail', cell: 'link', searchable: true, collectionName: 'users'},
    {name: 'firstname', label: 'Nombre', searchable: true},
    {name: 'lastname', label: 'Apellido', searchable: true}
];

const title = 'Aracar Control Panel / Usuarios';

class Users extends React.Component {
    render() {
        const props = this.props;
        return (
            <Grid
                className={'users'}
                createRoute="/users/create"
                title={title}
                columns={columns}
                forceRefresh={props.history.location.state && props.history.location.state.forceRefresh}
                collectionName="users"
                endPoint="users"
                sortBy={{lastname: 1}}
            />
        );
    }
}

export default Users;
