import React from 'react';
import {Form, Button, Col, Row} from 'antd';
import fieldResolver from '../../components/Fields/fieldResolver';
import validator from '../../helpers/validator';
import {withRouter} from 'react-router';
import {Link} from 'react-router-dom';
import {notification} from 'antd/lib/index';
import {getByID, put, post} from '../../api';
import enums from '../../helpers/enumerators';

const FormItem = Form.Item;

const fields = [
    {
        name: 'firstname',
        label: 'Nombre',
        required: true,
        letterOnly: true,
        min: 2,
        max: 50
    },
    {
        name: 'lastname',
        label: 'Apellido',
        required: true,
        letterOnly: true,
        min: 2,
        max: 50
    },
    {name: 'email', type: 'email', required: true},
    {name: 'password', label: 'Clave', type: 'password', required: true, min: 4},
    {
        name: 'tasks',
        type: 'selectTags',
        required: true,
        label: 'Tareas',
        enum: enums.eTASK
    },

    {
        name: 'role',
        type: 'select',
        required: true,
        label: 'Role',
        enum: enums.eROLE.sort((x, y) => x.description > y.description)
    },
    {
        name: 'mails',
        type: 'selectTags',
        label: 'Suscripción Mails',
        enum: enums.eMAIL_SUBSCRIPTIONS.sort((x, y) => x.name > y.name)
    },
    {name: 'enabled', label: 'Habilitado', type: 'checkbox'}
];

const defaultLayout = {sm: {span: 24}, md: {span: 12}, xl: {span: 12}};

class UserForm extends React.Component {
    constructor(props) {
        super();
        const id = props.match.params.id;
        if (id) {
            this.state = {fetching: true};
            getByID('users', id).then(response => this.setState({entity: response, fetching: false}));
        } else {
            this.state = {entity: {enabled: true}};
        }
    }

    onChange = (name, value) => {
        let {entity} = this.state;
        entity[name] = value;
        this.setState({entity});
    };

    onSubmit = () => {
        const payload = this.state.entity;
        const promise = payload._id ? put('user/update', payload, payload._id) : post('user/add', payload);
        promise
            .then(() => notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5}))
            .catch(e => notification.error({message: e.message, duration: 1.5}));
    };

    render() {
        const {entity, submitted, fetching} = this.state;

        if (fetching) {
            return null;
        }

        return (
            <Row className="channelsform">
                <Col lg={16}>
                    <h2>Usuario</h2>
                    <Form>
                        <fieldset>
                            {fields.map(x => {
                                const Input = fieldResolver(x);
                                return (
                                    <Col {...defaultLayout} key={x.name}>
                                        <Input
                                            required={x.required}
                                            onChange={this.onChange}
                                            options={x.enum}
                                            entity={entity}
                                            name={x.name}
                                            label={x.label}
                                            placeholder={x.placeholder}
                                            type={x.type}
                                            error={validator(entity, fields)[x.name]}
                                            submitted={submitted}
                                        />
                                    </Col>
                                );
                            })}
                            <FormItem className="save">
                                <Link className="ant-btn ant-btn-secondary" to={'/grids/user'}>
                                    Volver
                                </Link>
                                <Button type="primary" onClick={this.onSubmit}>
                                    Guardar
                                </Button>
                            </FormItem>
                        </fieldset>
                    </Form>
                </Col>
            </Row>
        );
    }
}

export default withRouter(UserForm);
