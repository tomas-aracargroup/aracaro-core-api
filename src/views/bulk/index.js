import React from 'react';
import {Button} from 'antd';
import {Link} from 'react-router-dom';
import {get} from '../../api/dealers';

const primaryButtonLabel = 'Bajar archivo modelo',
    fileName = 'ejemplo-archivo-para-carga-masiva-clientes.csv',
    sectionTitle = 'Ejemplo';

class Bulk extends React.Component {
    onDownload = () => {
        get(`bulk/example`).then(response => {
            const link = document.createElement('a');
            link.href = window.URL.createObjectURL(new Blob([response]));
            link.setAttribute('download', fileName);
            document.body.appendChild(link);
            link.click();
        });
    };

    render() {
        return (
            <div>
                <div className="grid-header">
                    <h2>
                        <Link to={'/carga-masiva'}>{'Carga Masiva'}</Link> / {sectionTitle}
                    </h2>
                </div>
                <div className={'ml-3'}>
                    <Button
                        onClick={this.onDownload}
                        style={{textTransform: 'uppercase'}}
                        type={'primary'}
                        icon={'download'}>
                        {primaryButtonLabel}
                    </Button>
                </div>
            </div>
        );
    }
}

export default Bulk;
