import React from 'react';
import {Table} from 'antd';
import {formatNumber} from 'accounting';
import moment from 'moment';
import 'moment/locale/es';

moment.locale('es');
const {Column} = Table;
const format = collection =>
    collection.map(item => {
        const formattedItem = {};
        Object.keys(item).forEach(
            key =>
                (formattedItem[key] =
                    typeof item[key] === 'number'
                        ? formatNumber(item[key], 0, '.')
                        : moment(item.date)
                              .locale('es')
                              .format('LL'))
        );
        return formattedItem;
    });

export default ({collection, headers}) => (
    <Table pagination={false} dataSource={format(collection)}>
        {headers.map(header => <Column title={header.title} dataIndex={header.dataIndex} key={header.dataIndex} />)}
    </Table>
);
