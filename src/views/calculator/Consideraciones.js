import React from 'react';

export default () => (
    <section>
        <h3>Consideraciones</h3>
        <ul>
            <li>Sistema de amortizacion frances</li>
            <li>Devengamiento de intereses diario</li>
            <li>Base anual para calculo de interes 360 dias</li>
            <li>Sin quebranto fijo ni variable</li>
            <li>Sin impuestos</li>
            <li>Sin seguro de vida</li>
        </ul>
    </section>
);
