import React from 'react';
import {Col, Form, Switch, Row} from 'antd';
import {getPaymentDates, getPeriodRate, calculateIRR} from '../../helpers/financial';
import NumberField from '../../components/Fields/NumberField';
import DecimalField from '../../components/Fields/DecimalField';
import Table from './Table';
import {formatNumber} from 'accounting';

class Calculadora extends React.Component {
    state = {
        entity: {
            tna: 18,
            iva: false,
            principal: 300000,
            term: 60,
            periodsInYear: 12,
            relativeExpenses: 0,
            fixedExpenses: 0,
            payDay: 5
        }
    };

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    render() {
        const entity = this.state.entity;
        const {tna, principal, periodsInYear, relativeExpenses, fixedExpenses, payDay, iva} = entity;

        const term = entity.term < 120 ? entity.term : 120;

        const finalPrincipal = principal * (1 + Number(relativeExpenses) / 100) + Number(fixedExpenses);
        const periodRate = getPeriodRate(tna / 100, periodsInYear);

        const payments = getPaymentDates({principal: finalPrincipal, rate: periodRate, term, cutDate: payDay}, iva);

        const itemLayout = {labelCol: {span: 12}, wrapperCol: {span: 12}};
        const fieldBuilder = (name, label) => (
            <NumberField
                itemLayout={itemLayout}
                min={0}
                entity={entity}
                label={label}
                name={name}
                onChange={this.onChange}
            />
        );

        return (
            <Col className="calculator">
                <Row>
                    <Col span={4}>
                        <Form layout="vertical">
                            {fieldBuilder('principal', 'Capital ($)')}
                            <DecimalField
                                itemLayout={itemLayout}
                                min={0}
                                max={100}
                                entity={entity}
                                label={'TNA (%)'}
                                name={'tna'}
                                onChange={this.onChange}
                            />
                            <NumberField
                                itemLayout={itemLayout}
                                min={1}
                                max={120}
                                entity={entity}
                                label="Plazo"
                                name="term"
                                onChange={this.onChange}
                            />
                            {fieldBuilder('relativeExpenses', 'Gastos Variables (%)')}
                            {fieldBuilder('fixedExpenses', 'Gastos Fijos ($)')}
                            <Form.Item label="IVA:" labelCol={{span: 12}} wrapperCol={{span: 12}}>
                                <Switch defaultChecked={iva} onChange={val => this.onChange('iva', val)} />
                            </Form.Item>
                            <p style={{fontWeight: 'bold', textAlign: 'right'}}>
                                CFT: {formatNumber(calculateIRR(principal, payments), 2, '.', ',')}%
                            </p>
                        </Form>
                    </Col>
                    <Col span={20}>
                        <Table
                            collection={payments}
                            headers={
                                iva
                                    ? [
                                          {title: 'Fecha', dataIndex: 'date'},
                                          {title: 'Período #', dataIndex: 'n'},
                                          {title: 'Capital', dataIndex: 'remainingPrincipal'},
                                          {title: 'Amortización', dataIndex: 'amortization'},
                                          {title: 'Interés', dataIndex: 'interest'},
                                          {title: 'IVA', dataIndex: 'iva'},
                                          {title: 'Total', dataIndex: 'periodPayment'}
                                      ]
                                    : [
                                          {title: 'Fecha', dataIndex: 'date'},
                                          {title: 'Período #', dataIndex: 'n'},
                                          {title: 'Capital', dataIndex: 'remainingPrincipal'},
                                          {title: 'Amortización', dataIndex: 'amortization'},
                                          {title: 'Interés', dataIndex: 'interest'},
                                          {title: 'Total', dataIndex: 'periodPayment'}
                                      ]
                            }
                        />
                    </Col>
                </Row>
            </Col>
        );
    }
}

export default Calculadora;
