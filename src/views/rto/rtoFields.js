import enums from '../../helpers/enumerators';
import {ACQUISITION_CHANNEL, CAR_USAGE_WHEN_LEAVING} from '../../constants/rtoEnumerators';

export default [
    {
        title: 'General',
        fields: [
            {
                name: 'documentNumber',
                label: 'Documento',
                placeholder: '10.200.300',
                required: true,
                type: 'number',
                min: 100000,
                max: 99999999
            },
            {name: 'gender', enum: enums.eGENERO, required: true, type: 'radioButton', label: 'Género'},
            {name: 'firstName', label: 'Nombre'},
            {name: 'lastName', label: 'Apellido'},
            {name: 'email', label: 'Mail'},
            {name: 'phoneNumber', label: 'Teléfono'},
            {name: 'yearsDriving', label: 'Años Manejando', type: 'number', min: 0, max: 35},
            {name: 'nextContactDate', label: 'Próximo Contacto', type: 'date'},
            // {name: 'disbursementDate', label: 'Fecha Desembolso', type: 'date'}, //todo revisar
            {name: 'acquisitionChannel', label: 'Canal Adquisición', type: 'select', enum: ACQUISITION_CHANNEL},
            {name: 'referredBy', label: 'Referido por'}
        ]
    },
    {
        title: 'Score Crediticio',
        readOnly: true,
        fields: [
            {name: 'bancarized', type: 'string', label: 'Bancarizado'},
            {name: 'employer', label: 'Empleador', readOnly: true},
            {name: 'siisaName', label: 'Nombre SIISA', readOnly: true},
            {name: 'beSmartBCRA', label: 'BCRA'},
            {name: 'beSmartBCRA_2_3', label: 'BCRA 2_3'},
            {name: 'beSmartBCRA_4_6', label: 'BCRA 4_6'},
            {name: 'beSmartBCRA_7_12', label: 'BCRA 7_12'},
            {name: 'beSmartSubproducto', label: 'Subproducto'},
            {name: 'beSmartRiesgoCategoria', label: 'Riesgo Categoria'},
            {name: 'beSmartExplicacion', label: 'Explicacion'},
            {name: 'beSmartMotivo', label: 'Motivo'},
            {name: 'beSmartResult', label: 'Resultado'},
            {name: 'monthlyIncome', label: 'Ingresos', type: 'money'},
            {name: 'monthlyCompromises', label: 'Compromisos', type: 'money'},
            {name: 'nosisScore', label: 'NOSIS', type: 'number'},
            {name: 'siisaScore', label: 'SIISA', type: 'number'}
        ]
    },
    {
        title: 'Scores',
        fields: [
            {name: 'russellCarScore', label: 'Russel Car', type: 'rate', min: 0, max: 10},
            {name: 'aracarScore', label: 'Aracar', type: 'rate', min: 0, max: 10}
        ]
    },
    {
        title: 'Vehículo',
        fields: [
            {name: 'vehicleBrand', label: 'Marca'},
            {name: 'vehicleModel', label: 'Modelo'},
            {name: 'vehicleYear', label: 'Año'},
            {name: 'vehicleValue', label: 'Valor'},
            {name: 'carUsageWhenLeaving', label: 'Uso', type: 'select', enum: CAR_USAGE_WHEN_LEAVING}
        ]
    }
];
