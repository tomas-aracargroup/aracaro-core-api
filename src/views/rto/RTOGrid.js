import React from 'react';
import Table from '../../components/Grid';
import {post} from '../../api/dealers';
import getStatusCellClass from '../../helpers/getStatusCellClass';

const resolveStyle = (col, item) => getStatusCellClass(item[col.name]);

const resolveStyleStatusDays = (col, item) => {
    const itemValue = item[col.name];
    if (itemValue >= 4) {
        return 'status reject';
    } else {
        return 'status success';
    }
};

const columns = [
    {
        name: 'documentNumber',
        label: 'Documento',
        cell: 'link',
        searchable: true,
        searchType: 'number',
        collectionName: 'entity/rto'
    },
    {name: 'firstName', label: 'Nombre', searchable: true},
    {name: 'lastName', label: 'Apellido', searchable: true},
    {name: 'isDriver', label: 'Es Conductor', searchable: true},
    {name: 'email', label: 'Mail', searchable: true},
    {name: 'phoneNumber', label: 'Teléfono', searchable: true},
    {name: 'vehicleBrand', label: 'Marca', searchable: true},
    {name: 'vehicleModel', label: 'Modelo', searchable: true},
    {name: 'vehicleYear', label: 'Año', searchable: true},
    {name: 'vehicleValue', label: 'Valor', cell: 'money'},
    {name: 'bancarized', cell: 'string', label: 'Bancarizado', searchable: true},
    {name: 'employer', label: 'Empleador', searchable: true},
    {name: 'beSmartBCRA', label: 'BCRA', searchable: true},
    {name: 'beSmartBCRA_2_3', label: 'BCRA 2_3', searchable: true},
    {name: 'beSmartBCRA_4_6', label: 'BCRA 4_6', searchable: true},
    {name: 'beSmartBCRA_7_12', label: 'BCRA 7_12', searchable: true},
    {name: 'beSmartSubproducto', label: 'Subproducto', searchable: true},
    {name: 'beSmartRiesgoCategoria', label: 'Riesgo Categoria', searchable: true},
    {name: 'beSmartExplicacion', label: 'Explicacion', searchable: true},
    {name: 'beSmartMotivo', label: 'Motivo', searchable: true},
    {name: 'beSmartResult', label: 'Resultado', searchable: true},
    {name: 'referralCode', label: 'Referido', searchable: true},
    {name: 'notes', label: 'Historial de Notas', cell: 'text', hideInColumnPicker: true},

    {
        name: 'status',
        label: 'Status',
        cell: 'action',
        modalName: 'rtoStatusHistory',
        searchable: true,
        resolveStyle: resolveStyle
    },
    {
        name: 'notesLastMessage',
        label: 'Notas',
        cell: 'action',
        modalName: 'rtoNotes',
        searchable: true
    },
    {
        name: 'statusDays',
        label: 'Días del Status',
        cell: 'number',
        resolveStyle: resolveStyleStatusDays
    },
    {name: 'statusHistory', label: 'Historial de Status', cell: 'text', hideInColumnPicker: true},
    {name: 'monthlyIncome', label: 'Ingresos', cell: 'number'},
    {name: 'monthlyCompromises', label: 'Compromisos', cell: 'money'},
    {name: 'nosisScore', label: 'NOSIS', cell: 'number', searchable: true},
    {name: 'siisaScore', label: 'SIISA', cell: 'number', searchable: true},
    {name: 'creationDate', label: 'Creado', cell: 'timeSince', searchable: true},
    {name: 'aracarScore', label: 'Aracar Score', cell: 'number'},
    {name: 'russellCarScore', label: 'Russell Car Score', cell: 'number'},
    {name: 'yearsDriving', label: 'Años Manejando', cell: 'number'},
    {name: 'carUsageWhenLeaving', label: 'Uso'},
    {name: 'acquisitionChannel', label: 'Canal Adquisición'},
    {name: 'nextContactDate', label: 'Próximo Contacto'},
    {name: 'referredBy', label: 'Referido Por'}
];

export default () => {
    return (
        <Table
            className="sessions"
            title={'RTO / Aplicaciones'}
            columnPicker
            post={post}
            columns={columns}
            downloadDealers
            lean={false}
            collectionName="rtos"
            endPoint="rtos"
            sortBy={{_id: -1}}
        />
    );
};
