import React from 'react';
import CommentsModal from '../../components/CommentsModal';
import {put} from '../../api/dealers';

export default props => <CommentsModal {...props} endPoint="rto/addNote" collectionName="rtos" putFunction={put} />;
