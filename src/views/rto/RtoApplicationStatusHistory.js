import React from 'react';
import StatusModal from '../../components/StatusModal';
import {put} from '../../api/dealers';
import {eAPP_STATUS} from '../../constants/rtoEnumerators';

const options = eAPP_STATUS.map(option => option.name);

export default props => (
    <StatusModal {...props} endPoint="rto/changeStatus" collectionName="rtos" options={options} putFunction={put} />
);
