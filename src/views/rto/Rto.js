import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as appActions from '../../actions/appActions';
import {Row, Col, Select} from 'antd';
import {getByID, put} from '../../api/dealers';
import EditableCard from '../../components/EditableCard';
import NotesCard from '../../components/NotesCard';
import getStatus from '../../helpers/getStatusCellClass';
import {eAPP_STATUS} from '../../constants/rtoEnumerators';
import cards from './rtoFields';
import {Link} from 'react-router-dom';
import {notification} from 'antd/lib/index';
import DocumentationCard from '../../components/DocumentationCard';

const Option = Select.Option;

const label = 'Candidatos Rent To Own';
const modelName = 'rto';
const dataGridLink = '/grids/rto';

const filesList = [
    {key: 'fileDocumentFront', label: 'DNI Frente'},
    {key: 'fileDocumentBack', label: 'DNI Reverso'},
    {key: 'fileDriversLicense', label: 'Licencia de Conducir'},
    {key: 'fileDriversPhoto', label: 'Foto'}
];

class Customer extends React.Component {
    state = {fetching: true, entity: {}, cardsState: cards};

    constructor(props) {
        super();
        getByID('rtos', props.match.params.id).then(response => {
            this.setState({
                _id: response._id,
                entity: response,
                fetching: false
            });
        });
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    onSubmit = () =>
        put(modelName, this.state.entity, this.state._id)
            .then(() => notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5}))
            .catch(e => notification.error({message: e.message, duration: 1.5}));

    addNote = note => {
        const {user} = this.props.app.auth;
        let {entity} = this.state;

        entity.notes.push({
            text: note,
            user: user.firstname + ' ' + user.lastname,
            timeStamp: new Date()
        });
        entity.notesLastMessage = note;
        put(`rto`, entity, entity._id);
        this.setState({entity});
    };

    handleSubmit = async () => {
        const {entity} = this.state;
        this.setState({iconSaving: true, submmited: true});
        let data;
        try {
            data = await put(`rto`, entity, entity._id);

            this.props.actions.updateDocument('rtos', entity);
            notification.success({message: 'DATOS SALVADOS CORRECTAMENTE', duration: 1.5});
        } catch (err) {
            notification.error({message: 'ERROR AL INTENTAR GUARDAR LOS DATOS', duration: 1.5});
            console.log(err);
        } finally {
            this.setState({iconSaving: false, submmited: false});
        }
        return data;
    };

    handleChangeStatus = value => {
        this.onChange('status', value);
        this.handleSubmit();
    };

    render() {
        const {entity, cardsState} = this.state;
        const {match} = this.props;

        return (
            <div>
                <div className="grid-header">
                    <h2>
                        <Link to={dataGridLink}>{label}</Link> / {entity.firstName} {entity.lastName}
                    </h2>
                    <Select
                        value={entity.status}
                        onChange={this.handleChangeStatus}
                        className={`status ${getStatus(entity.status)}`}>
                        {eAPP_STATUS.map(item => (
                            <Option key={item.id} value={item.id}>
                                {item.name}
                            </Option>
                        ))}
                    </Select>
                </div>
                <div className="manual-application">
                    <Row gutter={16}>
                        <Col lg={24}>
                            <div className="card-flex-wrapper">
                                <DocumentationCard
                                    modelName="rto"
                                    entityId={match.params.id}
                                    title="Documentación"
                                    filesList={filesList}
                                />
                                {cardsState.map(card => (
                                    <EditableCard
                                        key={card.title}
                                        fields={card.fields}
                                        readOnly={card.readOnly}
                                        onExitEditMode={this.onSubmit}
                                        entity={entity}
                                        onChange={this.onChange}
                                        className="card-custom-width"
                                        title={card.title}
                                    />
                                ))}

                                <NotesCard
                                    title="Notas"
                                    notes={entity.notes}
                                    onAddNote={this.addNote}
                                    className="card-custom-width"
                                    width="500px"
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapState = state => ({app: state.app});
const mapDispatch = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    mapState,
    mapDispatch
)(Customer);
