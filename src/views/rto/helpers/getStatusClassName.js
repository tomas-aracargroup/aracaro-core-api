export default status => {
    switch (status) {
        case '1 - ABIERTO':
        case '1 - INTENTANDO CONTACTAR':
        case '1 - ENTREVISTA AGENDADA':
        case '1 - ENTREVISTADO':
        case '1 - EN TRAMITE AFIP/SACTA':
            return 'status__preaprobado';

        case '2 - NO CALIFICA':
        case '2 - NO INTERESADO POR AUTO':
        case '2 - NO INTERESADO POR PRECIO':
        case '2 - NO INTERESADO OTRO':
            return 'status__rechazado';

        default:
            return 'status__verificar';
    }
};
