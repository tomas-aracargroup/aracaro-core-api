import React from 'react';
import Table from '../../components/Grid';
import {post} from '../../api/dealers';

const columns = [
    {
        name: 'authStrategyAvatarUrl',
        label: '-',
        cell: 'avatar',
        searchable: true,
        collectionName: 'entity/online-customer'
    },
    {name: 'firstName', label: 'Nombre', searchable: true},
    {name: 'lastName', label: 'Apellido', searchable: true},

    {name: 'bs_ciAntiguedad'},
    {name: 'bs_ciBancarizado'},
    {name: 'bs_ciVigCompMensual'},
    {name: 'bs_coefTasa'},
    {name: 'bs_coefThin'},
    {name: 'bs_cq12MCant'},
    {name: 'bs_cq24MCant'},
    {name: 'bs_cq60MCant'},
    {name: 'bs_cuil'},
    {name: 'bs_cuotaMax'},
    {name: 'bs_cuotasSum'},
    {name: 'bs_destinoCredito'},
    {name: 'bs_deudaActual'},
    {name: 'bs_dfTiene'},
    {name: 'bs_diasAtraso'},
    {name: 'bs_dictamen'},
    {name: 'bs_documento'},
    {name: 'bs_email'},
    {name: 'bs_estadoOk'},
    {name: 'bs_explicacion'},
    {name: 'bs_fechaDictamen'},
    {name: 'bs_garante'},
    {name: 'bs_ingresoBaseInterna'},
    {name: 'bs_ingresoComputado'},
    {name: 'bs_ingresoDeclarado'},
    {name: 'bs_ingresoEstimado'},
    {name: 'bs_ingresoVerificado'},
    {name: 'bs_ju12MCant'},
    {name: 'bs_ju24MCant'},
    {name: 'bs_ju60MCant'},
    {name: 'bs_licenciaTaxiNumero'},
    {name: 'bs_ltvFinal'},
    {name: 'bs_monto'},
    {name: 'bs_montoMaximoIngreso'},
    {name: 'bs_montoMaximoVehiculo'},
    {name: 'bs_motivo'},
    {name: 'bs_nacido'},
    {name: 'bs_nombre'},
    {name: 'bs_nosEstadoTransaccion'},
    {name: 'bs_nosMensajeTransaccion'},
    {name: 'bs_novedad'},
    {name: 'bs_nse'},
    {name: 'bs_plazoMaxAntig'},
    {name: 'bs_porcentajeCuotasPagas'},
    {name: 'bs_pq12MCant'},
    {name: 'bs_pq24MCant'},
    {name: 'bs_pq60MCant'},
    {name: 'bs_producto'},
    {name: 'bs_productoCod'},
    {name: 'bs_provincia'},
    {name: 'bs_puntoVenta'},
    {name: 'bs_rc12MCant'},
    {name: 'bs_rc12MFuente'},
    {name: 'bs_rc24MTiene'},
    {name: 'bs_rc6MTiene'},
    {name: 'bs_rci'},
    {name: 'bs_riesgoCategoria'},
    {name: 'bs_riesgoNivel'},
    {name: 'bs_sco12M'},
    {name: 'bs_sco6M'},
    {name: 'bs_siAutonomo'},

    {name: 'bs_siChequesBcra'},
    {name: 'bs_siCodigoError'},
    {name: 'bs_siConsEntCant1'},
    {name: 'bs_siConsEntCant12'},
    {name: 'bs_siConsEntCant24'},
    {name: 'bs_siConsEntCant3'},
    {name: 'bs_siConsEntCant6'},
    {name: 'bs_siCreditosVigentesCant'},
    {name: 'bs_siCreditosVigentesMonto'},
    {name: 'bs_siCreditosVigentesSaldo'},
    {name: 'bs_siEmpleador'},
    {name: 'bs_siEmpleadorActividad'},
    {name: 'bs_siEmpleadorAntiguedad'},
    {name: 'bs_siEmpleadorCuit'},
    {name: 'bs_siEmpleadorPcMora'},
    {name: 'bs_siEmpleadorRazonSocial'},
    {name: 'bs_siEmpleadorSector'},
    {name: 'bs_siEstadoTransaccion'},
    {name: 'bs_siFraudesCant12'},
    {name: 'bs_siFraudesCant24'},
    {name: 'bs_siFraudesCant6'},
    {name: 'bs_siFraudesCant60'},
    {name: 'bs_siGanancias'},
    {name: 'bs_siIva'},
    {name: 'bs_siJubiladoPensionado'},
    {name: 'bs_siMensajeTransaccion'},
    {name: 'bs_siMonotributo'},
    {name: 'bs_siRetornoCant'},
    {name: 'bs_siScore'},
    {name: 'bs_siScoreModelo'},
    {name: 'bs_siSes'},
    {name: 'bs_siSesModelo'},
    {name: 'bs_siSociedad'},
    {name: 'bs_situacionLaboralFinal'},
    {name: 'bs_siXtsActividad'},
    {name: 'bs_siXtsApellidoNombre'},
    {name: 'bs_siXtsCatLaboral'},
    {name: 'bs_siXtsCp1'},
    {name: 'bs_siXtsCp2'},
    {name: 'bs_siXtsCp3'},
    {name: 'bs_siXtsCp4'},
    {name: 'bs_siXtsCp5'},
    {name: 'bs_siXtsCpEmp'},
    {name: 'bs_siXtsCuil'},
    {name: 'bs_siXtsDir1'},
    {name: 'bs_siXtsDir2'},
    {name: 'bs_siXtsDir3'},
    {name: 'bs_siXtsDir4'},
    {name: 'bs_siXtsDir5'},
    {name: 'bs_siXtsDomEmp'},
    {name: 'bs_siXtsEntMora1'},
    {name: 'bs_siXtsEntMora2'},
    {name: 'bs_siXtsEntMora3'},
    {name: 'bs_siXtsEntMora4'},
    {name: 'bs_siXtsEntMora5'},
    {name: 'bs_siXtsEstimadorIngresos'},
    {name: 'bs_siXtsFIngreso'},
    {name: 'bs_siXtsFnac'},
    {name: 'bs_siXtsGeoX'},
    {name: 'bs_siXtsGeoY'},
    {name: 'bs_siXtsIdTipoDoc'},
    {name: 'bs_siXtsIndiceActividad'},
    {name: 'bs_siXtsLoc1'},
    {name: 'bs_siXtsLoc2'},
    {name: 'bs_siXtsLoc3'},
    {name: 'bs_siXtsLoc4'},
    {name: 'bs_siXtsLoc5'},
    {name: 'bs_siXtsLocEmp'},
    {name: 'bs_siXtsMesesAntLaboral'},
    {name: 'bs_siXtsNrodoc'},
    {name: 'bs_siXtsProv1'},
    {name: 'bs_siXtsProv2'},
    {name: 'bs_siXtsProv3'},
    {name: 'bs_siXtsProv4'},
    {name: 'bs_siXtsProv5'},
    {name: 'bs_siXtsProvEmp'},
    {name: 'bs_siXtsRectificaciones'},
    {name: 'bs_siXtsScoreDev'},
    {name: 'bs_siXtsSector'},
    {name: 'bs_siXtsSexo'},
    {name: 'bs_siXtsTel1'},
    {name: 'bs_siXtsTel2'},
    {name: 'bs_siXtsTel3'},
    {name: 'bs_siXtsTel4'},
    {name: 'bs_siXtsTel5'},
    {name: 'bs_siXtsTipoEmp'},
    {name: 'bs_solicitudFecha'},
    {name: 'bs_solicitudId'},
    {name: 'bs_subproducto'},
    {name: 'bs_telefono'},
    {name: 'bs_vehiculoAntiguedad'},
    {name: 'bs_vehiculoKm'},
    {name: 'bs_vehiculoMarcaModeloVersion'},
    {name: 'bs_vehiculoTipo'},
    {name: 'bs_vehiculoUsoDeclarado'},
    {name: 'bs_vehiculoUsoFinal'},
    {name: 'bs_vehiculoValor'},
    {name: 'bs_viAct01Cod'},
    {name: 'bs_viAct01Descrip'},
    {name: 'bs_viAct01FecInicio'},
    {name: 'bs_viAct01Sector'},
    {name: 'bs_viAntiguedadLaboral'},
    {name: 'bs_viApellido'},
    {name: 'bs_viDni'},
    {name: 'bs_viDomAfCalle'},
    {name: 'bs_viDomAfCp'},
    {name: 'bs_viDomAfDto'},
    {name: 'bs_viDomAfLoc'},
    {name: 'bs_viDomAfNro'},
    {name: 'bs_viDomAfPiso'},
    {name: 'bs_viDomAfProv'},
    {name: 'bs_viEmpleadoDomesticoEs'},
    {name: 'bs_viEmpleadoEs'},
    {name: 'bs_viFallecidoEs'},
    {name: 'bs_viFecNacimiento'},
    {name: 'bs_viIdentificacion'},
    {name: 'bs_viInscripAutonomo'},
    {name: 'bs_viInscripAutonomoEs'},
    {name: 'bs_viInscripAutonomoFec'},
    {name: 'bs_viInscripMonotributo'},
    {name: 'bs_viInscripMonotributoEs'},
    {name: 'bs_viInscripMonotributoFec'},
    {name: 'bs_viIntegranteSociedadEs'},
    {name: 'bs_viJubiladoEs'},
    {name: 'bs_viNombre'},
    {name: 'bs_viRazonSocialPorcCoinc'},
    {name: 'bs_viSexo'},
    {name: 'bs_viTel1CodArea'},
    {name: 'bs_viTel1Nro'},
    {name: 'bs_viTel2CodArea'},
    {name: 'bs_viTel2Nro'},
    {name: 'bs_viTel3CodArea'},
    {name: 'bs_viTel3Nro'},
    {name: 'bs_viTel4CodArea'},
    {name: 'bs_viTel4Nro'},
    {name: 'bs_viTel5CodArea'},
    {name: 'bs_viTel5Nro'},
    {name: 'bs_scoVig', label: 'Nosis', cell: 'number'},
    {name: 'authStrategy', label: 'Autenticación', searchable: true},

    {
        name: 'documentNumber',
        label: 'Documento',
        searchable: true
    },
    {name: 'createdAt', label: 'Creado', cell: 'timeSince', searchable: true}
];

export default props => {
    return (
        <Table
            collectionName="onlineCustomers"
            columnPicker
            columns={columns}
            endPoint="online-customer"
            filters={{}}
            downloadDealers
            forceRefresh={props.history.location.state && props.history.location.state.forceRefresh}
            post={post}
            sortBy={{_id: -1}}
            title={'Online / Clientes'}
        />
    );
};
