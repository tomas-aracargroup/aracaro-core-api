import React from 'react';
import Table from '../../components/Grid';
import {post} from '../../api/dealers';

const columns = [
    {
        name: 'documentNumber',
        label: 'Documento',
        searchable: true
    },

    {name: 'firstName', label: 'Nombre', searchable: true},
    {name: 'lastName', label: 'Apellido', searchable: true},
    {name: 'email', label: 'Email', searchable: true},
    {name: 'cellPhone', label: 'Apellido', searchable: true},
    {name: 'planType', label: 'Tipo de Plan', searchable: true},
    {name: 'planState', label: 'Estado del Plan', searchable: true},
    {name: 'vehicleBrand', label: 'Marca', searchable: true},
    {name: 'vehicleModel', label: 'Modelo', searchable: true},

    {name: 'vehicleYear', label: 'Año del auto', type: 'number'},

    {name: 'vehicleValue', label: '$ Vehiculo', cell: 'money'},
    {name: 'paymentsMade', label: 'Cantidad  pagos', cell: 'number'},
    {name: 'paymentValue', label: 'Cuota Mensual', cell: 'money'}
];

export default props => {
    return (
        <Table
            title={'Online / Plan de Ahorro'}
            columnPicker
            post={post}
            columns={columns}
            collectionName="plans"
            forceRefresh={props.history.location.state && props.history.location.state.forceRefresh}
            endPoint="plan-de-ahorro"
            downloadDealers
            filters={{}}
            sortBy={{_id: -1}}
        />
    );
};
