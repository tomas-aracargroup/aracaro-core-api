import React from 'react';
import Table from '../../components/Grid';
import {post} from '../../api/dealers';
import getStatusCellClass from '../../helpers/getStatusCellClass';
const resolveStyle = (col, item) => getStatusCellClass(item[col.name]);

const rowClassResolver = item => {
    const result = item.status;

    switch (result) {
        case 'ANULADO':
            return 'tr-disabled';

        default:
            return '';
    }
};

const columns = [
    {
        name: 'documentNumber',
        label: 'Documento',
        cell: 'link',
        searchable: true,

        collectionName: 'entity/online-application'
    },
    {name: 'createdAt', label: 'Creado', cell: 'timeSince', searchable: true},
    {name: 'applicationNumber', label: 'Nro.', cell: 'number', searchable: true},
    {name: 'firstName', label: 'Nombre', searchable: true},
    {name: 'lastName', label: 'Apellido', searchable: true},

    {
        name: 'status',
        label: 'Status',
        cell: 'action',
        modalName: 'newStatusHistory',
        searchable: true,
        resolveStyle: resolveStyle
    },
    {name: 'statusHistory', label: 'Historial de Status', cell: 'text', hideInColumnPicker: true},

    {name: 'bs_dictamen', label: 'Besmart', searchable: true},
    {name: 'vehicleModel', label: 'Modelo', searchable: true},
    {name: 'vehicleInsuranceCompany', label: 'Cia Seguros', searchable: true},
    {name: 'vehicleInsuranceCoverageType', label: 'Cobertura', searchable: true},
    {name: 'productNominalAnualRate', label: 'TNA', cell: 'percentage'},
    {name: 'productNetAmount', label: 'Neto', cell: 'money', searchable: true},
    {name: 'productPrincipalAmount', label: 'Bruto', cell: 'money'},
    {name: 'vehicleValuation', label: '$ Vehiculo', cell: 'money'},
    {name: 'vehicleInsuranceCost', label: 'Seguro Costo', cell: 'money'},
    {name: 'productTerm', label: 'Cuotas', cell: 'number', searchable: true},
    {name: 'riskSpread', label: 'Tasa riesgo', cell: 'percentage'},
    {name: 'companyBaseRate', label: 'Tasa base', cell: 'percentage'},
    {name: 'productUVAValue', label: 'Valor UVA', cell: 'decimal'},
    {name: 'productPeriodicPaymentPesos', label: 'Pago pesos', cell: 'money'},
    {name: 'productPeriodicPaymentUva', label: 'Pago UVA', cell: 'number'},
    {name: 'delete', label: '.', cell: 'delete', collectionName: 'applications'},
    {name: 'notes', label: 'Historial de Notas', cell: 'text', hideInColumnPicker: true},
    {
        name: 'notesLastMessage',
        label: '# Notas',
        cell: 'action',
        modalName: 'dealersNotes',
        searchable: true
    }
];

export default props => {
    return (
        <Table
            title={'Online / Aplicaciones'}
            columnPicker
            rowClassResolver={rowClassResolver}
            post={post}
            columns={columns}
            collectionName="onlineApplications"
            forceRefresh={props.history.location.state && props.history.location.state.forceRefresh}
            endPoint="online-application"
            downloadDealers
            filters={{}}
            sortBy={{_id: -1}}
        />
    );
};
