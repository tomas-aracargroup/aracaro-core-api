import React from 'react';
import {Col, Card, Form, Input, Modal} from 'antd';
import {withRouter} from 'react-router';
import {put} from '../../api/index';

class Session extends React.Component {
    constructor(props) {
        super();
        this.state = {
            entity: Object.assign({}, props.entity)
        };
    }

    onChange = (name, value) => {
        let entity = this.state.entity;
        entity[name] = value;
        this.setState({entity});
    };

    handleOk = () => {
        const {entity} = this.state;
        put(`settings`, entity, entity._id).then(data => console.log('saved ', data));
        //todo refactor Adolfo
        window.location.reload();
    };

    render() {
        const {entity} = this.state;
        const {width} = this.props;

        return (
            <Modal
                iconType={'cross-circle'}
                visible
                onOk={this.handleOk}
                onCancel={this.handleOk}
                width={width}
                closable>
                <Col>
                    <Card title={'Editar condiciones'}>
                        <Form.Item label={'Gastos Fijos'}>
                            <Input
                                value={entity.fixedOriginationFees}
                                onChange={e => this.onChange('fixedOriginationFees', e.target.value)}
                            />
                        </Form.Item>

                        <Form.Item label={'Gastos Variables'}>
                            <Input
                                value={entity.variableOriginationFees}
                                onChange={e => this.onChange('variableOriginationFees', e.target.value)}
                            />
                        </Form.Item>

                        <Form.Item label={'TNA'}>
                            <Input
                                value={entity.nominalAnualRate}
                                onChange={e => this.onChange('nominalAnualRate', e.target.value)}
                            />
                        </Form.Item>
                    </Card>
                </Col>
            </Modal>
        );
    }
}

export default withRouter(Session);
