import React from 'react';
import Grid from '../../components/Grid';

const columns = [
    {name: 'channel', label: 'Canal', searchable: true},
    {name: 'product', label: 'Producto', searchable: true},
    {name: 'nominalAnualRate', label: 'TNA', cell: 'percentage'},
    {name: 'fixedOriginationFees', label: 'Gastos Fijos', cell: 'money'},
    {name: 'variableOriginationFees', label: 'Gastos Variables', cell: 'percentage'},
    {name: 'edit', label: 'Edit', cell: 'edit', modalName: 'setting', width: 400}
];

export default () => <Grid columns={columns} collectionName="settings" endPoint="settings" sortBy={{}} />;
