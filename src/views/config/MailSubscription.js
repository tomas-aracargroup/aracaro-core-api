import React from 'react';
import {get as getDealer, post as postDealer, put as putDealer} from '../../api/dealers';
import {get, put, post} from '../../api';
import enums from '../../helpers/enumerators';
import {eMAIL_SUBSCRIPTIONS} from '../../constants/enumerators';

class MailSubscription extends React.Component {
    state = {
        addressList: [],
        users: [],
        usersDealers: [],
        selectedUsers: [],
        server: '',
        available: {},
        archivo: null
    };
    constructor() {
        super();

        post('queries/users', {sortBy: {lastname: 1}}).then(data => {
            this.setState({
                users: data.collection.map(x => ({
                    _id: x._id,
                    firstName: x.firstname,
                    lastName: x.lastname,
                    email: x.email
                }))
            });
        });
        postDealer('queries/users', {sortBy: {lastName: 1}}).then(data =>
            this.setState({usersDealers: data.collection})
        );
        post('queries/mailsubscriptions').then(data => {
            this.setState({mailsAvailability: data.collection});
        });
        postDealer('queries/mailsubscriptions').then(data => {
            this.setState({mailsAvailabilityDealers: data.collection});
        });
    }

    onHandleClick = (value, server) => {
        const {users, usersDealers, mailsAvailability, mailsAvailabilityDealers} = this.state;
        const endPoint = server === 'api' ? 'email/get-mails-to' : 'mailer/get-mails-to';
        const _get = server === 'api' ? getDealer : get;
        const _users = server === 'api' ? usersDealers : users;
        const _mails = server === 'api' ? mailsAvailabilityDealers : mailsAvailability;

        const isAvailable = _mails.filter(x => value === x._id);
        this.setState({available: isAvailable[0]});

        this.setState({server: server, selectedUsers: _users, mailSelected: value});
        _get(`${endPoint}/${value}`).then(data => {
            this.setState({
                addressList: data.addressList.split(','),
                selectedUsers: _users.map(item => {
                    item.selected = data.addressList.split(',').includes(item.email);
                    return item;
                })
            });
        });
    };

    onHandleCheck = (e, userId) => {
        const {server, addressList, mailSelected} = this.state;
        const _put = server === 'api' ? putDealer : put;
        let endPoint = server === 'api' ? 'email' : 'mailer';
        if (addressList.includes(e.target.id)) {
            addressList.pop(e.target.id);
            endPoint = `${endPoint}/unset-mails-to`;
        } else {
            addressList.push(e.target.id);
            endPoint = `${endPoint}/set-mails-to`;
        }
        _put(endPoint, {userId: userId}, mailSelected);
        this.setState({addressList});
    };

    onHandleAvailability = (e, mail) => {
        const {available, server} = this.state;
        const _put = server === 'api' ? putDealer : put;
        let endPoint = server === 'api' ? 'email' : 'mailer';

        endPoint = `${endPoint}/set-availability-to`;
        _put(endPoint, {status: e.target.checked}, mail);

        available.status = e.target.checked;
        this.setState({available});
    };

    render() {
        const {selectedUsers, mailSelected, addressList, available} = this.state;

        return (
            <table className="channelsform">
                <tbody>
                    <tr>
                        <td>
                            <div>
                                <h2>Gateway</h2>
                                <div style={{paddingLeft: '10px'}}>
                                    {enums.eMAIL_SUBSCRIPTIONS.map(item => (
                                        <div key={item.id}>
                                            <input
                                                type="radio"
                                                checked={mailSelected === item.id ? true : false}
                                                name="mails"
                                                value={item.id}
                                                onChange={e => this.onHandleClick(e.target.value, 'gateway')}
                                            />
                                            {item.name}
                                        </div>
                                    ))}
                                </div>
                            </div>
                            <div>
                                <h2>Api</h2>
                                <div style={{paddingLeft: '10px'}}>
                                    {eMAIL_SUBSCRIPTIONS.map(item => (
                                        <div key={item.id}>
                                            <input
                                                type="radio"
                                                checked={mailSelected === item.id ? true : false}
                                                name="mails"
                                                value={item.id}
                                                onChange={e => this.onHandleClick(e.target.value, 'api')}
                                            />
                                            {item.name}
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </td>
                        <td style={{paddingLeft: '20px'}}>
                            {available ? (
                                <div>
                                    {available.name}
                                    <input
                                        id={mailSelected}
                                        type="checkbox"
                                        checked={available.status}
                                        onChange={e => this.onHandleAvailability(e, mailSelected)}
                                    />
                                </div>
                            ) : null}
                            <div>
                                <h2>Usuarios hablitados</h2>
                                <table>
                                    <tbody>
                                        {selectedUsers.map(item => (
                                            <tr key={item._id}>
                                                <td>
                                                    <input
                                                        id={item.email}
                                                        type="checkbox"
                                                        checked={addressList.includes(item.email)}
                                                        onChange={e => this.onHandleCheck(e, item._id)}
                                                    />
                                                </td>
                                                <td>{item.lastName}</td>
                                                <td>{item.firstName}</td>
                                                <td>{item.email}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export default MailSubscription;
