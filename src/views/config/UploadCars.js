import React from 'react';
import axios from 'axios';
import {formatMoney, formatPercentage} from '../../helpers';

class UploadCars extends React.Component {
    state = {
        marca: 'FORD',
        archivo: null,
        data: [],
        rows: []
    };

    getPar = (codia, year) => {
        const {data} = this.state;
        const dato = data.filter(z => z.codia === codia && z.year === year)[0];
        return dato;
    };
    enviar = check => {
        let formData = new FormData();
        formData.append('file', this.state.archivo);
        if (this.state.archivo) {
            axios
                .post(`http://localhost:4005/upload/cars?checkfile=${check}&marca=${this.state.marca}`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(data => {
                    this.setState({data: data.data.data, rows: data.data.rows});
                })
                .catch(err => {
                    console.log('>ERROR');
                    console.log(err);
                });
        }
    };

    render() {
        const {data, rows} = this.state;
        const pepe =
            data && data.length > 0 ? (
                <table width="80%" border={1}>
                    <tbody>
                        <tr>
                            <td width="5%">Codia</td>
                            <td width="5%">Año</td>
                            <td width="30%">Modelo</td>
                            <td width="20%">Precio actual</td>
                            <td width="20%">Precio nuevo</td>
                            <td width="20%">Aumento</td>
                        </tr>
                        {rows.map(x => {
                            const par = this.getPar(x.codia, x.year, x.value);

                            return (
                                <tr key={`${x.codia}${x.year}`}>
                                    <td>{x.codia}</td>
                                    <td>{x.year}</td>
                                    <td>{x.model}</td>
                                    {par ? (
                                        <td style={{textAlign: 'right'}}>{formatMoney(par.value * 1000)}</td>
                                    ) : (
                                        <td />
                                    )}
                                    <td style={{textAlign: 'right'}}>{formatMoney(x.value * 1000)}</td>
                                    {par ? (
                                        <td style={{textAlign: 'right'}}>
                                            {formatPercentage(((par.value - x.value) / par.value) * 100 * -1)}
                                        </td>
                                    ) : (
                                        <td style={{color: 'green', textAlign: 'right'}}>Nuevo</td>
                                    )}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            ) : null;
        return (
            <span>
                <form action="http://localhost:4005/upload" encType="multipart/form-data" method="post">
                    <input
                        type="file"
                        name="file"
                        onChange={e => {
                            this.setState({archivo: e.target.files[0]}, () => this.enviar('check'));
                        }}
                    />
                    <select onChange={e => this.setState({marca: e.target.value}, () => this.enviar('check'))}>
                        <option value="CHEVROLET">CHEVROLET</option>
                        <option value="FIAT">FIAT</option>
                        <option value="FORD">FORD</option>
                        <option value="PEUGEOT">PEUGEOT</option>
                        <option value="RENAULT">RENAULT</option>
                    </select>
                    <input type="button" onClick={e => this.enviar('no')} value="Subir Datos" />
                </form>
                {pepe}
            </span>
        );
    }
}

export default UploadCars;
