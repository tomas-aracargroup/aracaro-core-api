import JS_PDF from 'jspdf';
import moment from 'moment';
import imgData from '../assets/img/form03-19cm.jpg';
import {formatNumber} from '../helpers';

import 'moment/locale/es';

moment.locale('es');
const resolveCABA = city => (city === 'CIUDAD AUTONOMA BUENOS AIRES' ? 'CAPITAL FEDERAL' : city);

export const form03Pdf = application => {
    //Creo un doc
    let doc = new JS_PDF('p', 'mm', 'a4');

    //Helper functions
    // const getTextWidth = text => (doc.getStringUnitWidth(text) * doc.internal.getFontSize()) / doc.internal.scaleFactor;
    // const textCenter = (text, y) => doc.text(text, (doc.internal.pageSize.getWidth() - getTextWidth(text)) / 2, y);
    const getApplicationText = field => (application[field] || '').toString();
    const getMomentText = (field, part) => {
        if (application[field]) {
            if (!part) {
                return moment(application[field], 'YYYY-MM-DD').format('DD-MM-YYYY');
            } else if (part === 'day') {
                return moment(application[field], 'YYYY-MM-DD').format('DD');
            } else if (part === 'month') {
                return moment(application[field], 'YYYY-MM-DD').format('MM');
            } else if (part === 'year') {
                return moment(application[field], 'YYYY-MM-DD').format('YY');
            }
        } else {
            return '';
        }
    };

    doc.setFontSize(9);
    doc.setTextColor('#000000');

    // doc.text(`Fecha: ${moment().format('LL')}`, 10, 20);

    const drawDocumentType = base => {
        const X = type => (type === application['documentType'] ? 'X' : '');

        const firstLeft = 130;
        doc.text(X('DNI'), firstLeft, base);
        doc.text(X('LE'), firstLeft + 11, base);
        doc.text(X('LC'), firstLeft + 22, base);
    };

    const drawMaritalStatus = base => {
        const X = type => (type === application['maritalStatus'] ? 'X' : '');

        const firstLeft = 154;
        doc.text(X('SOLTERO'), firstLeft, base);
        doc.text(X('CASADO'), firstLeft + 11, base);
        doc.text(X('DIVORCIADO'), firstLeft + 16, base);
        doc.text(X('UNION_CONVENCIONAL'), firstLeft + 21, base);
        doc.text(X('VIUDO'), firstLeft + 26, base);
        doc.text(getApplicationText(application['maritalStatusNuptial']), firstLeft + 27, base);
    };

    const drawContractConcept = base => {
        const X = type => (type === application['contractConcept'] ? 'X' : '');
        const firstLeft = 177;
        doc.text(X(true), firstLeft, base);
        doc.text(X(false), firstLeft + 13, base);
    };

    const drawContractTerm = base => {
        const X = type => (type === application['contractTerm'] ? 'X' : '');
        const firstLeft = 177;
        doc.text(X(true), firstLeft, base);
        doc.text(X(false), firstLeft + 13, base);
    };

    //Funcion para primera seccion
    const firstSection = base => {
        const leftCol1 = 44;
        const leftCol2 = 124;

        doc.text(`${getMomentText('createdAt', 'day')}`, leftCol1 + 6, base);
        doc.text(`${getMomentText('createdAt', 'month')}`, leftCol1 + 15, base);
        doc.text(`${getMomentText('createdAt', 'year')}`, leftCol1 + 24, base);

        doc.text(
            `$${formatNumber(getApplicationText('productPrincipalAmount'), 2)}.-`,
            leftCol1 + 8,
            (base = base + 8)
        );
        doc.text(`${getApplicationText('creditorIncription')}`, leftCol1 + 28, base + 18);
        doc.text(`CUIT/CUIL ${getApplicationText('cuit')}`, leftCol2 + 5, base + 18);

        doc.text(`${getApplicationText('vehicleDomain')}`, leftCol1 + 60, base);

        doc.text(`${getApplicationText('creditorCuit')}`, leftCol1 + 60, base);

        doc.text(`${getApplicationText('creditorPersonality')}`, leftCol1, (base = base + 26));
        doc.text(`${getApplicationText('lastName')} ${getApplicationText('firstName')}`, leftCol2, base);

        doc.text(`${getApplicationText('creditorEmail')}`, leftCol1, (base = base + 8));
        doc.text(`${getApplicationText('email')}`, leftCol2, base);

        doc.text(`${getApplicationText('creditorAddressStreet')}`, leftCol1, (base = base + 16));
        doc.text(`${getApplicationText('legalAddressStreet')}`, leftCol2, base);

        doc.text(`${getApplicationText('creditorAddressNumber')}`, leftCol1, (base = base + 9));
        doc.text(`${getApplicationText('legalAddressNumber')}`, leftCol2, base);

        doc.text(`${getApplicationText('creditorAddressFloor')}`, leftCol1 + 26, base);
        doc.text(`${getApplicationText('legalAddressFloor')}`, leftCol2 + 25, base);

        doc.text(`${getApplicationText('creditorAddressApartment')}`, leftCol1 + 38, base);
        doc.text(`${getApplicationText('legalAddressApartment')}`, leftCol2 + 40, base);

        doc.text(`${getApplicationText('creditorZipCode')}`, leftCol1 + 58, base);
        doc.text(`${getApplicationText('legalZipCode')}`, leftCol2 + 58, base);

        doc.text(`${resolveCABA(getApplicationText('creditorCity'))}`, leftCol1, (base = base + 8));
        doc.text(`${resolveCABA(getApplicationText('legalCity'))}`, leftCol2, base);

        doc.text(`${getApplicationText('creditorProvince')}`, leftCol1 + 47, (base = base + 8));
        doc.text(`${getApplicationText('legalProvince')}`, leftCol2 + 47, base);

        drawDocumentType((base = base + 19));
        doc.text(`${getApplicationText('documentNumber')}`, leftCol2, (base = base + 9));
        doc.text('RENAPER', leftCol2 + 50, base);

        doc.text(`${getMomentText('dob', 'day')}`, leftCol2, (base = base + 17));
        doc.text(`${getMomentText('dob', 'month')}`, leftCol2 + 9, base);
        doc.text(`${getMomentText('dob', 'year')}`, leftCol2 + 19, base);

        drawMaritalStatus(base);
        doc.text(
            `${getApplicationText('spouseLastName')} ${getApplicationText('spouseFirstName')}`,
            leftCol2,
            (base = base + 8)
        );

        doc.text(`${getApplicationText('creditorRegistration')}`, leftCol1, (base = base + 11));
        doc.text(`${getApplicationText('contractRegistration')}`, leftCol2, base);

        doc.text(`${getApplicationText('creditorRegistrationData')}`, leftCol1, (base = base + 15));
        doc.text(`${getApplicationText('contractRegistrationData')}`, leftCol2, base);

        doc.text(`${getMomentText('creditorRegistrationDate', 'day')}`, leftCol1 + 47, base);
        doc.text(`${getMomentText('creditorRegistrationDate', 'month')}`, leftCol1 + 58, base);
        doc.text(`${getMomentText('creditorRegistrationDate', 'year')}`, leftCol1 + 69, base);

        doc.text(`${getMomentText('contractRegistrationDate', 'day')}`, leftCol2 + 50, base);
        doc.text(`${getMomentText('contractRegistrationDate', 'month')}`, leftCol2 + 60, base);
        doc.text(`${getMomentText('contractRegistrationDate', 'year')}`, leftCol2 + 71, base);

        doc.text(`${getApplicationText('vehicleDomain')}`, leftCol1 + 45, (base = base + 42));
        doc.text(`${getApplicationText('vehicleMake')}`, leftCol1 + 10, (base = base + 8));
        doc.text(`${getApplicationText('vehicleType')}`, leftCol1 + 7, (base = base + 8));
        doc.text(`${getApplicationText('vehicleModelForm03')}`, leftCol1 + 11, (base = base + 7));
        doc.text(`${getApplicationText('vehicleEngineMake')}`, leftCol1 + 20, (base = base + 8));
        doc.text(`${getApplicationText('vehicleEngineNumber')}`, leftCol1 + 19, (base = base + 8));

        doc.text(`${getApplicationText('contractPledgeDegree')}`, leftCol2 + 12, base);

        doc.text(`${getApplicationText('vehicleChassisMake')}`, leftCol1 + 24, (base = base + 8));
        doc.text(`${getApplicationText('vehicleChassisNumber')}`, leftCol1 + 18, (base = base + 7));

        drawContractTerm(base - 19);
        drawContractConcept(base - 5);
    };

    doc.addImage(imgData, 'JPG', -0.5, -12, 207, 312);

    firstSection(28);

    //Bajar documento.
    doc.save(`${moment().format('YYYY-MM-DD')}_${getApplicationText('vehicleDomain')}_prenda.pdf`);
};
