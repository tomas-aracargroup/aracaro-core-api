import JS_PDF from 'jspdf';
import {lowerCase, startCase} from 'lodash';
import moment from 'moment';
import {formatMoney, formatPercentage, getAge} from '../helpers/index';
import imgData from '../assets/img/aracar-logo-uri';
import {formatNumber} from '../helpers/index';

import 'moment/locale/es';

moment.locale('es');

export const approvalLetter = application => {
    //Creo un doc
    let doc = new JS_PDF('p', 'mm', 'a4');
    //Helper functions
    // const getTextWidth = text => (doc.getStringUnitWidth(text) * doc.internal.getFontSize()) / doc.internal.scaleFactor;
    // const textCenter = (text, y) => doc.text(text, (doc.internal.pageSize.getWidth() - getTextWidth(text)) / 2, y);
    const getApplicationText = field => (application[field] || '').toString();
    const getMomentText = field => {
        if (application[field]) {
            return moment(application[field], 'YYYY-MM-DD').format('DD-MM-YYYY');
        } else {
            return '';
        }
    };

    const capitalFinaciar = formatNumber(
        getApplicationText('productPrincipalAmount') / getApplicationText('productUVAValue'),
        2
    );

    const cuotaCapitalEnUVA = () => {
        let result;

        if (
            getApplicationText('productNetAmount') !== '' &&
            getApplicationText('productTerm') &&
            getApplicationText('productUVAValue') !== ''
        ) {
            result = formatNumber(
                getApplicationText('productNetAmount') /
                    getApplicationText('productTerm') /
                    getApplicationText('productUVAValue'),
                2
            );
        }
        return result || '';
    };

    const MontoEnUVA = () => {
        let result;

        if (getApplicationText('productPrincipalAmount') !== '') {
            result = formatNumber(
                getApplicationText('productPrincipalAmount') / getApplicationText('productUVAValue'),
                2
            );
        }
        return result || '';
    };

    const cuota1EnUVA = () => {
        let result = '';

        if (getApplicationText('productPrincipalAmount') !== '' && getApplicationText('productUVAValue') !== '') {
            result = formatNumber(
                getApplicationText('productFirstPaymentAmount') / getApplicationText('productUVAValue'),
                2
            );
        }
        if (result) {
            return result;
        } else {
            return '';
        }
    };

    const cuotaPura = formatNumber(
        getApplicationText('productPeriodicPaymentPesos') / getApplicationText('productUVAValue'),
        2
    );

    //Titulo
    doc.setFontType('bold');
    doc.text(`${application.firstName} ${application.lastName}`, 10, 15);
    doc.setFontType('normal');
    doc.text('Carta de Aprobación', 10, 23);

    doc.setFontSize(9);
    doc.setTextColor('#000000');
    doc.setDrawColor('#43854f');
    doc.setLineWidth(0.3);

    // doc.text(`Fecha: ${moment().format('LL')}`, 10, 20);

    //Funcion para primera seccion
    const firstSection = base => {
        doc.line(10, base - 5, 205, base - 5);

        //Linea 1
        doc.text(`Solicitud Nro.: ${getApplicationText('applicationNumber')}`, 10, base);
        doc.text(`Fecha: ${getMomentText('createdAt')}`, 150, base);

        //Linea 2
        doc.text(`Canal: ${startCase(lowerCase(getApplicationText('salesChannel')))}`, 10, base + 5);
        doc.text(`Sucursal: ${getApplicationText('applicationBranch')}`, 80, base + 5);
        doc.text(`Vendedor: ${getApplicationText('salesPerson')}`, 150, base + 5);
    };

    //Funcion para segunda seccion
    const secondSection = base => {
        doc.line(10, base - 5, 205, base - 5);
        //Linea 1
        doc.text(`Solicitante: ${getApplicationText('firstName')} ${getApplicationText('lastName')}`, 10, base);
        doc.text(`Documento de identidad: ${formatNumber(getApplicationText('documentNumber'))}`, 80, base);
        doc.text(`Fecha de Nacimiento: ${getMomentText('dob')}`, 150, base);
        //Linea 2
        doc.text(`Edad: ${getAge(application.dob)}`, 10, base + 5);
        doc.text(`Tel Part: ${getApplicationText('phoneLandLine')}`, 80, base + 5);
        doc.text(`Cel: ${getApplicationText('phoneMobile')}`, 150, base + 5);
        //Linea 3
        doc.text(
            `Dirección: ${getApplicationText('addressStreet')} ${getApplicationText('addressNumber')}`,
            10,
            base + 10
        );
        doc.text(`Localidad: ${getApplicationText('city')} (${getApplicationText('zipCode')})`, 80, base + 10);
        doc.text(`Provincia: ${getApplicationText('province')}`, 150, base + 10);
        //Linea 4
        doc.text(`Estado Civil: ${getApplicationText('maritalStatus')}`, 10, base + 15);
        doc.text(
            `Conyuge/Cotitular: ${getApplicationText('spouseFirstName')} ${getApplicationText('spouseLastName')}`,
            80,
            base + 15
        );
        // doc.text(`Documento: ${formatNumber(application.spouseDocumentNumber)}`, 150, base + 15);
        doc.text(`Documento: ${getApplicationText('spouseDocumentNumber')}`, 150, base + 15);
    };

    //Funcion para tercera seccion
    const thirdSection = base => {
        doc.line(10, base - 6, 205, base - 6);
        //Linea 1
        doc.text(`Capital a financiar (UVA): ${capitalFinaciar}`, 10, base);
        doc.text(`Amortización: ${getApplicationText('productAmortizationMethod')}`, 80, base);
        doc.text(
            `Monto  prenda: ${formatMoney(getApplicationText('productPrincipalAmount'))} (${MontoEnUVA()})`,
            150,
            base
        );

        //Linea 2
        doc.text(`Cantidad de cuotas: ${getApplicationText('productTerm')}`, 10, base + 5);
        doc.text(`Producto: ${startCase(lowerCase(getApplicationText('productName')))}`, 80, base + 5);
        doc.text(`Cuota UVA: ${cuotaCapitalEnUVA()}`, 150, base + 5);
        //Linea 3
        doc.text(`Cuota pura (UVA): ${cuotaPura}`, 10, base + 10);
        doc.text(`TEA: ${formatPercentage(getApplicationText('productEffectiveAnualRate'))}`, 80, base + 10);
        doc.text(`Cuota 1: ${cuota1EnUVA()}`, 150, base + 10);

        //Linea 4
        doc.text(`Seguro de vida: -`, 10, base + 15);
        doc.text(`CFT: ${formatPercentage(getApplicationText('productTotalFinancialCost'))}`, 80, base + 15);
        doc.text(`TNA: ${formatPercentage(getApplicationText('productNominalAnualRate'))}`, 150, base + 15);

        //Linea 5
        doc.text(
            `Cuota inicial aprox.: ${formatMoney(getApplicationText('productFirstPaymentAmount'))}`,
            10,
            base + 20
        );
        doc.text(
            `CFT (+ IVA): ${formatPercentage(getApplicationText('productTotalFinancialCostWithVAT'))}`,
            80,
            base + 20
        );
        doc.text(`UVA: ${formatNumber(getApplicationText('productUVAValue'), 2)}`, 150, base + 20);
    };

    //Funcion para cuarta seccion
    const fourthSection = base => {
        doc.line(10, base - 6, 205, base - 6);
        //Linea 1
        doc.text(`Marca: ${getApplicationText('vehicleMake')}`, 10, base);
        doc.text(`Modelo: ${getApplicationText('vehicleModel')}`, 110, base);
        //Linea 2
        doc.text(`Año: ${getApplicationText('vehicleYear')}`, 10, base + 5);
        doc.text(`Valor: ${formatMoney(getApplicationText('vehicleValuation'))}`, 110, base + 5);
        //Linea 3
        doc.text(`Cía Aseguradora: ${getApplicationText('vehicleInsuranceCompany')}`, 10, base + 10);
        doc.text(`Cobertura: ${getApplicationText('vehicleInsuranceCoverageType')}`, 110, base + 10);
        //Linea 4
        doc.text(
            `Costo del Seguro del Automotor(***): ${formatMoney(getApplicationText('vehicleInsuranceCost'))}`,
            10,
            base + 15
        );
    };

    //Legales I
    const fifthSection = base => {
        doc.line(10, base - 6, 205, base - 6);
        doc.setFontSize(8);
        let text = `Para la aprobacion definitiva del credito, el Solicitante debera acompañar:
        * Solicitud de Crédito firmada por el Solicitante y Cónyuge o Cotitular, si correspondiera.
        * Carta de Pre aprobación firmada por el Solicitante.
        * Fotocopia de DNI del Solicitante y Cónyuge o Cotitular, si correspondiera.
        * Contrato de Prenda Inscripto o Fianza Transitoria (según operatoria).
        * Comprobante de CUIL o CUIT.
        * Fotocopia de factura de servicio público con su correspondiente pago al día.
        * Fotocopia de frente y dorso del título de propiedad del automotor o certificado de fabricación o importación.
        * Fotocopia del acta de divorcio o certificado de defunción si correspondiera.
        * Resumen de cuenta bancaria o Constancia de CBU (en caso de adhesión a débito automático).`;
        doc.text(10, base, doc.splitTextToSize(text, 180));

        text = `El Solicitante en este acto se notifica de la presenta pre-aprobación del crédito solicitado, y de las condiciones económicas y financieras del mismo, prestando conformidad en forma expresa e irrevocable a las mismas. Asimismo, acuerda expresamente que la emision de presente Carta de Preaprobacion no obliga a Aracar Financiera S.A. al otorgamiento del Crédito ni al desembolso de los fondos. El otorgamiento del Crédito se perfeccionará con el desembolso de los fondos, el que se realizará una vez que Aracar Financiera S.A. evalue la totalidad de la documentacion entregada.`;
        doc.text(text, 10, base + 35, {maxWidth: 190, align: 'justify'});

        doc.setFontSize(9);
    };

    //Legales II
    const sixthSection = base => {
        doc.setFontSize(8);
        doc.line(10, base - 6, 205, base - 6);
        let text = `(*)De acuerdo a las políticas comerciales y de riesgo vigentes: (i) A la presente Carta de Pre Aprobación se le aplican los mismos términos y condiciones generales dispuestos en la Solicitud de Crédito que el Solicitante ha suscripto. (ii) Esta Carta de Pre aprobación de la misma forma que la Solicitud de Crédito no constituye una oferta y se emite solo a título informativo. Aracar Financiera S.A. se reserva el derecho de aprobar ó rechazar esta Carta de Pre Aprobación y la Solicitud de Crédito luego de analizar la documentación presentada y corroborar la certeza y veracidad de los datos del Solicitante. (iii) La Solicitud de Crédito sólo se entenderá aceptada cuando resulte desembolsado el Crédito. (iv) La Vigencia de las Condiciones Financieras y Crediticias es de treinta (30) días corridos desde la fecha de emisión de la presente. Vencido dicho plazo quedarán sin efecto de pleno derecho sin necesidad de notificar al/ a los Solicitante/s. (v) En caso de producirse cambios adversos significativos en las condiciones económicas, financieras ó políticas imperantes en el mercado nacional o internacional, según exclusivo criterio de Aracar Financiera S.A., ó se dictaren nuevas regulaciones, por parte del Banco Central de la República Argentina (B.C.R.A.) u otra autoridad competente, de modo tal que pudieran afectar el costo del dinero en plaza ó la ecuación económica tenida en cuenta por Aracar Financiera S.A. y/o la institución financiera que se constituya como acreedor prendario a la fecha del análisis para el otorgamiento del Crédito, Aracar Financiera S.A. estará facultado a modificar las condiciones financieras y crediticias pactadas adecuándolas a la nueva situación existente. (vi) El Solicitante conviene que Aracar Financiera S.A. podrá transferir, ceder y/o vender el Crédito por cualquiera de los medios previstos en la normativa vigente, adquiriendo el o los cesionarios los mismos beneficios y/o derechos y/o acciones de Aracar Financiera S.A. bajo la presente Carta de Pre Aprobación y la Solicitud de Crédito. (vii) Para el caso que el Solicitante no hubiese informado su condición impositiva, la situación impositiva considerada frente al IVA es consumidor final.`;
        doc.text(text, 10, base, {maxWidth: 190, align: 'justify'});

        text = `(**) La cuota inicial: se compone además del capital, porlos intereses e impuestos descriptos por los cargos y comisiones por generación de informes, inscripción de prenda, formularios, gastos de gestión y cargos de recaudación mediante canal de cobranza si correspondiese conforme se indica en los términos y condiciones de la Solicitud de Crédito. La cuota inicial podría sufrir modificaciones de acuerdo a: la variación de la Unidad de Valor Adquisitivo que publica el Banco Central de la República Argentina a la fecha de desembolso; al impuesto a las transferencias financieras (ITF) y si existiese más de treinta (30) días entre la fecha de liquidación y el vencimiento de la 1°cuota. El cálculo de las cuotas es al sólo efecto informativo y no incluye el ITF ni el seguro de la unidad.`;
        doc.text(text, 10, base + 50, {maxWidth: 190, align: 'justify'});

        text = `(***) El Seguro del Automóvil: podrá ser actualizado conforme a la variación que pueda experimentar el precio del vehículo en el mercado y/o a raíz de las modificaciones que el titular del mismo (deudor del crédito prendario) hiciera al mismo. Para toda consulta o aclaración deberá comunicarse al tel. (11) 53657739.`;
        doc.text(text, 10, base + 71, {maxWidth: 190, align: 'justify'});
    };

    doc.addImage(imgData, 'JPEG', 185, 5, 23.5, 19.5);
    firstSection(35);
    secondSection(49);
    thirdSection(75);
    fourthSection(105);
    fifthSection(130);
    sixthSection(188);

    doc.text(
        'Firma del solicitante:____________________________               Aclaración:____________________________',
        20,
        280
    );

    //Bajar documento
    doc.save(
        `${moment().format('YYYY-MM-DD')}_${lowerCase(getApplicationText('firstName'))}_${lowerCase(
            getApplicationText('lastName')
        )}_carta_aprobacion.pdf`
    );
};
