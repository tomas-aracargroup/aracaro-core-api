// import * as autoTable from 'jspdf-autotable'; //eslint-disable-line
import JS_PDF from 'jspdf';
import * as autoTable from 'jspdf-autotable'; // eslint-disable-line no-unused-vars
import {lowerCase} from 'lodash';
import moment from 'moment';
import 'moment/locale/es';
import enums from '../helpers/enumerators';
import {getFormattedDate} from '../helpers/index';
import {formatMoney, formatNumber, formatPercentage} from '../helpers/index';
import {convertToUVAS} from '../helpers/financial';
import writtenNumber from 'written-number';

moment.locale('es');
writtenNumber.defaults.lang = 'es';

export const applicationLetter = application => {
    const doc = new JS_PDF('p', 'mm', 'a4', true);

    //Helper functions
    const getTextWidth = text => (doc.getStringUnitWidth(text) * doc.internal.getFontSize()) / doc.internal.scaleFactor;
    const textCenter = (text, y) => doc.text(text, (doc.internal.pageSize.getWidth() - getTextWidth(text)) / 2, y);
    const getApplicationText = field => (application[field] || '').toString();
    const getMomentText = (field, part) => {
        if (application[field]) {
            if (!part) {
                return moment(application[field], 'YYYY-MM-DD').format('DD-MM-YYYY');
            } else if (part === 'day') {
                return moment(application[field], 'YYYY-MM-DD').format('DD');
            } else if (part === 'month') {
                return moment(application[field], 'YYYY-MM-DD').format('MM');
            } else if (part === 'year') {
                return moment(application[field], 'YYYY-MM-DD').format('YY');
            }
        } else {
            return '';
        }
    };

    const RECT_L = 10;
    // const RECT_W = doc.internal.pageSize.getWidth() - 10;
    const RECT_W = 200;
    const ROW_H = 5;

    const drawCheckbox = (colLeft, base, match) => {
        if (match) {
            doc.rect(colLeft, base + 1, 3, 3);
            doc.text(colLeft + 0.5, base + 3.5, 'X');
        } else {
            doc.rect(colLeft, base + 1, 3, 3);
        }
    };
    const drawLaborBoxes = (base, profesion, profesionType) => {
        const X = type => (type === application[profesionType] ? 'X' : '');
        drawCheckbox(12, base + 3, X('PRINCIPAL'));
        doc.text('Rel. Dependencia', 16, base + 6.5);
        drawCheckbox(52, base + 3, X('PRINCIPAL1'));
        doc.text('Independiente', 56, base + 6.5);
        drawCheckbox(92, base + 3, X('PRINCIPAL2'));
        doc.text('Jubilado', 96, base + 6.5);
        drawCheckbox(132, base + 3, X('PRINCIPAL3'));
        doc.text('Estudiante', 136, base + 6.5);
        drawCheckbox(172, base + 3, X('PRINCIPAL4'));
        doc.text('Empleo Informal', 176, base + 6.5);

        drawCheckbox(12, base + 11, X('PRINCIPAL5'));
        doc.text('Ama de Casa', 16, base + 14.5);
        drawCheckbox(52, base + 11, X('PRINCIPAL6'));
        doc.text('Sin Trabajo', 56, base + 14.5);

        doc.text('Profesión:', 96, base + 14.5);
        doc.text(getApplicationText(profesion), 110, base + 14.5);
    };

    const drawTaxesGanancias = (base, income) => {
        const X = type => (type === application[income] ? 'X' : '');
        doc.setFontStyle('bold');
        doc.text('Condición frente al Impuesto a las Ganancias:', 11, base + 4);
        doc.setFontStyle('normal');

        drawCheckbox(12, base + 5, X('SUJETO_ALC_INS'));
        doc.text('Sujeto Alcanzado Inscripto', 16, base + 8.5);
        drawCheckbox(92, base + 5, X('SUJETO_ALC_NO_INS_EXCEP'));
        doc.text('Sujeto Alcanzado Inscripto EXCEP. RG830', 96, base + 8.5);
        drawCheckbox(172, base + 5, X('MONOTRIBUTO'));
        doc.text('Monotributo', 176, base + 8.5);

        drawCheckbox(12, base + 9, X('SUJETO_ALC_NO_INS'));
        doc.text('Sujeto Alcanzado No Inscripto', 16, base + 12.5);
        drawCheckbox(92, base + 9, X('SUJETO_NO_ALC'));
        doc.text('Sujeto No Alcanzado', 96, base + 12.5);
        drawCheckbox(172, base + 9, X('EXENTO'));
        doc.text('Exento', 176, base + 12.5);
    };

    const drawTaxesIVA = (base, iva) => {
        const X = type => (type === application[iva] ? 'X' : '');

        doc.setFontStyle('bold');
        doc.text('Condición frente al IVA:', 11, base + 3);
        doc.setFontStyle('normal');

        drawCheckbox(12, base + 4, X('RESP_INS'));
        doc.text('Responsable Inscripto', 16, base + 7.5);
        drawCheckbox(62, base + 4, X('MONOTRIBUTO6'));
        doc.text('Monotributo 6 0/00', 66, base + 7.5);
        drawCheckbox(101, base + 4, X('MONOTRIBUTO25'));
        doc.text('Monotributo 25 0/00', 105, base + 7.5);
        drawCheckbox(140, base + 4, X('RESP_INS_EXCRG3337'));
        doc.text('Responsable Inscripto Exc. RG3337', 144, base + 7.5);

        drawCheckbox(12, base + 9, X('NO_RESP'));
        doc.text('No Responsable', 16, base + 12.5);
        drawCheckbox(62, base + 9, X('NO_CATEGORIZADO'));
        doc.text('No Categorizado', 66, base + 12.5);
        drawCheckbox(101, base + 9, X('CONSUMIDOR_FINAL'));
        doc.text('Consumidor Final', 105, base + 12.5);
        drawCheckbox(140, base + 9, X('EXENTO_IVA'));
        doc.text('Exento', 144, base + 12.5);
    };

    const drawIIBB = (base, iibb, nroiibb) => {
        const X = type => (type === application[iibb] ? 'X' : '');

        doc.setFontStyle('bold');
        doc.text('Condición frente a Ingresos Brutos:', 11, base + 3);
        doc.setFontStyle('normal');

        drawCheckbox(12, base + 4, X('RESP_INS_IIBB'));
        doc.text('Responsable Inscripto', 16, base + 7.5);
        drawCheckbox(62, base + 4, X('RICICOBRA'));
        doc.text('R.I.C.I. COBRA 38/95', 66, base + 7.5);
        drawCheckbox(101, base + 4, X('RINOCOBRA'));
        doc.text('R.I. NO COBRA 38/95', 105, base + 7.5);
        drawCheckbox(140, base + 4, X('RICMCOBRA'));
        doc.text('R.I.C.M COBRA 38/95', 144, base + 7.5);

        drawCheckbox(12, base + 9, X('NO_RESP_IIBB'));
        doc.text('No Responsable', 16, base + 12.5);
        drawCheckbox(62, base + 9, X('CABAREGSIMP'));
        doc.text('CABA - REG. SIMPLIF.', 66, base + 12.5);
        drawCheckbox(101, base + 9, X('CONSUMIDOR_FINAL_IIBB'));
        doc.text('Consumidor Final', 105, base + 12.5);
        drawCheckbox(140, base + 9, X('EXENTO_IIBB'));
        doc.text('Exento', 144, base + 12.5);

        doc.text(`N° de incripción: ${getApplicationText(application[nroiibb])}`, 12, base + 17.5);
        doc.text('Practica Ajuste por Inflación:', 140, base + 17.5);
        drawCheckbox(180, base + 13.5, getApplicationText('conditionInflationAdjustment'));
        drawCheckbox(185, base + 13.5, !getApplicationText('conditionInflationAdjustment'));
    };

    const drawTextLegales = base => {
        const fontSize = doc.internal.getFontSize();
        doc.setFontSize(7);
        let text1 =
            'Aracar Financiera S.A. ("Aracar, o el "Acreedor") utiliza esta Solicitud como formulario para conocer a su cliente (KYC).';
        doc.text(text1, RECT_L + 4, base + 4);
        doc.setFontStyle('bold');
        text1 =
            '¿Algún titular de la cuenta realiza alguna de las actividades de alto riesgo detalladas a continuación? Sí |_| No |_|';
        doc.text(text1, RECT_L + 4, base + 4 + ROW_H);
        doc.setFontStyle('normal');
        //COLUMNA 1
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 2);
        doc.text('a) Mutuales', RECT_L + 8, base + 7.5 + ROW_H * 2);
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 3);
        doc.text('b) Cooperativas', RECT_L + 8, base + 7.5 + ROW_H * 3);
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 4);
        doc.text('c) Sociedades de Bolsa', RECT_L + 8, base + 7.5 + ROW_H * 4);
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 5);
        doc.text('d) Intermediación financiera (factoring', RECT_L + 8, base + 7.5 + ROW_H * 5);
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 6);
        doc.text('e) Casas de cambio', RECT_L + 8, base + 7.5 + ROW_H * 6);
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 7);
        doc.text('e) Casas de cambio', RECT_L + 8, base + 7.5 + ROW_H * 7);
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 8);
        doc.text('f) Agencias de Viaje y Turismo', RECT_L + 8, base + 7.5 + ROW_H * 8);
        drawCheckbox(RECT_L + 4, base + 4 + ROW_H * 9);
        doc.text('g) Hoteleria (Hoteles, Apart Hoteles, Cabañas, otros', RECT_L + 8, base + 7.5 + ROW_H * 9);

        //COLUMNA 2
        drawCheckbox(RECT_L + 74, base + 4 + ROW_H * 2);
        doc.text('h) Fideicomsios (Construcción,Finacieros)', RECT_L + 78, base + 7.5 + ROW_H * 2);
        drawCheckbox(RECT_L + 74, base + 4 + ROW_H * 3);
        doc.text('i) Organizaciones No Gubernamentales (ONG)', RECT_L + 78, base + 7.5 + ROW_H * 3);
        drawCheckbox(RECT_L + 74, base + 4 + ROW_H * 4);
        doc.text(
            RECT_L + 78,
            base + 7.5 + ROW_H * 4,
            doc.splitTextToSize('j) Asociaciones Civiles y Fundaciones sin fines de lucro', 50)
        );
        drawCheckbox(RECT_L + 74, base + 4 + 2 + ROW_H * 5);
        doc.text(
            RECT_L + 78,
            base + 7.5 + 2 + ROW_H * 5,
            doc.splitTextToSize(
                'k) Compra y Venta de obras de arte, antigüedades u otros bienes suntuarios, inversión filatélica o numismática',
                50
            )
        );
        // l) Joyerías – Exportación, importación, elaboración o
        // industrialización de joyas o bienes con metales o piedras preciosas
        //COLUMNA 3
        drawCheckbox(RECT_L + 134, base + 4 + ROW_H * 2);
        doc.text(
            RECT_L + 138,
            base + 7.5 + ROW_H * 2,
            doc.splitTextToSize(
                'Joyerías - Exportación, importación, elaboración o industrialización de joyas o bienes con metales o piedras preciosas',
                50
            )
        );
        drawCheckbox(RECT_L + 134, base + 4 + ROW_H * 4);
        doc.text(
            RECT_L + 138,
            base + 7.5 + ROW_H * 4,
            doc.splitTextToSize('m) Juegos de Azar (Casinos, Bingos, otros)', 50)
        );
        drawCheckbox(RECT_L + 134, base + 4 + ROW_H * 5);
        doc.text(
            RECT_L + 138,
            base + 7.5 + ROW_H * 5,
            doc.splitTextToSize('n) Personas Politicamente Expuestas PEP', 50)
        );
        drawCheckbox(RECT_L + 134, base + 4 + ROW_H * 6);
        doc.text(RECT_L + 138, base + 7.5 + ROW_H * 6, doc.splitTextToSize('o) Funcionarios Públicos', 50));
        drawCheckbox(RECT_L + 134, base + 4 + ROW_H * 7);
        doc.text(RECT_L + 138, base + 7.5 + ROW_H * 7, doc.splitTextToSize('p) Compra y Venta de Autos Usados', 50));

        doc.setFontSize(fontSize);
    };
    const drawRow = ({cols, labels, base, height, fill}) => {
        const widthTotal = RECT_W - RECT_L;
        let colLeft = RECT_L;
        cols.forEach(x => {
            doc.rect(colLeft, base, (widthTotal * x) / 100, height, fill);
            if (labels) {
                let label = labels.shift();
                let valueColLeft = colLeft + getTextWidth(label[0]) + 2;
                doc.text(label[0], colLeft + 1, base + 3.5);
                if (label[1]) {
                    if (typeof label[1] === 'string') {
                        doc.text(label[1], valueColLeft, base + 3.5);
                    } else {
                        label[1].forEach(x => {
                            if (x.id === label[2]) {
                                drawCheckbox(valueColLeft, base, true);
                            } else {
                                drawCheckbox(valueColLeft, base);
                            }

                            doc.text(x.name, (valueColLeft = valueColLeft + 3.5), base + 3.5);
                            valueColLeft = valueColLeft + getTextWidth(x.name) + 2;
                        });
                    }
                }
            }
            colLeft = colLeft + (widthTotal * x) / 100;
        });
    };

    const drawCols = (cols, base, height, fill) => {
        drawRow({cols: cols, base: base, height: height, fill: fill});
    };

    const drawPage1Rects = base => {
        doc.setFontType('bold');

        doc.setFillColor('#7e817e');
        drawCols([100], base, 11, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('FORMULARIO AC. 010', base + 4);

        doc.setFillColor('#c8c8c8');
        drawCols([100], (base = base + 11), 7, 'F');

        doc.setFontSize(10);
        textCenter('DATOS DE LA SOLICITUD', base + 4);

        doc.setFontSize(8);
        doc.setTextColor('#000000');

        // DATOS SUCURSAL
        doc.setFillColor(0);
        doc.setFontType('normal');
        drawRow({
            cols: [50, 50],
            labels: [
                ['Solicitud Nro:', getApplicationText('applicationNumber')],
                ['Fecha:', getMomentText('createdAt')]
            ],
            base: (base = base + 7),
            height: ROW_H
        });

        drawRow({
            cols: [50, 50],
            labels: [['Canal:'], ['Sucursal:', getApplicationText('applicationBranch')]],
            base: (base = base + ROW_H),
            height: ROW_H
        });
        // DATOS DEL CLIENTE
        doc.setFontType('bold');
        doc.setFontSize(10);
        doc.text('DATOS DEL CLIENTE (1° Titular):', 10, base + ROW_H * 2 - 1);
        doc.setFontType('normal');
        doc.setFillColor('#ebedeb');
        drawCols([100], (base = base + 2 * ROW_H), ROW_H * 6, 'F');
        doc.setFillColor(0);
        doc.setFontSize(8);
        drawRow({
            cols: [50, 50],
            labels: [['Nombre/s:', getApplicationText('firstName')], ['Apellido/s:', getApplicationText('lastName')]],
            base: base,
            height: ROW_H
        });
        drawRow({
            cols: [40, 30, 30],
            labels: [
                ['Tipo de Documento:', enums.eID_TYPE, getApplicationText('documentType')],
                ['Nro.Documento:', getApplicationText('documentNumber')],
                ['Fecha de Nacimiento:', getFormattedDate(application.dob)]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });
        drawRow({
            cols: [30, 13, 57],
            labels: [
                ['', enums.eCUIL_TYPE, getApplicationText('cuitType')],
                ['Sexo:', [{id: 'M', name: 'M'}, {id: 'F', name: 'F'}], getApplicationText('gender')],
                ['Estado Civil:', enums.eMARITAL_STATUS, getApplicationText('maritalStatus')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });
        doc.text(getApplicationText('cuit'), 48, base + 3.5);

        drawRow({
            cols: [30, 20, 50],
            labels: [
                ['Teléfono:', getApplicationText('phoneLandLine')],
                ['Celular:', getApplicationText('phoneMobile')],
                ['Email(*):', getApplicationText('email')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [33.33, 33.33, 33.33],
            labels: [
                ['País Nacimiento:', getApplicationText('countryOfBirth')],
                ['Nacionalidad:', getApplicationText('nationality')],
                ['Residencia:', getApplicationText('residenceType')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [100],
            labels: [
                [
                    'Indique conforme legislación de cada jurisdicción Sí |_| No |_| posee residencia a efectos fiscales en una o más jurisdicciones distintas de Argentina'
                ]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        // DATOS COMPLEMENTARIOS
        doc.setFontSize(9);
        doc.setFontType('bold');
        doc.text('DATOS COMPLEMENTARIOS: Domicilio Particular:', 10, base + ROW_H * 2 - 1);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setFillColor('#ebedeb');
        drawCols([100], (base = base + 2 * ROW_H), ROW_H * 2, 'F');
        doc.setFillColor(0);

        drawRow({
            cols: [60, 10, 10, 10, 10],
            labels: [
                ['Calle:', getApplicationText('addressStreet')],
                ['N°:', getApplicationText('addressNumber')],
                ['Piso:', getApplicationText('addressFloor')],
                ['Dpto.:', getApplicationText('addressApartment')],
                ['CP:', getApplicationText('zipCode')]
            ],
            base: base,
            height: ROW_H
        });

        drawRow({
            cols: [42, 25, 33],
            labels: [
                ['Entre calles:', getApplicationText('addressBetweenStreets')],
                ['Provincia:', getApplicationText('province')],
                ['Localidad:', getApplicationText('city')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        // CONYUGUE
        doc.setFontSize(9);
        doc.setFontType('bold');
        doc.text('CÓNYUGE/CONVIVIENTE:', 10, base + ROW_H * 2 - 1);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setFillColor('#ebedeb');
        drawCols([100], (base = base + 2 * ROW_H), ROW_H * 3, 'F');
        doc.setFillColor(0);

        drawRow({
            cols: [16, 42, 42],
            labels: [
                ['', [{id: '', name: '2° Titular'}]],
                ['Nombre/s:', getApplicationText('spouseFirstName')],
                ['Apellido/s:', getApplicationText('spouseLastName')]
            ],
            base: base,
            height: ROW_H
        });
        drawRow({
            cols: [46, 27, 27],
            labels: [
                ['Tipo Documento:', enums.eID_TYPE, getApplicationText('spouseDocumentType')],
                ['N° de Documento:', getApplicationText('spouseDocumentNumber')],
                ['Fecha de Nacimiento:', getFormattedDate(application.spouseDob)]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [50, 50],
            labels: [
                ['Nacionalidad:', getApplicationText('spouseNationality')],
                ['', enums.eCUIL_TYPE, getApplicationText('spouseCuitType')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });
        doc.text(getApplicationText('spouseCuit'), 143, base + 3.5);

        // DATOS LABORALES SOLICITANTE
        doc.setFontSize(9);
        doc.setFontType('bold');
        doc.text('DATOS LABORALES SOLICITANTE:', 10, base + ROW_H * 2 - 1);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setFillColor('#ebedeb');
        drawCols([100], (base = base + 2 * ROW_H), ROW_H * 8, 'F');
        doc.setFillColor(0);
        drawCols([100], base, ROW_H * 4);

        drawLaborBoxes(base, 'mainProfessionalActivity', 'mainProfessionalActivityType');

        drawRow({
            cols: [50, 30, 20],
            labels: [
                ['Empresa:', getApplicationText('workingCompany')],
                ['Ramo:', getApplicationText('workingBranch')],
                ['CUIT:', getApplicationText('workingCUIT')]
            ],
            base: (base = base + ROW_H * 4),
            height: ROW_H
        });

        drawRow({
            cols: [33.33, 33.33, 33.33],
            labels: [
                ['Fecha de Ingreso:', getMomentText('workingDateFrom')],
                ['Ingresos Netos Mensuales:', getApplicationText('netMonthlyIncome')],
                ['Cargo:', getApplicationText('workingJob')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [58, 10, 10, 10, 12],
            labels: [
                ['Calle:', getApplicationText('workingAddressStreet')],
                ['N°:', getApplicationText('workingAddressNumber')],
                ['Piso:', getApplicationText('workingAddressFloor')],
                ['Dpto:', getApplicationText('workingAddressApartment')],
                ['CP:', getApplicationText('workingZipCode')]
                // ['Teléfono:', getApplicationText('workingCompanyPhone')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [42, 25, 33],
            labels: [
                ['Entre calles:', getApplicationText('workingAddressBetweenStreets')],
                ['Provincia:', getApplicationText('workingProvince')],
                ['Localidad:', getApplicationText('workingCity')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        // DATOS LABORALES CONYUGE
        doc.setFontSize(9);
        doc.setFontType('bold');
        doc.text('DATOS LABORALES CÓNYUGE/CONVIVIENTE:', 10, base + ROW_H * 2 - 1);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setFillColor('#ebedeb');
        drawCols([100], (base = base + 2 * ROW_H), ROW_H * 8, 'F');
        doc.setFillColor(0);
        drawCols([100], base, ROW_H * 4);

        drawLaborBoxes(base, 'spouseMainProfessionalActivity', 'spouseMainProfessionalActivityType');

        drawRow({
            cols: [50, 30, 20],
            labels: [
                ['Empresa:', getApplicationText('spouseWorkingCompany')],
                ['Ramo:', getApplicationText('spouseWorkingBranch')],
                ['CUIT:', getApplicationText('spouseWorkingCUIT')]
            ],
            base: (base = base + ROW_H * 4),
            height: ROW_H
        });

        drawRow({
            cols: [33.33, 33.33, 33.33],
            labels: [
                ['Fecha de Ingreso:', getMomentText('spouseWorkingDateFrom')],
                ['Ingresos Netos Mensuales:', getApplicationText('spouseNetMonthlyIncome')],
                ['Cargo:', getApplicationText('spouseWorkingJob')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [58, 10, 10, 10, 12],
            labels: [
                ['Calle:', getApplicationText('spouseWorkingAddressStreet')],
                ['N°:', getApplicationText('spouseWorkingAddressNumber')],
                ['Piso:', getApplicationText('spouseWorkingAddressFloor')],
                ['Dpto:', getApplicationText('spouseWorkingAddressApartment')],
                ['CP:', getApplicationText('spouseWorkingAddressZipCode')]
                // ['Teléfono:', getApplicationText('spouseWorkingCompanyPhone')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [42, 25, 33],
            labels: [
                ['Entre calles:', getApplicationText('spouseWorkingAddressBetweenStreets')],
                ['Provincia:', getApplicationText('spouseWorkingProvince')],
                ['Localidad:', getApplicationText('spouseWorkingCity')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        // DATOS IMPOSITIVOS
        doc.setFontSize(9);
        doc.setFontType('bold');
        doc.text('DATOS IMPOSITIVOS:', 10, base + ROW_H * 2 - 1);
        doc.setFontType('normal');
        doc.setFontSize(8);
        doc.setFillColor('#ebedeb');
        drawCols([100], (base = base + 2 * ROW_H), ROW_H * 10, 'F');
        doc.setFillColor(0);

        drawCols([100], base, ROW_H * 3);
        drawTaxesGanancias(base, 'conditionIncomeTax');

        drawCols([100], (base = base + ROW_H * 3), ROW_H * 3);
        drawTaxesIVA(base, 'conditionVAT');

        drawCols([100], (base = base + ROW_H * 3), ROW_H * 4);
        drawIIBB(base, 'conditionIibb', 'conditionIibbNumber', 'conditionInflationAdjustment');

        // REFERENCIAS
        doc.setFontSize(9);
        doc.setFontType('bold');
        doc.text('REFERENCIAS:', 10, (base = base + ROW_H * 5) - 1);
        doc.setFontType('normal');
        doc.setFillColor('#ebedeb');
        doc.setFontSize(8);
        drawCols([100], base, ROW_H * 3, 'F');
        doc.setFillColor(0);
        drawRow({
            cols: [10, 31, 35, 24],
            labels: [
                ['Referencia1'],
                ['Nombre/s:', getApplicationText('referenceName1')],
                ['Apellido/s:', getApplicationText('referenceLastName1')],
                ['Teléfono:', getApplicationText('referenceTelephone1')]
            ],
            base: base,
            height: ROW_H
        });
        drawRow({
            cols: [10, 31, 35, 24],
            labels: [
                ['Referencia2'],
                ['Nombre/s:', getApplicationText('referenceName2')],
                ['Apellido/s:', getApplicationText('referenceLastName2')],
                ['Teléfono:', getApplicationText('referenceTelephone2')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });
        drawRow({
            cols: [10, 31, 35, 24],
            labels: [
                ['Referencia3'],
                ['Nombre/s:', getApplicationText('referenceName3')],
                ['Apellido/s:', getApplicationText('referenceLastName3')],
                ['Teléfono:', getApplicationText('referenceTelephone3')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });
    };

    const drawPage2Rects = base => {
        const percentage = formatPercentage(
            (getApplicationText('productNetAmount') * 100) / getApplicationText('vehicleValuation')
        );

        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], base, ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('DETALLE DE PRODUCTOS', base + 3.5);

        doc.setTextColor('#000000');
        doc.setFontSize(8);

        doc.text(
            'Habiéndome informado sobre los atributos, costos y prestaciones de las coberturas de seguro disponibles',
            RECT_L,
            (base = base + ROW_H + 3)
        );
        doc.text('PRESTAMO PRENDARIO AJUSTABLE POR LA VARIACION DE LA UVA', RECT_L, (base = base + ROW_H));
        doc.setFontType('normal');
        // drawCols([33, 31, 36], (base = base + 1), ROW_H * 5);
        drawRow({
            cols: [33, 31, 36],
            labels: [
                ['cód. Producto:', getApplicationText('productCode')],
                ['Moneda:', getApplicationText('productCurrency')],
                ['Monto:', formatMoney(getApplicationText('productPrincipalAmount'))]
            ],
            base: (base = base + 1),
            height: ROW_H * 5
        });
        doc.text(
            `Amortización: ${getApplicationText('productAmortizationMethod')}`,
            RECT_L + 64,
            base + -2 + ROW_H * 2
        );
        doc.text(
            `Cuotas iguales en  UVA: ${formatNumber(getApplicationText('productPeriodicPaymentUva'), 2)}`,
            RECT_L + 64,
            base + -2 + ROW_H * 3
        );
        const texto =
            'Clausula de Ajuste: el préstamo sera ajustable conforme la variación de la Unidad de Valor Adquisitivo que publica el Banco Central de la República Argentina desde la fecha de desembolso';
        doc.text(RECT_L + 123, base - 2 + ROW_H * 2, doc.splitTextToSize(texto, 63));
        drawRow({
            cols: [25, 25, 25, 25],
            labels: [
                ['CFTNA %:', formatPercentage(getApplicationText('productTotalFinancialCostWithVAT'))],
                ['TNA:', formatPercentage(getApplicationText('productNominalAnualRate'))],
                ['TEA:', formatPercentage(getApplicationText('productEffectiveAnualRate'))],
                ['Plazo:', getApplicationText('productTerm')]
            ],
            base: (base = base + ROW_H * 5),
            height: ROW_H
        });

        drawRow({
            cols: [50, 50],
            labels: [
                ['Fecha de pago cuota (1 al 30):', getApplicationText('productPaymentDate')],
                ['Motivo del Préstamo:', enums.eMOTIVO_PRESTAMO, getApplicationText('productDestination')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });
        doc.setFontType('bold');

        doc.text('SEGURO DEL AUTOMOTOR', RECT_L, (base = base + ROW_H * 2));
        doc.setFontType('normal');
        drawRow({
            cols: [37, 49, 14],
            labels: [
                ['Compañia:', getApplicationText('vehicleInsuranceCompany')],
                ['Cobertura:', getApplicationText('vehicleInsuranceCoverageType')],
                ['Costo:', formatMoney(getApplicationText('vehicleInsuranceCost'))]
            ],
            base: (base = base + 1),
            height: ROW_H
        });
        doc.setFontType('bold');

        doc.text('TARJETAS', RECT_L, (base = base + ROW_H * 2));
        doc.setFontType('normal');
        drawCols([100], (base = base + 1), ROW_H);

        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], (base = base + ROW_H * 2), ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('DESTINO DEL PRÉSTAMO - DATOS DEL AUTOMOTOR', base + 3.5);

        doc.setTextColor('#000000');
        doc.setFontSize(8);
        doc.setFontType('normal');

        drawRow({
            cols: [25, 25, 25, 25],
            labels: [
                ['Categoria:', enums.getEnumRow(enums.eVEHICULO_TIPO, getApplicationText('vehicleType')).name],
                ['0km:', getApplicationText('vehicleIsNew') ? 'Sí' : 'No'],
                ['Modelo año:', getApplicationText('vehicleYear')],
                ['Posee GNC:', getApplicationText('vehicleHasNaturalGas') ? 'Sí' : 'No']
            ],
            base: (base = base + ROW_H * 2),
            height: ROW_H
        });
        drawRow({
            cols: [25, 45, 15, 15],
            labels: [
                ['Marca:', getApplicationText('vehicleMake')],
                ['Modelo:', getApplicationText('vehicleModel')],
                ['Valor:', getApplicationText('vehicleValuation')],
                ['Patente:', getApplicationText('vehicleDomain')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        drawRow({
            cols: [50, 50],
            labels: [
                ['Nro. de chasis:', getApplicationText('vehicleChassisNumber')],
                ['Nro. de motor:', getApplicationText('vehicleEngineNumber')]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });
        drawRow({
            cols: [50, 50],
            labels: [
                [
                    'Uso:',
                    enums.getEnumRow(enums.eVEHICULO_USO_DECLARADO_APPLICATION, getApplicationText('vehicleUsageType'))
                        .name
                ],
                ['Porcentaje de financiación:', percentage]
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], (base = base + ROW_H * 2), ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('ENTREGA DE PÓLIZA Y DE GARANTIA', base + 3.5);

        doc.setTextColor('#000000');
        doc.setFontSize(8);
        doc.setFontType('normal');

        drawRow({
            cols: [10, 23, 23, 44],
            labels: [
                ['Póliza'],
                ['', [{id: 'POLIZA_PARTICULAR', name: 'Domicilio Particular'}]],
                ['', [{id: 'POLIZA_LABORAL', name: 'Domicilio Laboral'}]],
                ['']
            ],
            base: (base = base + ROW_H * 2),
            height: ROW_H
        });
        drawRow({
            cols: [10, 23, 23, 44],
            labels: [
                ['Garantía'],
                ['', [{id: 'GARANTIA_PARTICULAR', name: 'Domicilio Particular'}]],
                ['', [{id: 'GARANTIA_LABORAL', name: 'Domicilio Laboral'}]],
                ['']
            ],
            base: (base = base + ROW_H),
            height: ROW_H
        });

        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], (base = base + ROW_H * 2), ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('LEGALES', base + 3.5);

        doc.setTextColor('#000000');
        doc.setFontSize(7);
        doc.setFontType('normal');

        drawCols([100], (base = base + ROW_H * 2), ROW_H * 11);
        drawTextLegales(base);

        drawCols([100], (base = base + ROW_H * 11), ROW_H + 2);
        let text = `(2) Para el caso que posea una o más residencias a efectos fiscales en una jurisdicción distinta de Argentina se deberá completar y adjuntar la Declaración Jurada de Sujeto FATCA/CRS Persona Física.`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        // drawCols([100], (base = base + ROW_H + 2), ROW_H + 2);
        // text = `(3) El Costo Financiero Total Nominal Anual (CFTNA) incluye intereses, IVA sobre intereses, seguro de vida (en caso de corresponder) y gastos de otorgamiento, de corresponder.`;
        // doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H + 2), ROW_H * 11);
        doc.setFontType('bold');
        text = `Declaración Jurada: Persona Expuesta Políticamente (PEP)`;
        doc.text(text, RECT_L + 1, (base = base + 3), {maxWidth: 185, align: 'left'});

        doc.setFontType('normal');
        text = `Son personas políticamente expuestas (pep) las siguientes:`;
        doc.text(text, RECT_L + 1, (base = base + 4), {maxWidth: 185, align: 'left'});

        text = `a) Los funcionarios públicos extranjeros: quedan comprendidas las personas que desempeñen o hayan desempeñado dichas funciones hasta dos años anteriores a la fecha en que fue realizada la operatoria, ocupando alguno de los siguientes cargos: 1 - Jefes de Estado, jefes de Gobierno, gobernadores, intendentes, ministros, secretarios y subsecretarios de Estado y otros cargos gubernamentales equivalentes; 2 - Miembros del Parlamento/Poder Legislativo; 3 - Jueces, miembros superiores de tribunales y otras altas instancias judiciales y administrativas de ese ámbito del Poder Judicial; 4 - Embajadores y cónsules; 5 - Oficiales de alto rango de las fuerzas armadas (a partir de coronel o grado equivalente en la fuerza y/o país de que se trate) y de las fuerzas de seguridad pública (a partir de comisario o rango equivalente según la fuerza y/o país de que se trate); 6 - Miembros de los órganos de dirección y control de empresas de propiedad estatal; 7 - Directores, gobernadores, consejeros, síndicos o autoridades equivalentes de bancos centrales y otros organismos estatales de regulación y/o supervisión; b) Los cónyuges, o convivientes reconocidos legalmente, familiares en línea ascendiente o descendiente hasta el primer grado de consanguinidad y allegados cercanos de las personas a que se refieren los puntos 1 a 7 del artículo 1º, inciso a), durante el plazo indicado. A estos efectos, debe entenderse como allegado cercano a aquella persona pública y comúnmente conocida por su íntima asociación a la persona definida como Persona Expuesta Políticamente en los puntos precedentes, incluyendo a quienes están en posición de realizar operaciones por grandes sumas de dinero en nombre de la referida persona. c) Los funcionarios públicos nacionales que a continuación se señalan que se desempeñen o hayan desempeñado hasta dos años anteriores a la fecha en que fue realizada la operatoria: 1 - El Presidente y Vicepresidente de la Nación; 2 - Los Senadores y Diputados de la Nación; 3 - Los magistrados del Poder Judicial de la Nación; 4 - Los magistrados del Ministerio Público de la Nación; 5 - El Defensor del Pueblo de la Nación y los adjuntos del Defensor del Pueblo; 6- El Jefe de Gabinete de Ministros, los Ministros, Secretarios y Subsecretarios del Poder Ejecutivo Nacional; `;
        doc.text(text, RECT_L + 1, (base = base + 4), {maxWidth: 185, align: 'justify'});
    };

    const drawPage3Rects = base => {
        doc.setTextColor('#000000');
        doc.setFontSize(9);
        drawCols([100], base, ROW_H * 37);

        doc.setFontSize(7);
        let text = `7- Los interventores federales; 8- El Síndico General de la Nación y los Síndicos Generales Adjuntos de la Sindicatura General de la Nación, el presidente y los auditores generales de la Auditoría General de la Nación, las autoridades superiores de los entes reguladores y los demás órganos que integran los sistemas de control del sector público nacional, y los miembros de organismos jurisdiccionales administrativos; 9- Los miembros del Consejo de la Magistratura y del Jurado de Enjuiciamiento; 10- Los Embajadores y Cónsules; 11- El personal de las Fuerzas Armadas, de la Policía Federal Argentina, de Gendarmería Nacional, de la Prefectura Naval Argentina, del Servicio Penitenciario Federal y de la Policía de Seguridad Aeroportuaria con jerarquía no menor de coronel o grado equivalente según la fuerza; 12- Los Rectores, Decanos y Secretarios de las Universidades Nacionales; 13- Los funcionarios o empleados con categoría o función no inferior a la de director general o nacional, que presten servicio en la Administración Pública Nacional, centralizada o descentralizada, las entidades autárquicas, los bancos y entidades financieras del sistema oficial, las obras sociales administradas por el Estado, las empresas del Estado, las sociedades del Estado y el personal con similar categoría o función, designado a propuesta del Estado en las sociedades de economía mixta, en las sociedades anónimas con participación estatal y en otros entes del sector público; 14- Todo funcionario o empleado público encargado de otorgar habilitaciones administrativas para el ejercicio de cualquier actividad, como también todo funcionario o empleado público encargado de controlar el funcionamiento de dichas actividades o de ejercer cualquier otro control en virtud de un poder de policía; 15- Los funcionarios que integran los organismos de control de los servicios públicos privatizados, con categoría no inferior a la de director general o nacional; 16- El personal que se desempeña en el Poder Legislativo de la Nación, con categoría no inferior a la de director; 17- El personal que cumpla servicios en el Poder Judicial de la Nación y en el Ministerio Público de la Nación, con categoría no inferior a Secretario; 18- Todo funcionario o empleado público que integre comisiones de adjudicación de licitaciones, de compra o de recepción de bienes, o participe en la toma de decisiones de licitaciones o compras; 19- Todo funcionario público que tenga por función administrar un patrimonio público o privado, o controlar o fiscalizar los ingresos públicos cualquiera fuera su naturaleza; 20- Los directores y administradores de las entidades sometidas al control externo del Honorable Congreso de la Nación, de conformidad con lo dispuesto en el artículo 120 de la Ley Nº 24.156. d) Los funcionarios públicos provinciales, municipales y de la Ciudad Autónoma de Buenos Aires que a continuación se señalan, que se desempeñen o hayan desempeñado hasta dos años anteriores a la fecha en que fue realizada la operatoria: 1- Gobernadores, Intendentes y Jefe de Gobierno de la Ciudad Autónoma de Buenos Aires; 2- Ministros de Gobierno, Secretarios y Subsecretarios; Ministros de los Tribunales Superiores de Justicia de las provincias y de la Ciudad Autónoma de Buenos Aires; 3- Jueces y Secretarios de los Poderes Judiciales Provinciales y de la Ciudad Autónoma de Buenos Aires. 4- Legisladores provinciales, municipales y de la Ciudad Autónoma de Buenos Aires; 5- Los miembros del Consejo de la Magistratura y del Jurado de Enjuiciamiento; 6- Máxima autoridad de los Organismos de Control y de los entes autárquicos provinciales, municipales y de la Ciudad Autónoma de Buenos Aires; 7- Máxima autoridad de las sociedades de propiedad de los estados provinciales, municipales y de la Ciudad Autónoma de Buenos Aires; e) Las autoridades y apoderados de partidos políticos a nivel nacional, provincial y de la Ciudad Autónoma de Buenos Aires, que se desempeñen o hayan desempeñado hasta dos años anteriores a la fecha en que fue realizada la operatoria. f) Las autoridades y representantes legales de organizaciones sindicales y empresariales (cámaras, asociaciones y otras formas de agrupación corporativa con excepción de aquéllas que únicamente administren las contribuciones o participaciones efectuadas por sus socios, asociados, miembros asociados, miembros SOLICITANTE s y/o las que surgen de acuerdos destinados a cumplir con sus objetivos estatutarios) que desempeñen o hayan desempeñado dichas funciones hasta dos años anteriores a la fecha en que fue realizada la operatoria. El alcance establecido se limita a aquellos rangos, jerarquías o categorías con facultades de decisión resolutiva, por lo tanto se excluye a los funcionarios de niveles intermedios o inferiores. g) Las autoridades y representantes legales de las obras sociales contempladas en la Ley Nº 23.660, que desempeñen o hayan desempeñado dichas funciones hasta dos años anteriores a la fecha en que fue realizada la operatoria. El alcance establecido se limita a aquellos rangos, jerarquías o categorías con facultades de decisión resolutiva, por lo tanto se excluye a los funcionarios de niveles intermedios o inferiores. h) Las personas que desempeñen o que hayan desempeñado hasta dos años anteriores a la fecha en que fue realizada la operatoria, funciones superiores en una organización internacional y sean miembros de la alta gerencia, es decir, directores, subdirectores y miembros de la Junta o funciones equivalentes excluyéndose a los funcionarios de niveles intermedios o inferiores. i) Los cónyuges o convivientes reconocidos legalmente, y familiares en línea ascendiente o descendiente hasta el primer grado de consanguinidad, de las personas a que se refieren los puntos c; d; e; f; y h durante los plazos que para ellas se indican`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        doc.setFontType('bold');
        text = `El que suscribe declara bajo juramento que los datos consignados en la presente son correctos, completos y fiel expresión de la verdad y que SI - NO se encuentra incluido y/o alcanzado dentro de la "Nómina de Funciones de Personas Expuestas Políticamente" aprobada por la Unidad de Información Financiera, cuyos textos he leído y suscribo (Resolución 52/2012 de la UIF, complementarias y/o modificatorias).`;
        doc.text(text, RECT_L + 1, base + 113, {maxWidth: 185, align: 'justify'});
        doc.setFontType('normal');

        text = `En caso afirmativo indicar a continuacion:
        
        Cargo:_____________________________________________ Función:___________________________________________________
        
        Jerarquía, o carácter de la relación:________________________________________________________________________________
        
        Además, asume el compromiso de informar cualquier modificación que se produzca a este respecto, dentro de los treinta días de ocurrida, mediante la presentación de una nueva declaración jurada.
        Caso Afirmativo, firma y aclaración del funcionario interviniente por la empresa:_____________________________________________`;
        doc.text(text, RECT_L + 1, base + 130, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 37), ROW_H * 11);

        doc.setFontType('bold');
        text = `Cláusula sobre Persona Declarable de acuerdo a la RG 3826/2015 de la AFIP (CRS)`;
        doc.text(text, 11, (base = base + 3));
        doc.setFontType('normal');
        text = `El Titular declara, que conoce y acepta en todos sus términos la regulación relacionada al Régimen de información financiera de sujetos no residentes, normado en la RG 3826/2015 de la AFIP modificatorias y complementarias acorde al "Common Reporting Standard (CRS)", obligándome irrevocablemente frente a Aracar Financiera S.A. (en adelante, "ARACAR") a cumplir con todas las obligaciones que le sean impuestas a fin del cumplimiento del mencionado régimen. A los efectos de la presente, declara tener conocimiento de los siguientes conceptos bajo la regulación mencionada: El término "persona de una jurisdicción declarable" se refiere a una persona física o entidad que reside en una jurisdicción declarable de conformidad con la legislación tributaria de dicha jurisdicción, o el patrimonio de una sucesión de un causante residente de una jurisdicción declarable. En este sentido, una entidad, ya sea una asociación, una sociedad de responsabilidad limitada o acuerdo similar que carezca de residencia a los fines tributarios se considerará como residente en la jurisdicción en la cual se encuentra su lugar de administración efectiva. El término "jurisdicción declarable" se refiere a una jurisdicción (i) con la cual existe un acuerdo en vigencia en virtud del cual hay una obligación vigente de brindar la información establecida en el Artículo I, y (ii) que esté identificada en la lista publicada. Quien suscribe declara bajo juramento, a todos los efectos legales y regulatorios correspondientes, que: La totalidad de los datos e informaciones personales indicadas en esta Solicitud y en declaraciones juradas complementarias integradas y suscriptas por la Sociedad, para su presentación a la empresa, inclusive los números de inscripción fiscal (CUIT/CUIL/CDI/NIF y/o similar) son verdaderos, válidos, correctos y completos; El/la que suscribe se obliga a actualizar cualquiera de las informaciones brindadas en este Contrato, informando cualquier cambio dentro de los 10 (diez) días de ocurrido dicho cambio, quedando obligado a otorgar a la empresa todas las autorizaciones correspondientes a los efectos de cumplir con lo normado en la RG 3826/2015 de la AFIP modificatoria y complementarias, incluyendo -pero no limitado a- otorgar los permisos correspondientes a los fines de que la empresa pueda informar los datos que correspondan a las autoridades fiscales locales, las autoridades fiscales de las jurisdicciones declarables según definición del CRS, y/o cualquier otra entidad pública o privada que deba recibir esta información a los fines de cumplir con lo normado en la RG 3826/2015 de la AFIP modificatoria y complementarias`;
        doc.text(text, RECT_L + 1, (base = base + 3), {maxWidth: 185, align: 'justify'});
    };

    const drawPage4Rects = base => {
        doc.setTextColor('#000000');
        doc.setFontSize(8);
        drawCols([100], base, ROW_H * 2 + 2);
        let text = `Cualquier declaración falsa, incompleta, y/o cualquier falta de actualización de datos que no sea informada a la empresa dentro de los 10 (diez) días de ocurrida y/o la falta de otorgamiento de las autorizaciones que la empresa exija al Titular será considerada por la empresa como un grave incumplimiento de las obligaciones a cargo del Titular, y en consecuencia la empresa quedará facultada a proceder al cierre de cuentas del Titular.`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 2 + 2), ROW_H * 5 + 2);
        text = `El solicitante declara bajo juramento que los datos consignados en la solicitud de los servicios requeridos bajo la presente son correctos, completos y fiel expresión de la verdad. Además asume el compromiso de informar cualquier modificación que se produzca a este respecto, dentro de los treinta días de ocurrida, mediante la presentación de una nueva declaración jurada. En el caso de ser Sujeto Obligado de la Ley 25.246 y modificatorias Encubrimiento y Lavado de Activos de Origen Delictivo, según lo establecido en su Art. 20, declaro bajo juramento que damos estricto cumplimiento a las disposiciones legales vigentes en materia de Prevención del Lavado de Dinero y Financiamiento del Terrorismo. Sujeto Obligado: SI NO (De ser sujeto obligado debe presentar la "Constancia de Inscripción en la UIF")`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 5 + 2), ROW_H * 11);
        text = `Cualquier cambio o modificación en los conceptos, condiciones, comisiones y/o cargos establecidos en el presente será informado con una antelación no menor a (60) sesenta días corridos, salvo cuando se produzcan en el mercado financiero situaciones irregulares que no permitan actuar con la mencionada antelación. De no mediar rechazo por el Titular dentro del plazo mencionado, la empresa considerará que las nuevas condiciones tendrán vigencia al vencimiento de dicho plazo. En el caso de cambios que signifiquen disminuciones en las comisiones o gastos, los nuevos importes podrán ser aplicados sin necesidad de aguardar el plazo citado, ni podrán ser protestados. Toda nueva comisión y gasto debitado sin el consentimiento del Titular o mediando oposición al mismo, será reintegrado dentro de los cinco (5) días hábiles siguientes a la fecha en que se presenta el reclamo a la empresa, con más intereses compensatorios pertinentes, aplicando a ese efecto dos veces la tasa promedio del último mes disponible que surja de la encuesta de tasas de interés de depósitos a plazo fijo de 30 a 59 días - de pesos o dólares, según la moneda de la operación - informada por el Banco Central de la República Argentina a la fecha de celebración del contrato - o, en caso de que no estuviera disponible, la última informada - sobre la base de la información provista por la totalidad de bancos públicos y privados, hasta el límite equivalente al cien por ciento (100 %) de los débitos observados. Para el caso en que el producto contratado posea bonificaciones parciales y/o totales en comisiones y/o cargos, cualquier cambio y/o suspensión y/o finalización de la bonificación será notificada con una antelación mínima de (60) sesenta días corridos previo a su entrada en vigencia. Los plazos estipulados en la presente cláusula quedan sujetos a las variaciones que en el futuro disponga la autoridad de control. El Titular acepta expresamente que cualquier cambio o modificación en los conceptos, condiciones, comisiones y/o cargos, establecidos en el presente, sean notificados a través de nota enviada a su domicilio, correo electrónico oportunamente suministrado, y/o todos aquellos canales de comunicación que en un futuro establezca el Banco Central de la República Argentina.`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 11), ROW_H * 3);
        text = `El titular declara bajo juramento que toda la información y documentación suministrada a la empresa para el alta de este conjunto de servicios es exacta y auténtica. Asimismo, el titular toma conocimiento de que la falsificación de documentos o uso de datos falsos se encuentra reprimido con pena de (6) meses a dos (2) años de prisión de conformidad a los dispuesto por el Art. 292 del Código Penal, por lo de comprobarse algún delito, la empresa procederá a radicar la denuncia criminal correspondiente. En prueba de ello suscribe la presente.`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 3), ROW_H + 3);
        text = `Recibí copia de la siguiente documentación: Condiciones generales y particulares incluidos en la presente solicitud; cuyos términos y condiciones declaro conocer y aceptar íntegramente.`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H + 3), ROW_H * 10);
        text = `Dentro del plazo de (10) diez días hábiles contados desde la fecha de aprobación o disponibilidad de los productos o servicios solicitados, el Titular podrá retirar de la sucursal los Términos y Condiciones correspondiente al producto o servicio contratado. El Titular podrá revocar la aceptación del producto durante el plazo de (10) diez días hábiles contados a partir de la aprobación de la solicitud notificando a la empresa de manera fehaciente o por el mismo medio en que el producto y servicio fue contratado. La revocación será sin costo ni responsabilidad adicional alguna para el Titular en la medida que no haya utilizado el Préstamo. En caso de utilización del Préstamo, el DEUDOR deberá devolver el importe utilizado, como así también los intereses devengados hasta dicho momento. En los casos en que el producto o servicio se hubiera contratado a distancia (telefónica, por correspondencia, por medios electrónicos, promoción a través de terceros, etc.) el Titular podrá pactar la vía mediante la cual la empresa - dentro del plazo de (10) diez días hábiles de realizada la contratación o de la disponibilidad efectiva del producto o servicio (lo que suceda último)- le proporcionará un ejemplar de los Términos y Condiciones correspondiente al producto o servicio contratado, incluyendo pero no limitado a la puesta a disposición mediante envío por correo o puesta a disposición en la sucursal de radicación del Titular. El Titular podrá revocar la aceptación del producto o servicio dentro de los (10) diez días hábiles contados a partir de la fecha de recepción de los Términos y Condiciones. La revocación será sin costo ni responsabilidad adicional alguna para el Titular.`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 10), ROW_H * 10);
        text = `Manifiesto interés en recibir información de las propuestas comerciales que el la empresa brinda a sus clientes y presto en este acto mi consentimiento, conforme lo establecido en la ley 25.326 de protección de datos personales, a tenor de lo cual declaro conocer y aceptar, para que mis datos personales integren la base de Aracar Financiera S.A., otorgando por el presente, autorización expresa para i) el tratamiento automatizado de dichos datos o información y ii) su utilización en relación con la actividad bancaria, financiera, o de servicios actuales o futuros, que desarrolle Aracar Financiera S.A. o cualquiera de sus subsidiarias o afiliadas. iii)transfiera y/o ceda toda la información referente a su persona sus actividades económicas u operaciones que se hubieran concertado o que se concierten en el futuro, que requieran los organismos de control y/o contralor conforme normativa vigente (incluyendo, pero no limitando al BCRA, la Comisión Nacional de Valores, la Unidad de Información Financiera, etcétera), como así también a las empresas de informes crediticios en los términos del art. 26 de la ley N° 25326. El proporcionar mis datos a la empresa para estos fines no tiene carácter obligatorio, ni condiciona la contratación de servicios con la misma. Declaro conocer y aceptar que en cualquier momento durante la relación contractual en forma gratuita a intervalos no inferiores a seis meses con la empresa podré ejercer mis derechos de información, acceso, rectificación y supresión de datos de la base mencionada, regulado por los artículos 13 y siguientes de la Ley 25.326 de protección de datos personales. Para el ejercicio de tales derechos, he sido informado por la empresa que podré comunicarme sin cargo a info@aracargroup.com o al 011 5365-7739`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 10), ROW_H + 3);
        text = `(*)La dirección de mail informada por el Titular donde se tendrá por válida a todos los efectos de la presente, informe sobre disponibilidad de resúmenes de cuenta y/o tarjeta/s de crédito e información sobre los productos.`;
        doc.text(text, RECT_L + 1, base + 3, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 2 + 3), ROW_H + 2);
        text = `En la ciudad de _____________________________________________ a los ________ días de ________________________  de _____________`;
        doc.text(text, RECT_L + 1, base + 4, {maxWidth: 185, align: 'justify'});

        doc.setFontType('bold');
        drawCols([50, 50], (base = base + ROW_H * 2 + 2), ROW_H * 4);
        doc.text('Firma 1° Titular', RECT_L + 1, base + 3);
        doc.text('Firma 2° Titular', RECT_L + 97, base + 3);
        drawCols([50, 50], (base = base + ROW_H * 4), ROW_H);
        doc.text('Aclaración', RECT_L + 1, base + 3);
        doc.text('Aclaración', RECT_L + 97, base + 3);
        doc.setFontType('normal');
        doc.text('(Letra del Solicitante)', RECT_L + 17, base + 3);
        doc.text('(Letra del Solicitante)', RECT_L + 114, base + 3);
    };

    const drawPage5Rects = base => {
        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], base, ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('CONDICIONES GENERALES DEL CRÉDITO', base + 3.5);

        doc.setTextColor('#000000');
        doc.setFontType('normal');
        doc.setFontSize(8);

        drawRow({
            cols: [50, 50],
            labels: [['Solicitud:'], ['Fecha:']],
            base: (base = base + ROW_H + 3),
            height: ROW_H
        });

        let text = `CLAUSULA INTRODUCTORIA: El Solicitante declara bajo juramento que la información consignada al frente de la presente Solicitud de Crédito es correcta y se ajusta a la verdad y es proporcionada con el objeto de ser aplicada al otorgamiento de un crédito con garantía prendaria sobre un Automóvil que el suscripto solicita a Aracar Financiera S.A. ("la empresa" - el "Acreedor" indistintamente; y la empresa junto con el Solicitante, las "Partes", y cada una de ellas individualmente, la "Parte")`;
        doc.text(text, RECT_L, (base = base + ROW_H + 4), {maxWidth: 185, align: 'justify'});

        text = `OBJETO: El Solicitante por la presente solicita al Acreedor le otorgue un préstamo en pesos ajustable por la variación de la UVA (tal como se define más adelante) por el monto en pesos que se consigna al comienzo de esta Solicitud bajo el título "Detalle de Productos" cuyo repago se encontrara garantizado con una prenda con registro sobre un automóvil (en adelante, el "Préstamo"): Se entiende por UVA a las "Unidades de Valor Adquisitivo Actualizables por "CER" - Ley 25.827" establecidas por la Comunicación "A" 6.080 del Banco Central conforme con lo establecido en el Decreto 146/17, cuyo valor en pesos es informado por el Banco Central en su página web. El capital del Préstamo será ajustado conforme la evolución del valor de la UVA desde la fecha de su desembolso hasta el repago total del mismo. A los efectos del cálculo de dicho ajuste, el capital del préstamo será expresado en UVA tomando en cuenta el valor unitario del UVA a la fecha del desembolso.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 10), {maxWidth: 185, align: 'justify'});

        text = `FORMA DE AMORTIZACION DEL CAPITAL. El Solicitante se obliga a restituir el capital del Préstamo en la cantidad de cuotas mensuales y consecutivas que se consigan al comienzo de esta Solicitud bajo el titulo "Detalle del Producto". operando el vencimiento de cada cuota en la fecha allí consignada. El importe de cada cuota resultará de la evolución del valor de la UVA a la fecha de pago respectiva y será determinado en base a la aplicación del denominado "sistema francés" de amortización, conforme la formula que se indica más adelante. A los fines del pago correspondiente, el monto de cada cuota se expresará en UVA, incluyendo el importe de los intereses conforme a la cláusula INTERES de esta Solicitud... El Acreedor emitirá el correspondiente aviso de vencimiento indicando la cantidad de UVA que representa el importe a abonar en cada cuota por todo concepto, siendo obligación de el Solicitante consultar el valor de la UVA del día de pago mediante consulta en la página web del BCRA. Consecuentemente, los pagos de las cuotas de amortización e interés del Préstamo se realizarán en Pesos, por el equivalente del valor de la cantidad de UVA correspondiente a la fecha de hacerse efectivo el pago.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 20), {maxWidth: 185, align: 'justify'});

        text = `INTERESES. A partir de la fecha del desembolso del Préstamo y hasta su efectivo repago total, el Préstamo devengará un interés compensatorio vencido sobre saldos, pagadero por períodos mensuales conjuntamente con las cuotas de amortización de capital. Los intereses correspondientes se devengarán y liquidarán en pesos, calculados sobre el capital prestado con más su actualización conforme al valor de la UVA a la fecha de cálculo respectiva. En el aviso de vencimiento correspondiente, los intereses serán asimismo expresados en UVA, debiendo por ende el DEUDOR determinar el valor correspondiente a la fecha de pago respectiva, conforme lo previsto en la cláusula anterior. La tasa de interés del Préstamo será la consignada en el "Detalle de Productos", más arriba excluido el Impuesto al Valor Agregado o cualquier otro impuesto vigente o futuro que, en caso de corresponder, será a cargo del Solicitante y se cancelará conjuntamente con cada pago de interés. En todos los casos de mora, el saldo de capital adeudado devengará, además del interés compensatorio pactado, un interés punitorio equivalente al cincuenta por ciento (50%) del interés compensatorio hasta la cancelación total del saldo adeudado con su correspondiente actualización por la variación de la UVA más los intereses punitorios que se hayan devengado hasta entonces.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 27), {maxWidth: 185, align: 'justify'});

        text = `FORMA DE CALCULO DE LA CUOTA MENSUAL. Para el cálculo de la cuota periódica se empleará la siguiente fórmula:
        c = (D x i x (1+i)n )/ (((1+i)n ) - 1)) donde: c: cuota periódica. D: saldo adeudado (por capital) actualizado conforme al valor de la UVA luego de cancelada la cuota anterior. i: tasa de interés periódica. Se obtiene de la siguiente expresión: i = r/(m x 100) donde: r: tasa de interés nominal anual, en tanto por ciento. m: cantidad de períodos de pago pactados en un año (12 si se trata de cuotas mensuales). n: cantidad de cuotas que restan abonar incluida la que se calcula.
        La cuota periódica incluye los intereses del período, calculados sobre el saldo adeudado actualizada por la variación de la UVA de acuerdo con la tasa pactada, y una porción de amortización del capital que se obtiene de restar los intereses al importe de la cuota.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 30), {maxWidth: 185, align: 'justify'});

        text = `FORMA Y LUGAR DE PAGO: El Solicitante pagará el Préstamo, sus intereses, accesorios y cualquier otra suma que le correspondiere, en el domicilio del Acreedor. Si la fecha de pago de cualquier importe adeudado por el Solicitante en virtud del presente no fuera un día hábil bancario, dicho pago deberá ser efectuado por el Solicitante en el primer día hábil bancario inmediatamente posterior.
        Toda suma percibida por el Acreedor se imputará en primer lugar a gastos, intereses compensatorios, intereses punitorios y luego al capital.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 18), {maxWidth: 185, align: 'justify'});

        text = `GARANTIA PRENDARIA: En garantía del repago en tiempo y forma del Préstamo de conformidad con los términos del presente y sin perjuicio de la responsabilidad que asume de responder por el cumplimiento de dichas obligaciones con todos sus demás bienes presentes y futuros, el Solicitante gravara con derecho real de prenda en primer grado de privilegio el Automóvil que se describe en el "Detalle de Productos" (el "Bien Prendado") a ser instrumentada a través de un contrato de prenda con registro en los términos del Anexo I al presente (el "Contrato de Prenda"). La debida constitución y registro de este gravamen será condición precedente para el desembolso del Préstamo.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 10), {maxWidth: 185, align: 'justify'});

        text = `PAGARE: En la fecha de desembolso y como condición para el mismo, el Solicitante suscribirá un pagare por el importe del Préstamo para una mejor instrumentación del Préstamo.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 14), {maxWidth: 185, align: 'justify'});

        text = `SEGURO DE VIDA: La Solicitud de Adhesión implica la aceptación por parte del Solicitante de que sea incluida en la Póliza de Seguro de Vida contratada por el Acreedor, quien la podrá renovar automáticamente. La prima y gastos de seguro serán a cargo del Solicitante, el cual deberá efectuar el pago conjuntamente con las alícuotas establecidas. En caso de fallecimiento del Solicitante, la indemnización será percibida por el Acreedor. La documentación que acredite el fallecimiento, deberá estar en poder del Acreedor en un plazo no mayor a 15 días desde producido el hecho. Con el cobro de la indemnización, el prestamista cancelara todas las cuotas futuras del crédito`;
        doc.text(text, RECT_L, (base = base + ROW_H + 4), {maxWidth: 185, align: 'justify'});

        text = `SEGURO DEL BIEN PRENDADO: El deudor contratará un seguro de todo riesgo por el Bien Prendado conforme las condiciones usuales de plaza sobre las tres compañías de seguro provistas por el PRESTAMISTA.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 14), {maxWidth: 185, align: 'justify'});

        text = `EVENTOS DE INCUMPLIMIENTO. Caducidad de los plazos: La mora en el cumplimiento de cualquiera de las obligaciones asumidas por el Solicitante bajo la presente o bajo el Contrato de Prenda, en especial la falta de pago en término de las cuotas o si el Bien Prendado fuese robado o destruido o sufriera deterioro de grado tal que no cubra satisfactoriamente las obligaciones del Solicitante, siempre que el Solicitante no reponga la garantía disminuida por el deterioro o la refuerce o pague en efectivo una cantidad proporcional al deterioro del Bien Prendado, dentro del plazo de quince días contados desde la fecha de la notificación del Acreedor en tal sentido, permitirá al Acreedor declarar la caducidad de todos los plazos y, en consecuencia, a exigir la inmediata e íntegra devolución y reembolso del capital desembolsado, y la aplicación de los intereses compensatorios y punitorios pactados hasta la total devolución del capital adeudado con más los intereses y las costas y costos que se originen como consecuencia del procedimiento de ejecución. Se pacta expresamente que, en caso de mora, ambos intereses se capitalizarán en forma semestral.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 4), {maxWidth: 185, align: 'justify'});
    };

    const drawPage6Rects = base => {
        doc.setTextColor('#000000');
        doc.setFontType('normal');
        doc.setFontSize(8);

        let text = `CAMBIO DE CIRCUNSTANCIA. ILEGALIDAD: Si como consecuencia de un cambio en las leyes, reglamentaciones, o sus respectivas Interpretaciones o principios de aplicación, o como consecuencia de una orden o determinación gubernamental o administrativa de carácter general (una "Norma") el Acreedor quedara sujeto a tasas, gravámenes o impuestos que graven cualesquiera de los desembolsos que se otorguen con motivo del Préstamo, o el mantenimiento de este mismo; el prestamista podrá notificar al Solicitante para la renegociación de buena fe de las condiciones del Préstamo durante un lapso de quince (15) días, vencidos los cuales sin haberse logrado un acuerdo el Acreedor podrá exigir la cancelación íntegra del saldo adeudado, sus intereses y accesorios. No obstante cualquier otra previsión en el presente, si el Acreedor notificara al Solicitante que la introducción de cambios en cualquier ley o reglamentación o en su interpretación o como consecuencia de disposiciones emanadas de autoridad competente convierte en ilegal para el Acreedor mantener el otorgamiento del Préstamo, el Solicitante deberá en forma Inmediata pre cancelar totalmente, sin penalidad alguna, la totalidad de las sumas adeudadas y pendientes de pago bajo el Préstamo.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 4), {maxWidth: 185, align: 'justify'});

        text = `RESPONSABILIDAD SOLIDARIA: Si la presente fuere suscripta por más de un Solicitante, los mismos responderán en forma solidaria por el pago del crédito y las demás obligaciones establecidas en la presente.`;
        doc.text(text, RECT_L, (base = base + ROW_H * 6), {maxWidth: 185, align: 'justify'});

        text = `ASUNCIÓN CASO FORTUITO, FUERZA MAYOR: El Solicitante asume expresamente los riesgos que pudieran originarse del caso fortuito o fuerza Mayor, comprometiéndose a responder aun cuando el cumplimiento de las obligaciones que surgen del presente se vea obstaculizado o impedido por un acaecer fortuito o inevitable.`;
        doc.text(text, RECT_L, (base = base + ROW_H + 3), {maxWidth: 185, align: 'justify'});

        text = `ASUNCIÓN DE LOS IMPUESTOS Y GASTOS: Serán a cargo del Solicitante los impuestos, presentes o futuros, costos y costas, comisiones, tasas o cualquier otro gasto de cualquier naturaleza que existan o fuesen creados en el futuro por el Gobierno Nacional, o por cualquier otro organismo dependiente del Estado Nacional o por organismos provinciales o municipales y que se relacionen con el pago de todos o cualesquiera de los montos debidos de acuerdo al presente Préstamo, de manera tal que todos los pagos por capital, intereses y/o cualquier otro accesorio que fuese debido en virtud de esta Solicitud se efectúen libre de retenciones de dichos impuestos, tasas o gastos. El impuesto al valor agregado que devengue el interés y las comisiones del presente Préstamo no están incluidos en los montos informados por el Acreedor y serán a cargo del Solicitante y deberá ser pagado al prestamista en forma conjunta con el pago del interés y las comisiones. El Solicitante no pagará, y/o en su caso no reembolsará al Acreedor, los impuestos a las ganancias y a los ingresos brutos a los cuales el Acreedor esté sujeto como consecuencia de su propia actividad.`;
        doc.text(text, RECT_L, (base = base + ROW_H * 2), {maxWidth: 185, align: 'justify'});

        text = `El SOLICITANTE tiene el derecho a pre cancelar totalmente (pero no parcialmente) el Préstamo en cualquier momento del plazo del mismo. En dicho caso deberá abonar una comisión equivalente al 5% del Préstamo. La comisión por cancelación anticipada total solo será aplicada cuando al momento de la cancelación no hayan transcurrido al menos la cuarta parte del plazo original del Préstamo o 180 días desde su otorgamiento, de ambos el mayor. El Acreedor no estará obligado a aceptar cancelaciones parciales. Si el SOLICITANTE acordare con el Acreedor la efectivizacion de pagos anticipados parciales, el SOLICITANTE deberá pagar un cargo del 5% (cinco por ciento) del capital pre cancelado. En el caso de cancelaciones parciales anticipadas los intereses se recalcularán sobre el nuevo saldo de capital adeudado.`;
        doc.text(text, RECT_L, (base = base + ROW_H * 6), {maxWidth: 185, align: 'justify'});

        doc.setFontType('bold');
        drawCols([50, 50], (base = base + ROW_H * 6), ROW_H * 4);
        doc.text('Firma 1° Titular', RECT_L + 1, base + 3);
        doc.text('Firma Cónyuge/Cotitular', RECT_L + 97, base + 3);
        drawCols([50, 50], (base = base + ROW_H * 4), ROW_H);
        doc.text('Aclaración', RECT_L + 1, base + 3);
        doc.text('Aclaración', RECT_L + 97, base + 3);
        doc.setFontType('normal');
        doc.text('(Letra del Titular)', RECT_L + 17, base + 3);
        doc.text('(Letra del cónyuge/cotitular)', RECT_L + 113, base + 3);
    };

    const drawEntregaFondos = base => {
        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], base, ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('ENTREGA DE FONDOS', base + 3.5);
        doc.setFontType('normal');

        doc.setTextColor('#000000');
        doc.setFontSize(8);

        doc.text('________ de _____________________ de 20__', RECT_L, (base = base + 2 * ROW_H));
        doc.text('Señores', RECT_L, (base = base + 2 * ROW_H));
        doc.setFontType('bold');
        doc.text('Aracar Financiera S.A', RECT_L, (base = base + ROW_H));
        doc.setFontType('normal');
        doc.text('Presente', RECT_L, (base = base + ROW_H));
        doc.text('Ref. Solicitud No. [________]', RECT_L, (base = base + ROW_H));
        doc.text('Unidad garantía Prendaria Dominio [__________]', RECT_L, (base = base + ROW_H));

        let text = `Por medio de la presente dejo/jamos expresa constancia en mi/nuestro carácter de Solicitante/s que he/hemos RECIBIDO de plena conformidad la suma de $[____________________________] (Pesos [_____________________________________________________________________ __________________________]) en concepto de desembolso del préstamo identificado bajo la operación de la referencia mediante cheque o su acreditación en la Cuenta Corriente / Caja de Ahorros CBU Nº [_______________________________________________] de titularidad de: [________________________________________________________]. El presente recibo junto con el cheque o la constancia de la transferencia de fondos a la cuenta bancaria antes indicada constituye suficiente, formal y eficaz recibo y carta de pago de la suma antes indicada. Sin más, el Solicitante manifiesta que nada más tiene que reclamar a Aracar Financiera S.A. y/o entidad financiera acreedora del crédito de referencia. Así mismo, tomamos nota que, a la fecha de la presente, el valor unitario de la Unidad de Variación Adquisitiva es de _____ Pesos por lo que el capital del Préstamo equivale a ____ UVAS. por lo que prestamos conformidad con el detalle de los importes de las cuotas que se indican a continuación calculadas de conformidad con lo establecido en la Solicitud de Préstamo:`;
        doc.text(text, RECT_L, base + 5, {maxWidth: 185, align: 'justify'});

        drawCols([100], (base = base + ROW_H * 33), ROW_H * 4);
        doc.setFontType('bold');
        doc.text('Firma Solicitante', RECT_L + 1, base + 3);
        drawCols([100], (base = base + ROW_H * 4), ROW_H);
        doc.text('Aclaración', RECT_L + 1, base + 3);
        doc.setFontType('normal');
        doc.text('(Letra del Solicitante)', RECT_L + 18, base + 3);
    };

    const drawManifestacionConcesionario = base => {
        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], base, ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('MANIFESTACIÓN DEL CONCESIONARIO', base + 3.5);

        doc.setTextColor('#000000');
        doc.setFontSize(8);
        doc.setFontType('normal');

        let text = `Por la presente manifestamos que las firmas del/los Solicitantes y co-deudor/es han sido puestas en la presente solicitud en nuestra presencia y que los datos de filiación coinciden con los documentos que nos fueron exhibidos en original. En consecuencia, si se demostrase judicialmente que cualquiera de las firmas insertas en la Solicitud y/o en el Contrato de Prenda no pertenecen a las personas a quien se les atribuyen, nos comprometemos de manera expresa e irrevocable ante el primer requerimiento de la empresa, al pago total del Crédito, sus intereses, gastos, costos y honorarios, como así también al pago de los honorarios de los letrados de la contraparte y los daños y perjuicios que se pudieran haber reclamado a la empresa y nos sometemos a la competencia de los Tribunales Ordinarios de la Ciudad Autónoma de Buenos Aires, o la de los Tribunales correspondientes al lugar en donde se encuentra situado el Automóvil.`;
        doc.text(text, 10, (base = base + ROW_H + 4), {maxWidth: 185, align: 'justify'});

        doc.setFontType('bold');
        drawRow({
            cols: [100],
            labels: [['Firma Apoderado del Concesionario']],
            base: (base = base + ROW_H * 5),
            height: ROW_H * 4
        });
        drawRow({
            cols: [100],
            labels: [['Aclaración']],
            base: (base = base + ROW_H * 4),
            height: ROW_H
        });
        doc.setFontType('normal');
        text = `(Letra del Apoderado del Concesionario):`;
        doc.text(26, base + 3.5, text);
    };

    const drawManifestacionSolicitante = base => {
        doc.setFillColor('#c8c8c8');
        doc.setFontType('bold');
        drawCols([100], base, ROW_H, 'F');
        doc.setTextColor('#FFFFFF');
        doc.setFontSize(10);
        textCenter('MANIFESTACIÓN DEL SOLICITANTE', base + 3.5);

        doc.setTextColor('#000000');
        doc.setFontSize(8);
        doc.setFontType('normal');
        let text = `Por la presente acepto que los montos expresados en la presente solicitud están consignados en UVA y serán cancelados en su equivalente en pesos de acuerdo al índice previsto en el apartado 6.1.2 de la comunicación "A" 6080 del BCRA o aquella que en su futuro la reemplace. Así mismo doy mi conformidad para cumplir en tiempo y forma con el detalle de cuotas que se expresa a continuación:`;
        doc.text(text, 10, (base = base + ROW_H + 4), {maxWidth: 185, align: 'justify'});

        var columns = ['Cuota', 'Fecha', 'Saldo de Capital', 'Capital', 'Intereses', 'IVA', 'TOTAL'];
        const rows = [];
        if (application.productAmortizationSchedule) {
            let scheduleToUva = convertToUVAS(application.productAmortizationSchedule, application.productUVAValue);
            scheduleToUva.forEach(x =>
                rows.push([
                    x.n,
                    getFormattedDate(x.date),
                    'UVA ' + formatNumber(x.remainingPrincipal, 2),
                    'UVA ' + formatNumber(x.amortization, 2),
                    'UVA ' + formatNumber(x.interest, 2),
                    'UVA ' + formatNumber(x.iva, 2),
                    'UVA ' + formatNumber(x.periodPayment, 2)
                ])
            );
        }
        doc.autoTable(columns, rows, {
            headerStyles: {
                fillColor: 150
            },
            styles: {fontSize: 8, cellPadding: 0.5},
            margin: {top: (base = base + ROW_H * 3)}
        });

        doc.setFontType('bold');
        drawRow({
            cols: [100],
            labels: [['Firma Del Solicitante']],
            base: (base = base + ROW_H * 45),
            height: ROW_H * 4
        });
        drawRow({
            cols: [100],
            labels: [['Aclaración']],
            base: (base = base + ROW_H * 4),
            height: ROW_H
        });
    };

    // const drawPagare = base => {
    //     doc.setFillColor('#c8c8c8');
    //     doc.setFontType('bold');
    //     drawCols([100], base, ROW_H, 'F');
    //     doc.setTextColor('#FFFFFF');
    //     doc.setFontSize(10);
    //     textCenter('PAGARÉ', base + 3.5);

    //     doc.setTextColor('#000000');
    //     doc.setFontSize(8);
    //     doc.setFontType('normal');

    //     let text = `Ciudad de Buenos Aires                        de ${getMomentText('', 'year')}`;
    //     doc.text(text, 185, (base = base + ROW_H * 2), {maxWidth: 185, align: 'right'});

    //     text = `Referencia: Nº ${getApplicationText('applicationNumber')}`;
    //     doc.text(text, 10, (base = base + ROW_H + 4), {maxWidth: 185, align: 'left'});

    //     text = `Por UVAs: ${formatNumber(getApplicationText('productUVAEquivalent'))}`;
    //     doc.text(text, 10, (base = base + ROW_H + 4), {maxWidth: 185, align: 'left'});

    //     text = `PAGARÉ incondicionalmente A LA VISTA y SIN PROTESTO a la orden de a ARACAR FINANCIERA S.A. el equivalente en pesos argentinos a  ${writtenNumber(
    //         getApplicationText('productUVAEquivalent')
    //     ).toUpperCase()} Unidades de Valor Adquisitivo ("UVA"), (establecidas por el Banco Central de la República Argentina), conforme el valor de la UVA a la fecha del efectivo pago. A partir de la fecha de emisión, el monto adeudado en UVAs devengará un interés sobre dicho monto en UVA de ${formatNumber(
    //         getApplicationText('productNominalAnualRate')
    //     )} anual que se devengará por día transcurrido, tomando como base un año de 360 días, desde la fecha de emisión del presente Pagaré y hasta la fecha de efectivo pago y que será calculado en UVA. El término para efectuar la presentación del presente se extiende desde la fecha de su creación hasta el día [_____________]. El lugar para su presentación será , Ciudad de Buenos Aires. Este pagaré se rige por las leyes de la República Argentina.`;

    //     doc.text(text, 10, (base = base + ROW_H + 4), {maxWidth: 185, align: 'justify'});
    //     text = `Librador:________________________`;
    //     doc.text(text, 10, (base = base + ROW_H * 7), {maxWidth: 185, align: 'left'});
    //     text = `Codeudor:_______________________`;
    //     doc.text(text, 100, base, {maxWidth: 185, align: 'left'});

    //     text = `Nombre:_________________________`;
    //     doc.text(text, 10, (base = base + ROW_H * 2), {maxWidth: 185, align: 'left'});
    //     text = `Nombre:_________________________`;
    //     doc.text(text, 100, base, {maxWidth: 185, align: 'left'});

    //     text = `DNI:____________________________`;
    //     doc.text(text, 10, (base = base + ROW_H * 2), {maxWidth: 185, align: 'left'});
    //     text = `DNI:____________________________`;
    //     doc.text(text, 100, base, {maxWidth: 185, align: 'left'});
    // };

    drawPage1Rects(8);
    doc.addPage();
    drawPage2Rects(8);
    doc.addPage();
    drawPage3Rects(8);
    doc.addPage();
    drawPage4Rects(8);
    doc.addPage();
    drawPage5Rects(8);
    doc.addPage();
    drawPage6Rects(8);
    doc.addPage();
    drawEntregaFondos(8);
    doc.addPage();
    drawManifestacionConcesionario(8);
    doc.addPage();
    drawManifestacionSolicitante(8);

    // doc.addPage();
    // drawPagare(8);

    //Bajar documento
    doc.save(
        `${moment().format('YYYY-MM-DD')}_${lowerCase(application.firstName)}_${lowerCase(
            application.lastName
        )}_solicitud.pdf`
    );
};
