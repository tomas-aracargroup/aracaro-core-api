import Enumerable from 'linq/linq';

export const formatMarcas = marcas => {
    return Enumerable.from(marcas)
        .orderByDescending(x => x.anio)
        .groupBy('$.anio', null, (key, group) => ({
            value: key.toString(),
            label: key.toString(),
            children: Enumerable.from(group.getSource())
                .orderBy(x => x.marca)
                .groupBy('$.marca', null, (key, g) => ({
                    value: key,
                    label: key,
                    children: Enumerable.from(g.getSource())
                        .orderBy(x => x.nombre)
                        .select(item => ({
                            value: item.nombre,
                            label: item.nombre,
                            valor: item.valor,
                            codia: item.codia
                        }))
                        .toArray()
                }))
                .toArray()
        }))
        .toArray();
};
