export default value => {
    if (!value) {
        return 'status';
    }

    if (value.includes('0')) {
        return 'status zero';
    } else if (value.includes('1')) {
        return 'status one';
    } else if (value.includes('2')) {
        return 'status two';
    } else if (value.includes('3')) {
        return 'status three';
    } else if (value.includes('4')) {
        return 'status four';
    } else if (value.includes('5')) {
        return 'status five';
    } else if (value.includes('6')) {
        return 'status six';
    } else if (value.includes('7')) {
        return 'status seven';
    } else if (value.includes('8')) {
        return 'status eight';
    } else if (value.includes('9')) {
        return 'status nine';
    } else {
        return 'status warning';
    }
};
