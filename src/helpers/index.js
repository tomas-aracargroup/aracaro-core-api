import moment from 'moment';
import {formatNumber as accoountingFormatNumber} from 'accounting';

export const getAge = birthDate =>
    moment(birthDate).isValid() && birthDate !== undefined ? moment().diff(moment(birthDate), 'years') : '';

export const includes = (haystack, needle) => haystack.length > 0 && haystack.includes(needle);

export const getFormattedDate = date => (moment(date).isValid() ? moment(date, 'YYYY-MM-DD').format('DD-MM-YYYY') : '');
export const getAgeWithParenthesis = birthDate =>
    moment(birthDate).isValid() && birthDate !== undefined
        ? '(' + moment().diff(moment(birthDate), 'years') + ' años)'
        : '';

export const formatMoney = (value, decimals = 0) =>
    value !== '' ? '$' + accoountingFormatNumber(value, decimals, '.', ',') : '$0';
export const formatNumber = (value, decimals = 0) =>
    value !== '' ? accoountingFormatNumber(value, decimals, '.', ',') : '';
export const formatPercentage = value => accoountingFormatNumber(value, 2, '.', ',') + '%';

export const getByKey = (collection, value, key = '_id') => {
    if (!collection) return null;

    const item = collection.filter(item => item[key] === value);
    if (item) return item[0];
    return null;
};

export const sortByDateDesc = (x, y) => {
    const fechaX = moment(x.timeStamp).format('YYYYMMDDHHmmss');
    const fechaY = moment(y.timeStamp).format('YYYYMMDDHHmmss');

    return fechaX * 1 > fechaY * 1 ? -1 : fechaX * 1 < fechaY * 1 ? 1 : 0;
};
