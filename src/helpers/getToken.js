import {tokenName} from '../constants/config';

export default () => {
    let token;

    //Diego
    token =
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmUzNDI4MDVlM2VjODZiZWU1ZWVlOWYiLCJ0YXNrcyI6WyJncmlkcy9hcHBsaWNhdGlvbiIsImdyaWRzL2RlYWxlcnMtYXBwbGljYXRpb24iLCJncmlkcy9kZWFsZXJzLWN1c3RvbWVyIiwiZ3JpZHMvZGVhbGVycy11c2VyIiwiZ3JpZHMvbGVhZCIsImdyaWRzL2NvbmZpZyIsImdyaWRzL2NvbXBhbnkiLCJncmlkcy9zZXNzaW9uIiwiZ3JpZHMvdXNlciIsImdyaWRzL3J0byIsIm1haWxzL3N1YnNjcmlwdGlvbnMiLCJncmlkcy9iZXNtYXJ0IiwiY2FyZ2EtbWFzaXZhIiwiZ3JpZHMvb25saW5lLWFwcGxpY2F0aW9uIiwiZ3JpZHMvb25saW5lLWN1c3RvbWVyIl0sIm1haWxzIjpbXSwic3RhdHVzIjp0cnVlLCJmaXJzdG5hbWUiOiJEaWVnbyIsImxhc3RuYW1lIjoiUmV5ZXMiLCJlbWFpbCI6ImRyZXllc0BhcmFjYXJncm91cC5jb20iLCJwYXNzd29yZCI6IjEyMzQiLCJyb2xlIjoiQURNSU4iLCJjaGFubmVsSWQiOiI1YjE3ZjA3MTJjODg1ZTJkMzgxMTQ2MjUiLCJfX3YiOjAsImlhdCI6MTU0NDA0MzAyMH0.6-JjV09HcIUfXHTGhXgbJGmfxgfTZdlKeyyeiGIBSFY';

    if (process.env.NODE_ENV === 'production') {
        token = localStorage.getItem(tokenName);
    }
    return token;
};
