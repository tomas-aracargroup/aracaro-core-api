import moment from 'moment';
import {formatNumber} from 'accounting';

export const extractYear = str => str.substr(0, 4);

export const validateEmail = string => /\S+@\S+\.\S+/.test(string);
export const validateText = string => /^[ÁáÉéÍíÓóÚúÑñA-Za-z _]*[ÁáÉéÍíÓóÚúÑñA-Za-z][ñA-Za-z _]*$/.test(string);

//Solo se consideran validas fechas con año mayor a 999.
export const validateDate = date => extractYear(date) > 999 && moment(date).isValid();

// const sanitaze = entity => {
//     for (let x in entity) if (entity[x] === '') delete entity[x];
// };

const getRequiredFields = fields => fields.filter(x => x.required);
const getEmailFields = fields => fields.filter(x => x.type === 'email');
const getDateFields = fields => fields.filter(x => x.type === 'date');
const getNumberFields = fields => fields.filter(x => x.type === 'number' || x.type === 'currency');
const getTextFields = fields => fields.filter(x => x.type === undefined || x.type === 'text' || x.type === 'password');

const format = value => formatNumber(value, 0, '.');

/**
 * Validador.
 * @param {Object} entity Any object that can be validated.
 * @param {Object} fields A map with field definitions.

 * @returns {errors} Returns a map of errors, if it is empty, entity is OK.
 */
export default (entity, fields) => {
    let errors = {};

    getEmailFields(fields).forEach(field => {
        if (entity[field.name] && !validateEmail(entity[field.name])) {
            errors[field.name] = 'Formato Incorrecto';
        }
    });

    getDateFields(fields).forEach(field => {
        // if (entity[field.name] && !validateDate(entity[field.name])) {
        //     errors[field.name] = 'Formato Incorrecto';
        // }
    });

    getRequiredFields(fields).forEach(field => {
        if (!entity[field.name]) {
            errors[field.name] = field.error || 'El campo es obligatorio';
        }
    });

    getNumberFields(fields).forEach(field => {
        if (field.max && entity[field.name] > field.max) {
            errors[field.name] = `Valor máximo ${format(field.max)}`;
        } else if (field.min && entity[field.name] < field.min) {
            errors[field.name] = `Valor mínimo ${format(field.min)}`;
        }
    });

    getTextFields(fields).forEach(field => {
        if (entity[field.name]) {
            if (field.letterOnly && !validateText(entity[field.name])) {
                errors[field.name] = 'Solo se permiten letras';
            } else if (field.max && entity[field.name].length > field.max) {
                errors[field.name] = `Máximo ${field.max.toString()} caracteres`;
            } else if (field.min && entity[field.name].length < field.min) {
                errors[field.name] = `Mínimo ${field.min.toString()} caracteres`;
            } else if (field.noSpaces && entity[field.name].indexOf(' ') > 0) {
                errors[field.name] = 'No se permiten espacios';
            }
        }
    });

    return errors;
};
