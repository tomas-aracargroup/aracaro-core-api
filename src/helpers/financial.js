import moment from 'moment';

export const getPeriodicPayment = (rate, term, principal) =>
    (principal * rate * Math.pow(1 + rate, term)) / (Math.pow(1 + rate, term) - 1);

export const getMaximumCapitalForPayment = (rate, term, periodicPayment) =>
    (periodicPayment / rate / Math.pow(1 + rate, term)) * (Math.pow(1 + rate, term) - 1);

export const getPeriodRate = (tna, periodsInYear) => tna / periodsInYear;

export const getAmortizationSchedule = ({principal, term, rate}) => {
    let payments = [],
        remainingPrincipal = principal;
    let periodAmortization = 0;
    let periodInterest = 0;
    const periodicPayment = getPeriodicPayment(rate, term, principal);

    for (let n = 1; n <= term; n++) {
        let amortization = periodicPayment - remainingPrincipal * rate;
        let interest = periodicPayment - amortization;
        remainingPrincipal = remainingPrincipal - amortization;

        periodAmortization = periodAmortization + amortization;
        periodInterest = periodInterest + interest;
        payments.push({
            n: n,
            remainingPrincipal: remainingPrincipal,
            amortization: periodAmortization,
            interest: periodInterest,
            periodPayment: periodInterest + periodAmortization
        });
        periodAmortization = periodInterest = 0;
    }

    return payments;
};

const getDaysUntilCutDate = cutDate =>
    moment()
        .startOf('day')
        .date(cutDate)
        .add(moment().date() > cutDate ? 2 : 1, 'months')
        .diff(moment().startOf('day'), 'days');

export const getPaymentDates = ({principal, rate, term, cutDate}, iva = true) => {
    if (!term) {
        return [];
    }
    const amortizationSchedule = getAmortizationSchedule({principal, rate, term});
    const daysUntilCutDate = getDaysUntilCutDate(cutDate);
    const schedule = amortizationSchedule.map(payment => {
        const date = moment()
            .date(cutDate)
            .add(payment.n + (moment().date() > cutDate ? 1 : 0), 'months')
            .format('YYYY-MM-DD');
        return {date, ...payment};
    });

    let interest = (principal * rate * daysUntilCutDate) / 30;

    schedule[0] = {
        ...schedule[0],
        interest,
        periodPayment: Number(interest) + Number(schedule[0].amortization)
    };

    if (iva) {
        return addVAT(schedule);
    }

    return schedule;
};

const addVAT = payments =>
    payments.map(x => {
        const {date, remainingPrincipal, amortization, interest, periodPayment} = x;
        return {
            n: x.n,
            date,
            remainingPrincipal,
            amortization,
            interest,
            iva: interest * 0.21,
            periodPayment: periodPayment + interest * 0.21
        };
    });

const NPV = (rate, values) => {
    let xnpv = 0.0;
    for (let key in values) {
        let tmp = values[key];
        let value = tmp.Flow;
        let date = tmp.Date;
        xnpv += value / Math.pow(1 + rate, date / 365);
    }
    return xnpv;
};

const IRR = (values, guess) => {
    let rtb, dx;
    if (!guess) guess = 0.1;

    let x1 = 0.0;
    let x2 = guess;
    let f1 = NPV(x1, values);
    let f2 = NPV(x2, values);

    for (let i = 0; i < 100; i++) {
        if (f1 * f2 < 0.0) break;
        if (Math.abs(f1) < Math.abs(f2)) {
            f1 = NPV((x1 += 1.6 * (x1 - x2)), values);
        } else {
            f2 = NPV((x2 += 1.6 * (x2 - x1)), values);
        }
    }

    if (f1 * f2 > 0.0) return null;

    let f = NPV(x1, values);
    if (f < 0.0) {
        rtb = x1;
        dx = x2 - x1;
    } else {
        rtb = x2;
        dx = x1 - x2;
    }

    for (let z = 0; z < 100; z++) {
        dx *= 0.5;
        let x_mid = rtb + dx;
        let f_mid = NPV(x_mid, values);
        if (f_mid <= 0.0) rtb = x_mid;
        if (Math.abs(f_mid) < 1.0e-6 || Math.abs(dx) < 1.0e-6) return x_mid;
    }

    return null;
};

const generateCashFlow = (principal, payments) => {
    let flow = [];
    flow.push({Date: 0, Flow: -principal});
    payments.forEach(payment => {
        flow.push({Date: moment(payment.date).diff(moment(), 'days') + 1, Flow: payment.periodPayment});
    });
    return flow;
};

export const calculateIRR = (principal, payments) => IRR(generateCashFlow(principal, payments)) * 100;

export const convertToUVAS = (schedule, uvaValue) => {
    return schedule.map(payment => ({
        n: payment.n,
        date: payment.date,
        amortization: payment.amortization / uvaValue,
        interest: payment.interest / uvaValue,
        periodPayment: payment.periodPayment / uvaValue,
        iva: payment.iva / uvaValue,
        remainingPrincipal: payment.remainingPrincipal / uvaValue
    }));
};

export const calculateProductConditions = ({term, principal, cutDate, originationFees, tna, uvaValue}) => {
    const periodRate = getPeriodRate(tna / 100, 12);
    const payments = getPaymentDates({principal, rate: periodRate, term, cutDate}, true);
    const productTotalFinancialCostWithVAT = calculateIRR(principal - originationFees, payments);
    const productPeriodicPaymentPesos = getPeriodicPayment(periodRate, term, principal);
    const productPeriodicPaymentUva = productPeriodicPaymentPesos / uvaValue;

    const productTotalFinancialCost = calculateIRR(
        principal - originationFees,
        getPaymentDates({principal: principal, rate: periodRate, term, cutDate}, false)
    );

    const productUVAEquivalent = principal / uvaValue;

    return {
        productTerm: term,
        productFirstPaymentAmount: Math.round(payments[0].periodPayment * 100) / 100,
        productFirstPaymentDate: payments[0].date,
        productAmortizationSchedule: payments,
        productTotalFinancialCostWithVAT,
        productTotalFinancialCost,
        productPeriodicPaymentPesos,
        productPeriodicPaymentUva,
        productUVAEquivalent
    };
};
