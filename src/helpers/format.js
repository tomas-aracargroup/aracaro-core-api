import {formatPercentage, formatNumber, formatMoney} from './index';
import moment from 'moment';
import 'moment/locale/es';
import enums from './enumerators';

require('moment/locale/es');
// moment.locale('es'); //en caso que no lo setee la linea de arriba

export default (value, type, enumObj) => {
    switch (type) {
        case 'boolean':
            return value ? 'Sí' : 'No';
        case 'money':
        case 'currency':
            return formatMoney(value, 0);

        case 'decimal':
            return formatNumber(value, 2);

        case 'number':
            return value ? formatNumber(value, 0) : '-';

        case 'percentage':
            return formatPercentage(value);

        case 'date':
            return value ? moment.utc(value).format('LL') : '-';

        case 'time':
            return value ? moment().diff(value, 'years') : '-';

        case 'datetime':
            return value ? moment.utc(value).fromNow() : '-';

        case 'select':
            return enums.getEnumRow(enumObj, value).name;
        case 'selectTags':
            return value ? value.map(x => enums.getEnumRow(enumObj, x).name).join(' - ') : '-';

        default:
            return value;
    }
};
