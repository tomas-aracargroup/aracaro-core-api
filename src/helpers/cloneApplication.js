export const cloneApplication = entity => {
    let clonedApplication = {};
    if (entity.user) {
        clonedApplication.user = entity.user;
        clonedApplication.salesPerson = entity.salesPerson;
    }
    if (entity.documento) {
        clonedApplication.documento = entity.documento;
    }
    if (entity.genero) {
        clonedApplication.genero = entity.genero;
    }
    if (entity.nombre) {
        clonedApplication.nombre = entity.nombre;
    }
    if (entity.apellido) {
        clonedApplication.apellido = entity.apellido;
    }
    if (entity.monto) {
        clonedApplication.monto = entity.monto;
    }
    if (entity.email) {
        clonedApplication.email = entity.email;
    }
    if (entity.ingresoVerificado) {
        clonedApplication.ingresoVerificado = entity.ingresoVerificado;
    }
    if (entity.vehicleYear) {
        clonedApplication.anio = entity.vehicleYear + '';
    }
    if (entity.vehicleMarca) {
        clonedApplication.marca = entity.vehicleMarca;
    }
    if (entity.vehicleModelo) {
        clonedApplication.modelo = entity.vehicleModelo;
    }
    if (entity.vehicleCodia) {
        clonedApplication.codia = entity.vehicleCodia;
    }
    if (entity.vehiculoAntiguedad || entity.vehiculoAntiguedad === 0) {
        clonedApplication.vehiculoAntiguedad = entity.vehiculoAntiguedad;
    }
    if (entity.vehiculoValor || entity.vehiculoValor === 0) {
        clonedApplication.vehiculoValor = entity.vehiculoValor;
    }
    if (entity.vehiculoKm || entity.vehiculoKm === 0) {
        clonedApplication.kilometros = entity.vehiculoKm;
    }
    if (entity.vehicleIsNew) {
        clonedApplication.es0KM = entity.vehicleIsNew;
    }
    return clonedApplication;
};
