export const eAPP_STATUS = [
    {
        id: '0 - A CONTACTAR',
        name: '0 - A CONTACTAR',
        description: 'lead nuevo generado por la web rto.russellcar.com',
        requiredAction: 'LLAMAR',
        responsible: 'RussellCar - Anny/Mariana'
    },

    {
        id: '1AA - ENTREVISTA. PRIMER LLAMADO',
        name: '1AA - ENTREVISTA. PRIMER LLAMADO',
        description: 'primer llamado a lead',
        requiredAction: 'LLAMAR CLIENTE',
        responsible: 'RussellCar - Anny/Mariana'
    },
    {
        id: '1AB - ENTREVISTA. SEGUNDO LLAMADO',
        name: '1AB - ENTREVISTA. SEGUNDO LLAMADO',
        description: 'primer llamado no fue exitoso, se realiza segundo llamado',
        requiredAction: 'LLAMAR CLIENTE',
        responsible: 'RussellCar - Anny/Mariana'
    },
    {
        id: '1AC - ENTREVISTA. TERCER LLAMADO',
        name: '1AC - ENTREVISTA. TERCER LLAMADO',
        description:
            'segundo llamado no fue exitoso, se realiza tercer llamado, , si es exitoso pasa a estado 1b, caso contrario 2b',
        requiredAction: 'LLAMAR CLIENTE',
        responsible: 'Aracar - Thomas Hankin'
    },
    {
        id: '1B - ENTREVISTA. ENTREVISTA AGENDADA',
        name: '1B - ENTREVISTA. ENTREVISTA AGENDADA',
        description:
            'se agenda entrevista con lead, de no asistir, pasa a estado 1c. de asistir, lo entrevista gonzalo, anny o mariana y pasa estado 3a/b/c/d/e/f o 7 carar en notas comentario de la entrevista',
        requiredAction: 'ENTREVISTAR LEAD EN FECHA AGENDADA',
        responsible: 'RussellCar - Gonzalo Villani'
    },
    {
        id: '1C - ENTREVISTA. NO ASISTIÓ ENTREVISTA',
        name: '1C - ENTREVISTA. NO ASISTIÓ ENTREVISTA',
        description: 'se pauto una entrevista y el chofer no asistio',
        requiredAction: 'ASENTAR MOTIVO EN NOTAS / ENVIAR MAIL A LEAD',
        responsible: 'RussellCar - Mariana Gonzalo Anny / Aracar - Lucas , Agustina'
    },

    {
        id: '2A - RECHAZADO. INCONTACTABLE',
        name: '2A - RECHAZADO. INCONTACTABLE',
        description: 'leads que se llamaron 3 veces y no hubo respuesta',
        requiredAction: 'DESESTIMAR',
        responsible: 'N/A (Estado Terminal)'
    },
    {
        id: '2B - RECHAZADO. RECHAZADO RUSSELLCAR',
        name: '2B - RECHAZADO. RECHAZADO RUSSELLCAR',
        description: 'rc rechaza lead por motivos varios (ejemplo: mal educado, mucha deuda, bajo score)',
        requiredAction: 'EXPLICAR EN NOTAS MOTIVO DE RECHAZO',
        responsible: 'RussellCar - Mariana, Gonzalo'
    },
    {
        id: '2C - RECHAZADO. RECHAZADO ARACAR',
        name: '2C - RECHAZADO. RECHAZADO ARACAR',
        description:
            'aquellos leads rto que pasan a evaluacion crediticia de aracar para leasing o prenda y son rechazados',
        requiredAction: 'PASAR A 3A, ACLARAR MOTIVO RECHAZO EN NOTAS',
        responsible: 'Aracar - Laura Mercado'
    },
    {
        id: '2D - RECHAZADO. APLICANTE DUPLICADO',
        name: '2D - RECHAZADO. APLICANTE DUPLICADO',
        description: 'aquellos leads rto que se duplican',
        requiredAction: 'PASAR A 2D, AQUELLOS LEADS DUPLICADOS',
        responsible: 'Rusellcar - Mariana, Anny'
    },

    {
        id: '3A - CONDUCTOR EN PROCESO. EVALUANDO RUSSELLCAR',
        name: '3A - CONDUCTOR EN PROCESO. EVALUANDO RUSSELLCAR',
        description: 'se evalua perfil',
        requiredAction: 'PASAR A ESTADO 2B O 3B SEGUN CORRESPONDA',
        responsible: 'RussellCar - GONZALO'
    },
    {
        id: '3B - CONDUCTOR EN PROCESO. APROBADO RUSSELLCAR',
        name: '3B - CONDUCTOR EN PROCESO. APROBADO RUSSELLCAR',
        description: 'conductor aprobado, se firma documentacion correspondiente',
        requiredAction: 'PASAR A ESTADO 3E O 7 SEGUN CORRESPONDA',
        responsible: 'RC - GONZALO'
    },
    {
        id: '3C - CONDUCTOR EN PROCESO. EVALUANDO ARACAR',
        name: '3C - CONDUCTOR EN PROCESO. EVALUANDO ARACAR',
        description: 'se lo evalua crediticiamente en aracar para leasing o prenda',
        requiredAction: 'REALIZAR OFERTA PASAR A 3D / 2C SEGUN CORRESPONDA',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '3D - CONDUCTOR EN PROCESO. APROBADO ARACAR',
        name: '3D - CONDUCTOR EN PROCESO. APROBADO ARACAR',
        description: 'lead aprobado, se firma documentacion correspondiente',
        requiredAction: 'PASA A ESTADO 3 E SEGUN CORRESPONDA',
        responsible: 'L. MERCADO'
    },
    {
        id: '3E - CONDUCTOR EN PROCESO. CONTRATO FIRMADO',
        name: '3E - CONDUCTOR EN PROCESO. CONTRATO FIRMADO',
        description:
            'La oferta de financiación fue realizada por ARACAR y el cliente firmó la documentación correspondiente',
        requiredAction: 'PASAR A ESTADO 5 CORRESPONDIENTE',
        responsible: 'TBD'
    },
    {
        id: '3F - CONDUCTOR EN PROCESO. ESPERANDO DOC. TAXI DRIVER',
        name: '3F - CONDUCTOR EN PROCESO. ESPERANDO DOC. TAXI DRIVER',
        description: 'CONDUCTOR DEBE RETIRAR TARJETA, LUEGO SE LE ASIGNA AUTO',
        requiredAction: 'PASAR A ESTADO 5 CORRESPONDIENTE',
        responsible: 'RC M.OLMEDO G.VILLANI'
    },
    {
        id: '3G - CONDUCTOR EN PROCESO. ESPERANDO DOC. REMIS DRIVER',
        name: '3G - CONDUCTOR EN PROCESO. ESPERANDO DOC. REMIS DRIVER',
        description: 'CONDUCTOR ESPERA HABILITACION (Debe tener Registro Profesional C.A.B.A. / PROVINCIA BS.AS.)',
        requiredAction: 'PASAR A ESTADO 5 CORRESPONDIENTE',
        responsible: 'RC M.OLMEDO G.VILLANI'
    },
    {
        id: '4A - LOGISTICA. TAXI REPARACIÓN',
        name: '4A - LOGISTICA. A - TAXI REPARACIÓN',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '4B - LOGISTICA. TAXI PINTURA',
        name: '4B - LOGISTICA. B - TAXI PINTURA',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '4C - LOGISTICA. TAXI G.N.C.',
        nameC: '4 - LOGISTICA. C - TAXI G.N.C.',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '4D - LOGISTICA. REMIS REPARACIÓN',
        name: '4D - LOGISTICA. D - REMIS REPARACIÓN',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '4E - LOGISTICA. REMIS PINTURA',
        name: '4E - LOGISTICA. E - REMIS PINTURA',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '4F - LOGISTICA. REMIS GNC',
        name: '4F - LOGISTICA. F - REMIS GNC',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '5A - TRABAJANDO. ALQUILA – NO INTERESADO RTO',
        name: '5A - TRABAJANDO. ALQUILA – NO INTERESADO RTO',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '5B - TRABAJANDO. ALQUILA – LLAMAR PARA RTO',
        name: '5B - TRABAJANDO. ALQUILA – LLAMAR PARA RTO',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'Russell Car - Zulma'
    },
    {
        id: '5C - TRABAJANDO. R.T.O.',
        name: '5C - TRABAJANDO. R.T.O.',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '5D - TRABAJANDO. REMIS',
        name: '5D - TRABAJANDO. REMIS',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '5E - TRABAJANDO. ARACAR (TRANSFERIDO)',
        name: '5E - TRABAJANDO. ARACAR (TRANSFERIDO)',
        description: 'tbd',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },

    {
        id: '7A - DECLINADO COSTO ELEVADO',
        name: '7A - DECLINADO COSTO ELEVADO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '7B - DECLINADO NO QUIERE TAXI ',
        name: '7B - DECLINADO NO QUIERE TAXI',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '7C - DECLINADO POR AUTO ',
        name: '7C - DECLINADO POR AUTO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '7D - DECLINADO OTRO MOTIVO',
        name: '7D - DECLINADO OTRO MOTIVO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '7E - DECLINADO YA TIENE AUTO',
        name: '7E - DECLINADO YA TIENE AUTO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '7F - DECLINADO TIENE TRABAJO',
        name: '7F - DECLINADO TIENE TRABAJO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '9A - DESAFECTADO. DESPIDO',
        name: '9A - DESAFECTADO. DESPIDO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    },
    {
        id: '9B - DESAFECTADO. RENUNCIA',
        name: '9B - DESAFECTADO. RENUNCIA',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'TBD'
    }
];

export const ACQUISITION_CHANNEL = [
    {id: 'Car Ads', name: 'Car Ads'},
    {id: 'Facebook', name: 'Facebook'},
    {id: 'Google', name: 'Google'},
    {id: 'Colleage', name: 'Colleage'}
];

export const CAR_USAGE_WHEN_LEAVING = [
    {id: 'Taxi Prenda', name: 'Taxi Prenda'},
    {id: 'Remis Prenda', name: 'Remis Prenda'},
    {id: 'Taxi Leasing', name: 'Taxi Leasing'},
    {id: 'Remis Leasing', name: 'Remis Leasing'}
];
