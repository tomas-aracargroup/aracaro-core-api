let server;
let dealersServer_;

if (process.env.NODE_ENV === 'production') {
    server = 'https://api.prendarapida.com';
    // server = 'https://dev.aracargroup.com';
    dealersServer_ = 'https://api.aracargroup.com';
} else {
    server = 'http://localhost:3003';
    dealersServer_ = 'http://localhost:4005';
    // server = 'http://10.20.10.96:3003';
    // dealersServer_ = 'http://10.20.10.96:4005';
    // server = 'https://dev.aracargroup.com';
}

export default server;
export const dealersServer = dealersServer_;
