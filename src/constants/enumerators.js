export const eAPP_STATUS = [
    {
        id: '0 - A CONTACTAR',
        description: 'Estado Inicial',
        requiredAction: 'Contactar al cliente',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1AA - INTENTANDO CONTACTAR - PRIMER INTENTO',
        description: 'Primer llamado sin contestar',
        requiredAction: 'Contactar al cliente',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1AB - INTENTANDO CONTACTAR - SEGUNDO INTENTO',
        description: 'Segundo llamado sin contestar',
        requiredAction: 'Contactar al cliente',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1AC - INTENTANDO CONTACTAR - TERCER INTENTO',
        description: 'Tercer llamado sin contestar',
        requiredAction: 'Contactar al cliente',
        responsible: 'Aracar - Thomas Hankin'
    },
    {
        id: '1BA - CONTACTADO - PENSANDOLO',
        description: 'El cliente esta interesado, pero tiene que evaluar',
        requiredAction: 'Enviar información',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1BB - CONTACTADO - CALLBACK',
        description: 'El cliente fue contactado, estaba ocupado, solicita que lo llamen en otro momento',
        requiredAction: 'Agendar fecha de nuevo llamado en Panel',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1CA - NO ENTIENDE PRODUCTO - MONTO INSUFICIENTE',
        description: 'El cliente piensa que ',
        requiredAction: 'Enviar información',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1CB - NO ENTIENDE PRODUCTO - NO QUIERE UVA',
        description: 'TBD',
        requiredAction: 'Enviar información',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1CC - NO ENTIENDE PRODUCTO - NO QUIERE PRENDAR AUTO',
        description: 'El cliente tiene reparos en dejar auto en garantía',
        requiredAction: 'Enviar información acerca prestamo prendario vs personal',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1CD - NO ENTIENDE PRODUCTO - CUOTAS MUY CARAS',
        description: 'TBD',
        requiredAction: 'Enviar información',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '1CE - NO ENTIENDE PRODUCTO - OTRO MOTIVO',
        description: 'TBD',
        requiredAction: 'Enviar información',
        responsible: 'Aracar - Karla Buitron, Camila Lagiglia'
    },
    {
        id: '2A - RECHAZADO EN RIESGO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '2B - NO INTERESADO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '2C - ANULADO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '2D - DUPLICADO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '3A - EN EVALUACION CREDITICIA',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '3B - APROBADO EN RIESGO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '3C - PREAPROBADO BESMART',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '4A - ESPERANDO DOCUMENTACION',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '4B - INSCRIPCIÓN DE PRENDA',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '4C - GESTORÍA',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '4D - EMISION DE SEGURO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '5A - A DESEMBOLSAR',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '5B - DESEMBOLSADO',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    },
    {
        id: '6A - CARGADO EN WEBFLOW',
        description: 'TBD',
        requiredAction: 'TBD',
        responsible: 'ARACAR - D. MANOS L. MERCADO A. RODEIRO'
    }
];

const ePROVINCIA = [
    {id: 'BUENOS AIRES', name: 'BUENOS AIRES'},
    {id: 'CAPITAL FEDERAL', name: 'CAPITAL FEDERAL'},
    {id: 'CATAMARCA', name: 'CATAMARCA'},
    {id: 'CHACO', name: 'CHACO'},
    {id: 'CHUBUT', name: 'CHUBUT'},
    {id: 'CORDOBA', name: 'CÓRDOBA'},
    {id: 'CORRIENTES', name: 'CORRIENTES'},
    {id: 'ENTRE RIOS', name: 'ENTRE RÍOS'},
    {id: 'FORMOSA', name: 'FORMOSA'},
    {id: 'JUJUY', name: 'JUJUY'},
    {id: 'LA PAMPA', name: 'LA PAMPA'},
    {id: 'LA RIOJA', name: 'LA RIOJA'},
    {id: 'MENDOZA', name: 'MENDOZA'},
    {id: 'MISIONES', name: 'MISIONES'},
    {id: 'NEUQUEN', name: 'NEUQUÉN'},
    {id: 'RIO NEGRO', name: 'RÍO NEGRO'},
    {id: 'SALTA', name: 'SALTA'},
    {id: 'SAN JUAN', name: 'SAN JUAN'},
    {id: 'SAN LUIS', name: 'SAN LUIS'},
    {id: 'SANTA CRUZ', name: 'SANTA CRUZ'},
    {id: 'SANTA FE', name: 'SANTA FE'},
    {id: 'SANTIAGO DEL ESTERO', name: 'SANTIAGO DEL ESTERO'},
    {id: 'TIERRA DEL FUEGO', name: 'TIERRA DEL FUEGO'},
    {id: 'TUCUMAN', name: 'TUCUMÁN'}
];

export const eCOUNTRIES = [
    {id: '401', name: 'AFGANISTAN'},
    {id: '101', name: 'ALBANIA'},
    {id: '126', name: 'ALEMANIA'},
    {id: '124', name: 'ANDORRA'},
    {id: '202', name: 'ANGOLA'},
    {id: '388', name: 'ANGUILLA'},
    {id: '310', name: 'ANTIGUA Y BARBUDA'},
    {id: '384', name: 'ANTILLAS HOLANDESAS'},
    {id: '402', name: 'ARABIA SAUDI'},
    {id: '203', name: 'ARGELIA'},
    {id: '340', name: 'ARGENTINA'},
    {id: '148', name: 'ARMENIA'},
    {id: '386', name: 'ARUBA'},
    {id: '501', name: 'AUSTRALIA'},
    {id: '102', name: 'AUSTRIA'},
    {id: '442', name: 'AZERBAIYAN'},
    {id: '311', name: 'BAHAMAS'},
    {id: '403', name: 'BAHREIN'},
    {id: '404', name: 'BANGLADESH'},
    {id: '312', name: 'BARBADOS'},
    {id: '138', name: 'BELARUS'},
    {id: '103', name: 'BELGICA'},
    {id: '313', name: 'BELICE'},
    {id: '204', name: 'BENIN'},
    {id: '392', name: 'BERMUDAS'},
    {id: '453', name: 'BHUTÁN'},
    {id: '341', name: 'BOLIVIA'},
    {id: '145', name: 'BOSNIA Y HERZEGOVINA'},
    {id: '205', name: 'BOTSWANA'},
    {id: '342', name: 'BRASIL'},
    {id: '439', name: 'BRUNEI'},
    {id: '104', name: 'BULGARIA'},
    {id: '201', name: 'BURKINA FASO'},
    {id: '206', name: 'BURUNDI'},
    {id: '207', name: 'CABO VERDE'},
    {id: '417', name: 'CAMBOYA'},
    {id: '208', name: 'CAMERUN'},
    {id: '301', name: 'CANADA'},
    {id: '246', name: 'CHAD'},
    {id: '344', name: 'CHILE'},
    {id: '407', name: 'CHINA'},
    {id: '106', name: 'CHIPRE'},
    {id: '343', name: 'COLOMBIA'},
    {id: '209', name: 'COMORES'},
    {id: '210', name: 'CONGO'},
    {id: '430', name: 'COREA'},
    {id: '431', name: 'COREA DEL NORTE '},
    {id: '211', name: 'COSTA DE MARFIL'},
    {id: '314', name: 'COSTA RICA'},
    {id: '146', name: 'CROACIA'},
    {id: '315', name: 'CUBA'},
    {id: '107', name: 'DINAMARCA'},
    {id: '212', name: 'DJIBOUTI'},
    {id: '316', name: 'DOMINICA'},
    {id: '345', name: 'ECUADOR'},
    {id: '213', name: 'EGIPTO'},
    {id: '317', name: 'EL SALVADOR'},
    {id: '408', name: 'EMIRATOS ARABES UNIDOS'},
    {id: '253', name: 'ERITREA'},
    {id: '147', name: 'ESLOVENIA'},
    {id: '108', name: 'ESPAÑA'},
    {id: '302', name: 'ESTADOS UNIDOS DE AMERICA'},
    {id: '141', name: 'ESTONIA'},
    {id: '214', name: 'ETIOPIA'},
    {id: '502', name: 'FIJI'},
    {id: '409', name: 'FILIPINAS'},
    {id: '109', name: 'FINLANDIA'},
    {id: '110', name: 'FRANCIA'},
    {id: '215', name: 'GABON'},
    {id: '216', name: 'GAMBIA'},
    {id: '139', name: 'GEORGIA'},
    {id: '217', name: 'GHANA'},
    {id: '174', name: 'GIBRALTAR'},
    {id: '318', name: 'GRANADA'},
    {id: '111', name: 'GRECIA'},
    {id: '371', name: 'GROENLANDIA'},
    {id: '383', name: 'GUADALUPE'},
    {id: '454', name: 'GUAM'},
    {id: '319', name: 'GUATEMALA'},
    {id: '394', name: 'GUAYANA FRANCESA'},
    {id: '170', name: 'GUERNESEY'},
    {id: '218', name: 'GUINEA'},
    {id: '220', name: 'GUINEA ECUATORIAL'},
    {id: '219', name: 'GUINEA-BISSAU'},
    {id: '346', name: 'GUYANA'},
    {id: '320', name: 'HAITI'},
    {id: '321', name: 'HONDURAS'},
    {id: '450', name: 'HONG KONG'},
    {id: '112', name: 'HUNGRIA'},
    {id: '410', name: 'INDIA'},
    {id: '411', name: 'INDONESIA'},
    {id: '413', name: 'IRAN'},
    {id: '412', name: 'IRAQ'},
    {id: '113', name: 'IRLANDA'},
    {id: '173', name: 'ISLA DE MAN'},
    {id: '521', name: 'ISLA NORFOLK'},
    {id: '114', name: 'ISLANDIA'},
    {id: '177', name: 'ISLAS ALAND'},
    {id: '380', name: 'ISLAS CAIMÁN'},
    {id: '513', name: 'ISLAS COOK'},
    {id: '175', name: 'ISLAS DEL CANAL'},
    {id: '172', name: 'ISLAS FEROE'},
    {id: '395', name: 'ISLAS MALVINAS'},
    {id: '448', name: 'ISLAS MARIANAS DEL NORTE'},
    {id: '440', name: 'ISLAS MARSHALL'},
    {id: '524', name: 'ISLAS PITCAIRN'},
    {id: '506', name: 'ISLAS SALOMON'},
    {id: '381', name: 'ISLAS TURCAS Y CAICOS'},
    {id: '393', name: 'ISLAS VIRGENES BRITANICAS'},
    {id: '382', name: 'ISLAS VÍRGENES DE LOS ESTADOS UNIDOS'},
    {id: '414', name: 'ISRAEL'},
    {id: '115', name: 'ITALIA'},
    {id: '322', name: 'JAMAICA'},
    {id: '415', name: 'JAPON'},
    {id: '176', name: 'JERSEY'},
    {id: '416', name: 'JORDANIA'},
    {id: '443', name: 'KAZAJSTAN'},
    {id: '221', name: 'KENIA'},
    {id: '444', name: 'KIRGUISTAN'},
    {id: '522', name: 'KIRIBATI'},
    {id: '418', name: 'KUWAIT'},
    {id: '419', name: 'LAOS'},
    {id: '222', name: 'LESOTHO'},
    {id: '136', name: 'LETONIA'},
    {id: '420', name: 'LIBANO'},
    {id: '223', name: 'LIBERIA'},
    {id: '224', name: 'LIBIA'},
    {id: '116', name: 'LIECHTENSTEIN'},
    {id: '142', name: 'LITUANIA'},
    {id: '117', name: 'LUXEMBURGO'},
    {id: '455', name: 'MACAO'},
    {id: '156', name: 'MACEDONIA '},
    {id: '225', name: 'MADAGASCAR'},
    {id: '421', name: 'MALASIA'},
    {id: '226', name: 'MALAWI'},
    {id: '422', name: 'MALDIVAS'},
    {id: '227', name: 'MALI'},
    {id: '118', name: 'MALTA'},
    {id: '228', name: 'MARRUECOS'},
    {id: '390', name: 'MARTINICA'},
    {id: '229', name: 'MAURICIO'},
    {id: '230', name: 'MAURITANIA'},
    {id: '262', name: 'MAYOTTE'},
    {id: '303', name: 'MEXICO'},
    {id: '511', name: 'MICRONESIA'},
    {id: '137', name: 'MOLDAVIA'},
    {id: '119', name: 'MONACO'},
    {id: '423', name: 'MONGOLIA'},
    {id: '158', name: 'MONTENEGRO'},
    {id: '387', name: 'MONTSERRAT'},
    {id: '231', name: 'MOZAMBIQUE'},
    {id: '405', name: 'MYANMAR'},
    {id: '232', name: 'NAMIBIA'},
    {id: '515', name: 'NAURU'},
    {id: '424', name: 'NEPAL'},
    {id: '323', name: 'NICARAGUA'},
    {id: '233', name: 'NIGER'},
    {id: '234', name: 'NIGERIA'},
    {id: '523', name: 'NIUE'},
    {id: '120', name: 'NORUEGA'},
    {id: '526', name: 'NUEVA CALEDONIA'},
    {id: '504', name: 'NUEVA ZELANDA'},
    {id: '425', name: 'OMAN'},
    {id: '396', name: 'OTROS PAISES  O TERRITORIOS DE AMERICA DEL NORTE'},
    {id: '399', name: 'OTROS PAISES O TERRITORIOS  DE SUDAMERICA'},
    {id: '299', name: 'OTROS PAISES O TERRITORIOS DE AFRICA'},
    {id: '499', name: 'OTROS PAISES O TERRITORIOS DE ASIA'},
    {id: '198', name: 'OTROS PAISES O TERRITORIOS DE LA UNION EUROPEA'},
    {id: '599', name: 'OTROS PAISES O TERRITORIOS DE OCEANIA'},
    {id: '398', name: 'OTROS PAISES O TERRITORIOS DEL CARIBE Y AMERICA CENTRAL'},
    {id: '199', name: 'OTROS PAISES O TERRITORIOS DEL RESTO DE EUROPA'},
    {id: '121', name: 'PAISES BAJOS'},
    {id: '426', name: 'PAKISTAN'},
    {id: '516', name: 'PALAOS'},
    {id: '449', name: 'PALESTINA'},
    {id: '324', name: 'PANAMA'},
    {id: '505', name: 'PAPUA NUEVA GUINEA'},
    {id: '347', name: 'PARAGUAY'},
    {id: '348', name: 'PERU'},
    {id: '520', name: 'POLINESIA FRANCESA'},
    {id: '122', name: 'POLONIA'},
    {id: '123', name: 'PORTUGAL'},
    {id: '391', name: 'PUERTO RICO'},
    {id: '427', name: 'QATAR'},
    {id: '125', name: 'REINO UNIDO'},
    {id: '250', name: 'REP.DEMOCRATICA DEL CONGO'},
    {id: '235', name: 'REPUBLICA CENTROAFRICANA'},
    {id: '143', name: 'REPUBLICA CHECA'},
    {id: '326', name: 'REPUBLICA DOMINICANA'},
    {id: '144', name: 'REPUBLICA ESLOVACA'},
    {id: '261', name: 'REUNION'},
    {id: '237', name: 'RUANDA'},
    {id: '128', name: 'RUMANIA'},
    {id: '154', name: 'RUSIA'},
    {id: '263', name: 'SAHARA OCCIDENTAL'},
    {id: '507', name: 'SAMOA'},
    {id: '528', name: 'SAMOA AMERICANA'},
    {id: '389', name: 'SAN BARTOLOME'},
    {id: '329', name: 'SAN CRISTOBAL Y NIEVES'},
    {id: '129', name: 'SAN MARINO'},
    {id: '385', name: 'SAN MARTIN (PARTE FRANCESA)'},
    {id: '370', name: 'SAN PEDRO Y MIQUELON '},
    {id: '325', name: 'SAN VICENTE Y LAS GRANADINAS'},
    {id: '260', name: 'SANTA HELENA'},
    {id: '328', name: 'SANTA LUCIA'},
    {id: '130', name: 'SANTA SEDE'},
    {id: '238', name: 'SANTO TOME Y PRINCIPE'},
    {id: '239', name: 'SENEGAL'},
    {id: '157', name: 'SERBIA'},
    {id: '240', name: 'SEYCHELLES'},
    {id: '241', name: 'SIERRA LEONA'},
    {id: '432', name: 'SINGAPUR'},
    {id: '433', name: 'SIRIA'},
    {id: '242', name: 'SOMALIA'},
    {id: '434', name: 'SRI LANKA'},
    {id: '236', name: 'SUDAFRICA'},
    {id: '243', name: 'SUDAN'},
    {id: '131', name: 'SUECIA'},
    {id: '132', name: 'SUIZA'},
    {id: '349', name: 'SURINAM'},
    {id: '171', name: 'SVALBARD Y JAN MAYEN'},
    {id: '244', name: 'SWAZILANDIA'},
    {id: '445', name: 'TADYIKISTAN'},
    {id: '435', name: 'TAILANDIA'},
    {id: '245', name: 'TANZANIA'},
    {id: '517', name: 'TIMOR ORIENTAL'},
    {id: '247', name: 'TOGO'},
    {id: '525', name: 'TOKELAU'},
    {id: '508', name: 'TONGA'},
    {id: '327', name: 'TRINIDAD Y TOBAGO'},

    {id: '248', name: 'TUNEZ'},
    {id: '446', name: 'TURKMENISTAN'},
    {id: '436', name: 'TURQUIA'},
    {id: '512', name: 'TUVALU'},
    {id: '135', name: 'UCRANIA'},
    {id: '249', name: 'UGANDA'},
    {id: '350', name: 'URUGUAY'},
    {id: '447', name: 'UZBEKISTAN'},
    {id: '509', name: 'VANUATU'},
    {id: '351', name: 'VENEZUELA'},
    {id: '437', name: 'VIETNAM'},
    {id: '527', name: 'WALLIS Y FORTUNA'},
    {id: '441', name: 'YEMEN'},
    {id: '251', name: 'ZAMBIA'},
    {id: '252', name: 'ZIMBABWE'}
];

export const eCONTRACT_CONCEPT = [{id: 'SALDO', name: 'Saldo de Precio'}, {id: 'PRESTAMO', name: 'Préstamo'}];

export const eMOTIVO_PRESTAMO = [
    {id: 'ADQUISICION', name: 'Adquisición Automotor'},
    {id: 'GARANTIA', name: 'Personal con garantía'}
];

export const eRESIDENCIA = [{id: 'Temporaria', name: 'Temporal'}, {id: 'Permanente', name: 'Permanente'}];

export const eGENERO = [{id: 'M', name: 'Masculino'}, {id: 'F', name: 'Femenino'}];

export const eYES_NO = [{id: true, name: 'Sí'}, {id: false, name: 'No'}];

export const eMAIL_SUBSCRIPTIONS = [
    {id: 'manual-evaluation-dealers', name: 'Evaluación Crediticia Manual Dealers'},
    {id: 'register-dealers', name: 'Registro Usuario Dealers'},
    {id: 'register-online-user', name: 'Registro Usuarios Online'},
    {id: 'contact-dealers', name: 'Contacto Dealers'},
    {id: 'support-dealers', name: 'Soporte Técnico Dealers'},
    {id: 'application', name: 'Aplicación'},
    {id: 'rto', name: 'Rent To Own'}
];

export const eACTIVITY = [
    {id: 'PRINCIPAL', name: 'Relación de Dependencia'},
    {id: 'PRINCIPAL1', name: 'Independiente'},
    {id: 'PRINCIPAL2', name: 'Jubilado'},
    {id: 'PRINCIPAL3', name: 'Estudiante'},
    {id: 'PRINCIPAL4', name: 'Empleo Informal'},
    {id: 'PRINCIPAL5', name: 'Ama de Casa'},
    {id: 'PRINCIPAL6', name: 'Sin Trabajo'}
];

export const eID_TYPE = [
    {id: 'DNI', name: 'DNI'},
    {id: 'LC', name: 'LC'},
    {id: 'LE', name: 'LE'},
    {id: 'PASAPORTE', name: 'Pasaporte'}
];

export const eCUIL_TYPE = [{id: 'CUIL', name: 'CUIL'}, {id: 'CUIT', name: 'CUIT'}, {id: 'CDI', name: 'CDI'}];

export const eMARITAL_STATUS = [
    {id: 'SOLTERO', name: 'Soltero'},
    {id: 'CASADO', name: 'Casado'},
    {id: 'DIVORCIADO', name: 'Divorciado'},
    {id: 'UNION_CONVENCIONAL', name: 'Unión Convencional'},
    {id: 'VIUDO', name: 'Viudo'}
];

export const eVEHICULO_TIPO = [
    {id: '001', name: 'Autos'},
    {id: '002', name: 'Camiones'},
    {id: '003', name: 'Utilitarios'},
    {id: '004', name: 'Minibus'},
    {id: '005', name: 'Autos Castigados'},
    {id: '006', name: 'Excluidos'},
    {id: '007', name: 'SUV'},
    {id: '008', name: 'BUS'}
];

export const eVEHICULO_USO_DECLARADO = [
    {id: '001', name: 'Particular'},
    {id: '002', name: 'Carga'},
    {id: '003', name: 'Transporte Taxi'},
    {id: '004', name: 'Transporte Remis'},
    {id: '005', name: 'Colectivo / Bus'}
];

export const eVEHICULO_USO_DECLARADO_APPLICATION = [
    {id: 'PARTICULAR', name: 'Particular'},
    {id: 'CARGA', name: 'Carga'},
    {id: 'TRANSPORTE_TAXI', name: 'Transporte Taxi'},
    {id: 'TRANSPORTE_REMIS', name: 'Transporte Remis'},
    {id: 'COLECTIVO_BUS', name: 'Colectivo / Bus'}
];

export const eID_TAXES_GANANCIAS = [
    {id: 'SUJETO_ALC_INS', name: 'Sujeto Alcanzado Inscripto'},
    {id: 'SUJETO_ALC_NO_INS', name: 'Sujeto Alcanzado No Inscripto'},
    {id: 'SUJETO_ALC_NO_INS_EXCEP', name: 'Sujeto Alcanzado Inscripto EXCEP. RG830'},
    {id: 'SUJETO_NO_ALC', name: 'Sujeto No Alcanzado'},
    {id: 'MONOTRIBUTO', name: 'Monotributo'},
    {id: 'EXENTO', name: 'Exento'}
];

export const eTIPO_EMPRESA = [
    {id: 'AGENCIA', name: 'AGENCIA'},
    {id: 'COMERCIALIZADORA', name: 'COMERCIALIZADORA'},
    {id: 'CONCESIONARIO', name: 'CONCESIONARIO'},
    {id: 'MULTIMARCA', name: 'MULTIMARCA'},
    {id: 'ARACAR', name: 'ARACAR'}
];

export const eID_TAXES_IVA = [
    {id: 'RESP_INS', name: 'Responsable Inscripto'},
    {id: 'NO_RESP', name: 'No Responsable'},
    {id: 'MONOTRIBUTO6', name: 'Monotributo 6 0/00'},
    {id: 'NO_CATEGORIZADO', name: 'No Categorizado'},
    {id: 'MONOTRIBUTO25', name: 'Monotributo 25 0/00'},
    {id: 'RESP_INS_EXCRG3337', name: 'Responsable Inscripto Exc. RG3337'},
    {id: 'EXENTO_IVA', name: 'Exento'},
    {id: 'CONSUMIDOR_FINAL', name: 'Consumidor Final'}
];

export const eID_TAXES_INGRESOSBRUTOS = [
    {id: 'RESP_INS_IIBB', name: 'Responsable Inscripto'},
    {id: 'NO_RESP_IIBB', name: 'No Responsable'},
    {id: 'RICICOBRA', name: 'R.I.C.I. COBRA 38/95'},
    {id: 'RINOCOBRA', name: 'R.I. NO COBRA 38/95'},
    {id: 'RICMCOBRA', name: 'R.I.C.M COBRA 38/95'},
    {id: 'CABAREGSIMP', name: 'CABA - REG. SIMPLIF.'},
    {id: 'CONSUMIDOR_FINAL_IIBB', name: 'Consumidor Final'},
    {id: 'EXENTO_IIBB', name: 'Exento'}
];

export const getEnumRow = (enumObj, value) => {
    return enumObj.filter(x => x.id === value)[0] || {id: '', name: '-'};
};

export default {
    ePROVINCIA
};
