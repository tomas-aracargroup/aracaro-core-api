import initialState from './initialState';
import * as types from '../actions/actionTypes';
import {combineReducers} from 'redux';
import {isEmpty} from 'lodash';

const authReducer = (state = initialState.app.auth, action = {}) => {
    switch (action.type) {
        case types.SET_CURRENT_USER:
            return {
                isAuthenticated: !isEmpty(action.user),
                user: action.user
            };
        default:
            return state;
    }
};

const modalReducer = (state = initialState.app.modal, action) => {
    const payload = action.payload;

    switch (action.type) {
        case types.SET_ACTIVE_MODAL:
            return payload;

        default:
            return state;
    }
};

const configReducer = (state = initialState.app.config, action) => {
    const payload = action.payload;

    switch (action.type) {
        case types.SET_CONFIG:
            return payload;

        default:
            return state;
    }
};

export default combineReducers({
    modal: modalReducer,
    auth: authReducer,
    config: configReducer
});
