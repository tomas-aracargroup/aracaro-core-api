import {combineReducers} from 'redux'
import app from './appReducer'
import grid from './gridReducer'
import entities from './entitiesReducer'

export default combineReducers({app, grid, entities});
