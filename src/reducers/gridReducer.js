import initialState from './initialState';
import * as types from '../actions/actionTypes';
import {combineReducers} from 'redux';
import lodash from 'lodash';

const toggleArrayItem = (array, item) => (array.includes(item) ? [...array].filter(x => x !== item) : [...array, item]);

const filterReducer = (state = initialState.grid.filters, action) => {
    const payload = action.payload;

    switch (action.type) {
        case types.UPDATE_FILTER:
            return Object.assign({}, state, {
                [payload.filterName]: Object.assign({}, state[payload.filterName] || {}, {
                    [payload.column]: payload.value
                })
            });
        case types.DELETE_FILTER: {
            return lodash.omit(state[payload.filterName], payload.column);
        }

        default:
            return state;
    }
};

const visibleColumnsReducer = (state = initialState.grid.visibleColumns, action) => {
    const payload = action.payload;

    switch (action.type) {
        case types.GRID_TOGGLE_VISIBLE_COLUMN:
            return Object.assign({}, state, {
                [payload.collectionName]: toggleArrayItem(state[payload.collectionName], payload.column)
            });

        default:
            return state;
    }
};

export default combineReducers({
    filters: filterReducer,
    visibleColumns: visibleColumnsReducer
});
