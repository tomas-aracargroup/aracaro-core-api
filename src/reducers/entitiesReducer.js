import initialState from './initialState';
import * as types from '../actions/actionTypes';

export default (state = initialState.entities, action) => {
    const payload = action.payload;

    switch (action.type) {
        case types.COLLECTION_FETCH_SUCCESS:
            return Object.assign({}, state, {
                [payload.collectionName]: {collection: payload.collection, count: payload.count}
            });

        case types.COLLECTION_UPDATE_SUCCESS:
            const collectionItems = state[payload.collectionName].collection;

            let collection;

            if (collectionItems) {
                collection = [
                    ...collectionItems.map(item => {
                        if (item._id === action.payload.document._id) {
                            return Object.assign({}, action.payload.document);
                        }
                        return item;
                    })
                ];
            }

            const count = state[payload.collectionName].count;

            return Object.assign({}, state, {[payload.collectionName]: {collection, count}});

        default:
            return state;
    }
};
