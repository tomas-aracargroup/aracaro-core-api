export default {
    app: {
        modal: {},
        auth: {},
        config: {tna: 0}
    },
    grid: {
        filters: {},
        visibleColumns: {
            leads: [
                'documento',
                'dictamen',
                'fechaDictamen',
                'canal',
                'nombre',
                'coefTasa',
                'apellido',
                'explicacion',
                'siScore',
                'scoVig',
                'user'
            ],
            sessions: ['referrer', 'product', 'startTime', 'userAgent', 'actionsCount', 'result'],
            channels: ['name', 'description', 'address', 'type', 'email', 'companyId', 'status', '_id'],
            users: ['user', 'lastname', 'firstname', 'status', 'email', 'channelId', 'role', '_id'],
            company: [
                'name',
                'code',
                'createdAt',
                'type',
                'baseRate',
                'variableOriginationFees',
                'fixedOriginationFees',
                'addressProvince',
                'addressCity',
                'applicationCount',
                'customerCount'
            ],
            applicationsDealers: [
                'documentNumber',
                'applicationStatus',
                'lastName',
                'firstName',
                'salesPerson',
                'status',
                'notesLastMessage'
            ],
            onlineApplications: [
                'documentNumber',
                'applicationStatus',
                'lastName',
                'firstName',
                'status',
                'notesLastMessage'
            ],
            onlineCustomers: [
                'createdAt',
                'authStrategyAvatarUrl',
                'authStrategy',
                'documentNumber',
                'lastName',
                'firstName',

                'bs_scoVig'
            ],
            usersDealers: [
                'lastName',
                'firstName',
                'email',
                'companyCode',
                'lastTimeActive',
                'applicationCount',
                'customerCount'
            ],
            settings: [
                'channel',
                'edit',
                'product',
                'fixedOriginationFees',
                'variableOriginationFees',
                'nominalAnualRate',
                'minTerm',
                'maxTerm'
            ],
            applications: [
                'applicationNumber',
                'productTerm',
                'requestedAmount',
                'applicationDate',
                'documentNumber',
                'beSmartResult',
                'applicationStatus',
                'lastName',
                'firstName',
                'salesPerson'
            ],
            customers: [
                'documentNumber',
                'createdAt',
                'firstName',
                'lastName',
                'userFullName',
                'companyCode',
                'maximumGrossLoanAmount',
                'nosisScore',
                'siisaScore'
            ],
            plans: [
                'firstName',
                'lastName',
                'email',
                'cellPhone',
                'vehicleModel',
                'vehicleInsuranceCompany',
                'vehicleInsuranceCoverageType',
                'productNetAmount',
                'productPrincipalAmount',
                'vehicleBrand',
                'vehicleYear',
                'planType',
                'planState',
                'vehicleValue',
                'paymentsMade',
                'paymentValue'
            ],
            rtos: ['documentNumber', 'firstName', 'lastName', 'status', 'notesLastMessage'],
            besmartlog: ['documento', 'nombre', 'apellido', 'canalOriginacion', 'producto', 'bancarized', 'createdAt']
        }
    },
    entities: {
        leads: {},
        channels: {},
        dealers: {},
        settings: {},
        sessions: {},
        company: {},
        onlineApplications: {},
        onlineCustomers: {},
        applicationsDealers: {},
        applications: {},
        customers: {},
        usersDealers: {},
        users: {},
        besmartlog: {},
        rtos: {},
        plans: {}
    },
    auth: {
        isAuthenticated: false,
        user: {}
    },
    config: {
        tna: 0
    }
};
