import React from 'react';
import StatusModal from './StatusModal';
import {put} from '../api/dealers';
import {eAPP_STATUS} from '../constants/enumerators';

export default props => (
    <StatusModal
        {...props}
        endPoint="application/change-status"
        collectionName="applications"
        statusKey="status"
        historyKey="statusHistory"
        options={eAPP_STATUS.map(x => x.id)}
        putFunction={put}
    />
);
