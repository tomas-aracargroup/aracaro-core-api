import React from 'react';
import {Input} from 'antd';

export default ({onChange, column, onFilterSubmit, filters}) => {
    return (
        <span>
            <Input.Search
                onChange={e => {
                    const value = {
                        type: 'string',
                        value: e.target.value,
                        operator: filters[column.name] ? filters[column.name].operator : 'begin'
                    };

                    onChange(column.name, value);
                }}
                value={filters[column.name] ? filters[column.name].value : ''}
                placeholder={'Buscar'}
                onPressEnter={onFilterSubmit}
                onSearch={x => onFilterSubmit()}
            />
            <div style={{width: 20}}>
                <select
                    onChange={e => {
                        onChange(column.name, {
                            type: 'string',
                            value: filters[column.name] ? filters[column.name].value : null,
                            operator: e.target.value
                        });

                        if (filters[column.name] === undefined) {
                            e.preventDefault();
                            return false;
                        } else {
                            setTimeout(onFilterSubmit, 200);
                        }
                    }}>
                    <option value="begin">Al inicio</option>
                    <option value="any">En cualquier parte</option>
                </select>
            </div>
        </span>
    );
};
