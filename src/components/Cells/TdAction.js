import React from 'react';
import {connect} from 'react-redux';
import {Col, Tooltip} from 'antd';
import * as appActions from '../../actions/appActions';
import {bindActionCreators} from 'redux';

let newVar = ({col, columns, item, actions}) => {
    const localStyle = col.resolveStyle ? col.resolveStyle(col, item) : 'link';

    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    const f = x => {
        x.preventDefault();
        x.stopPropagation();
        actions.setActiveModal({name: col.modalName, item: item});
    };

    return (
        <Tooltip placement="topLeft" title={item[col.name]}>
            <Col key={col.name} style={style} className="td">
                <a className={localStyle} onClick={f}>
                    {item[col.name] || '-'}
                </a>
            </Col>
        </Tooltip>
    );
};

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    null,
    mapDispatchToProps
)(newVar);
