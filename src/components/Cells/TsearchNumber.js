import React from 'react';
import {Input} from 'antd';

let onKeyPressHandler = e => {
    if (!/^[0-9]/.test(e.key)) {
        e.preventDefault();
        return false;
    }
};

export default ({onChange, column, onFilterSubmit, filters}) => {
    return (
        <span>
            <Input.Search
                onKeyPress={onKeyPressHandler}
                onChange={e => {
                    const value = {
                        type: 'number',
                        value: Number(e.target.value),
                        operator: filters[column.name] ? filters[column.name].operator : ''
                    };

                    onChange(column.name, value);
                }}
                value={filters[column.name] ? filters[column.name].value : ''}
                placeholder={'Buscar'}
                onPressEnter={onFilterSubmit}
                onSearch={x => onFilterSubmit()}
            />
            <div style={{width: 20}}>
                <select
                    onChange={e => {
                        onChange(column.name, {
                            type: 'number',
                            value: filters[column.name] ? filters[column.name].value : null,
                            operator: e.target.value
                        });

                        if (filters[column.name] === undefined) {
                            e.preventDefault();
                            return false;
                        } else {
                            setTimeout(onFilterSubmit, 200);
                        }
                    }}>
                    <option value="">Elegir</option>
                    <option value="$gt">Mayor</option>
                    <option value="$gte">Mayor Igual</option>
                    <option value="$lt">Menor</option>
                    <option value="$lte">Menor Igual</option>
                </select>
            </div>
        </span>
    );
};
