import React from 'react';
import {Col, Tooltip} from 'antd';
import moment from 'moment';

export default ({col, item}) => {
    return (
        <Tooltip
            placement="topLeft"
            title={item[col.name] ? moment(item[col.name]).format('YYYY/MM/DD - HH:mm:ss [hs]') : '-'}
            mouseEnterDelay={1}>
            <Col className="td">
                <span>{item[col.name] ? moment(item[col.name]).fromNow() : '-'}</span>
            </Col>
        </Tooltip>
    );
};
