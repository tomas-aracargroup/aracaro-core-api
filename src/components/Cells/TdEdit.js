import React from 'react';
import {connect} from 'react-redux';
import {Col, Icon} from 'antd';
import * as appActions from '../../actions/appActions';
import {bindActionCreators} from 'redux';

let newVar = ({col, columns, item, actions}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    const f = x => {
        x.preventDefault();
        x.stopPropagation();
        actions.setActiveModal({name: col.modalName, width: col.width, item: item});
    };

    return (
        <Col key={col.name} style={style} className="td">
            <a className="link" onClick={f}>
                <Icon type={'edit'} />
            </a>
        </Col>
    );
};

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    null,
    mapDispatchToProps
)(newVar);
