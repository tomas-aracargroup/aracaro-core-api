import React from 'react';
import {DatePicker} from 'antd';

export default ({onChange, column, onFilterSubmit, filters}) => {
    return (
        <DatePicker.RangePicker
            onChange={(date, dateString) => {
                onChange(column.name, {type: 'date', value: dateString});
                setTimeout(onFilterSubmit, 200);
            }}
            placeholder={['De:', 'a:']}
        />
    );
};
