import React from 'react';
import {connect} from 'react-redux';
import {Col} from 'antd';
import * as appActions from '../../actions/appActions';
import {bindActionCreators} from 'redux';
import moment from 'moment-timezone';
import 'moment/locale/es';

moment.locale('es');

let newVar = ({col, columns, item, actions}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    const f = x => {
        x.preventDefault();
        x.stopPropagation();
        actions.setActiveModal({name: col.modalName, item: item});
    };

    return (
        <Col key={col.name} style={style} className="td">
            <a className="link" onClick={f}>
                {item[col.name]
                    ? moment.tz(item[col.name], 'AMERICA/ARGENTINA/BUENOS_AIRES').format('DD-MM-YYYY HH:mm')
                    : '-'}
            </a>
        </Col>
    );
};

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    null,
    mapDispatchToProps
)(newVar);
