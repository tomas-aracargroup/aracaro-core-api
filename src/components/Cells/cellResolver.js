import Text from './Td';
import Number from './TdNumber';
import Money from './TdMoney';
import Checkbox from './TdCheckbox';
import Dictamen from './TdDictum';
import Edit from './TdEdit';
import Count from './TdCount';
import Link from './TdLink';
import Action from './TdAction';
import ActionDate from './TdActionDate';
import TdAvatar from './TdAvatar';
import Date from './TdDate';
import Enum from './TdEnum';
import Percentage from './TdPercentage';
import TimeSince from './TdTimeSince';
import Delete from './TdDelete';
import Device from './TdDevice';
import TdQuery from './TdQuery';

export default column => {
    switch (column.cell) {
        case 'number':
            return Number;

        case 'money':
            return Money;

        case 'timeSince':
            return TimeSince;

        case 'query':
            return TdQuery;

        case 'avatar':
            return TdAvatar;

        case 'percentage':
            return Percentage;

        case 'date':
            return Date;

        case 'count':
            return Count;

        case 'checkbox':
            return Checkbox;

        case 'enum':
            return Enum;

        case 'link':
            return Link;

        case 'dictamen':
            return Dictamen;

        case 'action':
            return Action;

        case 'actionDate':
            return ActionDate;

        case 'device':
            return Device;

        case 'delete':
            return Delete;

        case 'edit':
            return Edit;

        default:
            return Text;
    }
};
