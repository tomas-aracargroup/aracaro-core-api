import React from 'react';
import {Col} from 'antd';
import moment from 'moment';

export default ({col, columns, item}) => {
    return (
        <Col className="td"><span>{moment(item[col.name]).format('DD/MM/YY')}</span></Col>)
    ;
}





