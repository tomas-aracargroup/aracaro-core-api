import React from 'react';
import {Col, Icon} from 'antd';

export default ({col, columns, item}) => {
    const style = {width: `${Math.round(1/columns.length * 100, 2)}%`};

    return <Col key={col.name} style={style} className="td">{item[col.name] ? <Icon type='check' />:null}</Col>;
}