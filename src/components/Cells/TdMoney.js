import React from 'react';
import {Col} from 'antd';
import {formatMoney} from '../../helpers';

export default ({col, columns, item}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    return (
        <Col key={col.name} style={style} className="td number">
            <span>{formatMoney(item[col.name])}</span>
        </Col>
    );
};
