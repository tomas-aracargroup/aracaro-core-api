import React from 'react';
import {connect} from 'react-redux';
import {Col, Icon} from 'antd';
import * as appActions from '../../actions/appActions';
import {bindActionCreators} from 'redux';

let newVar = ({col, columns, item, actions, app}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    const f = x => {
        x.preventDefault();
        x.stopPropagation();
        actions.deleteDocument(col.collectionName, item._id);
    };

    return (
        <Col key={col.name} style={style} className="td">
            {app.auth.user.role === 'ADMIN' && (
                <a className="link" onClick={f}>
                    <Icon type={'delete'} style={{color: 'red'}} />
                </a>
            )}
        </Col>
    );
};

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    x => ({app: x.app}),
    mapDispatchToProps
)(newVar);
