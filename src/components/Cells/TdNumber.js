import React from 'react';
import {Col, Tooltip, Icon} from 'antd';
import {formatNumber} from '../../helpers/index';
import {Link} from 'react-router-dom';

export default ({col, columns, item}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};
    const localStyle = col.resolveStyle ? col.resolveStyle(col, item) : '';

    return (
        <Tooltip placement="topLeft" title={formatNumber(item[col.name])} mouseEnterDelay={1}>
            <Col key={col.name} style={style} className="td number">
                {col.link ? (
                    <Link className="card-link" to={`/${col.collectionName}/${item[col.keyName] || item._id}`}>
                        {col.icon ? (
                            <Icon type={col.icon} />
                        ) : item[col.name] !== '' && item[col.name] !== null ? (
                            <span>{formatNumber(item[col.name])}</span>
                        ) : (
                            '-'
                        )}
                    </Link>
                ) : (
                    <span className={localStyle}>{formatNumber(item[col.name])}</span>
                )}
            </Col>
        </Tooltip>
    );
};
