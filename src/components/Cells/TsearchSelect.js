import React from 'react';

export default ({onChange, column, onFilterSubmit, filters}) => {
    return (
        <select
            onChange={e => {
                onChange(column.name, {type: 'string', value: e.target.value});
                setTimeout(onFilterSubmit, 200);
            }}>
            <option value="">Elegir</option>
            {column.searchData.map(x => (
                <option key={x.id} value={x.id}>
                    {x.name}
                </option>
            ))}
        </select>
    );
};
