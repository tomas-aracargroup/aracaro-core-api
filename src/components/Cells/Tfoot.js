import React from 'react';
import {Col, Pagination} from 'antd';

export default ({count, skip, collection, page, onPageChange}) => {
    return (
        <Col className="tfoot">
            <Col className="row">
                <Col className="td">
                    <Pagination
                        total={count}
                        pageSize={20}
                        current={page}
                        onChange={onPageChange}
                        showTotal={total => `Total: ${total}`}
                    />
                </Col>
            </Col>
        </Col>
    );
};
