import React from 'react';
import {Col, Tooltip} from 'antd';

export default ({col, columns, item}) => {
    const color = col.resolveColor ? col.resolveColor(item, col.name) : 'initial';

    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`, color: color};

    return (
        <Tooltip placement="topLeft" title={item[col.name]} mouseEnterDelay={1}>
            <Col key={col.name} style={style} className="td">
                <span>
                    <i
                        style={{
                            width: '10px',
                            height: '10px',
                            borderRadius: '50%',
                            marginRight: '6px',
                            display: 'inline-block',
                            backgroundColor: color
                        }}
                    />{' '}
                    <span style={{display: 'inline'}}>{item[col.name] || '-'}</span>
                </span>
            </Col>
        </Tooltip>
    );
};
