import React from 'react';
import {Col, Icon, Tooltip} from 'antd';
import {Link} from 'react-router-dom';

export default ({col, columns, item}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    return (
        <Tooltip placement="topLeft" title={item[col.name]} mouseEnterDelay={1}>
            <Col key={col.name} style={style} className="td">
                <Link className="card-link" to={`/${col.collectionName}/${item[col.keyName] || item._id}`}>
                    {col.icon ? <Icon type={col.icon} /> : item[col.name] || '-'}
                </Link>
            </Col>
        </Tooltip>
    );
};
