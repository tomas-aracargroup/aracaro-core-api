import React from 'react';
import {Col, Tooltip} from 'antd';
import {Link} from 'react-router-dom';

export default ({col, item}) => {
    const fullName = item.firstName + ' ' + item.lastName;
    return (
        <Tooltip placement="topLeft" title={fullName} mouseEnterDelay={1}>
            <Col key={col.name} style={{...col.style}} className="td">
                <Link to={`/${col.collectionName}/${item[col.keyName] || item._id}`}>
                    {item[col.name] ? (
                        <img alt={fullName} style={{borderRadius: '50%', maxWidth: 50}} src={item[col.name]} />
                    ) : (
                        'Ver'
                    )}
                </Link>
            </Col>
        </Tooltip>
    );
};
