import React from 'react';
import {Col, Tooltip} from 'antd';

export default ({col, columns, item}) => {
    const width = `${Math.round((1 / columns.length) * 100, 2)}%`;
    return (
        <Tooltip placement="topLeft" title={item[col.name]} mouseEnterDelay={1}>
            <Col key={col.name} style={{...col.style, width: width}} className="td">
                <span>{item[col.name] || '-'}</span>
            </Col>
        </Tooltip>
    );
};
