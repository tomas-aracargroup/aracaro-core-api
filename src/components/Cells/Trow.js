import React from 'react';
import {Col} from 'antd';
import cellResolver from '../Cells/cellResolver';

export default ({item, columns, rowClassResolver}) => {
    const hiddenColumns = [];
    const cells = columns.filter(x => !hiddenColumns.includes(x.name)).map((col, y) => {
        const InputView = cellResolver(col);
        return <InputView col={col} key={col.name} columns={columns} item={item} />;
    });

    return <Col className={`row ${rowClassResolver && rowClassResolver(item)}`}>{cells}</Col>;
};
