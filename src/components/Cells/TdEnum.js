import React from 'react';
import {Col} from 'antd';

export default ({col, columns, item}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};
    let value = '';

    if (col.enum && item[col.name]) {
        const enumItem = col.enum.filter(x => x.id === item[col.name]);
        if (enumItem.length > 0) {
            value = enumItem[0].name;
        }
    }
    return (
        <Col key={col.name} style={style} className="td">
            <span>{value}</span>
        </Col>
    );
};
