import React from 'react';
import {Col} from 'antd';
import {formatPercentage} from '../../helpers';

export default ({col, columns, item}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    return (
        <Col key={col.name} style={style} className="td number">
            <span>{formatPercentage(item[col.name])}</span>
        </Col>
    );
};
