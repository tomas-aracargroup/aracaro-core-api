import React from 'react';
import {connect} from 'react-redux';
import {Col} from 'antd';
import * as appActions from '../../actions/appActions';
import {bindActionCreators} from 'redux';
import {withRouter} from 'react-router';

let newVar = ({col, columns, item, actions, history}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};

    const f = x => {
        x.preventDefault();
        x.stopPropagation();
        actions.updateFilter(col.filterName, col.filterColumn, col.filterGenerator(item));
        history.push({pathname: `/grids/${col.collectionName}`, state: {forceRefresh: true}});
    };

    return (
        <Col key={col.name} style={style} className="td">
            <a className="link" onClick={f}>
                {item[col.name] || '-'}
            </a>
        </Col>
    );
};

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    null,
    mapDispatchToProps
)(withRouter(newVar));
