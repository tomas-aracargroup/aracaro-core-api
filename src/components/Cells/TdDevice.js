import React from 'react';
import {Col} from 'antd';

const UAParser = require('ua-parser-js');

export default ({col, columns, item}) => {
    const style = {width: `${Math.round((1 / columns.length) * 100, 2)}%`};
    const parser = new UAParser(item[col.name]);

    let show;

    const device = parser.getDevice();
    const os = parser.getOS();

    if (device.vendor) {
        show = device.vendor + ' ' + device.model;
    } else if (os.name) {
        show = os.name + ' ' + os.version;
    } else {
        show = 'ROBOT';
    }

    return (
        <Col key={col.name} style={style} className="td">
            <div>{show}</div>
        </Col>
    );
};
