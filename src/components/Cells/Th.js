import React from 'react';
import {Col, Icon} from 'antd';

export default ({col, onSortChange, sortBy}) => {
    const by = sortBy[col.name];
    const caret = by && <Icon type={`caret-${by === 1 ? 'up' : 'down'}`} />;

    let s = '';
    if (col.cell === 'number' || col.cell === 'percentage' || col.cell === 'money') {
        s = 'number';
    }

    return (
        <Col className={`th ${s}`} onClick={() => onSortChange(col.name)}>
            {col.label || col.name} {caret}
        </Col>
    );
};
