import Tsearch from './Tsearch';
import TsearchNumber from './TsearchNumber';
import TsearchSelect from './TsearchSelect';
import TsearchDate from './TsearchDate';

export default column => {
    if (!column.searchable) return null;

    switch (column.searchType || column.cell) {
        case 'number':
        case 'money':
        case 'percentage':
        case 'count':
            return TsearchNumber;

        case 'timeSince':
        case 'date':
        case 'actionDate':
            return TsearchDate;

        case 'checkbox':
            return Tsearch;

        case 'select':
            return TsearchSelect;

        case 'query':
        case 'delete':
        case 'edit':
            return null;

        default:
            return Tsearch;
    }
};
