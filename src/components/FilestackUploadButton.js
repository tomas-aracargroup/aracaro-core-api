import React from 'react';
import {Upload, Button, Icon} from 'antd';
import ReactFilestack from 'filestack-react';
import config from '../constants/config';

class FilestackUploadButton extends React.Component {
    onFileUploaded = result => {
        const {onSuccess} = this.props;
        let filesArray = [];
        if (result.filesUploaded && result.filesUploaded.length > 0) {
            let fileObject;
            result.filesUploaded.forEach(f => {
                fileObject = {
                    filename: f.filename,
                    mimetype: f.mimetype,
                    uploadId: f.uploadId,
                    url: f.url
                };
                filesArray.push(fileObject);
            });
        }
        if (onSuccess) {
            onSuccess(filesArray);
        }
        return true;
    };

    onUploadFailed = result => {
        const {onError} = this.props;
        if (onError) {
            onError(result);
        }
    };

    render() {
        const {link, text} = this.props;
        const apikey = config.filestack.apikey;
        const options = config.filestack.options;
        let component;
        if (link) {
            component = (
                <ReactFilestack
                    apikey={apikey}
                    options={options}
                    onSuccess={this.onFileUploaded}
                    onError={this.onError}
                    render={({onPick}) => (
                        <Upload>
                            <a onClick={onPick}>{text}</a>
                        </Upload>
                    )}
                />
            );
        } else {
            component = (
                <ReactFilestack
                    apikey={apikey}
                    options={options}
                    onSuccess={this.onFileUploaded}
                    onError={this.onUploadFailed}
                    render={({onPick}) => (
                        <Upload>
                            <Button onClick={onPick}>
                                <Icon type="upload" /> {text}
                            </Button>
                        </Upload>
                    )}
                />
            );
        }
        return component;
    }
}

export default FilestackUploadButton;
