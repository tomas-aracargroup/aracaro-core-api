import React from 'react';
import * as appActions from '../actions/appActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import resolver from './modalContentResolver';

class PanelModal extends React.Component {
    handleOk = () => this.props.actions.setActiveModal({});

    render() {
        let {app} = this.props;
        if (!app.modal.name) return null;

        const ActiveView = resolver(app.modal);

        return <ActiveView handleOk={this.handleOk} entity={app.modal.item} />;
    }
}

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    null,
    mapDispatchToProps
)(PanelModal);
