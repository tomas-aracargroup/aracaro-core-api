import React, {Component} from 'react';
import MaskedInput from 'react-maskedinput';
import moment from 'moment';

class MaskedDateField extends Component {
    onBlur = e => {
        const {onBlur} = this.props;
        const momentDate = moment(e.target.value, 'DD/MM/YYYY');
        if (momentDate.isValid()) {
            onBlur(momentDate.format('YYYY-MM-DD'));
        } else {
            e.target.value = '';
            onBlur('');
        }
    };

    render() {
        const {value, className, placeholder} = this.props;
        const valor = moment(value, 'YYYY-MM-DD').format('DD-MM-YYYY');
        return (
            <MaskedInput
                mask={'11-11-1111'}
                value={valor}
                className={className}
                placeholder={placeholder}
                onBlur={e => this.onBlur(e)}
            />
        );
    }
}

export default MaskedDateField;
