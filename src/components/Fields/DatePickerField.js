import React from 'react';
import {startCase} from 'lodash';
import {Form, DatePicker} from 'antd';
import moment from 'moment';
import propTypes from 'prop-types';

const dateFormat = 'DD-MM-YYYY';

class DateField extends React.Component {
    state = {
        defaultValue: null,
        min: null,
        max: null
    };

    constructor(props) {
        super();

        const {min, max} = props;

        this.state = Object.assign(this.state, {
            min: min || new Date().setFullYear(1900),
            max: max || new Date().setFullYear(2100)
        });
    }

    disabledStartDate = date => {
        const minValue = this.state.min;
        const maxValue = this.state.max;

        if (!date) {
            return false;
        }
        return date.valueOf() > maxValue.valueOf() || date.valueOf() < minValue.valueOf();
    };

    render() {
        const {name, label, placeholder, required, entity, onChange, disabled, submitted, error} = this.props;
        let dateValue = moment(entity[name], 'YYYY-MM-DD');
        dateValue = dateValue.isValid() ? dateValue : null;
        return (
            <Form.Item
                label={label || startCase(name)}
                required={required}
                labelCol={{span: 8}}
                wrapperCol={{span: 16}}
                validateStatus={error && submitted ? 'error' : 'success'}
                help={submitted && error}>
                <DatePicker
                    disabled={disabled}
                    value={dateValue}
                    // format={dateFormat}
                    disabledDate={this.disabledStartDate}
                    placeholder={placeholder || label || startCase(name)}
                    onChange={(date, dateString) => onChange(name, dateString)}
                />
            </Form.Item>
        );
    }
}

export default DateField;

DateField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
