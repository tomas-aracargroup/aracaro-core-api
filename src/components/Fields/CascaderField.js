import React from 'react';
import {startCase} from 'lodash';
import {Form, Cascader} from 'antd';
import propTypes from 'prop-types';

const cascaderField = ({
    name,
    label,
    required,
    onChangeCascader,
    cascaderDefaultValue,
    options,
    placeholder,
    error,
    submitted
}) => (
    <Form.Item
        required={required}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}
        label={label || startCase(name)}
        labelCol={{span: 8}}
        wrapperCol={{span: 16}}>
        <Cascader
            placeholder={placeholder || label || startCase(name)}
            options={options}
            onChange={onChangeCascader}
            defaultValue={cascaderDefaultValue}
        />
    </Form.Item>
);

export default cascaderField;

cascaderField.prototype = {
    name: propTypes.string.isRequired,
    onChangeCascader: propTypes.func.isRequired,
    options: propTypes.array.isRequired
};
