import TextField from './TextField';
import TextAreaField from './TextAreaField';
import RadioField from './RadioField';
import CascaderField from './CascaderField';
import NumberField from './NumberField';
import MoneyField from './MoneyField';
import PercentageField from './PercentageField';
import DecimalField from './DecimalField';
import AsyncSelectField from './AsyncSelectField';
import DateField from './DateField';
import SelectField from './SelectField';
import CheckboxField from './CheckboxField';
import EmailField from './EmailField';
import RateField from './RateField';

export default column => {
    switch (column.type) {
        case 'number':
            return NumberField;
        case 'percentage':
            return PercentageField;
        case 'money':
        case 'currency':
            return MoneyField;

        case 'decimal':
            return DecimalField;

        case 'rate':
            return RateField;

        case 'date':
            return DateField;

        case 'select':
        case 'selectdb':
        case 'selectTags':
            return SelectField;

        case 'asyncSelect':
            return AsyncSelectField;

        case 'cascader':
            return CascaderField;

        case 'radio':
        case 'radioButton':
            return RadioField;

        case 'checkbox':
            return CheckboxField;

        case 'textArea':
            return TextAreaField;

        case 'password':
            return TextField;

        case 'email':
            return EmailField;

        default:
            return TextField;
    }
};
