import React from 'react';
import {startCase} from 'lodash';
import {Select, Form} from 'antd';
import {post} from '../../api/dealers';

class AsyncSelectField extends React.Component {
    state = {};

    constructor(props) {
        super();

        post(`queries/${props.endPoint}`, {
            project: {[props.optionLabel]: 1, [props.optionKey]: 1},
            sortBy: {[props.optionLabel]: 1}
        }).then(response => this.setState({options: response.collection}));
    }

    render() {
        const {name, label, onChange, optionKey, optionLabel, entity} = this.props;
        const {options} = this.state;

        return (
            <Form.Item label={label || startCase(name)} labelCol={{span: 8}} wrapperCol={{span: 16}}>
                <Select placeholder={'Company'} onChange={value => onChange(name, value)} value={entity[name]}>
                    {(options || []).map(option => (
                        <Select.Option key={option[optionKey]} value={option[optionKey]}>
                            {option[optionLabel]}
                        </Select.Option>
                    ))}
                </Select>
            </Form.Item>
        );
    }
}

export default AsyncSelectField;
