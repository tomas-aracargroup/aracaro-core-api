import React from 'react';
import {startCase} from 'lodash';
import {Select, Form} from 'antd';
import propTypes from 'prop-types';

const selectField = ({
    name,
    label,
    type,
    placeholder,
    required,
    entity,
    vertical,
    disabled,
    onChange,
    onBlur,
    options,
    searchable,
    submitted,
    error
}) => (
    <Form.Item
        label={label || startCase(name)}
        required={required}
        labelCol={vertical ? null : {span: 8}}
        wrapperCol={vertical ? null : {span: 16}}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}>
        <Select
            placeholder={placeholder || label || startCase(name)}
            onChange={value => onChange(name, value)}
            onBlur={value => (onBlur ? onBlur(name, value) : null)}
            value={entity[name]}
            disabled={disabled}
            showSearch={searchable}
            mode={type === 'selectTags' ? 'multiple' : 'select'}>
            {(options || []).map(option => (
                <Select.Option key={option.id} value={option.id}>
                    {option.name}
                </Select.Option>
            ))}
        </Select>
    </Form.Item>
);

export default selectField;

selectField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
