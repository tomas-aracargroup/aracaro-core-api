import React from 'react';
import {startCase} from 'lodash';
import {Form} from 'antd';
import propTypes from 'prop-types';
import MaskedInput from 'react-maskedinput';
import moment from 'moment';

require('moment/locale/es');
moment.locale('es'); //en caso que no lo setee la linea de arriba

class DateField extends React.Component {
    onBlur = e => {
        const {onChange, name} = this.props;
        if (e.target.value !== '' && moment(e.target.value, 'DD/MM/YYYY').isValid()) {
            onChange(name, moment(e.target.value, 'DD/MM/YYYY').format('YYYY-MM-DDT00:00:00-03:00'));
        } else {
            e.target.value = '';
            onChange(name, '');
        }
    };

    render() {
        const {name, label, placeholder, required, vertical, entity, mask, submitted, error, disabled} = this.props;

        return (
            <Form.Item
                label={label || startCase(name)}
                required={required}
                labelCol={vertical ? null : {span: 8}}
                wrapperCol={vertical ? null : {span: 16}}
                validateStatus={error && submitted ? 'error' : 'success'}
                help={submitted && error}>
                <MaskedInput
                    mask={mask || '11-11-1111'}
                    value={moment(entity[name], 'YYYY-MM-DD').format('DD-MM-YYYY')}
                    style={{width: '100%'}}
                    className={'ant-input'}
                    placeholder={placeholder || '20/10/1980'}
                    onBlur={e => this.onBlur(e)}
                    disabled={disabled}
                />
            </Form.Item>
        );
    }
}

DateField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};

export default DateField;
