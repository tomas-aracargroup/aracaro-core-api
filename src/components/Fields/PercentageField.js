import React from 'react';
import {startCase} from 'lodash';
import {InputNumber, Form} from 'antd';
import propTypes from 'prop-types';

const defaultItemLayout = {labelCol: {span: 8}, wrapperCol: {span: 16}};

const percentageField = ({
    name,
    label,
    placeholder,
    onChange,
    required,
    min,
    max,
    entity,
    disabled,
    itemLayout,
    submitted,
    error
}) => (
    <Form.Item
        label={label || startCase(name)}
        required={required}
        {...itemLayout || defaultItemLayout}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}>
        <InputNumber
            disabled={disabled}
            placeholder={placeholder || label || startCase(name)}
            min={min || 0}
            max={max || 100}
            formatter={value => `${value}%`}
            parser={value => value.replace('%', '')}
            value={entity[name]}
            // onChange={value => onChange(name, parseFloat(value && value.toString().replace(/[^0-9.,]/g, '')))}
            onChange={value => onChange(name, value)}
        />
    </Form.Item>
);

export default percentageField;

percentageField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
