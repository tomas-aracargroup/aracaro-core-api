import React, {Component} from 'react';
import ReactDOM from 'react-dom';

class MaskedInput extends Component {
  state = {
    valor: 0
  };

  handleChange = e => {
    this.setState({valor: e.target.value});
  };

  componentDidMount = () => {
    var $elem = ReactDOM.findDOMNode(this.refs.maskedInput);
    var reverse = {reverse: false};

    if (this.props.isReverse) {
      reverse = {reverse: true};
    }

    $elem.mask(this.props.mask, reverse);
  };

  render() {
    return (
      <div>
        <input type="text" onChange={this.handleChange} ref="maskedInput" />
        {this.state.valor}
      </div>
    );
  }
}

export default MaskedInput;
