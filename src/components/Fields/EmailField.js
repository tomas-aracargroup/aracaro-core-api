import React from 'react';
import {Input} from 'antd';
import {Form} from 'antd/lib/index';
import {startCase, isEmpty} from 'lodash';
import {validateEmail} from '../../helpers/validator';
import {post} from '../../api';

const FormItem = Form.Item;
class EmailField extends React.Component {
    state = {
        mailGunValidation: {}
    };

    onBlur = (name, value) => {
        this.setState({mailGunValidation: {did_you_mean: false, is_valid: true}});
        if (value.trim() !== '') {
            if (validateEmail(value)) {
                this.setState({submittingValidation: true});
                post(`mail/validate/`, {to: value})
                    .then(response =>
                        this.setState({
                            mailGunValidation: response,
                            pristine: true,
                            submittingValidation: false
                        })
                    )
                    .catch(() => this.setState({submittingValidation: false}));
            }
        }
        if (this.props.onBlur) {
            this.props.onBlur(name, value);
        }
    };

    render() {
        const {
            name,
            label,
            placeholder,
            required,
            entity,
            vertical,
            onChange,
            type,
            error,
            submitted,
            autofocus,
            value
        } = this.props;

        const {mailGunValidation, submittingValidation, pristine} = this.state;
        const {did_you_mean, is_valid} = mailGunValidation;
        const onDidYouMeanClick = e => {
            e.preventDefault();
            onChange(name, did_you_mean);
            this.onBlur(name, did_you_mean);
        };
        const didYouMean = did_you_mean && <a onClick={onDidYouMeanClick}>¿Quisiste decir {did_you_mean}?</a>;
        const invalidAddress = !is_valid && !isEmpty(mailGunValidation);

        const invalidMessage =
            (invalidAddress || !validateEmail(entity[name])) && pristine ? 'Revise la direccion' : '';

        return (
            <FormItem
                label={label || startCase(name)}
                required={required}
                labelCol={vertical ? null : {span: 8}}
                wrapperCol={vertical ? null : {span: 16}}
                validateStatus={
                    ((error && submitted) || invalidAddress) && !submittingValidation
                        ? did_you_mean
                            ? 'warning'
                            : 'error'
                        : ''
                }
                help={
                    submittingValidation ? 'Validando email...' : didYouMean || invalidMessage || (submitted && error)
                }>
                <Input
                    placeholder={placeholder || startCase(name)}
                    autoFocus={autofocus}
                    onBlur={e => this.onBlur(name, e.target.value)}
                    type={type}
                    style={{textTransform: 'lowercase'}}
                    onChange={x => {
                        this.setState({pristine: false});
                        onChange(name, x.target.value.toLowerCase());
                    }}
                    value={value || entity[name]}
                />
            </FormItem>
        );
    }
}

export default EmailField;
