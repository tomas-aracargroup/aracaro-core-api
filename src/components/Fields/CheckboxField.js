import React from 'react';
import {startCase} from 'lodash';
import {Checkbox, Form} from 'antd';
import propTypes from 'prop-types';

const checkbox = ({name, label, required, entity, disabled, onChange, options}) => (
    <Form.Item label={label || startCase(name)} required={required} labelCol={{span: 8}} wrapperCol={{span: 16}}>
        <Checkbox disabled={disabled} checked={entity[name]} onChange={x => onChange(name, x.target.checked)} />
    </Form.Item>
);

export default checkbox;

checkbox.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
