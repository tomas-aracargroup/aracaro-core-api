import React from 'react';
import {startCase} from 'lodash';
import {Input} from 'antd';
import {Form} from 'antd/lib/index';
import propTypes from 'prop-types';

const FormItem = Form.Item;
const {TextArea} = Input;

const textField = ({
    name,
    label,
    placeholder,
    required,
    vertical,
    entity,
    onChange,
    type,
    disabled,
    submitted,
    error,
    onBlur
}) => (
    <FormItem
        label={label || startCase(name)}
        required={required}
        labelCol={vertical ? null : {span: 8}}
        wrapperCol={vertical ? null : {span: 16}}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}>
        <TextArea
            placeholder={placeholder || label || startCase(name)}
            type={type}
            autosize={{minRows: 6, maxRows: 16}}
            disabled={disabled}
            onChange={e => onChange(name, e.target.value)}
            onBlur={e => (onBlur ? onBlur(name, e.target.value) : null)}
            value={entity[name]}
        />
    </FormItem>
);

export default textField;

textField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
