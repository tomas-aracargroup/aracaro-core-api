import React from 'react';
import {startCase} from 'lodash';
import {Form} from 'antd';
import propTypes from 'prop-types';

const selectField = ({name, label, required, vertical, entity, disabled, onChange, options, submitted, error}) => (
    <Form.Item
        label={label || startCase(name)}
        required={required}
        labelCol={vertical ? null : {span: 8}}
        wrapperCol={vertical ? null : {span: 16}}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}>
        <select onChange={value => onChange(name, value)} value={entity[name]} disabled={disabled}>
            {(options || [{id: 'loading', name: 'loading'}]).map(option => (
                <option key={option.id} value={option.id}>
                    {option.name}
                </option>
            ))}
        </select>
    </Form.Item>
);

export default selectField;

selectField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
