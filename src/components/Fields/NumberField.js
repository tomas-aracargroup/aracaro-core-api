import React from 'react';
import {startCase} from 'lodash';
import {InputNumber, Form} from 'antd';
import propTypes from 'prop-types';

const defaultItemLayout = {labelCol: {span: 8}, wrapperCol: {span: 16}};

const numberField = ({
    name,
    label,
    placeholder,
    onChange,
    required,
    min,
    max,
    entity,
    disabled,
    itemLayout,
    submitted,
    error
}) => (
    <Form.Item
        label={label || startCase(name)}
        required={required}
        {...itemLayout || defaultItemLayout}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}>
        <InputNumber
            disabled={disabled}
            placeholder={placeholder || label || startCase(name)}
            min={min}
            max={max}
            value={entity[name]}
            onChange={value => onChange(name, value)}
        />
    </Form.Item>
);

export default numberField;

numberField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
