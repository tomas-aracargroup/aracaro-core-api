import React from 'react';
import {startCase} from 'lodash';
import {InputNumber, Form} from 'antd';
import propTypes from 'prop-types';

const defaultItemLayout = {labelCol: {span: 8}, wrapperCol: {span: 16}};

const decimalField = ({name, label, inputType, placeholder, onChange, required, min, max, disabled, entity, itemLayout}) => (
    <Form.Item label={label || startCase(name)} required={required} {...itemLayout || defaultItemLayout}>
        <InputNumber
            disabled={disabled}
            placeholder={placeholder || label || startCase(name)}
            min={min}
            max={max}
            value={entity[name]}
            onChange={value => onChange(name, value.toString().replace(/[^0-9.,]/g, ''))}
        />
    </Form.Item>
);

export default decimalField;

decimalField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
