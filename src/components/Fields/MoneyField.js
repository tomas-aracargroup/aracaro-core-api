import React from 'react';
import {startCase} from 'lodash';
import {InputNumber, Form} from 'antd';
import propTypes from 'prop-types';

const defaultItemLayout = {labelCol: {span: 8}, wrapperCol: {span: 16}};

const moneyField = ({
    name,
    label,
    placeholder,
    onChange,
    required,
    min,
    max,
    entity,
    disabled,
    itemLayout,
    submitted,
    type,
    error
}) => (
    <Form.Item
        label={label || startCase(name)}
        required={required}
        {...itemLayout || defaultItemLayout}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}>
        <InputNumber
            disabled={disabled}
            placeholder={placeholder || label || startCase(name)}
            min={min}
            max={max}
            formatter={value => `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            parser={value => value.replace(/\$\s?|(,*)/g, '')}
            value={entity[name]}
            // onChange={value => onChange(name, parseFloat(value && value.toString().replace(/[^0-9.,]/g, '')))}
            onChange={value => onChange(name, value)}
        />
    </Form.Item>
);

export default moneyField;

moneyField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
