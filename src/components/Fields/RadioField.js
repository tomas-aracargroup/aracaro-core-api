import React from 'react';
import {Radio, Form} from 'antd';
import propTypes from 'prop-types';

const radioField = ({name, label, type, required, onChange, allLabel, options, disabled, error, submitted, entity}) => {
    return (
        <Form.Item
            label={label}
            required={required}
            labelCol={{span: 8}}
            wrapperCol={{span: 16}}
            validateStatus={error && submitted ? 'error' : 'success'}
            help={submitted && error}>
            {allLabel ? (
                <label>{options.filter(x => entity[name] === x.id)[0].name}</label>
            ) : (
                <Radio.Group
                    value={entity[name]}
                    disabled={disabled}
                    onChange={e => onChange(name, e.target.value, options.filter(x => e.target.value === x.id)[0])}>
                    {options.map(option => {
                        if (type === 'radio') {
                            return (
                                <Radio key={option.id} value={option.id}>
                                    {option.name}
                                </Radio>
                            );
                        } else {
                            return (
                                <Radio.Button key={option.id} value={option.id}>
                                    {option.name}
                                </Radio.Button>
                            );
                        }
                    })}
                </Radio.Group>
            )}
        </Form.Item>
    );
};

export default radioField;

radioField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
