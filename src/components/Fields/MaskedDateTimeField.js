import React, {Component} from 'react';
import MaskedInput from 'react-maskedinput';
import moment from 'moment-timezone';
import 'moment/locale/es';

moment.locale('es');

class MaskedDateField extends Component {
    onBlur = e => {
        const {onBlur} = this.props;
        const momentDate = moment(e.target.value, 'DD-MM-YYYY HH:mm');
        if (momentDate.isValid()) {
            onBlur(momentDate.tz('AMERICA/ARGENTINA/BUENOS_AIRES').format('YYYY-MM-DD HH:mm'));
        } else {
            e.target.value = '';
            onBlur('');
        }
    };

    render() {
        const {value, className, placeholder} = this.props;
        const valor = moment.tz(value, 'AMERICA/ARGENTINA/BUENOS_AIRES').format('DD-MM-YYYY HH:mm');
        return (
            <MaskedInput
                mask={'11-11-1111 11:11'}
                value={valor}
                className={className}
                placeholder={placeholder}
                onBlur={e => this.onBlur(e)}
            />
        );
    }
}

export default MaskedDateField;
