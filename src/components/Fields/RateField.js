import React from 'react';
import {startCase} from 'lodash';
import {Form, Rate} from 'antd';
import propTypes from 'prop-types';

const defaultItemLayout = {labelCol: {span: 8}, wrapperCol: {span: 16}};

const numberField = ({
    name,
    label,

    onChange,
    required,
    entity,
    itemLayout,
    submitted,
    error
}) => (
    <Form.Item
        label={label || startCase(name)}
        required={required}
        {...itemLayout || defaultItemLayout}
        validateStatus={error && submitted ? 'error' : 'success'}
        help={submitted && error}>
        <Rate count={10} value={entity[name]} onChange={value => onChange(name, value)} />
    </Form.Item>
);

export default numberField;

numberField.propTypes = {
    name: propTypes.string.isRequired,
    onChange: propTypes.func.isRequired
};
