import React from 'react';
import {Form, Input, Icon, Button} from 'antd';
import {post} from '../api';
import jwtDecode from 'jwt-decode';
import * as appActions from '../actions/appActions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {tokenName} from '../constants/config';
import img from '../assets/img/aracar-logo.svg';
import setAuthorizationToken from '../helpers/setAuthorizationToken';

class Login extends React.Component {
    state = {user: '', password: '', userError: '', passwordError: ''};

    onChange = (name, value) => {
        let state = this.state;
        state[name] = value;
        this.setState({state});
    };

    handleLogin = (user, password) => {
        post('auth/login', {filters: {$or: [{email: user}, {user: user}], password: password, status: true}})
            .then(token => {
                localStorage.setItem(tokenName, token);
                setAuthorizationToken(token);
                this.props.actions.setCurrentUser(jwtDecode(token));
            })
            .catch(() => this.setState({passwordError: 'API Error'}));
    };

    onClick = () => {
        const {user, password} = this.state;

        if (user === '') {
            this.setState({userError: 'Debe ingresar un Usuario.'});
        } else if (password === '') {
            this.setState({passwordError: 'Debe ingresar un password.'});
            this.setState({userError: ''});
        } else {
            this.setState({userError: ''});
            this.setState({passwordError: ''});
            this.handleLogin(user, password);
        }
    };

    render() {
        const {userError, passwordError} = this.state;

        return (
            <div className={'login-wrapper'}>
                <img className="logo" src={img} alt="" height="96" width="96" />
                <h2>Control Panel</h2>
                <Form className="login-form">
                    <Form.Item validateStatus={userError ? 'error' : ''} help={userError}>
                        <Input
                            size="large"
                            onChange={x => this.onChange('user', x.target.value)}
                            prefix={<Icon type="user" />}
                            placeholder="Usuario"
                        />
                    </Form.Item>
                    <Form.Item validateStatus={passwordError ? 'error' : ''} help={passwordError}>
                        <Input
                            size="large"
                            onChange={x => this.onChange('password', x.target.value)}
                            prefix={<Icon type="lock" />}
                            placeholder="Password"
                            type="password"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" onClick={this.onClick}>
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
                <p className={'mt-3'} style={{color: 'white', fontWeight: 'bold'}}>
                    ¿Algún problema?
                </p>
                <p style={{color: 'white'}}>
                    Por favor, escribinos a{' '}
                    <a style={{color: 'white', textDecoration: 'underline'}} href="mailto:soporte@aracargroup.com">
                        soporte@aracargroup.com
                    </a>.
                </p>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    null,
    mapDispatchToProps
)(Login);
