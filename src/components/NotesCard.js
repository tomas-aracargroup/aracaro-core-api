import React from 'react';
import {Card} from 'antd';
import NotesSection from './NotesSection';

class NotesCard extends React.Component {
    render() {
        const {title, notes, onAddNote, className, width} = this.props;

        const cardTitle = (
            <div style={{color: 'white'}} className="justify-flex-between">
                <div>{title}</div>
            </div>
        );

        return (
            <Card title={cardTitle} style={{width: width ? width : '500px'}} className={`editable-card ${className}`}>
                <NotesSection notes={notes} onAddNote={onAddNote} />
            </Card>
        );
    }
}

export default NotesCard;
