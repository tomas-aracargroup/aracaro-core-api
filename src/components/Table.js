import React from 'react'

let style = {border: '1px solid black', textAlign: 'center'};
export default ({columns, fila}) => (
    <table style={{width: '100%'}}>
        <thead>
        <tr>
            {columns.map(x => <th key={x} style={style}>{x}</th>)}
        </tr>
        </thead>
        <tbody>
        <tr>
            {fila.map(cell => <td key={cell} style={style}>{cell}</td>)}
        </tr>
        </tbody>
    </table>
);
