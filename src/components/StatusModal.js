import React from 'react';
import {connect} from 'react-redux';
import {Col, Card, Modal, Row, Select, notification} from 'antd';
import {sortByDateDesc} from '../helpers';
import moment from 'moment';
import {bindActionCreators} from 'redux';
import * as appActions from '../actions/appActions';

const Option = Select.Option;

class StatusModal extends React.Component {
    state = {
        selectedState: ''
    };

    handleCurrentStatusChange = newValue => {
        this.setState({selectedState: newValue});
    };

    handleOk = () => {
        const {app, entity, actions, collectionName, endPoint, putFunction} = this.props;
        const applicationId = entity._id;
        const user = app.auth.user.firstname + ' ' + app.auth.user.lastname;
        const status = this.state.selectedState;
        const previousStatus = entity.status;
        const payload = {status: status, user: user};

        if (status && status !== previousStatus) {
            actions.setActiveModal({});
            notification.success({message: 'Actualizando...', duration: 1.5});
            putFunction(endPoint, payload, applicationId)
                .then(result => {
                    actions.updateDocument(collectionName, result);

                    notification.success({
                        message: (
                            <span>
                                Status = <b>{status}</b>
                            </span>
                        ),
                        duration: 2.5
                    });
                })
                .catch(err => {
                    notification.error({message: 'Se ha producido un error.', duration: 1.5});
                });
        }
    };

    render() {
        const {entity, handleOk, options, statusKey, historyKey} = this.props;
        let statusList = [];

        const historyKey2 = historyKey || 'statusHistory';
        if (entity[historyKey2]) {
            statusList = [...entity[historyKey2]];
        }

        const title = (
            <div className="name">
                <label style={{fontWeight: 'bold', fontSize: '1.2rem'}}>{'Cambiar Status'}</label>
            </div>
        );

        const callbackfn = (x, key) => (
            <tr key={key}>
                <td>{x.status}</td>
                <td>{x.user}</td>
                <td>{moment(x.timeStamp).format('DD/MM/YYYY - h:mm:ss a')}</td>
            </tr>
        );

        return (
            <Modal iconType={'cross-circle'} visible onOk={this.handleOk} onCancel={handleOk} width={'720px'} closable>
                <Col>
                    <Card title={title}>
                        <Row
                            style={{
                                display: 'block',
                                width: '100%',
                                overflow: 'hidden',
                                clear: 'both',
                                marginLeft: '0px',
                                marginBottom: '20px'
                            }}>
                            <Col span={24}>
                                <Select
                                    defaultValue={entity[statusKey || 'status']}
                                    style={{}}
                                    size="large"
                                    onChange={this.handleCurrentStatusChange}>
                                    {options.map((option, key) => (
                                        <Option value={option} key={key}>
                                            {option}
                                        </Option>
                                    ))}
                                </Select>
                            </Col>
                        </Row>
                        {statusList.length > 0 && (
                            <div>
                                <Row style={{marginBottom: '20px', fontWeight: 'bold', fontSize: '1rem'}}>
                                    <Col>{'Status Anteriores:'}</Col>
                                </Row>
                                <div className="table-responsive">
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Usuario</th>
                                                <th>Fecha y Hora</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {statusList.sort(sortByDateDesc).map((status, key) => {
                                                return callbackfn(status, key);
                                            })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        )}
                    </Card>
                </Col>
            </Modal>
        );
    }
}

const mapStateToProps = state => ({app: state.app});
const mapDispatch = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    mapStateToProps,
    mapDispatch
)(StatusModal);
