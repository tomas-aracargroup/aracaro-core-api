import React from 'react';
import {Card, Col, Icon, Tooltip} from 'antd';
import {startCase} from 'lodash';
import fieldResolver from './Fields/fieldResolver';
import format from '../helpers/format';
import {isEmpty} from 'lodash';

class Editable extends React.Component {
    state = {editing: false, submittedLocal: false};

    toggleEditMode = () => {
        const {errors} = this.props;
        const editing = this.state.editing;
        const onExitEditMode = this.props.onExitEditMode;

        editing && isEmpty(errors) && onExitEditMode && onExitEditMode();
        if (!editing) {
            this.setState({editing: !editing});
        } else {
            this.setState({submittedLocal: true});
            if (isEmpty(errors) && editing) {
                this.setState({editing: !editing, submittedLocal: false});
            }
        }
    };

    render() {
        const {editing, submittedLocal} = this.state;
        const {fields, entity, onChange, onChangeCascader, errors, submmited, title, readOnly, className} = this.props;

        const errorsLocal = errors || {};

        if (!entity) {
            return null;
        }

        const cardTitle = (
            <div style={{color: 'white'}} className="justify-flex-between">
                <div>{title}</div>
                {!readOnly && (
                    <div
                        onClick={this.toggleEditMode}
                        style={{cursor: 'pointer', fontSize: '20px', lineHeight: 'normal'}}>
                        <Tooltip placement="topLeft" title={editing ? 'Modo Normal' : 'Modo Edición'}>
                            <Icon type={editing ? 'rollback' : 'form'} />
                        </Tooltip>
                    </div>
                )}
            </div>
        );

        const edit = (
            <div>
                {fields.map(x => {
                    const Input = fieldResolver(x);

                    return (
                        <Col key={x.name}>
                            <Input
                                required={x.required}
                                onChange={onChange}
                                onChangeCascader={onChangeCascader}
                                entity={entity}
                                min={x.min}
                                options={x.enum}
                                max={x.max}
                                mask={x.mask}
                                disabled={x.readOnly}
                                name={x.name}
                                endPoint={x.endPoint}
                                optionKey={x.optionKey}
                                optionLabel={x.optionLabel}
                                label={x.label}
                                placeholder={x.placeholder}
                                type={x.type}
                                error={errorsLocal[x.name]}
                                submitted={submmited || submittedLocal}
                            />
                        </Col>
                    );
                })}
            </div>
        );

        const view = (
            <table>
                <tbody>
                    {fields.map(field => {
                        return (
                            <tr key={field.name}>
                                <td
                                    style={{
                                        color: errorsLocal[field.name] !== undefined && submmited ? 'red' : null,
                                        textAlign: 'right',
                                        fontWeight: 'bold'
                                    }}>
                                    {field.label || startCase(field.name)}:
                                </td>
                                <td style={{paddingLeft: '5px'}}>
                                    {format(entity[field.name], field.type, field.enum) || '-'}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );

        return (
            <Card title={cardTitle} style={{width: 500}} className={`editable-card ${className ? className : ''}`}>
                {editing && !readOnly ? edit : view}
            </Card>
        );
    }
}

export default Editable;
