import React from 'react';
import {Button, Col, Form, Checkbox} from 'antd';
import onClickOutside from 'react-onclickoutside';
import * as appActions from '../actions/appActions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

class GridColumnPicker extends React.Component {
    state = {active: false};

    handleClickOutside = () => {
        this.setState({active: false});
    };

    onChange = name => {
        this.props.actions.updateVisibleColumn(this.props.collectionName, name);
    };

    render() {
        const {active} = this.state;
        const {columns, grid, collectionName} = this.props;
        const vc = grid.visibleColumns[collectionName];

        return (
            <Col className="dropdown mr-1">
                <Button
                    type={'primary'}
                    shape={'circle'}
                    icon={'table'}
                    onClick={() => this.setState({active: !this.state.active})}
                />
                <Form className={`dropinside ${active ? 'active' : ''}`}>
                    <fieldset>
                        {columns.map(col => {
                            if (!col.hideInColumnPicker) {
                                return (
                                    <Form.Item key={col.name}>
                                        <Checkbox
                                            checked={vc.includes(col.name)}
                                            onChange={() => this.onChange(col.name)}>
                                            {col.label || col.name}
                                        </Checkbox>
                                    </Form.Item>
                                );
                            } else {
                                return <Form.Item key={col.name} />;
                            }
                        })}
                    </fieldset>
                </Form>
            </Col>
        );
    }
}

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    x => ({grid: x.grid}),
    mapDispatchToProps
)(onClickOutside(GridColumnPicker));
