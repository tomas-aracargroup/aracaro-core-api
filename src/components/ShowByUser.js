import React, {Component} from 'react';
import {connect} from 'react-redux';

class ShowByUser extends Component {
    render() {
        const {app, object} = this.props;
        return app.auth.user.role === 'ADMIN' ? <span>{object}</span> : null;
    }
}

const mapStateToProps = state => ({app: state.app});
export default connect(mapStateToProps)(ShowByUser);
