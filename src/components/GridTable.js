import React from 'react';
import {Col, Row, Spin, Alert} from 'antd';
import TableRow from './Cells/Trow';
import Header from './Cells/Th';
import TableFooter from './Cells/Tfoot';
import cellSearchResolver from './Cells/cellSearchResolver';

class Table extends React.Component {
    render() {
        const {
            error,
            columns,
            page,
            sortBy,
            stripped,
            onPageChange,
            onSortChange,
            rowClassResolver,
            onChange,
            onFilterSubmit,
            collection,
            count,
            fetching,
            filters
        } = this.props;

        const rows = collection.map((item, y) => {
            return <TableRow rowClassResolver={rowClassResolver} item={item} key={y} columns={columns} />;
        });

        const columnSearch = (
            <Col className="row">
                {columns.map(col => {
                    const Search = cellSearchResolver(col);
                    return (
                        <Col className="tsearch" key={col.name}>
                            {Search ? (
                                <Search
                                    onFilterSubmit={onFilterSubmit}
                                    onChange={onChange}
                                    column={col}
                                    filters={filters || {}}
                                />
                            ) : null}
                        </Col>
                    );
                })}
            </Col>
        );

        const columnHeaders = (
            <Col className="row">
                {columns.map(x => <Header sortBy={sortBy} key={x.name} col={x} onSortChange={onSortChange} />)}
            </Col>
        );

        const errorMessage = <Alert message="Ups... Se ha producido un error" description={error} type="error" />;

        return (
            <Row className="ant-table-content">
                <Col className={`ant-table-body table ${stripped ? 'table-stripped' : ''}`}>
                    <Col className="thead">
                        {columnHeaders}
                        {columnSearch}
                    </Col>
                    <Col className={fetching || error ? 'tbody loading' : 'tbody'}>
                        {error ? <p>{errorMessage}</p> : fetching ? <Spin size="large" /> : rows}
                    </Col>
                    <TableFooter
                        count={count}
                        page={page}
                        columns={columns}
                        onPageChange={onPageChange}
                        collection={collection}
                    />
                </Col>
            </Row>
        );
    }
}

export default Table;
