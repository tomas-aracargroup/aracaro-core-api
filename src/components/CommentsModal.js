import React from 'react';
import {connect} from 'react-redux';
import {Button, Col, Card, Input, Modal, Row, notification} from 'antd';
import moment from 'moment';
import {bindActionCreators} from 'redux';
import * as appActions from '../actions/appActions';
import {sortByDateDesc} from '../helpers';

class CommentsModal extends React.Component {
    state = {
        currentNote: ''
    };

    handleCurrentNoteChange = e => {
        this.setState({currentNote: e.target.value});
    };

    addNote = () => {
        const {app, entity, actions, endPoint, collectionName, putFunction} = this.props;
        const text = this.state.currentNote;
        const payload = {text, user: app.auth.user.firstname + ' ' + app.auth.user.lastname};

        text &&
            putFunction(endPoint, payload, entity._id)
                .then(result => {
                    actions.updateDocument(collectionName, result);
                    actions.setActiveModal({});
                    notification.success({message: 'Status Actualizado.', duration: 1.5});
                })
                .catch(err => {
                    notification.error({message: 'Se ha producido un error.', duration: 1.5});
                });
    };

    render() {
        const {entity, handleOk} = this.props;
        let notes = [];
        if (entity.notes) {
            notes = [...entity.notes];
        }

        const title = (
            <div className="name">
                <label style={{fontWeight: 'bold', fontSize: '1.2rem'}}>{'Notas de la Aplicación'}</label>
            </div>
        );

        const callbackfn = (x, key) => (
            <tr key={key}>
                <td>
                    <p>
                        <b>{x.user}</b> {' (' + moment(x.timeStamp).fromNow() + ') '}
                    </p>
                    <p>{x.text}</p>
                </td>
            </tr>
        );

        return (
            <Modal iconType={'cross-circle'} visible onOk={this.addNote} onCancel={handleOk} width={'960px'} closable>
                <Col>
                    <Card title={title}>
                        <Row
                            style={{
                                display: 'block',
                                width: '100%',
                                overflow: 'hidden',
                                clear: 'both',
                                marginLeft: '4px',
                                marginBottom: '20px'
                            }}>
                            <Col span={20}>
                                <Input
                                    placeholder="Escribe un comentario"
                                    size="large"
                                    onChange={this.handleCurrentNoteChange}
                                    defaultValue={this.state.currentNote}
                                />
                            </Col>
                            <Col span={4}>
                                <Button type="primary" size="large" span={6} onClick={this.addNote}>
                                    {' '}
                                    Agregar Nota
                                </Button>
                            </Col>
                        </Row>
                        {notes.length > 0 && (
                            <div>
                                <Row style={{marginBottom: '20px', fontWeight: 'bold', fontSize: '1rem'}}>
                                    <Col>{'Listado de Notas:'}</Col>
                                </Row>
                                <div className="table-responsive">
                                    <table className="table table-striped">
                                        <tbody>{notes.sort(sortByDateDesc).map(callbackfn)}</tbody>
                                    </table>
                                </div>
                            </div>
                        )}
                    </Card>
                </Col>
            </Modal>
        );
    }
}

const mapState = state => ({app: state.app});
const mapDispatch = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    mapState,
    mapDispatch
)(CommentsModal);
