import React from 'react';
import {Card} from 'antd';

export default ({entity}) => (
    <Card className="editable-card m-3 p-1" style={{width: 500}} title={<div>Crudo</div>}>
        <table>
            {Object.keys(entity).map(
                k =>
                    !(typeof entity[k] === 'object') ? (
                        <tr key={k}>
                            <td>{k}</td>
                            <td>{entity[k]}</td>
                        </tr>
                    ) : null
            )}
        </table>
    </Card>
);
