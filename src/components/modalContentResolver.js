import Session from '../views/sessions/Session';
import ApplicationComments from '../views/application/ApplicationComments';
import ApplicationAssigneeHistory from '../views/application/ApplicationAssigneeHistory';
import ApplicationFollowUp from '../views/application/ApplicationFollowUp';
import ApplicationStatusHistory from '../views/application/ApplicationStatusHistory';
import newApplicationStatusHistory from '../components/ApplicationStatusHistoryModal';
import DealersApplicationComments from '../views/dealers/applications/DealersApplicationComments';
import RtoApplicationComments from '../views/rto/RtoApplicationComments';
import RtoApplicationStatusHistory from '../views/rto/RtoApplicationStatusHistory';
import SettingEdit from '../views/config/SettingEdit';
import BeSmartLogModal from '../views/besmart/BeSmartLogModal';

export default activeView => {
    switch (activeView.name) {
        case 'assigneeHistory':
            return ApplicationAssigneeHistory;

        case 'followUp':
            return ApplicationFollowUp;

        case 'dealersNotes':
            return DealersApplicationComments;

        case 'notes':
            return ApplicationComments;

        case 'rtoNotes':
            return RtoApplicationComments;

        case 'besmartlog':
            return BeSmartLogModal;

        case 'rtoStatusHistory':
            return RtoApplicationStatusHistory;

        case 'session':
            return Session;

        case 'setting':
            return SettingEdit;

        case 'statusHistory':
            return ApplicationStatusHistory;

        case 'newStatusHistory':
            return newApplicationStatusHistory;

        default:
            return x => null;
    }
};
