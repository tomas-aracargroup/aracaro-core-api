import React from 'react';
import {Button, Col, Row, Icon} from 'antd';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as appActions from '../actions/appActions';
import Table from './GridTable';
import ColumnPicker from './GridColumnPicker';
import {post} from '../api';
import {camelCase} from 'lodash';
import {Link} from 'react-router-dom';

const resolveSort = (sortBy, columnName) => {
    if (sortBy[columnName]) {
        if (sortBy[columnName] === 1) {
            return undefined;
        }
        return 1;
    }
    return -1;
};

const decorateFilter = (props, filterName) => {
    return props.grid.filters[filterName];
};

const resolveProject = columns => {
    const project = {};
    columns.forEach(x => {
        project[x.name] = true;
    });
    return project;
};

class Grid extends React.Component {
    constructor(props) {
        super();

        const post_ = props.post ? props.post : post;
        const collectionInCache = props.entities[props.collectionName].collection === undefined;

        this.state = {
            showFilter: true,
            filters: {},
            sortBy: {},
            collection: [],
            pageSize: 25,
            skip: 0,
            page: 1,
            count: 0,
            fetching: collectionInCache
        };
        this.filterName = camelCase(props.collectionName);

        const filters = decorateFilter(props, this.filterName);
        const payload = {
            filters: {...filters, ...props.filters},
            sortBy: props.sortBy,
            limit: this.state.pageSize,
            skip: this.state.skip,
            lean: props.lean,
            project: resolveProject(props.columns)
        };

        if (collectionInCache || props.forceRefresh) {
            post_(`queries/${props.endPoint}`, payload)
                .then(response => {
                    return this.updateAfterFetch(response);
                })
                .catch(error => {
                    this.setState({error: error.message});
                });
        }
    }

    updateAfterFetch = response => {
        const {actions, collectionName} = this.props;
        const {count, collection} = response;
        actions.fetchSuccess(collectionName, collection, count);
        this.setState({fetching: false});
    };

    onChange = (name, value) => {
        let val = value;
        if (typeof value === 'object') val = value.value;

        if (val !== '') {
            this.props.actions.updateFilter(this.filterName, name, value);
        } else {
            this.props.actions.deleteFilter(this.filterName, name);
        }
    };

    onDownload = () => {
        const {sortBy} = this.state;
        const props = this.props;
        const filters = decorateFilter(props, this.filterName);
        let newVar = {
            filters: {...filters, ...props.filters},
            sortBy
        };
        post(`download/`, newVar).then(response => {
            const url = window.URL.createObjectURL(new Blob([response]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'export.csv');
            document.body.appendChild(link);
            link.click();
        });
    };

    onDownloadDealers = () => {
        const {sortBy} = this.state;
        const props = this.props;
        const post_ = props.post ? props.post : post;

        const filters = decorateFilter(props, this.filterName);
        let newVar = {
            download: true,
            limit: 1000,
            lean: props.lean,
            filters: {...filters, ...props.filters},
            sortBy,
            project: resolveProject(props.columns)
        };
        post_(`queries/${this.props.endPoint}`, newVar).then(data => {
            const url = window.URL.createObjectURL(new Blob([data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'export.csv');
            document.body.appendChild(link);
            link.click();
        });
    };

    onFilterSubmit = () => {
        const {sortBy, pageSize, page} = this.state;
        const props = this.props;
        const post_ = props.post ? props.post : post;

        const skipAux = (page - 1) * pageSize;

        this.setState({fetching: true, skip: skipAux});
        const filters = decorateFilter(props, this.filterName);
        post_(`queries/${this.props.endPoint}`, {
            filters: {...filters, ...props.filters},
            project: resolveProject(props.columns),
            lean: props.lean,
            sortBy,
            limit: pageSize,
            skip: skipAux
        }).then(response => this.updateAfterFetch(response));
    };

    onSortChange = columnName => {
        let {sortBy, pageSize, page} = this.state;
        const props = this.props;
        const post_ = props.post ? props.post : post;
        const skipAux = (page - 1) * pageSize;

        sortBy[columnName] = resolveSort(sortBy, columnName);

        this.setState({fetching: true, sortBy, skip: skipAux});

        const filters = decorateFilter(props, this.filterName);

        post_(`queries/${this.props.endPoint}`, {
            filters: {...filters, ...props.filters},
            sortBy,
            lean: props.lean,
            project: resolveProject(props.columns),
            limit: pageSize,
            skip: skipAux
        }).then(response => this.updateAfterFetch(response));
    };

    onPageChange = page => {
        let {sortBy, pageSize} = this.state;
        const props = this.props;
        const post_ = props.post ? props.post : post;
        const skipAux = (page - 1) * pageSize;
        this.setState({fetching: true, page: page, skip: skipAux});

        const filters = decorateFilter(props, this.filterName);

        post_(`queries/${this.props.endPoint}`, {
            filters: filters,
            lean: props.lean,
            sortBy,
            project: resolveProject(props.columns),
            limit: pageSize,
            skip: skipAux
        }).then(response => this.updateAfterFetch(response));
    };

    render() {
        const {
            columns,
            grid,
            className,
            collectionName,
            columnPicker,
            stripped,
            rowClassResolver,
            createRoute,
            entities,
            download,
            downloadDealers,
            title
        } = this.props;
        const {fetching, sortBy, page, error} = this.state;
        const filters = grid.filters[this.filterName];
        const visibleColumns = grid.visibleColumns[collectionName];

        const entity = entities[collectionName];
        const collection = entity.collection || [];
        const count = entity.count || 0;

        if (Number(window.screen.width) < 800) {
            return <div>MOBILE VERSION NOT READY</div>;
        }

        let tableColumns = [];

        columns.forEach(col => {
            if (visibleColumns.includes(col.name)) {
                tableColumns.push(col);
            }
        });

        return (
            <Row className={className}>
                <Col className="grid-header" lg={24}>
                    <div>
                        <h2>{title}</h2>
                    </div>
                    <div className="justify-flex-start">
                        {createRoute ? (
                            <Link
                                className="ant-btn ant-btn-primary ant-btn-circle ant-btn-icon-only mr-2"
                                style={{marginRight: '.5rem'}}
                                to={createRoute}>
                                <Icon type={'plus-circle-o'} />
                            </Link>
                        ) : null}
                        {columnPicker ? <ColumnPicker columns={columns} collectionName={collectionName} /> : null}
                        {download ? (
                            <Button type={'primary'} shape={'circle'} onClick={this.onDownload} icon={'download'} />
                        ) : null}
                        {downloadDealers ? (
                            <Button
                                type={'primary'}
                                shape={'circle'}
                                onClick={this.onDownloadDealers}
                                icon={'download'}
                            />
                        ) : null}
                    </div>
                </Col>
                <Col className="ant-table ant-table-large ant-table-scroll-position-left" lg={24}>
                    <Table
                        columns={tableColumns}
                        page={page}
                        error={JSON.stringify(error)}
                        onPageChange={this.onPageChange}
                        onSortChange={this.onSortChange}
                        filters={filters}
                        stripped={stripped}
                        rowClassResolver={rowClassResolver}
                        collection={collection}
                        count={count}
                        fetching={fetching}
                        onChange={this.onChange}
                        sortBy={sortBy}
                        onFilterSubmit={this.onFilterSubmit}
                        className="table"
                    />
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    app: state.app,
    grid: state.grid,
    entities: state.entities
});

const mapDispatchToProps = dispatch => ({actions: bindActionCreators(appActions, dispatch)});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Grid);
