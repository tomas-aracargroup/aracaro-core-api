import React from 'react';
import {Button, Col, Dropdown, Layout, Menu} from 'antd';
import img from '../assets/img/aracar-logo.svg';
import {tokenName} from '../constants/config';
import {get} from '../api/dealers';

const {Header} = Layout;

class AppHeader extends React.Component {
    state = {};

    constructor() {
        super();
        get('uvas/current').then(data => this.setState({uvaValue: data.currentValue}));
    }

    render() {
        const {uvaValue} = this.state;
        const {user} = this.props;

        return (
            <Header className={'user-menu'} style={{position: 'fixed', zIndex: 3, width: '100%'}}>
                <Col xs={8}>
                    <img className="logo" src={img} alt="" height="48" width="48" />
                </Col>
                <Col xs={12} style={{display: 'flex', justifyContent: 'flex-end'}}>
                    <i className={'ml-1 mr-1'}>Cotización U.V.A: </i>${uvaValue}
                </Col>
                <Col className="user" xs={4}>
                    <Dropdown
                        overlay={
                            <Menu
                                onClick={() => {
                                    localStorage.clear(tokenName);
                                    window.location = '/';
                                }}>
                                <Menu.Item>{'Cerrar Sesión'}</Menu.Item>
                            </Menu>
                        }
                        trigger={['click']}>
                        <Button ghost={true} icon="down">
                            {user.firstname} {user.lastname}
                        </Button>
                    </Dropdown>
                </Col>
            </Header>
        );
    }
}

export default AppHeader;
