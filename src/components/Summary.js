import React, {Component} from 'react';
import {post} from '../api/dealers';

const resolveTitle = model => {
    switch (model) {
        case 'online-application':
            return 'Prenda Rápida / ';
        case 'rtos':
            return 'RTOS / ';
        case 'dealers-application':
            return 'Dealers / ';
        default:
            return '';
    }
};

class Summary extends Component {
    state = {
        sumary: [],
        model: {}
    };

    //when the component is pushed two times in a raw into history, only the first time
    //the constructor is fired
    constructor(props) {
        super();

        post(`statistic/statuses/${props.match.params.model}`).then(data => {
            this.setState({sumary: data});
        });
    }

    //this event only fires next time the component is pushed into history only if the previous
    //was the same component
    componentWillReceiveProps(nextProps) {
        post(`statistic/statuses/${nextProps.match.params.model}`).then(data => {
            this.setState({sumary: data});
        });
    }
    render() {
        const {sumary} = this.state;
        return (
            <div className="help">
                <div className="loan-header">
                    <div className="loan-header__client">
                        <h2 className="loan-header__title">
                            {resolveTitle(this.props.match.params.model)}Aplicaciones por Estado
                        </h2>
                    </div>
                </div>
                <div className="ml-3 pl-3">
                    <table style={{width: '50%'}} className="table table-striped">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th style={{textAlign: 'right'}}>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            {sumary.map(x => (
                                <tr key={x.status}>
                                    <td>{x.status}</td>
                                    <td style={{textAlign: 'right'}}>{x.cnt}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default Summary;
