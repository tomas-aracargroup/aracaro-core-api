import React from 'react';
import {Card, notification} from 'antd';
import {put, getByID} from '../api/dealers';
import RtoDocumentationUploadRow from '../views/rto/RtoDocumentationUploadRow';

class DocumentationCard extends React.Component {
    state = {
        files: {},
        fetching: true
    };

    componentDidMount() {
        getByID(this.props.modelName, this.props.entityId).then(data => {
            let files = {};

            this.props.filesList.forEach(file => {
                const key = file.key;

                if (data[key] && data[key].url) {
                    files[key] = data[key];
                }
            });

            this.setState({files, fetching: false});
        });
    }

    onFileUpload = (result, fileDocumentName) => {
        this.setState({[fileDocumentName]: result[0]});
        put(`byId/${this.props.modelName}`, {[fileDocumentName]: result[0]}, this.props.entityId);
    };

    onError = () => notification.error({message: 'Error al subir el archivo!', duration: 1.5});

    render() {
        const {title, filesList} = this.props;
        const {files, fetching} = this.state;

        return (
            <Card
                title={
                    <div className="justify-flex-between text-white">
                        <div>{title}</div>
                    </div>
                }
                style={{width: 500}}
                className={`editable-card`}>
                {fetching ? (
                    'Buscando archivos...'
                ) : (
                    <table>
                        <tbody>
                            {filesList.map(file => (
                                <tr key={file.key}>
                                    <td style={{textAlign: 'right', fontWeight: 'bold'}}>{file.label}:</td>
                                    <td className="pl-2">
                                        <RtoDocumentationUploadRow
                                            onSuccess={result => this.onFileUpload(result, file.key)}
                                            onError={this.onError}
                                            src={files[file.key]}
                                            alt={file.label}
                                        />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                )}
            </Card>
        );
    }
}

export default DocumentationCard;
