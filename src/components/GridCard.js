import React from 'react';
import {Card} from 'antd';
import format from '../helpers/format';

export default ({data, title, headers, className, onRowClick}) => {
    return (
        <Card title={title} style={{width: 500}} className={`editable-card ${className ? className : ''}`}>
            {data && data.length > 0 ? (
                <div className="card">
                    <div className="cotizacion-financiacion">
                        <table className="previous-quotes">
                            <tbody>
                                <tr>{headers.map(x => <td key={x.header}>{x.header}</td>)}</tr>

                                {data.map(app => (
                                    <tr key={app._id} style={{cursor: 'pointer'}} onClick={() => onRowClick(app)}>
                                        {headers.map(x => <td key={x.header}>{format(app[x.field], x.type)}</td>)}
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            ) : null}
        </Card>
    );
};
