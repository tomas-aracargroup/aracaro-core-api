import React, {Component} from 'react';
import moment from 'moment';

const sortByDateDesc = (x, y) => {
    const fechaX = moment(x.timeStamp).format('YYYYMMDDHHmmss');
    const fechaY = moment(y.timeStamp).format('YYYYMMDDHHmmss');
    return fechaX * 1 > fechaY * 1 ? -1 : fechaX * 1 < fechaY * 1 ? 1 : 0;
};

class ContactSection extends Component {
    state = {
        currentNote: ''
    };

    onChangeCurrentNote = e => {
        this.setState({currentNote: e.target.value});
    };

    addNote = () => {
        this.props.onAddNote(this.state.currentNote);
        this.setState({currentNote: ''});
    };

    render() {
        const {notes} = this.props;
        const {currentNote} = this.state;

        return (
            <div className="client-profile-contact-notas">
                <div>
                    <textarea
                        onChange={this.onChangeCurrentNote}
                        value={currentNote}
                        placeholder={'Dejar una nota...'}
                        style={{width: '100%', borderColor: '#ccc', padding: '.5rem'}}
                        rows="4"
                        className="form-control"
                    />
                </div>
                <div>
                    <button
                        className="save ant-btn ant-btn-secondary mt-2"
                        style={{opacity: currentNote ? 1 : 0, transition: 'opacity .3s ease-out'}}
                        disabled={!currentNote}
                        onClick={this.addNote}>
                        Guardar
                    </button>
                </div>
                {notes &&
                    notes.sort(sortByDateDesc).map(note => {
                        return (
                            <div className="notes-container row" key={note.timeStamp}>
                                <span className="date col-3">
                                    {moment(note.timeStamp).format('DD/MM/YYYY HH:mm') + ' hs'}
                                    {'     '}
                                    {note.user}
                                </span>
                                <p className="note col-9">{note.text}</p>
                            </div>
                        );
                    })}
            </div>
        );
    }
}

export default ContactSection;
