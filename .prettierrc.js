module.exports = {
    printWidth: 120,
    bracketSpacing: false,
    singleQuote: true,
    jsxBracketSameLine: true,
    tabWidth: 4
};
